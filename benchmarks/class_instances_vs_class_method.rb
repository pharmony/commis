# frozen_string_literal: true

#
# The aim of this benchmarck is to check if it is faster to use class methods or
# class instance methods in order to implement the app/interactors/sanity_checks
# files.
#

require 'benchmark'

EXECUTION_COUNT = 500_000

class WithInstanceMethod
  def initialize(hash)
    @hash = hash
  end

  def check!
    @hash[:error] = 'key is missing' unless @hash.key?(:test)

    return if @hash[:test] == 'OK'

    @hash[:error] = "Expected test to equal 'OK' but is #{@hash[:test].inspect}"
  end
end

class WithClassMethod
  def self.check!(hash)
    hash[:error] = 'key is missing' unless hash.key?(:test)

    return if hash[:test] == 'OK'

    hash[:error] = "Expected test to equal 'OK' but is #{hash[:test].inspect}"
  end
end

benchs = Benchmark.bm do |benchmark|
  benchmark.report 'Instance method' do
    EXECUTION_COUNT.times { WithInstanceMethod.new(test: 'NOK').check! }
  end

  benchmark.report 'Class methode' do
    EXECUTION_COUNT.times { WithClassMethod.check!(test: 'NOK') }
  end
end

puts ''
puts "The \"#{benchs.min_by(&:real).label}\" benchmark won !"

# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '>= 2.0.0', '< 3'

# rails-react-devise-bootstrap repository dependencies, please don't touch them.
gem 'bootsnap', '~> 1.4.4', require: false
gem 'devise', '~> 4.7.0'
gem 'jwt', '~> 2.2.1'
gem 'puma', '~> 4.0.1'
gem 'rails', '~> 6.0'
gem 'sass-rails', '~> 6.0'
gem 'uglifier', '>= 4.1.20'
gem 'webpacker', '~> 4.0.7'

# Add your dependencies after this lines.
# This will avoids conflicts when merging the upstream project.
gem 'aasm', '~> 5.0.5'
gem 'chef-dk', '~> 4.9.0'
gem 'devise-nobrainer', '~> 0.5.0'
gem 'faraday', '~> 1.0.0'
gem 'git', '~> 1.5.0'
gem 'interactor', '~> 3.1.1'
gem 'knife-zero', '~> 2.2.0'
gem 'nobrainer', '~> 0.34.0'
gem 'sidekiq', '~> 6.0.0'
gem 'sidekiq-cron', '~> 1.1.0'
gem 'sshkey', '~> 2.0.0'

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '~> 3.26.0'
  gem 'capybara-chromedriver-logger', '~> 0.2.1'
  gem 'capybara-screenshot', '~> 1.0.22'
  gem 'cucumber-rails', '~> 1.7.0', require: false
  # gem 'database_cleaner', '~> 1.7.0'
  gem 'email_spec', '~> 2.2.0'
  gem 'ffaker', '~> 2.11.0'
  gem 'rspec-expectations', '~> 3.8.4'
  gem 'selenium-webdriver', '~> 3.142.0'
end

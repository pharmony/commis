# Commis

![](screenshots/commis-dashboard.png)

## What is Commis?

Commis is a web application, orchestrating the [Chef](https://www.chef.sh), [Knife](https://docs.chef.io/knife.html) and [Knife-Zero](https://knife-zero.github.io) commands in order to bootstrap or converge your nodes without using a Chef Server.

A Chef Server can be quite expensive for small startups or mid companies, but it can also be a SPOF (Single Point Of Failure) as your nodes have to connect/authenticate to the Chef Server, and then get their cookbooks and everything from it.

Commis avoids those issues but also adds additionnal features:

 - **Node history** to see when, which action has been performed and if it succeed or failed
 - **Pull & Converge** to pull the chef repository before to converge the node.
 - **Bootstrap before hook** to execute a command before to install Chef on your node [Read more](https://gitlab.com/pharmony/commis/wikis/Bootstrap-before-hook).
 - **Customisable bootstrap steps** to run custom bootstrap/converge actions before to bootstrap a specific policy [Read more](https://gitlab.com/pharmony/commis/wikis/Customisable-bootstrap-steps)

## Requirements

 - Git
 - Docker
 - Docker Compose

## Usage

You can use Commis on your local machine, or you can deploy it on a server. 

Commis has been made with Docker, but of course you can use it as a traditionnal Rails application. This documentation covers only the Docker way.

This project use [Docker compose](https://docs.docker.com/compose/) in order to manage Commis dependencies (Databases and background tasks worker).

_Docker compose is not recommended for production usage (on a server) but is fine for a local usage. In the case you want to deploy Commis in a Kubernetes or Swarm cluster, you can find Commis' app Docker container in [the project's registry](https://gitlab.com/pharmony/commis/container_registry)._

In a terminal:

1. Clone this repository with `git clone https://gitlab.com/pharmony/commis.git`
2. Go to the project's folder with `cd commis`
3. To start the project:
    ```
    $ docker-compose --file production/docker-compose.yml up
    ```
4. Create the database indexes:
   ```
   $ docker-compose exec app bundle exec rake nobrainer:sync_indexes
   ```
5. Create the admin account (in another terminal):
    ```
    $ docker-compose --file production/docker-compose.yml exec app bundle exec rails nobrainer:seed
    ```
6. Access the app at http://localhost:3000/ and login with the email `admin@commis` and the password `commis`.

## What do you mean by orchestration?

All in all, Commis is actually doing the following when you request a node bootstrap or converge:

1. Runs a `chef update <policy path>`
2. Runs a `chef export <polocy path> <action base path>`
3. Copy your chef repository `data_bags/` folder to the `<action base path>`
4. Copy your chef repository `nodes/` folder to the `<action base path>`
5. Renames the `<action base path>/policy_groups/local.json` file to your policy group's name
6. Updates the `<action base path>/.chef/config.rb` file in order to change the `policy_group` with your policy group's name
7. Runs the `knife zero (bootstrap|converge) ...` command
8. Copy the `<action base path>/nodes` over your chef's repo `nodes/` folder
9. Add/commit the `nodes/` folder changes and push your chef repo

To summarise all those steps, Commis prepare a folder with all the required data in order to have the `knife zero (bootstrap|converge) ...` command working, and then _refresh_ your chef repo's nodes folder with the updated node files (new node file or updated node file).

The Git steps allows you to share the work with other and have a copy of the data.

## Roadmap

In the case you'd like to have a look at our roadmap, please have a look at [the repository's milestones](https://gitlab.com/pharmony/commis/-/milestones).

We are open for ideas and feedbacks, so please feel free to [open issues](https://gitlab.com/pharmony/commis/issues)!

## Upgrade local chef version

This project is heavily based on the chef-dk gem and hooks in many places in the gem in order fro commis to work.

The local version is the version used to run Commis, but is not necessarily the same version installed on your nodes.
To upgrade the local Chef version you have to:

1. Update the gem version selector from the `Gemfile`
2. Run `docker-compose exec app bundle update chef-dk`
3. Delete the `Setting` document in order to reload the local tools versions (TODO: Segregate settings in multiple documents composed of `key`/`value` and write a Rake task in order to clear only the version settings.)

Next time Commis bootstraps, it will fetch the Chef tools versions.
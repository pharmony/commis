#
# Builds a temporary image, with all the required dependencies used to compile
# the dependencies' dependencies.
# This image will be destroyed at the end of the build command.
#
FROM registry.gitlab.com/pharmony/commis:latest

ARG RAILS_ROOT=/application/
ARG BUILD_PACKAGES="build-base shared-mime-info"
ARG DEV_PACKAGES="nodejs npm"
ARG RUBY_PACKAGES="tzdata"

USER root

RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $BUILD_PACKAGES \
                                   $DEV_PACKAGES \
                                   $RUBY_PACKAGES

WORKDIR $RAILS_ROOT

RUN rm -f /usr/local/bundle/config \
    && bundle install --jobs $(nproc) \
    && rm -rf /usr/local/bundle/cache/*.gem \
    && yarn \
    && find /usr/local/bundle/gems/ -name "*.c" -delete \
    && find /usr/local/bundle/gems/ -name "*.o" -delete

USER pharmony

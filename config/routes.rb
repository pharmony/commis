# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  # ~~~~ Application API ~~~~
  #
  # Declare all your resources in the `:api` scope.
  # The API scope calls controllers executing requested actions from the UI.
  # (Create a post, delete a user, and so on ...).
  #
  # Frontend routes are defined in the JavaScript application's router.
  # @see get '*path' at the bottom of this file.
  #
  # `scope :api, module: :api ...` instead of `namespace :api, ...` in order to
  # keep named routes without the `_api_` part.
  # (`new_user_session_path` instead of `new_api_user_session_path`).
  scope :api, module: :api, constraints: { format: 'json' } do
    # We are defining the controllers paths avoding to search them from an `api`
    # folder.
    devise_for :users, controllers: {
      confirmations: 'devise/confirmations',
      passwords: 'devise/passwords',
      registrations: 'devise/registrations',
      sessions: 'sessions',
      unlocks: 'devise/unlocks'
    }

    # ~~~~ Application Resources ~~~~
    resource :bootstrap, only: %w[show]
    resources :clusters, only: %w[index create update destroy]
    resources :cookbooks, only: %w[index]
    resource :dashboard, only: %w[show]

    namespace :chef do
      resource :cache, only: %w[destroy]
      resources :policies, only: %w[index]
      resources :policy_groups, only: %w[index]
      resources :nodes, only: %w[index show update] do
        resources :actions,
                  only: %i[index show create],
                  controller: 'node_actions'
      end
      resources :repositories, only: %w[index show create destroy] do
        member do
          post :pull
        end
        resources :policies, only: %w[index] do
          resource :policy_action_stack, only: %w[show]
        end
      end
      resources :versions, only: %w[index]
    end

    resources :node_action_stacks, only: %i[index show create] do
      member do
        post :cancel
      end
      resources :node_actions, only: %i[index]
    end

    resources :node_actions, only: %i[index rerun] do
      member do
        post :rerun
      end
      resources :logs, only: %i[index]
    end

    resources :policies, only: %w[index] do
      resource :hooks, only: %w[show update]
    end

    resources :policy_action_stacks, only: %i[show update]

    resource :settings, only: %w[update]

    namespace :ssh do
      resource :key, only: %w[show regenerate] do
        collection do
          post :regenerate
        end
      end
    end
  end

  # ~~~~ Devise URL overrides ~~~~
  #
  # Override the Devise confirmation URL so that it doesn't include the `/api/`
  # part of the URL since the `devise_for` is included in the `:api` scope.
  # (See above)
  get '/users/:id/confirmation', to: 'home#index', as: :confirmation
  get '/users/:id/password/edit', to: 'home#index', as: :edit_password
  get '/users/:id/unlock', to: 'home#index', as: :unlock

  # Application root is required.
  root to: 'home#index'

  # Forward everything else to the JavaScript application router
  get '*path', to: 'home#index'
end

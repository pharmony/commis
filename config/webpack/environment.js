const { environment } = require('@rails/webpacker')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

/*
** Plugins
*/
environment.plugins.prepend('MomentLocalesPlugin', new MomentLocalesPlugin({
  localesToKeep: ['es-us']
}))

module.exports = environment

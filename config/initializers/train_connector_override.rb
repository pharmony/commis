# frozen_string_literal: true

require 'chef/knife/bootstrap/train_connector'
require 'commis/persist_and_broadcast_log'

class Chef
  class Knife
    class Bootstrap < Knife
      #
      # Overrides the TrainConnector.new so that a custom logger is passed to
      # the net-ssh gem, logging SSH commands, and the node_action is hold and
      # is used later
      #
      class TrainConnector
        include Commis::PersistAndBroadcastLog

        alias old_initialize initialize
        def initialize(host_url, default_protocol, opts)
          if Rails.env.development? && opts.key?(:node_action)
            node_action = opts.delete(:node_action)
            opts[:logger] = Logger.new(Commis::LogDevice.new(node_action))
            opts[:logger].level = Logger::DEBUG
          end

          old_initialize(host_url, default_protocol, opts)
        end
      end
    end
  end
end

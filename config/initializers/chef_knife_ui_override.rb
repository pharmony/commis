# frozen_string_literal: true

require 'chef/knife/core/ui'
require 'commis/persist_and_broadcast_log'

class Chef
  class Knife
    #
    # Overrides the Chef::Knife::UI class so that the knife logs can be
    # persisted and broadcasted to the UI.
    # See app/interactors/node_actions/initialisers/initialise_chef_knife_ui.rb
    #
    class UI
      include Commis::PersistAndBroadcastLog

      attr_accessor :node_action

      alias old_msg msg
      def msg(message)
        persist_and_broadcast(message, :msg)
        old_msg(message)
      end

      alias old_log log
      def log(message)
        persist_and_broadcast(message, :log)
        old_log(message)
      end

      alias old_debug debug
      def debug(message)
        persist_and_broadcast(message, :debug)
        old_debug(message)
      end

      alias old_warn warn
      def warn(message)
        persist_and_broadcast(message, :warn)
        old_warn(message)
      end

      alias old_error error
      def error(message)
        persist_and_broadcast(message, :error)
        old_error(message)
      end

      alias old_fatal fatal
      def fatal(message)
        persist_and_broadcast(message, :fatal)
        old_fatal(message)
      end
    end
  end
end

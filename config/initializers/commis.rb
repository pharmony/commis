require 'commis'

Commis.configure do |config|
  # Defines if the user registrations is open.
  #
  # When true the "Register" button, on the login page, will appear and anyone
  # accessing the app can create an account and login in the app.
  # Default: false
  # config.registration_opened = false
end

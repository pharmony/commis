# frozen_string_literal: true

require 'chef/knife/bootstrap'
require 'commis/persist_and_broadcast_log'

class Chef
  class Knife
    #
    # Overrides the Chef::Knife::Bootstrap class in order to update the
    # `do_connect` method so that it passes its `node_action` in the options
    # passed to the Chef::Knife::Bootstrap::TrainConnector class initialize.
    #
    class Bootstrap < Knife
      attr_accessor :node_action

      alias old_do_connect do_connect
      def do_connect(conn_options)
        old_do_connect(conn_options.merge(node_action: node_action))
      end
    end
  end
end

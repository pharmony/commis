# frozen_string_literal: true

Sidekiq.default_worker_options = {
  backtrace: true,
  # As of today, no Commis jobs should not be retried as they could mess a node
  # by retrying a converge again and again.
  retry: false
}

redis_url = ENV.fetch('REDIS_URL') do
  "redis://#{ENV.fetch('REDIS_HOST', 'localhost')}:" \
  "#{ENV.fetch('REDIS_PORT', '6379')}/#{ENV.fetch('REDIS_DB', '0')}"
end

puts "Connecting Sidekiq to Redis URL #{redis_url} ..."

Sidekiq.configure_server do |config|
  config.redis = { url: redis_url }
  config.average_scheduled_poll_interval = 5
end

Sidekiq.configure_client do |config|
  config.redis = { url: redis_url }
end

schedule_file = 'config/schedule.yml'
if File.exist?(schedule_file) && Sidekiq.server?
  yaml = YAML.load_file(schedule_file)

  Sidekiq::Cron::Job.load_from_hash(yaml) if yaml
end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Sidekiq overrides
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
require 'sidekiq/api'

module Sidekiq
  #
  # Sidekiq::JobSet override so that the UI is updated on job deletion
  #
  class JobSet < SortedSet
    alias old_delete_by_value delete_by_value
    def delete_by_value(name, value)
      parsed = JSON.parse(value)

      CommisChannel::BackgroundTasks.broadcast_delete(parsed['jid'])

      old_delete_by_value(name, value)
    end
  end
end

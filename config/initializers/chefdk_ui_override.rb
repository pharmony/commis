# frozen_string_literal: true

require 'chef-dk/ui'
require 'commis/persist_and_broadcast_log'

module ChefDK
  #
  # Overrides the ChefDk::UI class so that the chef logs can be persisted and
  # broadcasted to the UI.
  # See app/interactors/node_actions/initialisers/initialise_chef_dk_ui.rb
  #
  class UI
    include Commis::PersistAndBroadcastLog

    attr_accessor :node_action

    def initialize(node_action = nil)
      self.node_action = node_action
    end

    def err(message)
      persist_and_broadcast(message, :err)
    end

    def msg(message)
      persist_and_broadcast(message, :msg)
    end

    def print(message)
      persist_and_broadcast(message, :print)
    end
  end
end

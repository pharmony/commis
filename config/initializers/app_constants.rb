# frozen_string_literal: true

#
# Contants are used across back and front end
#
module AppConstants
  module Ssh
    AUTH_METHODS = %w[
      none
      password
      ssh_keys
    ].freeze
  end
end

# frozen_string_literal: true

require 'chef/knife'

class Chef
  #
  # Overrides the Chef::Knife.apply_computed_config method in order to preserve
  # the initialised Chef::Log with the Commis::LogDevice when Chef tries to
  # reconfigures its logger.
  #
  class Knife
    # Removes the cached WorkstationConfigLoader instance so that it reloads it
    # between each Sidekiq jobs
    def self.config_loader
      @config_loader = WorkstationConfigLoader.new(nil, Chef::Log)
    end

    def parse_options(args)
      super
    rescue OptionParser::InvalidOption => e
      raise # Re-raise the error so that Commis can catch it and show it.
    end

    private

    alias old_apply_computed_config apply_computed_config
    def apply_computed_config
      previous_logger = Chef::Log.logger
      old_apply_computed_config
      Chef::Log.logger = previous_logger
    end
  end
end

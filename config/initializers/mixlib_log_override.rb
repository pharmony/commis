# frozen_string_literal: true

require 'mixlib/log/logger'

module Mixlib
  module Log
    #
    # Adds a node_action attribute to the Mixlib::Log::Logger.
    # This allows the Commis::LogDevice to store an instance of a NodeAction,
    # that is then used by PersistAndBroadcastLog in order to persist and
    # broadcast the chef logs.
    # See lib/commis/log_device.rb
    # See lib/commis/persist_and_broadcast_log.rb
    #
    class Logger < ::Logger
      attr_accessor :node_action
    end
  end
end

# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (or created
# alongside the database with db:setup).

# The first account
admin_email = 'admin@commis'

unless User.where(email: admin_email).present?
  admin_password = 'commis'
  puts "Creating the #{admin_email} user account with password " \
       "#{admin_password.inspect} ..."
  admin_user = User.new(
    email: admin_email,
    password: admin_password,
    password_confirmation: admin_password
  )
  admin_user.skip_confirmation!
  admin_user.confirmation_sent_at = Time.now # Login doesn't work without it.
  admin_user.save!
  user_created = User.where(email: admin_email).present?
  puts "User #{admin_email} has#{' not' unless user_created} been created."
end

# Contributing to Commis

1. When opening an issue, please provide as much as information as you can and screenshots (Don't forget to remove sensible data if any).
2. When openin a Merge Request, please explain the change, with screenshots if needed for a better comprehension.
3. Sign your commits
4. Rebase with `master` in the interactive mode to shrink down your commit history. (https://thoughtbot.com/blog/git-interactive-rebase-squash-amend-rewriting-history)

## Booting the Docker app for dev

This project has 2 `Dockerfile` and 2 `docker-compose.yml` file:

`Dockerfile` and `docker-compose.yml` from the `production/` folder are used by the CI in order to build the image and running the tests so that if all went fine, the image is tagged as `latest` and people can get the updated version.

`Dockerfile`, `docker-compose*.yml` and `docker-sync.yml` files, at the root of the project, are used for the development.

We are working on Macs, so we need to use the awesome [docker-sync](http://docker-sync.io) in order to sync source files with the Docker containers, so the `docker-sync.yml`, `docker-compose-dev.yml` and `docker-compose.yml` files are meant to makes the stack working on the Mac.

To understand what does each files, please refer to the docker-sync documentation.

### First time

In order to not have to re-install all the gems when running the `bundle` command, we are using a data container (the `bundle` service from the `docker-compose.yml` file) but it will empty the `/usr/local/bundle` folder from the Docker image the first time, plus the `node_modules` folder isn't in the Git repo, so you have to install all the dependencies the first time.

_If you know a better way to solve this issue, please feel free to open a Merge Request :)_

So the first step is to start the stack in order to create all the containers:

```bash
$ docker-sync-stack start
```

In order to achive this, here is a long-copy-past-command for you:

```bash
$ docker-compose run --user root --rm -v commis-sync:/application:nocopy app chown -R pharmony:pharmony /usr/local/bundle && docker-compose run --rm -v commis-sync:/application:nocopy app bundle && docker-compose run --rm -v commis-sync:/application:nocopy app yarn
```

After this has finished, you just have to restart the stack again, and all should be fine.

Opening http://localhost:3000 should work.

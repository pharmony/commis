# frozen_string_literal: true

#
# Updates the markers of the given query with the right values.
#
class ConvergeQueryUpdator
  include Interactor

  def call
    sanity_checks!

    markers = context.query.scan(/#([\w-]+)#/).flatten

    context.final_query = context.query.dup

    markers.each do |marker|
      case marker
      when 'node-group'
        context.final_query.gsub!(/#node-group#/,
                                  context.policy_group.name)
      else
        Rails.logger.warn 'WARNING: Unknown Chef query marker ' \
                          "#{marker.inspect}."
      end
    end
  end

  private

  def sanity_checks!
    SanityChecks::ChefPolicyGroup.check!(context)

    return if context.query

    context.fail!(error: 'query is required')
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a policy_action_stack attribute,
  # and is an instance of PolicyActionStack.
  #
  class PolicyActionStack
    def self.check!(context)
      unless context.policy_action_stack
        context.fail!(
          error: "A policy_action_stack is required (From #{caller[0]})"
        )
      end

      return true if context.policy_action_stack.is_a?(::PolicyActionStack)

      context.fail!(
        error: 'Expected policy_action_stack to be an instance of ' \
               'PolicyActionStack but is an instance of ' \
               "#{context.policy_action_stack.class.name} (From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a repository attribute, and is an
  # instance of ChefRepository.
  #
  class ChefRepository
    def self.check!(context)
      unless context.repository
        context.fail!(error: "A repository is required (From #{caller[0]})")
      end

      return true if context.repository.is_a?(::ChefRepository)

      context.fail!(
        error: 'Expected repository to be an instance of ChefRepository but ' \
               "is an instance of #{context.repository.class.name} " \
               "(From #{caller[0]})"
      )
    end
  end
end

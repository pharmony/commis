# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a node_action attribute, and is an
  # instance of NodeAction.
  #
  class NodeAction
    def self.check!(context)
      unless context.node_action
        context.fail!(error: "A NodeAction is required (From #{caller[0]})")
      end

      return true if context.node_action.is_a?(::NodeAction)

      context.fail!(
        error: 'Expected node_action to be an instance of NodeAction but ' \
               "is an instance of #{context.node_action.class.name} " \
               "(From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a git_url attribute
  #
  class GitUrl
    def self.check!(context)
      return if context.git_url

      context.fail!(error: "The git_url is required (From #{caller[0]})")
    end
  end
end

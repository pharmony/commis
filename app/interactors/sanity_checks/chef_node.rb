# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a node attribute, and is an
  # instance of ChefNode.
  #
  class ChefNode
    def self.check!(context)
      unless context.node
        context.fail!(error: "A node is required (From #{caller[0]})")
      end

      return true if context.node.is_a?(::ChefNode)

      context.fail!(
        error: 'Expected node to be an instance of ChefNode but is ' \
               "an instance of #{context.node.class.name} " \
               "(From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a node_action_stack attribute, and
  # is an instance of NodeActionStack.
  #
  class NodeActionStack
    def self.check!(context)
      unless context.node_action_stack
        context.fail!(
          error: "A NodeActionStack is required (From #{caller[0]})"
        )
      end

      return true if context.node_action_stack.is_a?(::NodeActionStack)

      context.fail!(
        error: 'Expected node_action_stack to be an instance of ' \
               'NodeActionStack but is an instance of ' \
               "#{context.node_action_stack.class.name} (From #{caller[0]})"
      )
    end
  end
end

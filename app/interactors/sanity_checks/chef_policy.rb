# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a policy attribute, and is an
  # instance of ChefPolicy.
  #
  class ChefPolicy
    def self.check!(context)
      unless context.policy
        context.fail!(error: "A policy is required (From #{caller[0]})")
      end

      return true if context.policy.is_a?(::ChefPolicy)

      context.fail!(
        error: 'Expected policy to be an instance of ChefPolicy but is ' \
               "an instance of #{context.policy.class.name} " \
               "(From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a policy_group attribute, and is an
  # instance of ChefPolicyGroup.
  #
  class ChefPolicyGroup
    def self.check!(context)
      unless context.policy_group
        context.fail!(error: "A policy_group is required (From #{caller[0]})")
      end

      return true if context.policy_group.is_a?(::ChefPolicyGroup)

      context.fail!(
        error: 'Expected policy_group to be an instance of ChefPolicyGroup ' \
               "but is an instance of #{context.policy_group.class.name} " \
               "(From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a action_base_path attribute
  #
  class ActionBasePath
    def self.check!(context)
      return if context.action_base_path

      context.fail!(
        error: "The action_base_path is required (From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a cluster attribute, and is an
  # instance of Cluster.
  #
  class Cluster
    def self.check!(context)
      unless context.cluster
        context.fail!(error: "A cluster is required (From #{caller[0]})")
      end

      return true if context.cluster.is_a?(::Cluster)

      context.fail!(
        error: 'Expected cluster to be an instance of Cluster but is ' \
               "an instance of #{context.cluster.class.name} " \
               "(From #{caller[0]})"
      )
    end
  end
end

# frozen_string_literal: true

module SanityChecks
  #
  # Ensures the interactor's context has a git_local_path attribute
  #
  class GitLocalPath
    def self.check!(context)
      return if context.git_local_path

      context.fail!(error: "The git_local_path is required (From #{caller[0]})")
    end
  end
end

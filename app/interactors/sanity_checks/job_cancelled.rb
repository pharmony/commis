# frozen_string_literal: true

module SanityChecks
  #
  # Checks if the Sidekiq job has been cancelled and if it has been cancelled,
  # marks the interactor as failed.
  #
  class JobCancelled
    def self.check!(context)
      return unless context.provider_job_id

      job_cancelled = Sidekiq.redis do |redis|
        redis.exists("cancelled-#{context.provider_job_id}")
      end
      return unless job_cancelled

      context.fail!(error: 'Job cancelled')
    end
  end
end

# frozen_string_literal: true

module Git
  #
  # Sets the git user.name and user.email
  #
  class ConfigureUserNameAndEmail
    include Interactor

    def call
      sanity_checks!

      context.chef_knife_ui.msg('')
      context.chef_knife_ui.msg('=> Updating git config user ...')

      context.git.config 'user.name', git_username
      context.git.config 'user.email', git_email
    end

    private

    def sanity_checks!
      context.fail!(error: 'A git instance is required ') unless context.git

      return if context.chef_knife_ui

      context.fail!(error: 'An initialised UI object is required')
    end

    def git_email
      setting.git_email
    end

    def git_username
      setting.git_username
    end

    def setting
      Setting.first || Setting.new
    end
  end
end

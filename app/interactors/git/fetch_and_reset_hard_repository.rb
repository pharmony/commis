# frozen_string_literal: true

module Git
  #
  # Git fetch and reset hard the chef repository
  #
  class FetchAndResetHardRepository
    include Interactor

    def call
      sanity_checks!

      notify_ui if context.chef_knife_ui

      open_git_repository

      context.git.fetch
      context.git.reset_hard 'origin/master'
    end

    private

    def sanity_checks!
      SanityChecks::JobCancelled.check!(context)
      SanityChecks::ChefRepository.check!(context)
    end

    def notify_ui
      context.chef_knife_ui.msg('')
      context.chef_knife_ui.msg('=> Reseting Chef repository (Git fetch & ' \
                                'reset --hard) ...')
    end

    def open_git_repository
      context.repository_path = context.repository.local_path

      # Uses a wrapper bash script in order to set ssh flag to not check
      # host
      Git.configure do |config|
        config.git_ssh = File.join(Rails.root, 'bin', 'ssh_wrapper')
      end

      context.git = Git.open(context.repository_path)
    end
  end
end

# frozen_string_literal: true

module Git
  #
  # Add the files from the `nodes/` folder, commit and push
  #
  class AddCommitAndPushUpdatedNodes
    include Interactor

    def call
      sanity_checks!

      context.chef_knife_ui.msg('')
      context.chef_knife_ui.msg('=> Adding and committing node files ...')

      context.git.add 'nodes'
      context.git.commit build_commit_message
      context.git.push
    end

    private

    def sanity_checks!
      SanityChecks::JobCancelled.check!(context)

      context.fail!(error: 'A git instance is required ') unless context.git

      SanityChecks::ChefPolicy.check!(context)

      unless context.policy_group
        context.fail!(error: 'Expected policy.policy_group to be present')
      end

      case context.node_action.name
      when 'bootstrap'
        SanityChecks::ChefNode.check!(context)
      when 'converge'
        return if context.action_query

        context.fail!(error: 'An action_query is required')
      end

      return if context.chef_knife_ui

      context.fail!(error: 'An initialised UI object is required')
    end

    def build_commit_message
      msg = case context.node_action.name
            when 'bootstrap'
              "Updates node files after bootstrap\nSuccessfully bootstrapped " \
              "the node #{context.node_action.node_ip_address} with "
            when 'converge'
              "Updates node files after converge\n" \
              "Successfully converged query #{context.action_query} with "
            end

      msg + "policy name #{context.policy.name} and " \
            "group #{context.policy_group.name}"
    end
  end
end

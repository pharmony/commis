# frozen_string_literal: true

module NodeActions
  #
  # The NodeActions::OrganiseNodeBootstrapping organiser organise the bootstrap
  # of a new node (ChefNode) for a given policy (ChefPolicy) and policy group
  # (ChefPolicyGroup).
  #
  class OrganiseNodeBootstrapping
    include Interactor::Organizer

    organize Initialisers::InitialiseChefDkUi,
             Initialisers::InitialiseChefKnifeUi,
             Initialisers::InitialiseChefLog,
             Initialisers::InitialiseChefZeroLog,
             Initialisers::BuildNewNode,
             Initialisers::SummariseNodeAction,
             # From this point, before each interactors, the job will be checked
             # if it has been cancelled and therefore fails the interactor.
             Initialisers::EnsuresSshKeyReadableWhenUsingSshKeyAuth,
             Preparators::MarkNodeActionAsRunning,
             Preparators::PrepareActionBasePath,
             Preparators::RunChefUpdate,
             Preparators::RunChefExport,
             Preparators::UpdateChefConfigFile,
             Preparators::CopyAddtionnalData,
             Preparators::RenamePolicyGroup,
             Actions::Bootstrap,
             Git::FetchAndResetHardRepository,
             Git::ConfigureUserNameAndEmail,
             Finishers::CopyNodeFilesToTheChefRepository,
             Git::AddCommitAndPushUpdatedNodes,
             Finishers::PersistNewNode,
             Finishers::RefreshNodeFromJsonFile,
             Finishers::DeleteTheActionBasePath,
             Finishers::MarkNodeActionAsSuccess
  end
end

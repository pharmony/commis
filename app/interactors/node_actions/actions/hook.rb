# frozen_string_literal: true

module NodeActions
  module Actions
    #
    # Runs the NodeAction PolicyHook `run` method.
    #
    class Hook
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Executing hook ...')

        context.node_action.policy_hook.run(context.node_action)

        context.chef_knife_ui.msg('=> Node hook executed')
      rescue StandardError => error
        context.fail!(
          error: "Hook execution failed: #{error.message}\n" \
                 "#{error.backtrace.join("\n")}"
        )
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::NodeAction.check!(context)

        unless context.node_action.policy_hook
          context.fail!(error: 'node_action is misisng a policy_hook')
        end

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end
    end
  end
end

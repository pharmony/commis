# frozen_string_literal: true

require 'chef/application/knife'
require 'chef/knife/zero_bootstrap'

module NodeActions
  module Actions
    #
    # Runs the `knife zero bootstrap` equivalent API
    #
    class Bootstrap
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Starting to bootstrap node ...')

        run_knife_zero_bootstrap
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)

        SanityChecks::ActionBasePath.check!(context)
        SanityChecks::ChefNode.check!(context)
        SanityChecks::ChefPolicy.check!(context)
        SanityChecks::ChefPolicyGroup.check!(context)
        SanityChecks::NodeAction.check!(context)

        unless context.chef_knife_ui
          context.fail!(error: 'An initialised UI object is required')
        end

        SanityChecks::ActionBasePath.check!(context)
      end

      def appendix_config_flags
        client_rb_path = File.join(context.action_base_path, 'client.rb')

        return unless File.exist?(client_rb_path)

        [
          '--appendix-config',
          client_rb_path
        ]
      end

      def boostrap_klass
        klass = Chef::Knife::ZeroBootstrap
        klass.options = knife_app_options.merge!(klass.options)
        klass.load_deps
        klass
      end

      def command_options
        command_options = [context.node.ipaddress]
        command_options += ['--node-name', context.node.name]
        command_options += node_ssh_port_flag
        command_options += node_ssh_auth_flags
        command_options += node_policy_flags
        command_options += bootstrap_chef_version_flags
        command_options += node_before_bootstrap_flags
        command_options += appendix_config_flags
        command_options << node_sudo_flag
        command_options << '--no-color'
        command_options.compact
      end

      def initantiate_and_configure_bootstrap_command
        knife_klass = boostrap_klass
        if Rails.env.development?
          context.chef_knife_ui.msg 'knife zero bootstrap arguments: ' \
                                    "#{command_options.inspect}"
        end
        knife_instance = knife_klass.new(command_options)
        knife_instance.configure_chef
        knife_instance.ui = context.chef_knife_ui
        # node_action passed to Chef::Knife::Bootstrap, which then passes to the
        # Chef::Knife::Bootstrap::TrainConnector in order to log executed
        # commands on the server.
        # (See config/initializers/train_connector_override.rb)
        knife_instance.node_action = context.node_action
        knife_instance
      end

      def knife_app_options
        Chef::Application::Knife.options
      end

      def bootstrap_chef_version_flags
        return [] unless setting.bootstrap_chef_version

        [
          '--bootstrap-version',
          setting.bootstrap_chef_version.version
        ]
      end

      def node_before_bootstrap_flags
        return [] unless setting.node_action_before_bootstrap

        [
          '--bootstrap-preinstall-command',
          setting.node_action_before_bootstrap
        ]
      end

      def node_policy_flags
        [
          '--policy-name',
          context.policy.name,
          '--policy-group',
          context.policy_group.name
        ]
      end

      #
      # Build `knife zero bootstrap` flag list related to the SSH authentication
      #
      def node_ssh_auth_flags
        case context.node.ssh_auth_method
        when AppConstants::Ssh::AUTH_METHODS.index('none')
          []
        when AppConstants::Ssh::AUTH_METHODS.index('password')
          node_ssh_password_auth_flags
        when AppConstants::Ssh::AUTH_METHODS.index('ssh_keys')
          node_ssh_key_auth_flags
        end
      end

      def node_ssh_key_auth_flags
        [
          '--ssh-user', context.node.ssh_username,
          '--ssh-identity-file', context.node.ssh_key_path
        ]
      end

      def node_ssh_password_auth_flags
        [
          '--ssh-user', context.node.ssh_username,
          '--ssh-password', context.node.ssh_password
        ]
      end

      def node_ssh_port_flag
        [
          '--ssh-port', context.node.ssh_port.to_s
        ]
      end

      def node_sudo_flag
        '--sudo' if context.node.sudo
      end

      def run_knife_zero_bootstrap
        Dir.chdir(context.action_base_path) do
          knife_instance = initantiate_and_configure_bootstrap_command
          # Passing true to `run_with_pretty_exceptions` allows to get
          # exceptions to be raised, so using the Rails.env in order to get
          # exceptions raised while developing.
          knife_instance.run_with_pretty_exceptions(Rails.env.development?)
          # Closing the SSH connection
          #
          # Chef is actually never closing the SSH sessions which makes a second
          # node action, being executed on the same node, to fail when executing
          # the following command:
          # `(bash -c 'd=$(mktemp -d ${TMPDIR:-/tmp}/chef_XXXXXX); echo $d')`
          #
          # with the following error:
          # SSH command failed (remote forwarding request failed)
          #
          # Forcing to close the connection fixed the above issue.
          knife_instance.connection.connection.close
        end
      end

      def setting
        @setting ||= Setting.first || Setting.new
      end
    end
  end
end

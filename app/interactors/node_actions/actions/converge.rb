# frozen_string_literal: true

require 'chef/application/knife'
require 'chef/knife/zero_converge'

module NodeActions
  module Actions
    #
    # Runs the `knife zero converge` equivalent API
    #
    class Converge
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Starting to converge node ...')

        run_knife_zero_converge
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::ChefNode.check!(context)

        ssh_auth_method_none = AppConstants::Ssh::AUTH_METHODS.index('none')

        if context.node.ssh_auth_method == ssh_auth_method_none
          context.fail!(
            error: "The node #{context.node.name} with IP address " \
                   "#{context.node.ipaddress} doesn't have a selected " \
                   'authentification mode.'
          )
        end

        unless context.chef_knife_ui
          context.fail!(error: 'An initialised UI object is required')
        end

        unless context.action_query
          context.fail!(error: 'An action_query is required')
        end

        SanityChecks::ActionBasePath.check!(context)
      end

      def command_options
        command_options = [context.action_query]
        command_options += node_ssh_port_flag
        command_options += node_ssh_auth_flags
        command_options << node_sudo_flag
        command_options << '--no-color'
        command_options
      end

      def converge_klass
        klass = Chef::Knife::ZeroConverge
        klass.options = knife_app_options.merge!(klass.options)
        klass.load_deps
        klass
      end

      def initantiate_and_configure_converge_command
        knife_klass = converge_klass
        if Rails.env.development?
          context.chef_knife_ui.msg 'knife zero converge arguments: ' \
                                    "#{command_options.inspect}"
        end
        knife_instance = knife_klass.new(command_options)
        knife_instance.configure_chef
        knife_instance.ui = context.chef_knife_ui
        knife_instance
      end

      def knife_app_options
        Chef::Application::Knife.options
      end

      #
      # Build `knife zero converge` flag list related to the SSH authentication.
      #
      def node_ssh_auth_flags
        case context.node.ssh_auth_method
        when AppConstants::Ssh::AUTH_METHODS.index('none')
          []
        when AppConstants::Ssh::AUTH_METHODS.index('password')
          node_ssh_password_auth_flags
        when AppConstants::Ssh::AUTH_METHODS.index('ssh_keys')
          node_ssh_key_auth_flags
        end
      end

      def node_ssh_key_auth_flags
        [
          '--ssh-user', context.node.ssh_username,
          '--ssh-identity-file', context.node.ssh_key_path
        ]
      end

      def node_ssh_password_auth_flags
        [
          '--ssh-user', context.node.ssh_username,
          '--ssh-password', context.node.ssh_password
        ]
      end

      def node_ssh_port_flag
        [
          '--ssh-port', context.node.ssh_port.to_s
        ]
      end

      def node_sudo_flag
        "--#{'no-' if context.node.sudo}sudo"
      end

      def run_knife_zero_converge
        Dir.chdir(context.action_base_path) do
          knife_instance = initantiate_and_configure_converge_command
          # Passing true to `run_with_pretty_exceptions` allows to get
          # exceptions to be raised, so using the Rails.env in order to get
          # exceptions raised while developing.
          knife_instance.run_with_pretty_exceptions(Rails.env.development?)
        end
      rescue Exception => error
        context.fail!(
          error: "Action failed: #{error.message}\n\n" \
                 "#{error.backtrace.join("\n")}"
        )
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  #
  # The NodeActions::OrganiseNodeConvergence organiser organise the convergence
  # of a given node (ChefNode) for a given policy (ChefPolicy).
  #
  class OrganiseNodeConvergence
    include Interactor::Organizer

    organize Initialisers::InitialiseChefDkUi,
             Initialisers::InitialiseChefKnifeUi,
             Initialisers::InitialiseChefLog,
             Initialisers::InitialiseChefZeroLog,
             Initialisers::BuildActionQuery,
             Initialisers::SummariseNodeAction,
             # From this point, before each interactors, the job will be checked
             # if it has been cancelled and therefore fails the interactor.
             Initialisers::EnsuresSshKeyReadableWhenUsingSshKeyAuth,
             Preparators::MarkNodeActionAsRunning,
             Preparators::PrepareActionBasePath,
             Initialisers::FetchNodePolicyFromRepository,
             Preparators::RunChefUpdate,
             Preparators::RunChefExport,
             Preparators::UpdateChefConfigFile,
             Preparators::CopyAddtionnalData,
             Preparators::RenamePolicyGroup,
             Actions::Converge,
             Finishers::MarkNodeAsHealthy,
             Git::FetchAndResetHardRepository,
             Git::ConfigureUserNameAndEmail,
             Finishers::CopyNodeFilesToTheChefRepository,
             Git::AddCommitAndPushUpdatedNodes,
             Finishers::DeleteTheActionBasePath,
             Finishers::MarkNodeActionAsSuccess
  end
end

# frozen_string_literal: true

module NodeActions
  #
  # Organise the execution of the steps from a PolicyActionStack.
  #
  class OrganisePolicyActionStackStepsExecution
    include Interactor::Organizer

    organize Api::FetchPolicyActionStack,
             Initialisers::InitialiseChefKnifeUi,
             NodeActions::ExecutePolicyActionStackSteps
  end
end

# frozen_string_literal: true

module NodeActions
  #
  # Executes the given PolicyActionStack steps.
  #
  # When there's no custom PolicyActionStack, only the unsortable steps are
  # executed (as of now is bootstraping the selected policy), otherwise the
  # steps from the sortable steps list are executed in the order of the
  # `position` attribute, and then the unsortable steps are executed.
  # @see PolicyActionStack
  #
  class ExecutePolicyActionStackSteps
    include Interactor

    def call
      sanity_checks!

      return if context.node_action.unrunnable?

      step = fetch_policy_action_stack_step

      execute_step(step)
    end

    private

    def sanity_checks!
      SanityChecks::JobCancelled.check!(context)
      SanityChecks::NodeAction.check!(context)
      SanityChecks::PolicyActionStack.check!(context)

      return if context.chef_knife_ui

      context.fail!(error: 'chef_knife_ui is missing')
    end

    def execute_bootstrap_step(step)
      dup_context = context.dup
      dup_context.policy = step.chef_policy

      bootstrap = NodeActions::OrganiseNodeBootstrapping.call(dup_context)

      if bootstrap.success?
        # Copy the ChefNode instance so that it can be used later
        context.node = dup_context.node
        return
      end

      message = "Bootstrap action for policy #{step.chef_policy.name} has " \
                "failed: #{dup_context.error}"

      context.chef_knife_ui.msg("ERROR: #{message}")
      context.fail!(error: message)
    end

    def execute_converge_step(step)
      converge_query = context.node_action.query_override.presence || step.query

      dup_context = context.dup
      dup_context.action_query = update_query_if_needed(converge_query)
      dup_context.node ||= context.node_action.chef_node
      dup_context.policy_group ||= context.node_action.chef_policy_group

      converge = NodeActions::OrganiseNodeConvergence.call(dup_context)

      return if converge.success?

      message = "Converge action with query #{dup_context.action_query} has " \
                "failed: #{dup_context.error}"

      context.chef_knife_ui.msg("ERROR: #{message}")
      context.fail!(error: message)
    end

    def execute_step(step)
      if step.type == 'bootstrap'
        execute_bootstrap_step(step)
      else
        execute_converge_step(step)
      end
    end

    def fetch_policy_action_stack_step
      step_id = context.node_action.policy_action_stack_step_id

      step = context.node_action
                    .policy_action_stack
                    .policy_action_sortable_steps
                    .find?(step_id)

      return step if step

      context.node_action
             .policy_action_stack
             .policy_action_unsortable_steps
             .find?(step_id)
    end

    #
    # Updates the given query by replacing Commis' special markers like the
    # `#node-group#` which is replaced by
    # the `context.node_action.chef_policy_group.name`.
    #
    def update_query_if_needed(query)
      interactor = ConvergeQueryUpdator.call(
        policy_group: context.node_action.chef_policy_group,
        query: query
      )

      if interactor.success?
        interactor.final_query
      else
        context.fail!(error: "ConvergeQueryUpdator failed: #{interactor.error}")
      end
    end
  end
end

# frozen_string_literal: true

require 'chef-dk/command/export'

module NodeActions
  module Preparators
    #
    # Update the generated `.chef/config.rb` file from the
    # context.action_base_path path.
    #
    class UpdateChefConfigFile
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Updating the .chef/config.rb file ...')
        update_chef_config_file!

        context.chef_knife_ui.msg('=> Verfiying the .chef/config.rb file ...')
        return if chef_config_file_valid?

        context.fail!(error: 'Failed to update the .chef/config.rb file')
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::ActionBasePath.check!(context)
        SanityChecks::ChefPolicy.check!(context)
        SanityChecks::ChefPolicyGroup.check!(context)

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end

      def chef_config_file_path
        File.join(context.action_base_path, '.chef', 'config.rb')
      end

      def chef_config_file_valid?
        chef_config = read_chef_config_file

        return false unless chef_config =~ /#{wanted_policy_group}/

        true
      end

      def read_chef_config_file
        File.read(chef_config_file_path)
      end

      def update_chef_config_file!
        # Read the `.chef/config.rb` file
        chef_config = read_chef_config_file

        # Update its content
        chef_config = update_policy_group_and_add_local_mode!(chef_config)

        # Write the updated content to the file
        File.open(chef_config_file_path, 'w') { |file| file.puts chef_config }
      end

      def update_policy_group_and_add_local_mode!(chef_config)
        chef_config.gsub(
          /policy_group 'local'/,
          "#{wanted_policy_group}\n"
        )
      end

      def wanted_policy_group
        "policy_group '#{context.policy_group.name}'"
      end
    end
  end
end

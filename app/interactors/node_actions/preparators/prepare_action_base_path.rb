# frozen_string_literal: true

module NodeActions
  module Preparators
    class PrepareActionBasePath
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Preparing working directory ...')

        context.action_root_path = root_path
        context.action_base_path = build_action_base_path
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::NodeAction.check!(context)
        SanityChecks::ChefRepository.check!(context)

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end

      def root_path
        context.repository.local_path
      end

      def build_base_path
        File.join(root_path, 'actions', context.node_action.id)
      end

      def build_action_base_path
        base_path = build_base_path

        FileUtils.mkdir_p(base_path) unless context.prevent_mkdir

        base_path
      end
    end
  end
end

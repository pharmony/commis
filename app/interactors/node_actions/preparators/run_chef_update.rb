# frozen_string_literal: true

require 'chef-dk/command/update'

module NodeActions
  module Preparators
    #
    # The NodeActions::RunChefUpdate interactor runs the Chef update API for the
    # given ChefPolicy object.
    #
    class RunChefUpdate
      include Interactor

      def call
        sanity_checks!

        context.chefdk_ui.msg('')
        context.chefdk_ui.msg('=> Running chef update ...')

        output = run_chef_command

        return unless output == 1

        mark_node_action_as_failed

        raise NodeActions::Errors::ConvergeFailure, 'Chef updated failed'
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::NodeAction.check!(context)
        SanityChecks::ChefPolicy.check!(context)
      end

      def command_options
        command_options = [context.policy.policyfile_path]
        command_options << '--debug' if Rails.env.development?
        command_options
      end

      def mark_node_action_as_failed
        context.node_action.mark_as_failed
        context.node_action.save!
      end

      def run_chef_command
        chef_command = ChefDK::Command::Update.new
        # Reset the UI
        # See app/interactors/node_actions/initialize_chef_dk_ui.rb
        chef_command.ui = context.chefdk_ui
        Rails.logger.info '[NodeActions::RunChefUpdate] Updating Policyfile ' \
                          "from #{context.policy.policyfile_path} ..."
        chef_command.run(command_options)
      end
    end
  end
end

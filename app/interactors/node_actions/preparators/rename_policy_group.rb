# frozen_string_literal: true

module NodeActions
  module Preparators
    #
    # This interactor make possible to use policy groups even with knife zero by
    # renaming the generated policy group `policy_groups/local.json` to the
    # expected group name from the ChefNode.
    #
    class RenamePolicyGroup
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg '=> Renaming policy group to ' \
                              "#{chef_policy_group_name} ..."

        File.rename(local_policy_group_path, policy_chef_policy_group_path)

        context.chef_knife_ui.msg 'Checking policy group ' \
                              "#{chef_policy_group_name} ..."

        return if File.exist?(policy_chef_policy_group_path)

        message = 'Policy group renaming failed: Expected ' \
                  "#{local_policy_group_path.inspect} to be renamed as " \
                  "#{policy_chef_policy_group_path.inspect}, but the latter " \
                  "doesn't exists"
        context.chef_knife_ui.err(message)
        context.fail!(error: message)
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::ChefPolicy.check!(context)

        unless context.chef_knife_ui
          context.fail!(error: 'An initialised UI object is required')
        end

        unless context.policy_group
          context.fail!(error: 'Expected policy.policy_group to be present')
        end

        SanityChecks::ActionBasePath.check!(context)
      end

      def chef_policy_group_name
        context.policy_group.name
      end

      def local_policy_group_path
        File.join(policy_groups_path, 'local.json')
      end

      def policy_chef_policy_group_path
        File.join(policy_groups_path, "#{chef_policy_group_name}.json")
      end

      def policy_groups_path
        File.join(context.action_base_path, 'policy_groups')
      end
    end
  end
end

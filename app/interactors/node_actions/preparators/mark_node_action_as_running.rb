# frozen_string_literal: true

module NodeActions
  module Preparators
    class MarkNodeActionAsRunning
      include Interactor

      def call
        sanity_checks!

        context.node_action.mark_as_running
        context.node_action.save!
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::NodeAction.check!(context)
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  module Preparators
    class MarkNodeActionStackAsRunning
      include Interactor

      def call
        sanity_checks!

        context.node_action_stack.mark_as_running
        context.node_action_stack.save!
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)
        SanityChecks::NodeActionStack.check!(context)
      end
    end
  end
end

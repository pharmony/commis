# frozen_string_literal: true

module NodeActions
  module Preparators
    #
    # Copy some additionnal data to the context.action_base_path like:
    #
    #  - Data bags
    #  - Nodes
    #  - Policy group (rename generated one)
    #
    class CopyAddtionnalData
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Importing additionnal data ...')

        copy_data_bags_if_any
        copy_nodes_if_any
        copy_client_rb_file_if_any
        copy_knife_rb_file_if_any
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)

        unless context.action_root_path
          context.fail!(error: 'The action_root_path is required')
        end

        SanityChecks::ActionBasePath.check!(context)
      end

      def copy_folder_to_the_action_base(from)
        context.chef_knife_ui.msg(
          "Copying #{from} to #{context.action_base_path} ..."
        )

        FileUtils.cp_r(from, context.action_base_path)
      end

      def check_copied_file!(from)
        base_file_path = destination_path_from(from)

        context.chef_knife_ui.msg("Checking #{base_file_path} ...")

        return if File.exist?(base_file_path)

        message = "file copy failed: Expected #{base_file_path} to exist " \
                  'but the file is missing.'
        context.chef_knife_ui.err(message)
        context.fail!(error: message)
      end

      def check_copied_folder!(from)
        base_folder_path = destination_path_from(from)

        context.chef_knife_ui.msg("Checking #{base_folder_path} ...")

        source_size = Dir.glob(File.join(from, '**', '*')).size
        destination_size = Dir.glob(
          File.join(base_folder_path, '**', '*')
        ).size

        return if source_size == destination_size

        message = "folder copy failed: Expected #{source_size.inspect} but " \
                  "copied #{destination_size.inspect}"
        context.chef_knife_ui.err(message)
        context.fail!(error: message)
      end

      def client_rb_path
        File.join(context.action_root_path, 'client.rb')
      end

      def copy_client_rb_file_if_any
        return unless File.exist?(client_rb_path)

        copy_file_to_the_action_base(client_rb_path)

        check_copied_file!(client_rb_path)
      end

      def copy_data_bags_if_any
        return unless File.directory?(data_bags_path)

        copy_folder_to_the_action_base(data_bags_path)

        check_copied_folder!(data_bags_path)
      end

      def copy_knife_rb_file_if_any
        return unless File.exist?(knife_rb_path)

        copy_file_to_the_action_base(knife_rb_path)

        check_copied_file!(knife_rb_path)
      end

      def copy_nodes_if_any
        return unless File.directory?(nodes_path)

        copy_folder_to_the_action_base(nodes_path)

        check_copied_folder!(nodes_path)
      end

      def copy_file_to_the_action_base(from)
        context.chef_knife_ui.msg(
          "Copying #{from} to #{context.action_base_path} ..."
        )

        FileUtils.cp(from, context.action_base_path)
      end

      def copy_folder_to_the_action_base(from)
        context.chef_knife_ui.msg(
          "Copying #{from} to #{context.action_base_path} ..."
        )

        FileUtils.cp_r(from, context.action_base_path)
      end

      def data_bags_path
        File.join(context.action_root_path, 'data_bags')
      end

      def destination_path_from(from)
        File.join(context.action_base_path, File.basename(from))
      end

      def knife_rb_path
        File.join(context.action_root_path, 'knife.rb')
      end

      def nodes_path
        File.join(context.action_root_path, 'nodes')
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  module LogPersistence
    #
    # Persists the given message in a temporary place.
    # Periodically the NodeActionLogsUpdatorJob will grab
    # NodeActionTemporaryLogs belonging to the same NodeAction and move them.
    #
    class PersistLogMessage
      include Interactor

      def call
        sanity_checks!

        NodeActionTemporaryLogs.create!(
          node_action_id: context.node_action_id,
          log: build_log_message_object
        )

      rescue RethinkDB::ReqlRuntimeError
        retry
      end

      private

      def sanity_checks!
        unless context.node_action_id
          context.fail!(error: 'node_action_id is missing')
        end

        context.fail!(error: 'Message is missing') unless context.message

        return if context.timestamp

        context.fail!(error: 'timestamp is missing')
      end

      def build_log_message_object
        {
          log_msg_id: context.log_msg_id,
          message: context.message,
          timestamp: context.timestamp,
          type: context.type
        }
      end
    end
  end
end

# frozen_string_literal: true

require 'commis/attributes_builders/node'

module NodeActions
  module Finishers
    #
    # Reads the node's JSON file and update the ChefNode database document.
    #
    class RefreshNodeFromJsonFile
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Refreshing node from JSON file ...')

        if Rails.env.development?
          context.chef_knife_ui.msg('Looking for a file at ' \
                                    "#{node_json_file_path} ...")
        end

        refresh_node_document_from_json!
      end

      private

      def sanity_checks!
        SanityChecks::ActionBasePath.check!(context)
        SanityChecks::ChefNode.check!(context) # Check node object in memory
        SanityChecks::ChefRepository.check!(context)

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end

      def find_policy_from_current_repository_by_name(name)
        ChefPolicy.where(
          chef_repository_id: context.repository.id,
          name: name
        ).first
      end

      def find_policy_group_from_current_repository_by_name(name)
        ChefPolicyGroup.where(
          chef_repository_id: context.repository.id,
          name: name
        ).first
      end

      def node_already_have_the_policy?(name)
        current_policy = node_policy

        return false unless current_policy

        current_policy.name == name
      end

      # A node is linked to one policy per chef repositories.
      # With a node that has just been bootstrapped, this will return nil.
      def node_policy
        ChefNodesPolicy.where(
          chef_policy_id: context.node.id,
          chef_repository_id: context.repository.id
        ).first
      end

      def node_json_file_path
        File.join(
          context.action_base_path,
          'nodes',
          "#{context.node.name}.json"
        )
      end

      def refresh_node_document_from_json!
        if File.exist?(node_json_file_path)
          json = JSON.parse(File.read(node_json_file_path))
          attributes = Commis::AttributesBuilders::Node.build_from(json)
          attributes = update_attributes_associations_from(attributes)
          context.node.update!(attributes)
        else
          context.fail!(error: "Not such file #{node_json_file_path}.")
        end
      end

      def update_attributes_associations_from(attributes)
        if attributes.key?(:policy_name)
          policy_name = attributes.delete(:policy_name)
          policy_group_name = attributes.delete(:policy_group)
          update_chef_policy_association_with(policy_name, policy_group_name)
        end

        attributes
      end

      def update_chef_policy_association_with(policy_name, policy_group_name)
        return unless policy_name
        return if node_already_have_the_policy?(policy_name)

        # Search the new policy by its name
        chef_policy = find_policy_from_current_repository_by_name(policy_name)
        chef_policy_group = find_policy_group_from_current_repository_by_name(
          policy_group_name
        )

        node_policy_link = ChefNodesPolicy.where(
          chef_node_id: context.node.id,
          chef_repository_id: context.repository.id
        ).first

        node_policy_link && node_policy_link.delete

        ChefNodesPolicy.where(
          chef_node_id: context.node.id,
          chef_policy_id: chef_policy.id,
          chef_policy_group_id: chef_policy_group.id,
          chef_repository_id: context.repository.id
        ).first_or_create!
      end
    end
  end
end

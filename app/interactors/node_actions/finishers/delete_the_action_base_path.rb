# frozen_string_literal: true

module NodeActions
  module Finishers
    #
    # When all is done, the action folder, produced by the `chef export` command
    # and where the `knife zero converge` has run, can be deleted.
    #
    class DeleteTheActionBasePath
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Deleting action folder ...')

        FileUtils.rm_rf(context.action_base_path)
      end

      private

      def sanity_checks!
        SanityChecks::ActionBasePath.check!(context)

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end
    end
  end
end

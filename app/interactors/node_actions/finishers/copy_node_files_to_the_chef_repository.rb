# frozen_string_literal: true

module NodeActions
  module Finishers
    class CopyNodeFilesToTheChefRepository
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Copy the nodes files back to the chef ' \
                              'repository ...')

        if Rails.env.development?
          [base_nodes_path, root_nodes_path].each do |nodes_path|
            context.chef_knife_ui.msg('')
            context.chef_knife_ui.msg "Files at #{nodes_path}:"
            Dir[File.join(nodes_path, '*')].each do |node_file|
              context.chef_knife_ui.msg node_file.gsub(nodes_path + '/', '')
            end
          end
        end

        if context.node_action.name == 'bootstrap'
          verify_new_node_file_is_present!
        end

        FileUtils.cp_r(base_nodes_path, context.action_root_path)
      end

      private

      def sanity_checks!
        unless context.action_root_path
          context.fail!(error: 'The action_root_path is required')
        end

        unless context.chef_knife_ui
          context.fail!(error: 'An initialised UI object is required')
        end

        SanityChecks::ActionBasePath.check!(context)
      end

      def base_nodes_path
        File.join(context.action_base_path, 'nodes')
      end

      def new_node_json_file_path
        @new_node_json_file_path ||= File.join(
          base_nodes_path,
          context.node_action.node_name + '.json'
        )
      end

      def root_nodes_path
        File.join(context.action_root_path, 'nodes')
      end

      def verify_new_node_file_is_present!
        return if File.exist?(new_node_json_file_path)

        context.fail!(
          error: "A new JSON file at #{new_node_json_file_path} was " \
                 "expected, but this file doesn't exist."
        )
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  module Finishers
    #
    # Node bootstrap passed, now save the node object and link it to the chef
    # repository and to policy and group.
    #
    class PersistNewNode
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Saving the node to the DB ...')
        context.chef_knife_ui.msg(
          '=> Creating/Updating a node with the IP address ' \
          "#{context.node.ipaddress}, policy #{context.policy.name} and " \
          "#{context.policy_group.name} ..."
        )

        context.node.save!

        link_node_to_chef_repository!
        link_node_to_chef_policy!

        context.node.mark_as_healthy
        context.node.save!

        # Links the node to the NodeAction so that if there is a configured hook
        # it will be able to access the bootstrapped node.
        context.node_action.chef_node = context.node
        context.node_action.save!
      end

      private

      def sanity_checks!
        SanityChecks::ChefNode.check!(context) # Check node object in memory
        SanityChecks::ChefPolicy.check!(context)
        SanityChecks::ChefPolicyGroup.check!(context)
        SanityChecks::ChefRepository.check!(context)

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end

      def find_node_chef_nodes_policy
        context.node.chef_nodes_policies.detect do |chef_nodes_policy|
          chef_nodes_policy.chef_repository_id == context.repository.id
        end
      end

      #
      # Links the ChefNode to the ChefRepository.
      #
      # `first_or_create!` is used here as the bootstrap node action can be used
      # in order to change the node's policy/policy group.
      #
      def link_node_to_chef_repository!
        ChefRepositoriesNode.where(
          chef_node: context.node,
          chef_repository: context.repository
        ).first_or_create!
      end

      #
      # Links the ChefNode with a ChefPolicy and a ChefPolicyGroup.
      #
      # This method updates existing record or create a new one as the bootstrap
      # node action can be used in order to change the node's policy/policy
      # group.
      #
      def link_node_to_chef_policy!
        node_chef_nodes_policy = find_node_chef_nodes_policy

        if node_chef_nodes_policy
          node_chef_nodes_policy.update(
            chef_policy: context.policy,
            chef_policy_group: context.policy_group
          )
        else
          ChefNodesPolicy.create!(
            chef_node: context.node,
            chef_policy: context.policy,
            chef_policy_group: context.policy_group,
            chef_repository: context.repository
          )
        end
      end
    end
  end
end

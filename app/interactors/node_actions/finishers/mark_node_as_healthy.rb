# frozen_string_literal: true

module NodeActions
  module Finishers
    class MarkNodeAsHealthy
      include Interactor

      def call
        sanity_checks!

        return if context.node.healthy?

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Marking node as Healthy ...')

        context.node.mark_as_healthy
        context.node.save!
      end

      private

      def sanity_checks!
        SanityChecks::ChefNode.check!(context)
      end
    end
  end
end

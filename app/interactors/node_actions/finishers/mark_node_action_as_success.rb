# frozen_string_literal: true

module NodeActions
  module Finishers
    class MarkNodeActionAsSuccess
      include Interactor

      def call
        sanity_checks!

        context.node_action.mark_as_success
        context.node_action.save!

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Finished successfully ! 🎉')
      end

      private

      def sanity_checks!
        SanityChecks::NodeAction.check!(context)

        return if context.chef_knife_ui

        context.fail!(error: 'An initialised UI object is required')
      end
    end
  end
end

# frozen_string_literal: true

require 'commis/std_out'
require 'commis/std_err'
require 'commis/std_in'

module NodeActions
  module Initialisers
    #
    # Initialise and store the Chef::Knife::UI object to the context.
    # This object has been overriden because Chef::Knife::UI has been made
    # for CLIs, while here we're a web app, so we want to persist and broadcast
    # logs.
    #
    class InitialiseChefKnifeUi
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui = Chef::Knife::UI.new(
          Commis::StdOut.new(context.node_action),
          Commis::StdErr.new(context.node_action),
          Commis::StdIn,
          {}
        )
        context.chef_knife_ui.node_action = context.node_action

        return unless Rails.env.development?

        context.chef_knife_ui.msg 'Chef::Knife::UI override ready'
      end

      private

      def sanity_checks!
        SanityChecks::NodeAction.check!(context)
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  module Initialisers
    #
    # Action query is the query passed to the knife command to search the node
    # against which the action will be perfomed.
    #
    class BuildActionQuery
      include Interactor

      def call
        sanity_checks!

        context.action_query ||= "name:#{context.node.name}"
      end

      private

      def sanity_checks!
        SanityChecks::ChefNode.check!(context)
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  module Initialisers
    #
    # Just show a summary of the node action to be performed.
    #
    class SummariseNodeAction
      include Interactor

      def call
        sanity_checks!

        context.chef_knife_ui.msg('Starting a new node action.')
        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('NodeAction details:')
        context.chef_knife_ui.msg(" |-> name: #{context.node_action.name}")

        context.chef_knife_ui.msg(" |-> repository: #{chef_repo_url}")

        if context.node_action.name == 'bootstrap'
          context.chef_knife_ui.msg(" |-> policy name: #{context.policy.name}")
        elsif context.node_action.name == 'converge'
          context.chef_knife_ui.msg(" |-> action query: #{context.action_query}")
        end

        if %w[bootstrap converge].include?(context.node_action.name)
          context.chef_knife_ui.msg(" '-> policy group: #{policy_group_name}")
        end

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('Node details:')

        if node.hostname
          context.chef_knife_ui.msg(" |-> hostname: #{node.hostname}")
        end

        context.chef_knife_ui.msg(" |-> ipaddress: #{node.ipaddress}")
        context.chef_knife_ui.msg(" |-> SSH port: #{node.ssh_port}")
        context.chef_knife_ui.msg(" `-> Using sudo: #{node.sudo ? 'yes' : 'no'}")
      end

      private

      def sanity_checks!
        context.fail!(error: 'The UI is required') unless context.chef_knife_ui

        SanityChecks::NodeAction.check!(context)

        if context.node_action.name == 'bootstrap'
          SanityChecks::ChefPolicy.check!(context)
        elsif context.node_action.name == 'converge'
          unless context.action_query
            context.fail!(error: 'An action_query is required when ' \
                                 'performing a converge action.')
          end
        else
          # This is a "hook" step
          return
        end

        return if context.node_action.chef_policy_group

        context.fail!(error: 'The given NodeAction reqiures a ' \
                             'chef_policy_group but it was nil.')
      end

      def chef_repo_url
        context.node_action.chef_repository.url
      end

      def node
        @node ||= begin
          if %w[bootstrap converge].include?(context.node_action.name)
            context.node_action.chef_node || context.node
          else # hooks
            context.node_action.node_action.chef_node
          end
        end
      end

      def policy_group_name
        Rails.logger.debug "context.node_action: #{context.node_action.inspect}"
        Rails.logger.debug "context.node_action.chef_policy_group: #{context.node_action.chef_policy_group.inspect}"
        context.node_action.chef_policy_group.name
      end
    end
  end
end

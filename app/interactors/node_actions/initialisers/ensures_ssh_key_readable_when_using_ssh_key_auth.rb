# frozen_string_literal: true

module NodeActions
  module Initialisers
    #
    # When the node uses the SSH Key authentication method, this interactor
    # checks wether the key is accessible and readable or not.
    #
    class EnsuresSshKeyReadableWhenUsingSshKeyAuth
      include Interactor

      def call
        sanity_checks!

        # Just go to the next interactor if the node is not using the SSH key
        # auth method
        return unless node_uses_ssh_key_auth?

        context.chef_knife_ui.msg('')
        context.chef_knife_ui.msg('=> Checking SSH key ...')

        check_ssh_key!
      end

      private

      def sanity_checks!
        SanityChecks::JobCancelled.check!(context)

        context.fail!(error: 'The UI is required') unless context.chef_knife_ui

        SanityChecks::ChefNode.check!(context)
      end

      def check_ssh_key!
        unless context.node.ssh_key_path
          context.fail!(error: 'An SSH key path is required when SSH key ' \
                               'authentication method is selected.')
        end

        unless File.exist?(context.node.ssh_key_path)
          context.fail!(error: "File #{context.node.ssh_key_path} not found")
        end

        key_data = File.read(context.node.ssh_key_path)

        return if key_data

        context.fail!(error: "The file at #{context.node.ssh_key_path} is " \
                             'unreadable or empty.')
      end

      def node_uses_ssh_key_auth?
        context.node.ssh_auth_method == ssh_key_auth_method
      end

      def ssh_key_auth_method
        AppConstants::Ssh::AUTH_METHODS.index('ssh_keys')
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  module Initialisers
    #
    # Initialise and store the ChefDk::UI object to the context.
    # This object has been overriden because ChefDk::UI has been made for CLIs,
    # while here we're a web app, so we want to persist and broadcast logs.
    #
    # This works for the `chef update` and `chef export` commands.
    # See config/initializers/chefdk_overrides.rb
    #
    # Commis reuse it in order to avoid duplicating the persist and broadcast
    # principle.
    #
    class InitialiseChefDkUi
      include Interactor

      def call
        sanity_checks!

        context.chefdk_ui = ChefDK::UI.new(context.node_action)

        return unless Rails.env.development?

        context.chefdk_ui.msg 'ChefDK::UI override ready'
      end

      private

      def sanity_checks!
        SanityChecks::NodeAction.check!(context)
      end
    end
  end
end

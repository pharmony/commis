# frozen_string_literal: true

require 'commis/log_device'

module NodeActions
  module Initialisers
    class InitialiseChefLog
      include Interactor

      def call
        sanity_checks!

        Chef::Log.init(Commis::LogDevice.new(context.node_action))

        Chef::Log.level = :info if Rails.env.development?

        Chef::Log.info 'Chef::Log override ready'
      end

      private

      def sanity_checks!
        SanityChecks::NodeAction.check!(context)
      end
    end
  end
end

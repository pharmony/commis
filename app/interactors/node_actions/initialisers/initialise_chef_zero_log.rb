# frozen_string_literal: true

require 'chef_zero/log'
require 'commis/log_device'

module NodeActions
  module Initialisers
    class InitialiseChefZeroLog
      include Interactor

      def call
        sanity_checks!

        ChefZero::Log.init(Commis::LogDevice.new(context.node_action))

        ChefZero::Log.level = :debug if Rails.env.development?

        ChefZero::Log.debug 'ChefZero::Log override ready'
      end

      private

      def sanity_checks!
        SanityChecks::NodeAction.check!(context)
      end
    end
  end
end

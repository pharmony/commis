# frozen_string_literal: true

module NodeActions
  module Initialisers
    #
    # Build a new node (ChefNode) but do not persist yet. If the bootstrapping
    # finish on success, the node will be saved.
    #
    class BuildNewNode
      include Interactor

      def call
        sanity_checks!

        context.node = ChefNode.where(
          ipaddress: context.node_action.node_ip_address
        ).first

        if context.node
          context.node.update(build_node_attributes)
        else
          context.node ||= ChefNode.new(build_node_attributes)
        end
      end

      private

      def sanity_checks!
        SanityChecks::NodeAction.check!(context)
      end

      def build_node_attributes
        auth_method = context.node_action.auth_method
        auth_method_index = AppConstants::Ssh::AUTH_METHODS.index(auth_method)

        node_attributes = {
          cluster_id: context.node_action.cluster_id,
          ipaddress: context.node_action.node_ip_address,
          name: context.node_action.node_name,
          ssh_auth_method: auth_method_index,
          ssh_port: context.node_action.ssh_port,
          ssh_username: context.node_action.ssh_username,
          sudo: context.node_action.sudo
        }

        key = auth_method == 'password' ? :ssh_password : :ssh_key_path
        node_attributes[key] = context.node_action.ssh_credential

        node_attributes
      end
    end
  end
end

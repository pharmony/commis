# frozen_string_literal: true

module NodeActions
  module Initialisers
    #
    # This interactor fetches the default node's policy for the given
    # repository.
    #
    # This is useful for the converge action where there is no selected policy
    # (policies are applied only with the bootstrap action).
    #
    class FetchNodePolicyFromRepository
      include Interactor

      def call
        sanity_checks!

        chef_node_policy = ChefNodesPolicy.where(
          chef_node_id: context.node.id,
          chef_repository_id: context.repository.id
        ).first

        if chef_node_policy
          context.policy = chef_node_policy.chef_policy
          context.policy_group = chef_node_policy.chef_policy_group
        else
          context.fail!(
            error: "Unable to find the ChefNode with ID #{context.node.id} " \
                   "and ChefRepository with ID #{context.repository.id} link."
          )
        end
      end

      private

      def sanity_checks!
        SanityChecks::ChefNode.check!(context)
        SanityChecks::ChefRepository.check!(context)
      end
    end
  end
end

# frozen_string_literal: true

module NodeActions
  #
  # The NodeActions::OrganiseNodeActionHookExecution organiser organise the
  # NodeAction PolicyHook execution.
  #
  class OrganiseNodeActionHookExecution
    include Interactor::Organizer

    organize Initialisers::InitialiseChefKnifeUi,
             Initialisers::SummariseNodeAction,
             # From this point, before each interactors, the job will be checked
             # if it has been cancelled and therefore fails the interactor.
             Preparators::MarkNodeActionAsRunning,
             Actions::Hook,
             Finishers::MarkNodeActionAsSuccess
  end
end

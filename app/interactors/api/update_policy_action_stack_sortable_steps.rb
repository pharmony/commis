# frozen_string_literal: true

module Api
  class UpdatePolicyActionStackSortableSteps
    include Interactor

    def call
      sanity_checks!

      sortables = context.steps.map do |step|
        attributes = {
          position: step['position'],
          type: step['type']
        }

        if step['type'] == 'bootstrap'
          if step['policy_name']
            policy = find_chef_policy_from(step['policy_name'])
            attributes[:chef_policy_id] = policy.id
          end
        else
          attributes[:query] = step['query']
        end

        PolicyActionSortableStep.new(attributes)
      end

      destroy_all_policy_action_stack_sortable_steps!

      create_policy_action_stack_sortable_steps_from(sortables)
    end

    private

    def sanity_checks!
      context.fail!(error: 'steps is missing') unless context.steps

      context.steps.each do |step|
        if step['type'] == 'bootstrap'
          unless step['policy_name']
            context.fail!(error: 'Policy name is missing for a bootstrap step')
            break
          end
        else
          unless step['query']
            context.fail!(error: 'Query is missing for a converge step')
            break
          end
        end
      end

      unless context.policy_action_stack
        context.fail!(error: 'policy_action_stack is missing')
      end

      return if context.policy_action_stack.is_a?(PolicyActionStack)

      context.fail!(
        error: 'Expected policy_action_stack to be an instance of ' \
               'PolicyActionStack but is an instance of ' \
               "#{context.policy_action_stack.class.name}"
      )
    end

    def create_policy_action_stack_sortable_steps_from(steps)
      steps.each do |step|
        step.policy_action_stack_id = context.policy_action_stack.id
        step.save!
      end
    end

    def destroy_all_policy_action_stack_sortable_steps!
      PolicyActionSortableStep.where(
        policy_action_stack_id: context.policy_action_stack.id
      ).delete_all
    end

    def find_chef_policy_from(name)
      context.policy_action_stack
             .chef_repository
             .chef_policies
             .where(name: name)
             .first
    end
  end
end

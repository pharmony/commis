# frozen_string_literal: true

module Api
  class UpdateNodesClusterNodeCount
    include Interactor

    def call
      Cluster.all.map(&:update_node_count)
    end
  end
end

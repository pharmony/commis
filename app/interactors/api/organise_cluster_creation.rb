# frozen_string_literal: true

module Api
  #
  # Create a new cluster
  #
  class OrganiseClusterCreation
    include Interactor::Organizer

    organize Api::CreateCluster
  end
end

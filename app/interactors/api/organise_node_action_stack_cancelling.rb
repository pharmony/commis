# frozen_string_literal: true

module Api
  class OrganiseNodeActionStackCancelling
    include Interactor::Organizer

    organize Api::Chef::FetchNodeActionStack,
             Api::CancelNodeActionStack,
             Api::MarkNodeActionStackAsCancelled
  end
end

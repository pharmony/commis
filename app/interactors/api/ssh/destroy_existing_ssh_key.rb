# frozen_string_literal: true

module Api
  module Ssh
    #
    # Delete any existing SSH key files from the `.ssh/` folder and delete
    # existing records of SshKeyPair.
    #
    class DestroyExistingSshKey
      include Interactor

      def call
        context.private_ssh_key_path = File.expand_path('~/.ssh/id_rsa')

        if File.exist?(context.private_ssh_key_path + '*')
          File.rm_f(context.private_ssh_key_path)
        end

        SshKeyPair.delete_all
      end
    end
  end
end

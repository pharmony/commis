# frozen_string_literal: true

module Api
  module Ssh
    class BuildInteractorOutput
      include Interactor

      def call
        sanity_checks!

        context.output = {
          ssh_key: context.ssh_key
        }
      end

      private

      def sanity_checks!
        return if context.ssh_key

        context.fail!(errors: { ssh_key: 'is missing' })
      end
    end
  end
end

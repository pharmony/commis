# frozen_string_literal: true

module Api
  module Ssh
    class OrganiseFindOrCreateSshKeys
      include Interactor::Organizer

      organize Api::Ssh::SearchAnExistingSshKey,
               Api::Ssh::CreateANewSshKeyIfNeeded,
               Api::Ssh::BuildInteractorOutput
    end
  end
end

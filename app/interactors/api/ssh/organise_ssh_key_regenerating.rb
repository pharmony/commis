# frozen_string_literal: true

module Api
  module Ssh
    #
    # Destroy the existing SSH key, and generate a new one.
    #
    class OrganiseSshKeyRegenerating
      include Interactor::Organizer

      organize Api::Ssh::DestroyExistingSshKey,
               Api::Ssh::CreateANewSshKeyIfNeeded,
               Api::Ssh::BuildInteractorOutput
    end
  end
end

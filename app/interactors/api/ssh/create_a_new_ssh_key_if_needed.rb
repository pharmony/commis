# frozen_string_literal: true

module Api
  module Ssh
    #
    # When no existing SshKeyPair records, creates a new record with a generated
    # ssh key, and write it on the disk.
    #
    class CreateANewSshKeyIfNeeded
      include Interactor

      def call
        return if context.ssh_key

        find_ssh_keys || generate_new_ssh_keys_and_persists_them!

        create_home_ssh_folder_if_needed!

        write_private_ssh_key! && write_public_ssh_key!
      end

      private

      def find_ssh_keys
        return false if SshKeyPair.count.zero?

        ssh_key_pair = SshKeyPair.first
        return false unless ssh_key_pair

        context.ssh_private_key = ssh_key_pair.private_key
        context.ssh_key = ssh_key_pair.ssh_public_key

        true # Do not create a new key
      end

      def generate_new_ssh_keys_and_persists_them!
        Rails.logger.info '[Api::Ssh::CreateANewSshKeyIfNeeded] Generating a ' \
                          'new SSH key ...'

        ssh_key_pair = SSHKey.generate

        SshKeyPair.create!(
          private_key: ssh_key_pair.private_key,
          ssh_public_key: ssh_key_pair.ssh_public_key
        )

        context.ssh_private_key = ssh_key_pair.private_key
        context.ssh_key = ssh_key_pair.ssh_public_key
      end

      def create_home_ssh_folder_if_needed!
        # Creates the .ssh folder if missing
        ssh_dir_path = File.expand_path(
          File.dirname(context.private_ssh_key_path)
        )
        FileUtils.mkdir_p(ssh_dir_path) unless File.exist?(ssh_dir_path)
        # FileUtils.chmod_R doesn't work
        system("chmod -R 700 #{ssh_dir_path}")
      end

      def write_private_ssh_key!
        Rails.logger.info '[Api::Ssh::CreateANewSshKeyIfNeeded] Writing ' \
                          'private SSH key ...'

        # Writes the private key on the disk
        File.write(context.private_ssh_key_path, context.ssh_private_key)
        # FileUtils.chmod_R doesn't work
        system("chmod -R 600 #{context.private_ssh_key_path}")

        true
      end

      def write_public_ssh_key!
        Rails.logger.info '[Api::Ssh::CreateANewSshKeyIfNeeded] Writing ' \
                          'public SSH key ...'

        # Writes the public key on the disk
        public_key_path = context.private_ssh_key_path + '.pub'
        File.write(public_key_path, context.ssh_key)
        # FileUtils.chmod_R doesn't work
        system("chmod -R 644 #{public_key_path}")
      end
    end
  end
end

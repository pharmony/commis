# frozen_string_literal: true

module Api
  module Ssh
    class SearchAnExistingSshKey
      include Interactor

      def call
        context.private_ssh_key_path = File.expand_path('~/.ssh/id_rsa')

        if File.exists?(context.private_ssh_key_path)
          ssk_key_file = File.read(context.private_ssh_key_path)
          ssk_key = SSHKey.new(ssk_key_file)

          context.ssh_key = ssk_key.ssh_public_key
        else
          Rails.logger.info '[Api::Ssh::SearchAnExistingSshKey] No SSH key ' \
                            "found at #{context.private_ssh_key_path}."
        end
      rescue OpenSSL::PKey::DSAError => error
        Rails.logger.warn '[Api::Ssh::SearchAnExistingSshKey] Invalid SSH ' \
                          "key at #{context.private_ssh_key_path}."

        # Removing the file will make the key regenerated
        FileUtils.rm_rf(context.private_ssh_key_path)
        FileUtils.rm_rf(context.private_ssh_key_path + 'pub')
      end
    end
  end
end

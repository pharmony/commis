# frozen_string_literal: true

module Api
  #
  # Update the steps of a given PolicyActionStack
  #
  class OrganisePolicyActionStepsUpdate
    include Interactor::Organizer

    organize Api::FetchPolicyActionStack,
             Api::UpdatePolicyActionStackSortableSteps
  end
end

# frozen_string_literal: true

module Api
  #
  # Retrieves the PolicyHook belonging to the given NodeAction ID.
  #
  class FetchNodeActionPolicyHook
    include Interactor

    def call
      sanity_checks!

      node_action = NodeAction.find(context.node_action_id)

      unless node_action
        context.fail!(
          error: "Cannot find any NodeAction with ID #{context.node_action_id}"
        )
      end

      context.hook = node_action.policy_hook
    end

    private

    def sanity_checks!
      return if context.node_action_id

      context.fail!(error: 'node_action_id is missing')
    end
  end
end

# frozen_string_literal: true

module Api
  class MarkNodeActionStackAsCancelled
    include Interactor

    def call
      sanity_checks!

      context.node_action_stack.mark_as_cancelled
      context.node_action_stack.save!
    end

    private

    def sanity_checks!
      return if context.node_action_stack

      context.fail!(error: 'node_action_stack is missing')
    end
  end
end

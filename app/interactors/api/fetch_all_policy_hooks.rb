# frozen_string_literal: true

module Api
  #
  # Retrieves all the hooks from a given policy ID.
  #
  class FetchAllPolicyHooks
    include Interactor

    def call
      sanity_checks!

      context.hooks = PolicyHook.where(chef_policy_id: context.policy_id).to_a
    end

    private

    def sanity_checks!
      return if context.policy_id

      context.fail!(error: 'policy_id is missing')
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class FetchAllVersions
      include Interactor

      def call
        context.versions = ChefVersion.to_a
      end
    end
  end
end

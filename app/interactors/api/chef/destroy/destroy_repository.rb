# frozen_string_literal: true

module Api
  module Chef
    module Destroy
      class DestroyRepository
        include Interactor

        def call
          sanity_checks!

          context.repository.destroy
        end

        private

        def sanity_checks!
          SanityChecks::ChefRepository.check!(context)
        end
      end
    end
  end
end

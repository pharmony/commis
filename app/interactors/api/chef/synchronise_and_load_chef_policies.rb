# frozen_string_literal: true

module Api
  module Chef
    #
    # Creates/Deletes policies and policy_groups and load them all.
    #
    class SynchroniseAndLoadChefPolicies
      include Interactor

      def call
        sanity_checks!

        # Skip to the next interactor when no Policyfile found
        return unless context.policyfile_pathes.present?

        context.policies = []
        context.policy_groups = []

        # Collect groups
        load_policies_and_groupes_from_chef_repo_files
        load_policies_and_groupes_from_database

        # Partition groups
        to_be_created, to_be_deleted = partition_policy_groups

        # Create/Delete groups
        create_new_groupes_in_database!(to_be_created)
        delete_groupes_if_missing_in_chef_repo!(to_be_deleted)

        populate_context_policy_group

        # Update ChefPolicy
        create_or_update_repository_policies
      end

      private

      def sanity_checks!
        SanityChecks::ChefRepository.check!(context)
      end

      def create_new_groupes_in_database!(group_names)
        group_names.map { |group_name| find_or_create_policy_group(group_name) }
      end

      def create_new_policy(name, policyfile_path)
        ChefPolicy.create!(
          name: name,
          policyfile_path: policyfile_path,
          chef_repository: context.repository
        )
      end

      def create_or_update_repository_policies
        context.policyfile_pathes.each do |name, policyfile_path|
          context.policies << find_or_create_policy(name, policyfile_path)
        end
      end

      def delete_groupes_if_missing_in_chef_repo!(group_names)
        group_names.map do |group_name|
          chef_policy_group = ChefPolicyGroup.where(
            chef_repository: context.repository,
            name: group_name
          ).first

          next unless chef_policy_group

          chef_policy_group.destroy
        end
      end

      def find_or_create_policy(name, policyfile_path)
        policy = ChefPolicy.where(
          chef_repository: context.repository,
          name: name
        ).first

        return policy if policy

        create_new_policy(name, policyfile_path)
      end

      def find_or_create_policy_group(group_name)
        group = find_policy_group(group_name)

        return group if group

        ChefPolicyGroup.create!(
          chef_repository: context.repository,
          name: group_name
        )
      end

      def find_policy_group(group_name)
        ChefPolicyGroup.where(
          chef_repository: context.repository,
          name: group_name
        ).first
      end

      def load_policies_and_groupes_from_chef_repo_files
        groups_path = File.join(context.repository.local_path,
                                'policy_groups',
                                '*.json')

        context.file_group_names = Dir[groups_path].collect do |group_path|
          File.basename(group_path, '.json')
        end
      end

      def load_policies_and_groupes_from_database
        context.db_group_names = ChefPolicyGroup.where(
          chef_repository: context.repository
        ).collect(&:name)
      end

      def partition_policy_groups
        to_be_created = context.file_group_names - context.db_group_names
        to_be_deleted = context.db_group_names - context.file_group_names

        [to_be_created, to_be_deleted]
      end

      def policies
        @policies ||= Array(context.nodes_attributes).map do |node|
          { name: node[:policy_name], group: node[:policy_group] }
        end
      end

      def populate_context_policy_group
        context.policy_groups = ChefPolicyGroup.where(
          chef_repository: context.repository
        ).to_a
      end
    end
  end
end

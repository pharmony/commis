# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a ChefPolicy instance from its id
    #
    class FetchPolicy
      include Interactor

      def call
        sanity_checks!

        context.policy = ChefPolicy.find(context.id || context.policy_id)
      end

      private

      def sanity_checks!
        return if context.id || context.policy_id

        context.fail!(error: 'Policy ID is required')
      end
    end
  end
end

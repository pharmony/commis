# frozen_string_literal: true

module Api
  module Chef
    #
    # Delete the Chef cache folder
    #
    class ClearCache
      include Interactor

      def call
        sanity_checks!

        FileUtils.rm_rf(context.setting.chef_cache_dir)
      end

      private

      def sanity_checks!
        return if context.setting

        context.fail!(error: 'setting is missing')
      end
    end
  end
end

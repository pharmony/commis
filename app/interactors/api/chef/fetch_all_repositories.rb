# frozen_string_literal: true

module Api
  module Chef
    class FetchAllRepositories
      include Interactor

      def call
        context.repositories = ChefRepository.to_a
      end
    end
  end
end

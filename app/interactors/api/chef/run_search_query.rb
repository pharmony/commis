# frozen_string_literal: true

require 'chef/search/query'
require 'chef/local_mode'

module Api
  module Chef
    #
    # Calls the Chef search API passing the given query and stores the results
    # as context.chef_nodes.
    #
    class RunSearchQuery
      include Interactor

      def call
        sanity_checks!

        configure_chef

        nodes = execute_search_query

        context.chef_nodes = fetch_chef_nodes_matching(nodes) || []
      end

      private

      def sanity_checks!
        SanityChecks::ChefRepository.check!(context)

        return if context.query

        context.fail!(error: 'query is missing')
      end

      def configure_chef
        ::Chef::Config.local_mode = true
        ::Chef::Config.chef_repo_path = context.repository.local_path
      end

      # The search will return only the IP Address of the matching nodes, so
      # that it will be used in order to load ChefNodes.
      def execute_search_query
        final_query = update_query_if_needed(context.query)
        ::Chef::LocalMode.with_server_connectivity do
          query = ::Chef::Search::Query.new
          query.search(
            :node,
            final_query,
            filter_result: {
              ipaddress: ['ipaddress']
            }
          ).first # search returns an Array with the results as the first item
        end
      end

      def fetch_chef_nodes_matching(nodes)
        node_ids = ChefRepositoriesNode.where(
          chef_repository_id: context.repository.id
        ).map(&:chef_node_id)

        ChefNode.where(
          :id.in => node_ids,
          :ipaddress.in => nodes.map { |node| node['ipaddress'] }
        ).to_a
      end

      #
      # Updates the given query by replacing Commis' special markers like the
      # `#node-group#` which is replaced by
      # the `context.node_action.chef_policy_group.name`.
      #
      def update_query_if_needed(query)
        interactor = ConvergeQueryUpdator.call(
          policy_group: context.current_policy_group,
          query: query
        )

        if interactor.success?
          interactor.final_query
        else
          context.fail!(
            error: "ConvergeQueryUpdator failed for query #{query.inspect} " \
                   "with the error #{interactor.error.inspect}"
          )
        end
      end
    end
  end
end

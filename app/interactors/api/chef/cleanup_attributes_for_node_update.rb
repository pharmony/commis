# frozen_string_literal: true

module Api
  module Chef
    #
    # Replaces some values when necessary before to update a Chef::Node.
    #
    class CleanupAttributesForNodeUpdate
      include Interactor

      def call
        cleanup_cluster_id
      end

      private

      def cleanup_cluster_id
        return if context.attributes['cluster_id'].present?

        context.attributes['cluster_id'] = nil
      end
    end
  end
end

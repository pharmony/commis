# frozen_string_literal: true

module Api
  module Chef
    class OrganiseCacheClearing
      include Interactor::Organizer

      organize Api::Settings::FindOrCreateTheSettingInstance,
               Api::Chef::ClearCache
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class OrganiseRepositoryPull
      include Interactor::Organizer

      organize Api::Chef::EnsureRepositoryExists,
               Api::Chef::Pull::InitialiseChefDkUi,
               Api::Chef::Pull::MarkRepositoryAsPulling,
               Git::FetchAndResetHardRepository,
               Api::Chef::BuildLocalGitRepoDetails,
               Api::Chef::LoadNodesIfAny,
               Api::Chef::SearchPolicyfiles,
               Api::Chef::SynchroniseAndLoadChefPolicies,
               Api::Chef::CreateOrUpdateChefNodes,
               Api::Chef::CreateChefNodesPolicies,
               Api::Chef::DeletePoliciesWhichDoesntExistsInChefRepo,
               Api::Chef::DeleteNodesWhichDoesntExistsInChefRepo,
               Api::UpdateNodesClusterNodeCount,
               Api::Chef::Pull::MarkRepositoryAsPulled
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Links nodes to policies
    #
    class CreateChefNodesPolicies
      include Interactor

      def call
        return unless context.nodes.present?
        return unless context.policies.present?

        sanity_checks!

        links_nodes_to_policies
      end

      private

      def sanity_checks!
        SanityChecks::ChefRepository.check!(context)
      end

      def links_nodes_to_policies
        context.nodes.each do |node|
          node_attributes = find_node_attributes_for(node)
          policy = find_policy_for(node_attributes[:policy_name])
          policy_group = find_policy_group_for(node_attributes[:policy_group])

          chef_nodes_policy = node.chef_nodes_policies.where(
            chef_node_id: node.id,
            chef_repository_id: context.repository.id
          ).first

          if chef_nodes_policy
            chef_nodes_policy.update(
              chef_policy_id: policy.id,
              chef_policy_group_id: policy_group.id
            )
          else
            ChefNodesPolicy.create!(
              chef_node_id: node.id,
              chef_policy_id: policy.id,
              chef_policy_group_id: policy_group.id,
              chef_repository_id: context.repository.id
            )
          end
        end
      end

      def find_node_attributes_for(node)
        context.nodes_attributes.detect do |node_attributes|
          node_attributes[:ipaddress] == node.ipaddress
        end
      end

      def find_policy_for(policy_name)
        context.policies.detect { |policy| policy.name == policy_name }
      end

      def find_policy_group_for(policy_group)
        context.policy_groups.detect { |group| group.name == policy_group }
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Look for Policyfiles based on the Node's policy_name.
    #
    class SearchPolicyfiles
      include Interactor

      def call
        sanity_checks!

        policies_path = File.join(context.git_local_path, 'policies')
        return unless File.exist?(policies_path)

        context.policyfile_pathes = {}

        load_policies_from(policies_path)
      end

      private

      def sanity_checks!
        SanityChecks::GitLocalPath.check!(context)
      end

      def load_policies_from(policies_path)
        Dir[File.join(policies_path, '*.rb')].map do |file|
          context.policyfile_pathes[File.basename(file, '.rb')] = file
        end
      end
    end
  end
end

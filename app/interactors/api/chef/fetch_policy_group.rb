# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a ChefPolicyGroup instance from its id
    #
    class FetchPolicyGroup
      include Interactor

      def call
        sanity_checks!

        context.policy_group = fetch_chef_policy_group_through_strategies
      end

      private

      def sanity_checks!
        return if policy_group_id || (repository_id && node_id)

        context.fail!(error: 'PolicyGroup ID is required')
      end

      def fetch_chef_policy_group_by(options = {})
        if options.key?(:repository_id) && options.key?(:node_id)
          # A ChefNode can have only one policy/group per chef repositories
          chef_nodes_policy = ChefNodesPolicy.where(
            chef_node_id: options[:node_id],
            chef_repository_id: options[:repository_id]
          ).first

          chef_nodes_policy&.chef_policy_group
        elsif options.key?(:repository_id)
          ChefPolicyGroup.where(
            id: options[:policy_group_id],
            chef_repository_id: options[:repository_id]
          ).first
        else
          ChefPolicyGroup.find(options[:policy_group_id])
        end
      end

      # Fetch a ChefNode by its ID or Name
      def fetch_chef_node_if_possible
        return context.node if context.node

        return nil unless node_id

        ChefNode.find?(node_id) || ChefNode.where(name: node_id).first
      end

      def fetch_chef_policy_group_through_strategies
        chef_repository = fetch_chef_repository_if_possible
        chef_node = fetch_chef_node_if_possible

        if chef_repository && chef_node
          fetch_chef_policy_group_by(repository_id: chef_repository.id,
                                     node_id: chef_node.id)
        elsif chef_repository && chef_node.nil?
          fetch_chef_policy_group_by(repository_id: chef_repository.id,
                                     policy_group_id: policy_group_id)
        elsif chef_repository.nil?
          fetch_chef_policy_group_by(policy_group_id: policy_group_id)
        end
      end

      def fetch_chef_repository_if_possible
        return context.repository if context.repository

        return nil unless repository_id

        ChefRepository.find?(repository_id)
      end

      def node_id
        context.node_id || context.chef_node_id
      end

      def policy_group_id
        context.id || context.policy_group_id ||
          # TODO : Replace this with the new linking model for node to
          # repo/policy/policy_group
          # (See https://gitlab.com/pharmony/commis/issues/16)
          context.policy&.chef_policy_group_id
      end

      def repository_id
        context.repository_id || context.chef_repository_id
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    module NodeActions
      #
      # Creates a NodeActionStack its NodeActions based on a PolicyActionStack
      # sortable and unsortable steps (Running custom bootstrap).
      #
      class CreateABootstrapNodeActionStack
        include Interactor

        def call
          sanity_checks!

          fetch_policy_action_stack

          context.node_action_stack = NodeActionStack.create!(
            chef_repository_id: context.repository.id,
            type: 'bootstrap'
          )

          ordered_custom_steps.each { |step| create_node_action_for(step) }

          # "Frozen" steps are steps that aren't custom and cannot be modified.
          # As of writing this, there is always only one step which is a
          # boostrap one of a policy. It is created automatically when creating
          # a ChefPolicy.
          # @see ChefPolicy#create_policy_action_stack
          ordered_frozen_steps.each do |step|
            # Marking them as original will let the "Retry" feature use them in
            # order to request a retry.
            create_node_action_for(step, original: true)
          end
        end

        private

        def sanity_checks!
          SanityChecks::ChefPolicyGroup.check!(context)
          SanityChecks::ChefRepository.check!(context)
          SanityChecks::ChefPolicy.check!(context)
        end

        def create_a_bootstrap_node_action_from(step, options)
          NodeAction.create!(
            # Global params
            name: 'bootstrap',
            node_action_stack_id: context.node_action_stack.id,
            original: options[:original] || false,
            policy_action_stack_id: context.policy_action_stack.id,
            policy_action_stack_step_id: step.id,
            position: step.position,
            # Bootstrap params
            cluster_id: context.cluster_id,
            chef_policy_id: step.chef_policy_id,
            chef_policy_group_id: context.policy_group.id,
            # Connection details
            auth_method: context.auth_method,
            node_ip_address: context.node_ip_address,
            node_name: context.node_name,
            ssh_credential: context.ssh_credential,
            ssh_port: context.ssh_port,
            ssh_username: context.ssh_username,
            sudo: context.sudo
          )
        end

        def create_converge_node_actions_from(step)
          search = Api::Chef::RunSearchQuery.call(
            current_policy_group: context.policy_group,
            query: step.query,
            repository: context.repository
          )

          if search.failure?
            context.fail!(
              error: "The query #{step.query} failed: #{search.error}"
            )
          end

          node_actions = []

          search.chef_nodes.each do |chef_node|
            node_actions << NodeAction.create!(
              # Global params
              name: 'converge',
              node_action_stack_id: context.node_action_stack.id,
              policy_action_stack_id: context.policy_action_stack.id,
              policy_action_stack_step_id: step.id,
              position: step.position,
              # Converge params
              chef_node_id: chef_node.id,
              chef_policy_group_id: context.policy_group.id,
              # ipaddress is uniq across Commis, therefore it ensures this query
              # select the rigth node.
              query_override: "ipaddress:#{chef_node.ipaddress}"
            )
          end

          return node_actions unless search.chef_nodes.empty?

          NodeAction.create!(
            name: 'converge',
            node_action_stack_id: context.node_action_stack.id,
            policy_action_stack_id: context.policy_action_stack.id,
            policy_action_stack_step_id: step.id,
            position: step.position,
            query_result_empty: true,
            state: 'unrunnable'
          )
        end

        #
        # Creates the bootstrap or converge NodeActions and then, in the case
        # the ChefPolicy has hooks, creates the "hook" NodeActions and links
        # them to the bootstrap/converge NodeAction so that later the hook could
        # use the produced data.
        #
        def create_node_action_for(step, options = { original: false })
          node_actions = if step.type == 'bootstrap'
                           create_a_bootstrap_node_action_from(step, options)
                         else
                           create_converge_node_actions_from(step)
                         end

          create_node_action_for_policy_hook_if_any(step, Array(node_actions))
        end

        def create_node_action_for_policy_hook_if_any(step, node_actions)
          return unless step.chef_policy_id

          hooks = PolicyHook.where(chef_policy_id: step.chef_policy_id).to_a
          hooks.each do |hook|
            node_actions.each do |node_action|
              NodeAction.create!(
                name: 'hook',
                node_action_id: node_action.id,
                node_action_stack_id: context.node_action_stack.id,
                policy_action_stack_id: context.policy_action_stack.id,
                policy_action_stack_step_id: step.id,
                policy_hook_id: hook.id,
                position: step.position
              )
            end
          end
        end

        def fetch_policy_action_stack
          interactor = Api::FetchPolicyActionStack.call(
            policy: context.policy,
            repository: context.repository
          )

          if interactor.failure?
            context.fail!(error: 'Unable to find the PolicyActionStack: ' \
                                 "#{interactor.error}")
          end

          context.policy_action_stack = interactor.policy_action_stack
        end

        def ordered_custom_steps
          context.policy_action_stack.policy_action_sortable_steps.order_by(
            position: :asc
          ).to_a
        end

        def ordered_frozen_steps
          context.policy_action_stack.policy_action_unsortable_steps.order_by(
            position: :asc
          ).to_a
        end
      end
    end
  end
end

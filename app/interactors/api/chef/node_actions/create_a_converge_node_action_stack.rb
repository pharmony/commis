# frozen_string_literal: true

module Api
  module Chef
    module NodeActions
      #
      # Creates a NodeActionStack for a "simple" converge NodeAction.
      #
      # As of now, converving a node means selecting a node and click the
      # converge button, so there is no "query" at the moment in this case, so
      # the NodeAction belongs to a ChefNode.
      #
      # TODO : Implement the query mode where the Chef search is used in order
      #        to collect all the related ChefNodes and create a NodeAction for
      #        each of them (allowing to paralellise and hold a status per node)
      #
      class CreateAConvergeNodeActionStack
        include Interactor

        def call
          sanity_checks!

          fetch_policy_action_stack

          context.node_action_stack = NodeActionStack.create!(
            chef_repository_id: context.repository.id,
            type: 'converge'
          )

          NodeAction.create!(
            chef_node_id: context.node.id,
            chef_policy_group_id: context.policy_group.id,
            name: 'converge',
            node_action_stack_id: context.node_action_stack.id,
            original: true,
            policy_action_stack_id: context.policy_action_stack.id,
            policy_action_stack_step_id: step.id,
            pull_chef_repo: context.pull_chef_repo
          )
        end

        private

        def sanity_checks!
          SanityChecks::ChefNode.check!(context)
          SanityChecks::ChefRepository.check!(context)
          SanityChecks::ChefPolicyGroup.check!(context)
        end

        def fetch_policy_action_stack
          interactor = Api::FetchPolicyActionStack.call(
            node: context.node,
            repository: context.repository
          )

          if interactor.failure?
            context.fail!(error: 'Unable to find the PolicyActionStack: ' \
                                 "#{interactor.error}")
          end

          context.policy_action_stack = interactor.policy_action_stack
        end

        def step
          context.policy_action_stack.policy_action_unsortable_steps.first
        end
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    module NodeActions
      #
      # Create the node action and enqeue it to Sidekiq
      #
      class CreateActionsFromPolicyActionStack
        include Interactor

        def call
          sanity_checks!

          interactor = build_and_run_creation_interactor

          if interactor.success?
            context.node_action_stack = interactor.node_action_stack
            enqeue_and_update_node_action_stack!(interactor.node_action_stack)
          else
            context.fail!(
              error: "NodeActionStack creation failed: #{interactor.error}"
            )
          end
        end

        private

        def sanity_checks!
          unless context.action_name
            context.fail!(error: 'Action name is required')
          end

          unless allowed_action_names.include?(context.action_name)
            context.fail!(
              error: "Action name #{context.action_name} is not supported. " \
                     "Supported actions are #{allowed_action_names.join(', ')}"
            )
          end

          SanityChecks::ChefRepository.check!(context)
          SanityChecks::ChefPolicyGroup.check!(context)

          case context.action_name
          when 'bootstrap'
            SanityChecks::ChefPolicy.check!(context)
          when 'converge'
            SanityChecks::ChefNode.check!(context)
          end
        end

        def allowed_action_names
          %w[bootstrap converge]
        end

        def build_active_job_class
          "NodeActions::#{context.action_name.classify}Job".constantize
        end

        def build_and_run_creation_interactor
          params = build_node_action_attributes

          case context.action_name
          when 'bootstrap'
            CreateABootstrapNodeActionStack.call(params)
          when 'converge'
            CreateAConvergeNodeActionStack.call(params)
          end
        end

        def build_common_node_action_attributes
          {
            name: context.action_name,
            policy_group: context.policy_group,
            repository: context.repository
          }
        end

        def build_node_action_attributes
          node_action_attributes = build_common_node_action_attributes

          case context.action_name
          when 'bootstrap'
            node_action_attributes[:auth_method] = context.auth_method
            node_action_attributes[:cluster_id] = context.cluster_id
            node_action_attributes[:node_ip_address] = context.node_ip_address
            node_action_attributes[:node_name] = context.node_name
            node_action_attributes[:policy] = context.policy
            node_action_attributes[:ssh_credential] = context.ssh_credential
            node_action_attributes[:ssh_port] = context.ssh_port
            node_action_attributes[:ssh_username] = context.ssh_username
            node_action_attributes[:sudo] = context.sudo
          when 'converge'
            node_action_attributes[:node] = context.node
            node_action_attributes[:pull_chef_repo] = context.pull_chef_repo
          end

          node_action_attributes
        end

        def enqeue_and_update_node_action_stack!(stack)
          job = build_active_job_class.perform_later(stack.id)

          stack.update(jid: job.provider_job_id)
        end
      end
    end
  end
end

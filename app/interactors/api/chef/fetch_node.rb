# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a ChefNode instance from its id or name
    #
    class FetchNode
      include Interactor

      def call
        # In the case this interactor is used to create a NodeAction, therefore
        # context has an `action_name` attribute, and this attribute value is
        # `bootstrap`, this interactor can be ignored completely.
        return if context.action_name && context.action_name == 'bootstrap'

        sanity_checks!

        context.node = ChefNode.where(
          or: [
            { id: context.id || context.node_id },
            { name: context.id || context.node_id }
          ]
        ).first
      end

      private

      def sanity_checks!
        return if context.id || context.node_id

        context.fail!(error: 'Node ID is required')
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves available Chef versions and update the database
    #
    class OrganiseFetchingAvailableChefVersions
      include Interactor::Organizer

      organize Versions::FetchFromChefWebsite,
               Versions::ParseRetrievedVersions,
               Versions::UpdateDatabase
    end
  end
end

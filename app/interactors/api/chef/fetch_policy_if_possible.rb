# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a ChefPolicy instance from its id, in the case a policy_id has
    # been given.
    #
    class FetchPolicyIfPossible
      include Interactor

      def call
        return unless context.policy_id

        context.policy = ChefPolicy.find(context.policy_id)

        return unless context.policy

        context.policy_group = context.policy.chef_policy_group
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a PolicyActionStack belonging to the given ChefRepository and
    # ChefPolicy.
    #
    class FetchPolicyActionStack
      include Interactor

      def call
        sanity_checks!

        context.policy_action_stack = PolicyActionStack.where(
          chef_policy_id: context.policy_id,
          chef_repository_id: context.repository_id
        ).first

        return if context.policy_action_stack

        context.fail!(
          error: 'Unable to find the PolicyActionStack with the ' \
          "chef_policy_id #{policy_id} and chef_repository_id: #{repository_id}"
        )
      end

      private

      def sanity_checks!
        unless context.repository_id
          context.fail!(error: 'repository_id is missing')
        end

        return if context.policy_id

        context.fail!(error: 'policy_id is missing')
      end
    end
  end
end

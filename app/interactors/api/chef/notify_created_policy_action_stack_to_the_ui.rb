# frozen_string_literal: true

module Api
  module Chef
    class NotifyCreatedPolicyActionStackToTheUi
      include Interactor

      def call
        sanity_checks!

        CommisChannel::NodeActionStackState.broadcast(
          context.node_action_stack.id,
          'new',
          context.provider_job_id
        )
      end

      private

      def sanity_checks!
        SanityChecks::NodeActionStack.check!(context)

        return if context.provider_job_id

        context.fail!(error: 'provider_job_id is missing')
      end
    end
  end
end

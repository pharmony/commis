module Api
  module Chef
    module Pull
      class MarkRepositoryAsPulled
        include Interactor

        def call
          sanity_checks!

          context.repository.pull_success
          context.repository.save!

          # Notify the UI about the ChefRepository update
          CommisChannel::Repositories.broadcast_update(
            context.repository.as_json
          )
        end

        private

        def sanity_checks!
          SanityChecks::ChefRepository.check!(context)
        end
      end
    end
  end
end

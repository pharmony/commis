# frozen_string_literal: true

module Api
  module Chef
    module Pull
      #
      # This file is a copy/past from
      # app/interactors/node_actions/initialisers/initialise_chef_dk_ui.rb
      # so that we can use the UI logging mechanism when user do Pull & Converge
      # allowing to show the repo pulling status.
      #
      # To avoid confusions, instead of adding the
      # NodeActions::Initialisers::InitialiseChefDkUi interactor to the
      # OrganiseRepositoryPull organiser, and adapting it so that it works for
      # both cases, I prefered to copy/past and then adapt it.
      #
      class InitialiseChefDkUi
        include Interactor

        def call
          return unless context.node_action

          context.chefdk_ui = ChefDK::UI.new(context.node_action)

          return unless Rails.env.development?

          context.chefdk_ui.msg 'ChefDK::UI override ready'
        end
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class EnsureRepositoryExists
      include Interactor

      def call
        sanity_checks!

        context.repository = ChefRepository.find?(context.repository_id)

        return if context.repository

        context.fail!(error: 'No repository found with this ID.')
      end

      private

      def sanity_checks!
        unless context.repository_id
          context.fail!(error: 'Repository ID is required')
        end

        return if context.repository_id.is_a?(String)

        context.fail!(
          error: 'Repository ID must be a string but is a ' \
                 "#{context.repository_id.class.name}"
        )
      end
    end
  end
end

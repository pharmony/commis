# frozen_string_literal: true

module Api
  module Chef
    class OrganiseRepositoryDestroy
      include Interactor::Organizer

      organize Api::Chef::EnsureRepositoryExists,
               Api::Chef::Destroy::DestroyRepository
    end
  end
end

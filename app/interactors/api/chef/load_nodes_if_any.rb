# frozen_string_literal: true

require 'commis/attributes_builders/node'

module Api
  module Chef
    #
    # Looks for `nodes/*.json` files, parse them and prepare attributes for
    # ChefNode documents.
    #
    class LoadNodesIfAny
      include Interactor

      def call
        sanity_checks!

        context.nodes_attributes = []

        nodes_path = File.join(context.git_local_path, 'nodes')
        return unless File.exist?(nodes_path)

        build_nodes_attributes_from(nodes_path)
      end

      private

      def sanity_checks!
        SanityChecks::GitLocalPath.check!(context)
      end

      def build_node_attributes_from(json)
        Commis::AttributesBuilders::Node.build_from(json)
      end

      def build_nodes_attributes_from(nodes_path)
        Dir[File.join(nodes_path, '*.json')].map do |node_json|
          node_json = JSON.parse(File.read(node_json))
          context.nodes_attributes << build_node_attributes_from(node_json)
        end
      end
    end
  end
end

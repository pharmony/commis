# frozen_string_literal: true

module Api
  module Chef
    class UpdateNode
      include Interactor

      def call
        sanity_checks!

        return if context.node.update(context.attributes)

        context.fail!(
          error: 'Unable to save the node object: ' \
                 "#{context.node.errors.full_messages.to_sentence}"
        )
      end

      private

      def sanity_checks!
        SanityChecks::ChefNode.check!(context)
      end
    end
  end
end

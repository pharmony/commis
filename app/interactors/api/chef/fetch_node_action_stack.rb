# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a NodeAction instance belonging to a given ChefNode by its ID
    #
    class FetchNodeActionStack
      include Interactor

      def call
        sanity_checks!

        context.node_action_stack = NodeActionStack.where(
          id: context.id || context.node_action_stack_id
        ).first

        return if context.node_action_stack

        context.fail!(error:
          'Unable to find a NodeActionStack with ID ' \
          "#{context.id || context.node_action_stack_id}"
        )
      end

      private

      def sanity_checks!
        return if context.id || context.node_action_stack_id

        context.fail!(error: 'NodeActionStack ID is required')
      end
    end
  end
end

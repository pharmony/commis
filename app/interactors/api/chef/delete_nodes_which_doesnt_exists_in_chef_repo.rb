# frozen_string_literal: true

module Api
  module Chef
    #
    # Delete the ChefNode where the repository doesn't have a file representing
    # them.
    #
    class DeleteNodesWhichDoesntExistsInChefRepo
      include Interactor

      def call
        sanity_checks!

        cleanup_chef_nodes_from(context.nodes_attributes)
      end

      private

      def sanity_checks!
        SanityChecks::ChefRepository.check!(context)

        unless context.nodes_attributes
          context.fail!(error: 'Missing required nodes_attributes')
        end

        return if context.nodes_attributes.is_a?(Array)

        context.fail!(
          error: 'Expected nodes_attributes to be an instance of a Array ' \
                 "but is an instance of #{context.nodes_attributes.class.name}"
        )
      end

      def cleanup_chef_nodes_from(nodes_attributes)
        ipaddresses = nodes_attributes.collect { |node| node[:ipaddress] }

        # repository.chef_nodes is a has_many through, therefore `where` cannot
        # be used directly.
        # See http://nobrainer.io/docs/associations/#has_many_through_association
        chef_node_ids = context.repository.chef_nodes.map(&:id)

        ChefNode.where(:id.in => chef_node_ids)
                .where(not: { :ipaddress.in => ipaddresses })
                .destroy_all
      end
    end
  end
end

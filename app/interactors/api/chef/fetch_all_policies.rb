# frozen_string_literal: true

module Api
  module Chef
    #
    # Returns all the ChefPolicy from the given chef repository ID or all.
    #
    class FetchAllPolicies
      include Interactor

      def call
        context.policies = if context.repository_id
                             ChefPolicy.where(
                               chef_repository_id: context.repository_id
                             ).to_a
                           else
                             ChefPolicy.to_a
                           end
      end
    end
  end
end

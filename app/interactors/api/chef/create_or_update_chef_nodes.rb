# frozen_string_literal: true

module Api
  module Chef
    class CreateOrUpdateChefNodes
      include Interactor

      class AccessDenied < StandardError; end

      def call
        return unless context.nodes_attributes.present?

        sanity_checks!

        context.nodes = []

        Array(context.nodes_attributes).each do |node_attributes|
          # Makes a duplicate as we will remove keys
          new_node_attributes = node_attributes.dup

          # Removes attributes not part of the ChefNode
          new_node_attributes.delete(:cookbooks)
          new_node_attributes.delete(:policy_name)
          new_node_attributes.delete(:policy_group)

          node = find_or_create_node!(new_node_attributes)

          link_node_to_repository_if_needed(node)

          context.nodes << node
        end
      end

      private

      def sanity_checks!
        SanityChecks::ChefRepository.check!(context)

        unless context.nodes_attributes.is_a?(Array)
          context.fail!(error: 'Nodes attributes should be an Array but it ' \
                               "#{context.nodes_attributes.class.name}")
        end

        return if context.nodes_attributes.size.zero?

        return if context.nodes_attributes.all? { |node| node.is_a?(Hash) }

        context.fail!(error: 'Nodes attributes should be an Array of ' \
                             'Hashes but is not')
      end

      def find_or_create_node!(attributes)
        node = ChefNode.where(ipaddress: attributes[:ipaddress]).first

        if node
          node.update!(attributes)
          node
        else
          ChefNode.create!(attributes)
        end
      end

      def link_node_to_repository_if_needed(node)
        ChefRepositoriesNode.where(
          chef_repository: context.repository,
          chef_node: node
        ).first_or_create!
      end
    end
  end
end

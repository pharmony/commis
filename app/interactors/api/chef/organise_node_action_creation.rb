# frozen_string_literal: true

module Api
  module Chef
    #
    # Organise the creation and the queuing of NodeAction objects.
    #
    # Based on the `action_name` interactors will treat or ignore the treatment.
    # For instance, with the `action_name` 'bootstrap', the `FetchNode`
    # interactor doesn't make sense as we are looking to add a new node.
    #
    class OrganiseNodeActionCreation
      include Interactor::Organizer

      organize Api::Chef::FetchNode,
               Api::Chef::FetchRepository,
               Api::Chef::FetchPolicyIfPossible,
               Api::Chef::FetchPolicyGroup,
               Api::Chef::NodeActions::CreateActionsFromPolicyActionStack,
               Api::Chef::NotifyCreatedPolicyActionStackToTheUi
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    module Import
      class CreateChefCookbooks
        include Interactor

        def call
          # Nothing to do if no nodes
          return unless context.nodes_attributes.present?
          return unless context.nodes.present?

          sanity_checks!

          create_and_link_cookbooks_if_any
        end

        private

        def sanity_checks!
          SanityChecks::ChefRepository.check!(context)
        end

        def create_and_link_cookbooks_if_any
          Array(context.nodes_attributes).each do |node_attributes|
            cookbooks = node_attributes[:cookbooks]

            next if cookbooks.blank?

            create_cookbooks(cookbooks)
          end
        end

        def create_cookbooks(cookbooks)
          cookbooks.each do |cookbook|
            cookbook.each do |name, version|
              # Cookbook creation
              cookbook_attributes = {
                name: name,
                version: version
              }
              cookbook = ChefCookbook.where(cookbook_attributes).first
              unless cookbook.present?
                cookbook = ChefCookbook.create!(cookbook_attributes)
              end

              link_cookbook_to_the_chef_repository(cookbook)
            end
          end
        end

        def link_cookbook_to_the_chef_repository(cookbook)
          ChefCookbooksRepository.create!(
            chef_repository: context.repository,
            chef_cookbook: cookbook
          )
        end
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    module Import
      class ImportSshKeysIfNeeded
        include Interactor

        def call
          load_ssh_keys

          sanity_checks!

          return if ssh_keys_present_and_up_to_date?

          write_ssh_keys_to_the_disk!
        end

        private

        def sanity_checks!
          return if context.ssh_keys.present?

          context.fail!(error: 'Ssh key pair is missing while it was ' \
                               'expected to be present.')
        end

        def key_content_for(path)
          return context.ssh_keys.ssh_public_key if path.ends_with?('.pub')

          context.ssh_keys.private_key
        end

        def load_ssh_keys
          context.ssh_keys = SshKeyPair.first
        end

        def ssh_keys_present_and_up_to_date?
          context.private_ssh_key_path = File.expand_path('~/.ssh/id_rsa')
          public_ssh_key_path = context.private_ssh_key_path + '.pub'

          return false unless ssk_key_present?(context.private_ssh_key_path)
          return false unless ssh_key_is_up_to_date?(context.private_ssh_key_path)
          return false unless ssk_key_present?(public_ssh_key_path)
          return false unless ssh_key_is_up_to_date?(public_ssh_key_path)

          true
        end

        def ssk_key_present?(path)
          File.exists?(path)
        end

        def ssh_key_is_up_to_date?(path)
          File.read(path) == key_content_for(path)
        end

        def write_ssh_keys_to_the_disk!
          interactor = Api::Ssh::CreateANewSshKeyIfNeeded.call(context)

          return if interactor.success?

          context.fail!(
            error: "Unable to create new SSH key pair: #{interactor.error}"
          )
        end
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    module Import
      #
      # Updates the ChefRepository import_state as succeed
      #
      class MarkChefRepositoryImportAsSuccess
        include Interactor

        def call
          sanity_checks!

          context.repository.import_success
          context.repository.save!

          # Notify the UI about the ChefRepository update
          CommisChannel::Repositories.broadcast_update(
            context.repository.as_json
          )
        end

        private

        def sanity_checks!
          SanityChecks::ChefRepository.check!(context)
        end
      end
    end
  end
end

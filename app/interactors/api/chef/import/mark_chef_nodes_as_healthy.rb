# frozen_string_literal: true

module Api
  module Chef
    module Import
      #
      # We concider the nodes being healthy when importing the chef repository.
      #
      class MarkChefNodesAsHealthy
        include Interactor

        def call
          return unless context.nodes

          sanity_checks!

          return unless context.nodes.present?

          context.nodes.each do |node|
            begin
              # Update node status
              node.mark_as_healthy
            rescue AASM::InvalidTransition => error
              context.fail!(error: 'Unable to mark the Chef::Node with ID: ' \
                                   "#{node.id} as healthy: #{error.message}")
            end

            next if node.save

            context.fail!(error: 'Unable to update the Chef::Node with ID ' \
                                 "#{node.id}: #{node.errors.full_messages}")
          end
        end

        private

        def sanity_checks!
          unless context.nodes.is_a?(Array)
            context.fail!(
              error: 'Node list expected to be an instance of Array but it ' \
                     "an instance of #{context.nodes.class.name}"
            )
          end

          return if context.nodes.all? { |node| node.is_a?(ChefNode) }

          context.fail!(
            error: 'Node list expected to be an Array of ChefNode but is not.'
          )
        end
      end
    end
  end
end

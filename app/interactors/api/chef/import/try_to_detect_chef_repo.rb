# frozen_string_literal: true

module Api
  module Chef
    module Import
      class TryToDetectChefRepo
        include Interactor

        class AccessDenied < StandardError; end

        def call
          sanity_checks!

          if File.exists?(knife_file_path)
            knife_file = File.read(knife_file_path)
            unless knife_file =~ /^local_mode\s+true/
              context.fail!(error: 'The knife.rb file was expected to ' \
                                   'contain local_mode to true, but it did ' \
                                   'not. Is this repository a chef repository?')
            end

            unless knife_file =~ /^chef_repo_path\s+/
              context.fail!(error: 'The knife.rb file was expected to ' \
                                   'contain chef_repo_path, but it did not.')
            end
          else
            context.fail!(error: 'A knife.rb file was expected but could ' \
                                 'not be found in this repository.')
          end
        end

        private

        def sanity_checks!
          SanityChecks::GitLocalPath.check!(context)
        end

        def knife_file_path
          @knife_file_path ||= File.join(context.git_local_path, 'knife.rb')
        end
      end
    end
  end
end

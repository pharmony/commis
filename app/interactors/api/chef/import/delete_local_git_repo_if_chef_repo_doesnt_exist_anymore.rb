# frozen_string_literal: true

module Api
  module Chef
    module Import
      class DeleteLocalGitRepoIfChefRepoDoesntExistAnymore
        include Interactor

        def call
          sanity_checks!

          return if ChefRepository.where(url: context.git_url).present?

          Rails.logger.info 'Deleting local Git path ' \
                            "#{context.git_local_path} since no " \
                            'ChefRepository exists for this folder.'
          FileUtils.rm_rf(context.git_local_path)
        end

        private

        def sanity_checks!
          SanityChecks::GitUrl.check!(context)
          SanityChecks::GitLocalPath.check!(context)
        end
      end
    end
  end
end

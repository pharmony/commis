# frozen_string_literal: true

module Api
  module Chef
    module Import
      class TryToCloneGitRepo
        include Interactor

        class AccessDenied < StandardError; end

        def call
          sanity_checks!

          Rails.logger.info "[Api::Chef::TryToCloneGitRepo] Cloning with URL " \
                            "#{context.git_url} with name " \
                            "#{context.repository_name} ..."

          try_to_git_clone!
        end

        private

        def sanity_checks!
          SanityChecks::GitLocalPath.check!(context)
          SanityChecks::GitUrl.check!(context)
        end

        def try_to_git_clone!
          # Uses a wrapper bash script in order to set ssh flag to not check
          # host
          Git.configure do |config|
            config.git_ssh = File.join(Rails.root, 'bin', 'ssh_wrapper')
          end

          context.git = Git.clone(
            context.git_url,
            context.repository_name,
            path: File.expand_path('~/git/')
          )

          context.git_repo_name = context.repository_name
        rescue Git::GitExecuteError => error
          case error.message
          when /Host key verification failed/
            context.fail!(
              error: 'Access denied to the repository. Have the SSH key been ' \
                     'added to the account?'
            )
          when /destination path '#{context.git_local_path}' already exists and is not an empty directory./
            if ChefRepository.where(url: context.git_url).present?
              Rails.logger.warn '[Api::Ssh::TryToCloneGitRepo] No `rm -rf ' \
                                "#{context.git_local_path}` as a " \
                                'ChefRepository exists pointing to that ' \
                                'folder.'
              context.fail!(
                error: 'A folder already exists for the given Git URL.'
              )
            end
          else
            raise
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    module Import
      class CreateChefRepo
        include Interactor

        class AccessDenied < StandardError; end

        def call
          sanity_checks!

          context.repository = ChefRepository.create!(
            local_path: context.git_local_path,
            name: context.git_repo_name,
            url: context.git_url
          )

          # Notify the UI about the newly created ChefRepository
          CommisChannel::Repositories.broadcast_add(
            context.repository.attributes
          )
        end

        private

        def sanity_checks!
          SanityChecks::GitUrl.check!(context)
          SanityChecks::GitLocalPath.check!(context)
        end
      end
    end
  end
end

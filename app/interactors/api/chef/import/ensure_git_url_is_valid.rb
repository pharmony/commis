# frozen_string_literal: true

module Api
  module Chef
    module Import
      class EnsureGitUrlIsValid
        include Interactor

        def call
          sanity_checks!

          return if context.git_url =~ /git@[A-z\.\:\/]+/

          context.fail!(error: 'The Git URL is not a valid SSH+GIT URL')
        end

        private

        def sanity_checks!
          SanityChecks::GitUrl.check!(context)
        end
      end
    end
  end
end

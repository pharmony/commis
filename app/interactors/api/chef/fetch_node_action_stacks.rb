# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves all the NodeActionStack instances which touched the given
    # ChefNode ID.
    #
    class FetchNodeActionStacks
      include Interactor

      def call
        sanity_checks!

        node_action_stack_ids = NodeAction.where(
          chef_node_id: context.node_id
        ).map(&:node_action_stack_id)

        context.node_action_stacks = NodeActionStack.where(
          :id.in => node_action_stack_ids
        )
      end

      private

      def sanity_checks!
        context.fail!(error: 'A Node ID is required') unless context.node_id
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class OrganiseNodeUpdate
      include Interactor::Organizer

      organize Api::Chef::FetchNode,
               Api::Chef::CleanupAttributesForNodeUpdate,
               Api::Chef::UpdateNode
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class FetchAllPolicyGroups
      include Interactor

      def call
        context.policy_groups = ChefPolicyGroup.all
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class OrganiseRepositoryImport
      include Interactor::Organizer

      organize Api::Chef::Import::EnsureGitUrlIsValid,
               Api::Chef::Import::ImportSshKeysIfNeeded,
               Api::Chef::BuildLocalGitRepoDetails,
               Api::Chef::Import::DeleteLocalGitRepoIfChefRepoDoesntExistAnymore,
               Api::Chef::Import::TryToCloneGitRepo,
               Api::Chef::Import::TryToDetectChefRepo,
               Api::Chef::LoadNodesIfAny,
               Api::Chef::SearchPolicyfiles,
               Api::Chef::Import::CreateChefRepo,
               Api::Chef::SynchroniseAndLoadChefPolicies,
               Api::Chef::CreateOrUpdateChefNodes,
               Api::Chef::CreateChefNodesPolicies,
               Api::Chef::Import::CreateChefCookbooks,
               Api::Chef::Import::MarkChefNodesAsHealthy,
               Api::Chef::Import::MarkChefRepositoryImportAsSuccess
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Delete the ChefPolicy and ChefPolicyGroup where the repository doesn't
    # have a file representing them.
    #
    class DeletePoliciesWhichDoesntExistsInChefRepo
      include Interactor

      def call
        sanity_checks!

        return if context.policyfile_pathes.keys.size.zero?

        cleanup_chef_policies_and_groups_from(context.policyfile_pathes)
      end

      private

      def sanity_checks!
        SanityChecks::ChefRepository.check!(context)

        unless context.policyfile_pathes
          context.fail!(error: 'Missing required policyfile_pathes')
        end

        return if context.policyfile_pathes.is_a?(Hash)

        context.fail!(
          error: 'Expected policyfile_pathes to be an instance of a Hash but ' \
                 "is an instance of #{context.policyfile_pathes.class.name}"
        )
      end

      def cleanup_chef_policies_and_groups_from(policyfile_pathes)
        names = policyfile_pathes.collect(&:first)

        return if names.empty?

        context.repository.chef_policies.where(
          not: { :name.in => names }
        ).destroy_all
      end
    end
  end
end

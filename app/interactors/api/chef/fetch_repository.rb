# frozen_string_literal: true

module Api
  module Chef
    #
    # Retrieves a ChefRepository instance from its id
    #
    class FetchRepository
      include Interactor

      def call
        sanity_checks!

        context.repository = ChefRepository.find(repository_id)
      end

      private

      def sanity_checks!
        context.fail!(error: 'Repository ID is required') unless repository_id
      end

      def repository_id
        context.id || context.repository_id ||
          # TODO : Replace this with the new linking model for node to
          # repo/policy/policy_group
          # (See https://gitlab.com/pharmony/commis/issues/16)
          context.policy&.chef_repository_id
      end
    end
  end
end

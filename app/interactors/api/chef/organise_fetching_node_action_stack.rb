# frozen_string_literal: true

module Api
  module Chef
    class OrganiseFetchingNodeActionStack
      include Interactor::Organizer

      organize FetchNodeActionStack
    end
  end
end

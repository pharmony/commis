# frozen_string_literal: true

module Api
  module Chef
    module Versions
      #
      # Parses given body for Chef versions
      #
      class ParseRetrievedVersions
        include Interactor

        def call
          sanity_checks!

          version_list = context.body.scan(
            %r{<select id="js-versions-selector-" class=".*">([\s<>\w=".&;/\-]+)</select>}
          ).flatten.first

          if version_list.blank?
            context.fail!(
              error: 'Unable to parse Chef versions, returned version_list ' \
                     'is blank'
            )
          end

          context.versions = version_list.scan(
            %r{<option value="[\d\.]+"\s?>(.*)</option>}
          ).flatten

          return if context.versions.present?

          context.fail!(error: 'Unable to parse the version_list')
        end

        private

        def sanity_checks!
          return if context.body

          context.fail!(error: 'body is missing')
        end
      end
    end
  end
end

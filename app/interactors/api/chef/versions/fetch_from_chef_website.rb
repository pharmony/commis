# frozen_string_literal: true

module Api
  module Chef
    module Versions
      #
      # Fetches HTML page containing available Chef versions from
      # https://downloads.chef.io/chef/stable.
      #
      class FetchFromChefWebsite
        include Interactor

        CHEF_STABLE_URL = 'https://www.chef.io/downloads/tools/infra-client'

        def call
          context.body = fetch_data_from_chef_stable_url

          unless context.body
            Rails.logger.warn "[#{self.class.name}] Chef Stable body fetch " \
                              'FAILED!'
            return
          end

          Rails.logger.info "[#{self.class.name}] Chef Stable body fetched " \
                            'with SUCCESS!'
        end

        private

        def fetch_data_from_chef_stable_url
          response = Faraday.get(CHEF_STABLE_URL)

          return response.body if response.status == 200

          context.fail!(
            error: "GET request to #{CHEF_STABLE_URL} ended with status " \
                   "#{response.status}: #{response.reason_phrase}"
          )
        end
      end
    end
  end
end

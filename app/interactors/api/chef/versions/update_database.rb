# frozen_string_literal: true

module Api
  module Chef
    module Versions
      #
      # Updates the database entries for the available Chef versions
      #
      class UpdateDatabase
        include Interactor

        def call
          sanity_checks!

          context.versions.each do |chef_version|
            ChefVersion.where(version: chef_version).first_or_create!
          end
        end

        private

        def sanity_checks!
          return if context.versions

          context.fail!(error: 'versions is missing')
        end
      end
    end
  end
end

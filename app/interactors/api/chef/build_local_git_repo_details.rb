# frozen_string_literal: true

module Api
  module Chef
    class BuildLocalGitRepoDetails
      include Interactor

      def call
        sanity_checks!

        context.repository_name = grab_repository_name_from_git_url

        context.git_local_path = File.expand_path(
          File.join('~/git/', context.repository_name)
        )
      end

      private

      def sanity_checks!
        return if context.git_url || context.repository

        context.fail!(error: 'A Git URL or repository URL is required')
      end

      def git_url
        context.git_url || context.repository.url
      end

      def grab_repository_name_from_git_url
        git_url.scan(/:([A-z\-_\/\d+]+)\.git/).flatten.first
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class FetchAllNodes
      include Interactor

      def call
        context.nodes = ChefNode.to_a
      end
    end
  end
end

# frozen_string_literal: true

module Api
  class OrganiseClusterUpdate
    include Interactor::Organizer

    organize Api::FindCluster,
             Api::UpdateCluster
  end
end

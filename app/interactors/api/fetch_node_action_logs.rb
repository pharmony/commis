# frozen_string_literal: true

module Api
  class FetchNodeActionLogs
    include Interactor

    def call
      sanity_checks!

      node_action = NodeAction.find(context.node_action_id)

      context.logs = node_action.logs
    rescue NoBrainer::Error::DocumentNotFound
      context.fail!(error: 'NodeAction not found')
    end

    private

    def sanity_checks!
      return if context.node_action_id

      context.fail!(error: 'NodeAction ID is missing')
    end
  end
end

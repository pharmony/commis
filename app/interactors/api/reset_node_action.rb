# frozen_string_literal: true

module Api
  #
  # Reset the NodeAction state and logs from the context.
  #
  class ResetNodeAction
    include Interactor

    def call
      sanity_checks!

      context.node_action.logs = []
      context.node_action.reset_state
      # Not using `context.node_action.update!(...)` otherwise AASM is not
      # triggering the NodeAction.broadcast_status_change method.
      context.node_action.save!
    end

    private

    def sanity_checks!
      SanityChecks::NodeAction.check!(context)
    end
  end
end

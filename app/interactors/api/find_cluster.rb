# frozen_string_literal: true

module Api
  class FindCluster
    include Interactor

    def call
      sanity_checks!

      context.cluster = Cluster.find?(context.id)

      return if context.cluster

      context.fail!(error: 'Cluster not found')
    end

    private

    def sanity_checks!
      return if context.id

      context.fail!(error: 'Cluster ID is missing')
    end
  end
end

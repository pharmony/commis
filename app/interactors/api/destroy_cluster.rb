# frozen_string_literal: true

module Api
  class DestroyCluster
    include Interactor

    def call
      Rails.logger.debug 'Api::DestroyCluster.call ...'
      sanity_checks!

      Rails.logger.debug 'Destroying cluster ....'
      return if context.cluster.destroy
    end

    private

    def sanity_checks!
      return if context.id

      context.fail!(error: 'Cluster ID is missing')
    end
  end
end

# frozen_string_literal: true

module Api
  class FetchCookbooks
    include Interactor

    def call
      context.data = ChefCookbook.all.to_a
    end
  end
end

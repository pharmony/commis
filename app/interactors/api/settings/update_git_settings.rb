# frozen_string_literal: true

module Api
  module Settings
    #
    # Updates the Setting instance Git fields from the params
    #
    class UpdateGitSettings
      include Interactor

      def call
        sanity_checks!

        return unless context.params.key?('git')

        update_git_email_if_needed
        update_git_username_if_needed
      end

      private

      def sanity_checks!
        context.params || context.fail!(error: 'Params are missing')

        return if context.params.is_a?(ActionController::Parameters)

        context.fail!(error: 'Expected params to be an instance of Hash ' \
                             'but is an instance of ' \
                             "#{context.params.class.name}")
      end

      def update_git_email_if_needed
        return unless context.params['git']['git_email']

        context.setting.git_email = context.params['git']['git_email']
      end

      def update_git_username_if_needed
        return unless context.params['git']['git_username']

        context.setting.git_username = context.params['git']['git_username']
      end
    end
  end
end

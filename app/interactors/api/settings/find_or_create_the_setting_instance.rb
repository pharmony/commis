# frozen_string_literal: true

require 'chef/version'
require 'chef-dk/cookbook_omnifetch'
require 'chef-dk/version'
require 'chef_zero/version'
require 'knife-zero/version'

module Api
  module Settings
    #
    # Find or create the Setting record
    #
    class FindOrCreateTheSettingInstance
      include Interactor

      def call
        context.setting = Setting.first

        return if context.setting

        create_new_setting!
      rescue NoBrainer::Error::DocumentInvalid
        context.fail!(error: 'Something went wrong when creating the Setting ' \
                             'record')
      end

      private

      def create_new_setting!
        context.setting = Setting.create!(
          chef_cache_dir: CookbookOmnifetch.cache_path,
          chef_version: ::Chef::VERSION,
          chef_dk_version: ChefDK::VERSION,
          chef_zero_version: ChefZero::VERSION,
          knife_zero_version: Knife::Zero::VERSION
        )
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Settings
    #
    # Updates the Setting instance object fields for Node Action
    #
    class UpdateNodeActionSettings
      include Interactor

      def call
        sanity_checks!

        return unless context.params.key?('node_action')

        update_node_action_before_bootstrap_if_needed

        update_node_action_bootstrap_chef_version_if_needed
      end

      private

      def sanity_checks!
        context.params || context.fail!(error: 'Params are missing')

        return if context.params.is_a?(ActionController::Parameters)

        context.fail!(error: 'Expected params to be an instance of Hash ' \
                             'but is an instance of ' \
                             "#{context.params.class.name}")
      end

      def update_node_action_before_bootstrap_if_needed
        before_bootstrap = context.params['node_action']['before_bootstrap']

        return unless before_bootstrap

        context.setting.node_action_before_bootstrap = before_bootstrap
      end

      def update_node_action_bootstrap_chef_version_if_needed
        chef_version = context.params['node_action']['bootstrap_chef_version']

        return unless chef_version

        chef_version = ChefVersion.where(version: chef_version).first

        return unless chef_version

        context.setting.node_action_bootstrap_chef_version_id = chef_version.id
      end
    end
  end
end

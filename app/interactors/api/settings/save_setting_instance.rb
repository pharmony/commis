# frozen_string_literal: true

module Api
  module Settings
    #
    # Call `save` on the Setting instance
    #
    class SaveSettingInstance
      include Interactor

      def call
        sanity_checks!

        return if context.setting.save

        error_messages = context.setting.errors.full_messages.to_sentence
        context.fail!(error: "Unable to save Setting: #{error_messages}")
      end

      private

      def sanity_checks!
        return if context.setting

        context.fail!(error: 'Setting instance is missing')
      end
    end
  end
end

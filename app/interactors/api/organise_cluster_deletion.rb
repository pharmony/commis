# frozen_string_literal: true

module Api
  #
  # Delete an existing cluster
  #
  class OrganiseClusterDeletion
    include Interactor::Organizer

    organize Api::FindCluster,
             Api::DestroyCluster
  end
end

# frozen_string_literal: true

module Api
  class CreateCluster
    include Interactor

    def call
      sanity_checks!

      context.cluster = Cluster.create(
        name: context.attributes['name'],
        country_code: context.attributes['country_code']
      )

      return if context.cluster.valid?

      context.fail!(
        error: 'Cannot create the cluster: ' \
               "#{context.cluster.errors.full_messages}"
      )
    end

    private

    def sanity_checks!
      return if context.attributes['name']

      context.fail!(error: 'Cluster name is missing')
    end
  end
end

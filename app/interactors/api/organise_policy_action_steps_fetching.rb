# frozen_string_literal: true

module Api
  #
  # Retrieves the steps from a given PolicyActionStack
  #
  class OrganisePolicyActionStepsFetching
    include Interactor::Organizer

    organize Api::FetchPolicyActionStack,
             Api::BuildPolicyActionStackStepListReponse
  end
end

# frozen_string_literal: true

module Api
  module Hooks
    #
    # Updates the Authentication Method hooks attributes of the given ChefPolicy
    #
    class UpdateAuthenticationMethodPolicyHook
      include Interactor

      HOOK_NAME = 'PolicyHooks::NodeAuthorisationHook'

      def call
        sanity_checks!

        create_or_update_authentication_method_hook_if_needed
      end

      private

      def sanity_checks!
        SanityChecks::ChefPolicy.check!(context)

        return if context.hook_attributes

        context.fail!(error: 'hook_attributes is missing')
      end

      def hook_attributes
        @hook_attributes ||= search_hook_by_name HOOK_NAME
      end

      def authentication_method_hook_fields_present?
        return false if hook_attributes.nil?

        %w[auth_method auth_user ssh_credential].all? do |key|
          hook_attributes.key?(key)
        end
      end

      def find_policy_hook
        PolicyHooks::NodeAuthorisationHook.where(
          chef_policy_id: context.policy.id
        ).first
      end

      def search_hook_by_name(name)
        context.hook_attributes.detect do |hook_attribute|
          return hook_attribute if hook_attribute['_type'] == name
        end
      end

      def create_authentication_method_hook!
        PolicyHooks::NodeAuthorisationHook.create!(
          hook_attributes.merge!(
            chef_policy_id: context.policy.id
          )
        )
      end

      def create_or_update_authentication_method_hook_if_needed
        return unless authentication_method_hook_fields_present?

        policy_hook = find_policy_hook

        if policy_hook
          policy_hook.update!(hook_attributes)
        else
          create_authentication_method_hook!
        end
      end
    end
  end
end

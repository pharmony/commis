# frozen_string_literal: true

module Api
  class OrganiseNodeActionReRun
    include Interactor::Organizer

    organize Api::FetchNodeAction,
             Api::ResetNodeAction,
             Api::ClearNodeActionFolder,
             Api::RunNodeAction
  end
end

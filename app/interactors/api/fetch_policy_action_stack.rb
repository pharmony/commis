# frozen_string_literal: true

module Api
  #
  # Fetch a PolicyActionStack instance as context.policy_action_stack from its
  # id, policy_action_stack_id or from the combinaison of a ChefRepository and
  # a ChefPolicy.
  #
  class FetchPolicyActionStack
    include Interactor

    def call
      sanity_checks!

      context.policy_action_stack = find_policy_action_stack
    end

    private

    def sanity_checks!
      if context.policy_action_stack_id || context.id ||
         (context.repository && (context.node || context.policy))
        return
      end

      context.fail!(
        error: 'PolicyActionStack identification attributes are missing (' \
               'policy_action_stack_id, id or a ChefRepository and a ' \
               'ChefNode or a ChefPolicy instances are required)'
      )
    end

    def fetch_chef_policy_id
      return context.policy.id if context.policy

      policy_link = find_node_linked_policy_from_repository
      policy_link.chef_policy_id
    end

    def find_node_linked_policy_from_repository
      policy_link = ChefNodesPolicy.where(
        chef_node_id: context.node.id,
        chef_repository_id: context.repository.id
      ).first

      return policy_link if policy_link

      context.fail!(
        error: 'No linked policy to the node with IP address ' \
               "#{context.node.ipaddress} in the Chef repository with URL " \
               "#{context.repository.url} could be found."
      )
    end

    def find_from_repository
      chef_policy_id = fetch_chef_policy_id

      PolicyActionStack.where(
        chef_policy_id: chef_policy_id,
        chef_repository_id: context.repository.id
      ).first
    end

    def find_policy_action_stack
      id ? PolicyActionStack.find(id) : find_from_repository
    end

    def id
      context.policy_action_stack_id || context.id
    end
  end
end

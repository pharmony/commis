# frozen_string_literal: true

module Api
  class FetchAllNodeActionsForAStack
    include Interactor

    def call
      sanity_checks!

      context.node_actions = NodeAction.where(
        node_action_stack_id: context.node_action_stack_id
      ).to_a
    end

    private

    def sanity_checks!
      return if context.node_action_stack_id

      context.fail!(error: 'node_action_stack_id is missing')
    end
  end
end

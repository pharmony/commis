# frozen_string_literal: true

module Api
  class FetchAllNodeActionStacks
    include Interactor

    def call
      context.node_action_stacks = NodeActionStack.all.to_a
    end
  end
end

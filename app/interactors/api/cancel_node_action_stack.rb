# frozen_string_literal: true

module Api
  class CancelNodeActionStack
    include Interactor

    def call
      sanity_checks!

      ApplicationJob.cancel!(context.node_action_stack.jid)
    end

    private

    def sanity_checks!
      unless context.node_action_stack
        context.fail!(error: 'NodeActionStack is missing')
      end

      return if context.node_action_stack.jid

      context.fail!(
        error: "The NodeActionStack with ID #{context.node_action_stack.id} " \
               "doesn't have a JID."
      )
    end
  end
end

# frozen_string_literal: true

module Api
  class FetchDashboardData
    include Interactor

    def call
      context.data = {
        cluster_count: Cluster.count,
        node_count: ChefNode.count,
        repository_count: ChefRepository.count
      }
    end
  end
end

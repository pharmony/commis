# frozen_string_literal: true

module Api
  class FetchAllNodeActionStack
    include Interactor

    def call
      sanity_checks!

      context.node_action_stack = NodeActionStack.find?(
        context.node_action_stack_id
      )

      return if context.node_action_stack

      context.fail!(
        error: "NodeActionStack with ID #{context.node_action_stack_id} " \
               'could not be found'
      )
    end

    private

    def sanity_checks!
      return if context.node_action_stack_id

      context.fail!(error: 'A NodeActionStack ID is required')
    end
  end
end

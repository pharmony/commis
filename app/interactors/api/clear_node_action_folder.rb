# frozen_string_literal: true

module Api
  #
  # Destroys a previously created NodeAction folder (the folder where the
  # `chef export` command exports its data), which has been created previously.
  # This is useful in the case of the "ReRun" or "Run again" feature.
  #
  class ClearNodeActionFolder
    include Interactor

    def call
      sanity_checks!

      action_base_path = retrieve_action_base_path

      FileUtils.rm_rf(action_base_path)
    end

    private

    def sanity_checks!
      SanityChecks::NodeAction.check!(context)
    end

    def chef_knife_ui
      interactor = NodeActions::Initialisers::InitialiseChefKnifeUi.call(
        node_action: context.node_action
      )

      return interactor.chef_knife_ui if interactor.success?

      fail_context_with(
        error: "Can't initialise the Chef Knife UI: #{interactor.error}"
      )
    end

    def fail_context_with(error)
      context.fail!(error)
      mark_node_action_as_failed
    end

    def mark_node_action_as_failed
      context.node_action.mark_as_failed
      context.node_action.save!
    end

    def retrieve_action_base_path
      interactor = NodeActions::Preparators::PrepareActionBasePath.call(
        chef_knife_ui: chef_knife_ui,
        node_action: context.node_action,
        prevent_mkdir: true,
        repository: context.node_action.chef_repository
      )

      return interactor.action_base_path if interactor.success?

      fail_context_with(
        error: "Unable to get the action base path: #{interactor.error}"
      )
    end
  end
end

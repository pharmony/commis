# frozen_string_literal: true

module Api
  class FetchAllClusters
    include Interactor

    def call
      context.clusters = Cluster.to_a
    end
  end
end

# frozen_string_literal: true

module Api
  #
  # Persists the settings into the DB
  #
  class OrganiseSettingsUpdate
    include Interactor::Organizer

    organize Settings::FindOrCreateTheSettingInstance,
             Settings::UpdateGitSettings,
             Settings::UpdateNodeActionSettings,
             Settings::SaveSettingInstance
  end
end

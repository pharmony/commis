# frozen_string_literal: true

module Api
  #
  # Retrieves a NodeAction from its id
  #
  class FetchNodeAction
    include Interactor

    def call
      sanity_checks!

      context.node_action = NodeAction.find?(node_action_id)

      return if context.node_action

      context.fail!(
        error: "Not NodeAction could be found with ID #{node_action_id}"
      )
    end

    private

    def sanity_checks!
      return if node_action_id

      context.fail!(error: 'NodeAction ID is required')
    end

    def node_action_id
      context.id || context.node_action_id
    end
  end
end

# frozen_string_literal: true

module Api
  class OrganisePolicyHooksUpdate
    include Interactor::Organizer

    organize Api::Chef::FetchPolicy,
             Api::Hooks::UpdateAuthenticationMethodPolicyHook
  end
end

# frozen_string_literal: true

module Api
  class UpdateCluster
    include Interactor

    def call
      sanity_checks!

      return if context.cluster.update(context.attributes)

      context.fail!(
        error: 'Unable to save the cluster object: ' \
               "#{context.cluster.errors.full_messages.to_sentence}"
      )
    end

    private

    def sanity_checks!
      SanityChecks::Cluster.check!(context)
    end
  end
end

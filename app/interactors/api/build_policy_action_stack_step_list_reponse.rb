# frozen_string_literal: true

module Api
  class BuildPolicyActionStackStepListReponse
    include Interactor

    def call
      sanity_checks!

      context.response = {
        sortable: context.policy_action_stack.policy_action_sortable_steps,
        unsortable: context.policy_action_stack.policy_action_unsortable_steps
      }
    end

    private

    def sanity_checks!
      unless context.policy_action_stack
        context.fail!(error: 'policy_action_stack is missing')
      end

      return if context.policy_action_stack.is_a?(PolicyActionStack)

      context.fail!(
        error: 'Expected policy_action_stack to be an instance of ' \
               'PolicyActionStack but is an instance of ' \
               "#{context.policy_action_stack.class.name}"
      )
    end
  end
end

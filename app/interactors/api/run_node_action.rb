# frozen_string_literal: true

module Api
  #
  # Execute a specific NodeAction.
  # This interactor is *only* used by the "Re-run" or "Run again" feature.
  # The normal execution is made with the `NodeActions::BootstrapJob` and
  # `NodeActions::ConvergeJob` jobs.
  #
  class RunNodeAction
    include Interactor

    def call
      sanity_checks!

      interactor = case context.node_action.name
                   when 'bootstrap'
                     NodeActions::OrganiseNodeBootstrapping.call(action_context)
                   when 'converge'
                     NodeActions::OrganiseNodeConvergence.call(action_context)
                   when 'hook'
                     NodeActions::OrganiseNodeActionHookExecution.call(
                       action_context
                     )
                   end

      return if interactor.success?

      context.fail!(error: "NodeAction execution failed: #{interactor.error}")
    end

    private

    def sanity_checks!
      SanityChecks::NodeAction.check!(context)
    end

    def action_context
      case context.node_action.name
      when 'bootstrap'
        build_bootstrap_context
      when 'converge'
        build_converge_context
      when 'hook'
        build_hook_context
      end
    end

    def build_bootstrap_context
      common_context_attributes
    end

    def build_converge_context
      common_context_attributes.merge(
        node: context.node_action.chef_node
      )
    end

    def build_hook_context
      {
        node_action: context.node_action
      }
    end

    def common_context_attributes
      {
        node_action: context.node_action,
        policy: context.node_action.chef_policy,
        policy_group: context.node_action.chef_policy_group,
        provider_job_id: context.provider_job_id,
        repository: context.node_action.chef_repository
      }
    end
  end
end

# frozen_string_literal: true

module Api
  class FetchJobsStats
    include Interactor

    def call
      context.stats = {}

      # Processes that runs the jobs
      process_set = Sidekiq::ProcessSet.new
      context.stats[:process_set_size] = process_set.size

      if context.include_jobs == 'true'
        context.stats[:jobs] = []

        # Adds the running jobs
        queue = Sidekiq::Queue.new('default')
        queue.each do |job|
          context.stats[:jobs] << {
            args: cleanup_args(job.display_args),
            id: job.job_id,
            name: job.printable_name,
            requested: job.created_at,
            status: :running
          }
        end

        retry_set = Sidekiq::RetrySet.new
        retry_set.each do |job|
          context.stats[:jobs] << {
            args: cleanup_args(job.display_args),
            error: {
              message: job.item['error_message']
            },
            id: job.jid,
            name: job.display_class.constantize::PRINTABLE_NAME,
            requested: job.created_at,
            retry_count: job.item['retry_count'],
            status: :failed
          }
        end
      end
    rescue StandardError => error
      context.fail!(error: error.message)
    end

    private

    def cleanup_args(args)
      args.dup.map do |arg|
        arg.delete('_aj_symbol_keys')
        arg
      end
    end
  end
end

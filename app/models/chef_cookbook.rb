# frozen_string_literal: true

class ChefCookbook
  include NoBrainer::Document

  # ~~~~ Associations ~~~~
  # No dependent: :destroy here as this model is a the very bottom of the data
  # model, so it will be deleted only from a ChefRepository or a ChefNode.
  has_many :chef_cookbooks_repositories
  has_many :chef_repositories, through: :chef_cookbooks_repositories
  has_many :chef_cookbooks_nodes
  has_many :chef_nodes, through: :chef_cookbooks_nodes

  # ~~~~ Fields ~~~~
  field :name, type: String, required: true
  field :version, type: String, required: true

  # ~~~~ Validations ~~~~
  validates :version, uniqueness: { scope: :name }
end

# frozen_string_literal: true

#
# A Chef::Policy represents a Policyfile from a ChefRepository which is used
# when bootstraping/converging a node.
#
class ChefPolicy
  include NoBrainer::Document

  # ~~~~ Callbacks ~~~~
  after_create :create_policy_action_stack
  before_destroy :delete_chef_nodes_policy

  # ~~~~ Fields ~~~~
  field :name, type: String
  field :policyfile_path, type: String

  # ~~~~ Associations ~~~~
  belongs_to :chef_repository, index: true
  belongs_to :chef_nodes_policy
  # chef_policy_group is deleted by the chef_repository so no need for a
  # dependent option here.
  has_one :chef_policy_group, through: :chef_nodes_policy
  has_one :policy_action_stack, dependent: :destroy
  has_many :policy_hooks, dependent: :delete

  # ~~~~ Instance Methods ~~~~
  def as_json(*)
    super(include: [:chef_policy_group], methods: %i[customised hooked])
  end

  def customised
    policy_action_stack.policy_action_sortable_steps.present?
  end
  alias customised? customised

  def hooked
    policy_hooks.present?
  end
  alias hooked? hooked

  private

  def create_policy_action_stack
    stack = PolicyActionStack.create!(
      chef_policy_id: id,
      chef_repository_id: chef_repository_id
    )

    PolicyActionUnsortableStep.create!(
      chef_policy_id: id,
      policy_action_stack_id: stack.id,
      type: 'bootstrap'
    )
  end

  def delete_chef_nodes_policy
    chef_nodes_policy && chef_nodes_policy.delete
  end
end

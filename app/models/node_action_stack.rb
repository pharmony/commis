# frozen_string_literal: true

#
# A NodeActionStack is a container for NodeAction instances.
#
class NodeActionStack
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps
  include AASM

  # ~~~~ Fields ~~~~
  field :jid, type: String # Sidekiq Job ID
  field :type, type: String, in: %w[bootstrap converge], required: true
  field :state, type: String

  # ~~~~ Associations ~~~~
  belongs_to :chef_repository
  has_many :node_actions

  # ~~~~ Special Behavior ~~~~
  aasm column: 'state' do
    state :new, initial: true # NodeActionStack just created
    state :running
    state :success
    state :cancelled
    state :failed

    after_all_transitions :broadcast_status_change

    event :mark_as_running do
      transitions from: :new, to: :running
    end

    event :mark_as_success do
      transitions from: :running, to: :success
    end

    event :mark_as_cancelled do
      transitions from: :running, to: :cancelled
    end

    event :mark_as_failed do
      transitions from: %i[new running], to: :failed
    end
  end

  # ~~~~ Instance Methods ~~~~
  def broadcast_status_change
    CommisChannel::NodeActionStackState.broadcast(id, aasm.to_state)
  end
end

# frozen_string_literal: true

#
# A Chef::PolicyGroup represents the policy_group of a ChefNode.
#
class ChefPolicyGroup
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :name, type: String, required: true, uniq: {
    scope: :chef_repository_id
  }

  # ~~~~ Associations ~~~~
  belongs_to :chef_repository, required: true, index: true
end

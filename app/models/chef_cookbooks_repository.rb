# frozen_string_literal: true

class ChefCookbooksRepository
  include NoBrainer::Document

  # ~~~~ Associations ~~~~
  # No dependent: :delete as it is managed by a custom callback in the
  # ChefRepository model.
  belongs_to :chef_cookbook
  belongs_to :chef_repository
end

# frozen_string_literal: true

class ChefNodesPolicy
  include NoBrainer::Document

  # ~~~~ Associations ~~~~
  belongs_to :chef_node, uniq: {
    scope: %i[chef_policy_id chef_policy_group_id chef_repository_id]
  }
  belongs_to :chef_policy
  belongs_to :chef_policy_group
  belongs_to :chef_repository

  # ~~~~ Validations ~~~~~
  validates :chef_repository_id, uniqueness: { scope: %i[chef_node_id] }
end

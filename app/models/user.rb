# frozen_string_literal: true

class User
  include NoBrainer::Document

  # ~~~~ Callbacks ~~~~
  before_validation :sets_created_at_and_updated_at

  # ~~~~ Fields ~~~~
  field :email,              type: String, required: true, default: '', uniq: true
  field :encrypted_password, type: String, required: true, default: ''

  ## Recoverable
  field :reset_password_token, type: String, uniq: true
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count, type: Integer, default: 0, required: true
  field :current_sign_in_at, type: Time
  field :last_sign_in_at, type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip, type: String

  ## Confirmable
  field :confirmation_token, type: String, uniq: true
  field :confirmed_at, type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email, type: String # Only if using reconfirmable

  ## Lockable
  # Only if lock strategy is :failed_attempts
  field :failed_attempts, type: Integer, default: 0, required: true
  field :unlock_token, type: String, uniq: true # Only if unlock strategy is :email or :both
  field :locked_at, type: Time

  field :created_at, type: Time, required: true
  field :updated_at, type: Time, required: true

  field :token, type: String

  # Include default devise modules. Others available are:
  # :timeoutable and :omniauthable
  if Commis.configuration.registration_opened
    devise :database_authenticatable, :registerable, :confirmable, :lockable,
           :recoverable, :rememberable, :trackable, :validatable
  else
    devise :database_authenticatable, :confirmable, :lockable, :recoverable,
           :rememberable, :trackable, :validatable
  end

  # to fix an issue with devise
  # https://github.com/plataformatec/devise/issues/4542
  def will_save_change_to_email?; end

  private

  def sets_created_at_and_updated_at
    now = Time.now
    self.created_at ||= now
    self.updated_at = now
  end
end

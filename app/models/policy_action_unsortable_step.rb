# frozen_string_literal: true

#
# This model is a container of steps which are executed when running a bootstrap
# action on a node.
# Adding/moving/removing steps allows a user to customise how chef bootstrap a
# new node.
#
class PolicyActionUnsortableStep < PolicyActionStackStep
end

# frozen_string_literal: true

#
# A PolicyHook is used in order to execute some defined tasks when the belonging
# ChefPolicy is bootstrapped/converge to a node.
#
# This class is the parent class of all the available hooks,
# see `app/models/policy_hooks/`.
#
class PolicyHook
  include NoBrainer::Document

  class NotImplementedError < StandardError; end

  # ~~~~ Associations ~~~~
  belongs_to :chef_policy, index: true

  # ~~~~ Instance Method ~~~~
  # All hooks implements the `run` method.
  def run
    raise NotImplementedError
  end
end

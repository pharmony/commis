# frozen_string_literal: true

#
# A NodeAction represents bootstrap or converge action, and holds all
# the required information in order to perform the requested action.
#
class NodeAction
  include NoBrainer::Document
  include AASM

  attr_accessor :log_msg_id

  # ~~~~ Fields ~~~~
  field :logs, type: Array, default: []
  field :name, type: String, in: %w[bootstrap converge hook], required: true
  field :position, type: Integer, required: true, default: 0
  field :state, type: String
  # The original NodeAction represents the normal step (not a custum one), so
  # that it can be used for retry as it holds the information for retry.
  field :original, type: Boolean, default: false, required: true
  # Bootstrap action
  field :cluster_id, type: String
  field :node_name, type: String
  field :node_ip_address, type: String
  field :node_name, type: String
  field :auth_method, type: String
  field :ssh_credential, type: String
  field :ssh_port, type: Integer
  field :ssh_username, type: String
  field :sudo, type: Boolean
  # ~~~~ Converge action ~~~~
  # Should the Chef Repository being pulled before to run the converge action.
  field :pull_chef_repo, type: Boolean, default: false
  # When running a custum bootstrap, with converge steps, the step has a query
  # which selects the nodes where to execute the converge action, and it is used
  # to build all the steps to be performed during the custom bootstrap.
  # See the `create_converge_node_actions_from` method from
  # the Api::Chef::NodeActions::CreateABootstrapNodeActionStack.
  #
  # When this custom bootstrap runs, and executes a converge step, the step is
  # actually linked to a selected node, therefore the converge query shouldn't
  # be the one from the step, otherwise you would converge all the nodes that
  # the query returned.
  # Instead the following `query_override` field will contain a query
  # that selects a specific node and will be used instead of the step's query.
  field :query_override, type: String
  # Did the query return no result when it has been executed from a custom
  # bootstrap?
  #
  # See the `create_converge_node_actions_from` method from
  # the Api::Chef::NodeActions::CreateABootstrapNodeActionStack.
  field :query_result_empty, type: Boolean, default: false

  # ~~~~ Associations ~~~~
  # Converge associations
  belongs_to :chef_node, required: false, index: true
  # Bootstrap associations
  belongs_to :chef_policy, required: false
  # Hook associations
  belongs_to :node_action, required: false
  # Common assocations
  belongs_to :chef_policy_group, required: false # Not required for a "hook"
  belongs_to :node_action_stack, index: true
  belongs_to :policy_action_stack
  belongs_to :policy_action_stack_step
  belongs_to :policy_hook

  # ~~~~ Special Behavior ~~~~
  aasm column: 'state' do
    state :new, initial: true # NodeAction just created
    state :running
    state :success
    state :failed
    state :unrunnable

    after_all_transitions :broadcast_status_change

    event :reset_state do
      transitions to: :new
    end

    event :mark_as_running do
      transitions from: :new, to: :running
    end

    event :mark_as_success do
      transitions from: :running, to: :success
    end

    event :mark_as_failed do
      transitions from: %i[new running], to: :failed
    end
  end

  # ~~~~ Instance Methods ~~~~
  def serializable_hash(options = {})
    options = {
      include: %i[policy_hook], methods: %i[chef_repository_id]
    }.update(options)
    super(options)
  end

  def broadcast_status_change
    CommisChannel::NodeActionState.broadcast(node_action_stack_id, id,
                                             aasm.to_state)
  end

  def chef_repository
    node_action_stack.chef_repository
  end

  def chef_repository_id
    node_action_stack.chef_repository_id
  end

  def increment_log_msg_id!
    NoBrainer::Lock.new(
      "node_actions:#{id}:increment_log_msg_id"
    ).synchronize do
      self.log_msg_id ||= 0
      self.log_msg_id += 1
    end
  end
end

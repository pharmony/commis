# frozen_string_literal: true

class Setting
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  # Chef
  field :chef_cache_dir, type: String
  field :chef_version, type: String
  field :chef_dk_version, type: String
  field :chef_zero_version, type: String
  field :knife_zero_version, type: String
  # Git
  field :git_email, type: String
  field :git_username, type: String
  # Node Action
  field :node_action_before_bootstrap, type: String
  field :node_action_bootstrap_chef_version_id, type: String

  # ~~~~ Associations ~~~~
  belongs_to :bootstrap_chef_version,
             class_name: 'ChefVersion',
             foreign_key: :node_action_bootstrap_chef_version_id

  # ~~~~ Instance Methods ~~~~
  def as_json(*)
    super(
      only: [],
      methods: %i[
        chef
        chef_dk
        chef_zero
        knife_zero
        git
        node_action
      ]
    )
  end

  # The following methods transform the flat Setting object in structured data
  def chef
    {
      cache_dir: chef_cache_dir,
      version: chef_version
    }
  end

  def chef_dk
    {
      version: chef_dk_version
    }
  end

  def chef_zero
    {
      version: chef_zero_version
    }
  end

  def knife_zero
    {
      version: knife_zero_version
    }
  end

  def git
    {
      git_email: git_email,
      git_username: git_username
    }
  end

  def node_action
    {
      before_bootstrap: node_action_before_bootstrap,
      bootstrap_chef_version: bootstrap_chef_version&.version || 'unknown'
    }
  end
end

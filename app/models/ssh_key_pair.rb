# frozen_string_literal: true

#
# This model saved the generated SSH keys so that they can be written on any
# nodes missing it.
# Think about it running in a Docker environment, the node could miss the keys.
#
class SshKeyPair
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :private_key, type: Text, required: true
  field :ssh_public_key, type: Text, required: true
end

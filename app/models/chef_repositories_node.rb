# frozen_string_literal: true

class ChefRepositoriesNode
  include NoBrainer::Document

  # ~~~~ Callbacks ~~~~
  before_destroy :destroy_node_too_if_possible!

  # ~~~~ Associations ~~~~
  belongs_to :chef_node, index: true
  belongs_to :chef_repository, index: true

  # ~~~~ Validations ~~~~~
  validates :chef_repository_id, uniqueness: { scope: %i[chef_node_id] }

  private

  def destroy_node_too_if_possible!
    return unless chef_node

    return if chef_node.chef_repositories.count > 1

    chef_node.destroy
  end
end

# frozen_string_literal: true

class ChefCookbooksNode
  include NoBrainer::Document

  # ~~~~ Associations ~~~~
  belongs_to :chef_cookbook
  belongs_to :chef_node, index: true
end

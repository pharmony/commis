# frozen_string_literal: true

#
# A Chef version available for download
#
class ChefVersion
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :version, type: String, required: true, uniq: true
end

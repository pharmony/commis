# frozen_string_literal: true

class Cluster
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :name, type: String, required: true, uniq: true
  field :country_code, type: String
  field :node_count, type: Integer, default: 0

  # ~~~~ Associations ~~~~
  has_many :chef_nodes

  # ~~~~ Validations ~~~~~

  # ~~~~ Instance Methods ~~~~
  def update_node_count
    update(node_count: chef_nodes.count)
  end
end

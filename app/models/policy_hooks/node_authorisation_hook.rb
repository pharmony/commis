# frozen_string_literal: true

module PolicyHooks
  #
  # This hook allow to update the node's authentication method after it has
  # been boostrapped or converge with the belonging policy.
  #
  class NodeAuthorisationHook < PolicyHook
    # ~~~~ Fields ~~~~
    field :auth_method, type: String, required: true
    field :auth_user, type: String
    field :ssh_credential, type: String

    # ~~~~ Instance Methods ~~~~
    def run(node_action)
      #
      # The so-called parent is the NodeAction which bootstrapped or converged
      # the node.
      #
      parent_node_action = node_action.node_action

      parent_node_action.chef_node.update!(build_attributes)
    end

    private

    def auth_method_index
      AppConstants::Ssh::AUTH_METHODS.index(auth_method)
    end

    def build_attributes
      attributes = {
        ssh_auth_method: auth_method_index,
        ssh_username: auth_user
      }

      attributes[:ssh_password] = ssh_credential if auth_method == 'password'
      attributes[:ssh_key_path] = ssh_credential if auth_method == 'ssh_keys'

      attributes
    end
  end
end

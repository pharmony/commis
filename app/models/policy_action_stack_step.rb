# frozen_string_literal: true

#
# This model is a container of steps that are executed in a specific order, and
# is the parent model of the PolicyActionSortableStep and
# PolicyActionUnsortableStep.
#
class PolicyActionStackStep
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :position, type: Integer, default: 0, required: true
  field :type, type: String, in: %w[bootstrap converge], required: true

  # ~~~~ Associations ~~~~
  belongs_to :chef_policy, required: false
  belongs_to :policy_action_stack, index: true

  # ~~~~ Instance Methods ~~~~
  def as_json(*)
    super(methods: %i[policy_name])
  end

  def policy_name
    chef_policy.try(:name)
  end
end

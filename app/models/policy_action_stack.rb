# frozen_string_literal: true

#
# This model is a container of steps which are executed when running a bootstrap
# action on a node.
# Adding/moving/removing steps allows a user to customise how chef bootstrap a
# new node.
#
class PolicyActionStack
  include NoBrainer::Document

  # ~~~~ Associations ~~~~
  belongs_to :chef_policy, index: true
  belongs_to :chef_repository
  # Sortable steps are muable and executed before the unsortable steps
  has_many :policy_action_sortable_steps, dependent: :delete
  # Unsortable steps are immuable fixed steps, executed at the end
  has_many :policy_action_unsortable_steps, dependent: :delete
end

# frozen_string_literal: true

#
# Represents a Git versionned Chef repository
#
class ChefRepository
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps
  include AASM

  # ~~~~ Callbacks ~~~~
  # When deleting a ChefRepository, we check if the cookbooks belonging to this
  # ChefRepository belongs to other ChefRepository records, otherwise we can
  # delete it.
  before_destroy :destroy_cookbook_too_if_possible!

  # ~~~~ Fields ~~~~
  field :local_path, type: String, required: true
  field :name, type: String, required: true
  field :url, type: String, required: true, uniq: true
  field :import_state, type: String
  field :pull_state, type: String

  # ~~~~ Associations ~~~~
  # Cookbooks
  has_many :chef_cookbooks_repositories, dependent: :destroy
  has_many :chef_cookbooks, through: :chef_cookbooks_repositories
  # Nodes
  has_many :chef_repositories_nodes, dependent: :destroy
  has_many :chef_nodes, through: :chef_repositories_nodes
  # Policies
  has_many :chef_policies, dependent: :destroy
  has_many :chef_policy_groups, dependent: :delete

  # ~~~~ Special Behavior ~~~~
  aasm :import_state do
    state :importing, initial: true # Newly created repository from import
    state :import_succeed
    state :import_failed

    event :import_success do
      transitions from: :importing, to: :import_succeed
    end

    event :import_failure do
      transitions from: :importing, to: :import_failed
    end
  end

  aasm :pull_state do
    state :new, initial: true # Newly created repository from import
    state :pulling
    state :pull_succeed
    state :pull_failed

    event :pull do
      transitions to: :pulling
    end

    event :pull_success do
      transitions from: :pulling, to: :pull_succeed
    end

    event :pull_failure do
      transitions from: %i[new pulling pull_succeed], to: :pull_failed
    end
  end

  # ~~~~ Instance Methods ~~~~
  def as_json(*)
    super(
      include: {
        chef_policies: { only: [:id] }
      },
      methods: :node_count
    )
  end

  def node_count
    chef_nodes.count
  end

  private

  def destroy_cookbook_too_if_possible!
    chef_cookbooks.each do |chef_cookbook|
      next if chef_cookbook.chef_repositories.count > 1

      # delete has ChefCookbook has no callbacks
      chef_cookbook.delete
    end
  end
end

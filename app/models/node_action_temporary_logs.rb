# frozen_string_literal: true

#
# Stores temporarilly a log so that the NodeActionLogsUpdatorJob will group them
# and update the NodeAction logs periodically.
#
class NodeActionTemporaryLogs
  include NoBrainer::Document

  # ~~~~ Fields ~~~~
  field :log, type: Hash, required: true

  # ~~~~ Associations ~~~~
  belongs_to :node_action
end

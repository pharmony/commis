# frozen_string_literal: true

class ChefNode
  include NoBrainer::Document
  include AASM

  # ~~~~ Virtual Attributes ~~~~
  # Cluster update management fields
  attr_accessor :cluster_changed
  attr_accessor :previous_cluster_id

  # ~~~~ Callbacks ~~~~
  before_save :detect_cluster_changes
  after_save :trigger_cluster_update_if_needed

  # ~~~~ Fields ~~~~
  field :chef_version, type: String
  field :cpu_cores, type: Integer
  field :cpu_count, type: Integer
  field :filesystem_total, type: Integer
  field :filesystem_type, type: String
  field :hostname, type: String
  field :ipaddress, type: String, required: true, uniq: true
  field :kernel, type: String
  field :memory_total, type: Integer
  field :name, type: String, required: true
  field :platform, type: String
  field :platform_version, type: String
  field :state, type: String
  # SSH details
  field :ssh_auth_method,
        type: Integer,
        in: (0..AppConstants::Ssh::AUTH_METHODS.size - 1).to_a,
        default: AppConstants::Ssh::AUTH_METHODS.index('none')
  field :ssh_username, type: String, required: true, default: 'root'
  field :ssh_password, type: String
  field :ssh_port, type: Integer, required: true, default: 22
  field :ssh_key_path, type: String
  field :sudo, type: Boolean, required: true, default: true

  # ~~~~ Indeces ~~~~
  index %i[id name]

  # ~~~~ Associations ~~~~
  belongs_to :cluster
  # Cookbooks
  # dependent: :delete as the ChefCookbook deletion is managed by the
  # ChefRepository, see the :destroy_cookbook_too_if_possible! callback from the
  # ChefRepository model.
  has_many :chef_cookbooks_nodes, dependent: :delete
  has_many :chef_cookbooks, through: :chef_cookbooks_nodes
  # Repositories
  # dependent: :delete in order to manage the case where a node is unregistered
  # so we want to delete the ChefRepositoryNode record but no need for callbacks
  # as the existing callback is to delete the ChefNode when deleting the
  # ChefRepositoryNode record when deleting a ChefRepository.
  has_many :chef_repositories_nodes, dependent: :delete
  has_many :chef_repositories, through: :chef_repositories_nodes
  # Policies
  # dependent: :delete as we want to also delete the ChefNodesPolicy record for
  # the current ChefNode, but ChefPolicy and ChefPolicyGroup are deleted from a
  # ChefRepository delete action.
  has_many :chef_nodes_policies, dependent: :delete
  has_many :chef_policies, through: :chef_nodes_policies
  has_many :chef_policy_groups, through: :chef_nodes_policies
  # NodeActions
  # dependent: :delete as NodeAction has no callbacks
  has_many :node_actions, dependent: :delete

  # ~~~~ Special Behavior ~~~~
  aasm column: 'state' do
    state :new, initial: true # Brand new node just added
    # The node has been succesfully bootstraped, converged or imported from a
    # chef repo
    state :healthy

    event :mark_as_healthy do
      transitions from: %i[new healthy], to: :healthy
    end
  end

  # ~~~~ Instance Methods ~~~~
  def as_json(*)
    super(
      include: {
        chef_policies: { only: %i[id chef_repository_id] },
        chef_policy_groups: { only: %i[id chef_repository_id] },
        chef_repositories: { only: [:id] }
      }
    )
  end

  private

  #
  # Before saving the model to the database, it checks if the cluster_id has
  # changed, and save that information to the `attr_accessor :cluster_changed`
  # so that the `trigger_cluster_update_if_needed` method can know if
  # the cluster has changed or not.
  #
  def detect_cluster_changes
    self.cluster_changed = cluster_id_changed?

    self.previous_cluster_id = cluster_id_was
  end

  #
  # When updating the node's cluster, it triggers the previous (if any) and the
  # new cluster (if any) `update_node_count` method so that we keep a consitent
  # `node_count` on `Cluster`.
  #
  def trigger_cluster_update_if_needed
    return unless cluster_changed

    if previous_cluster_id
      previous_cluster = Cluster.find?(previous_cluster_id)
      previous_cluster.update_node_count if previous_cluster
    end

    cluster.update_node_count if cluster
  end
end

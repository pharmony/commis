# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  # Render the error as a JSON when request has been made in JSON format
  # otherwise render in HTML.
  rescue_from Exception do |error|
    raise unless request.format == :json

    Rails.logger.error "\n\n#{error.class.name} (#{error.message}):"
    Rails.logger.error error.backtrace.join("\n")

    render json: {
      error: { message: error.message, backtrace: error.backtrace }
    }, status: :internal_server_error
  end

  private

  def render_interactor(interactor, attributes = { success: nil, error: nil })
    if interactor.success?
      render response_from_interactor(interactor, attributes)
    else
      render json: {
        error: interactor.send(attributes[:error])
      }, status: :unprocessable_entity
    end
  end

  def response_from_interactor(interactor, attributes)
    if attributes[:success]
      body = interactor.send(attributes[:success])
      status = body.nil? ? :not_found : :ok
    else
      body = {}
      status = :ok
    end

    { json: body, status: status }
  end
end

# frozen_string_literal: true

module Api
  #
  # Manages a policy hooks data
  #
  class HooksController < ApplicationController
    def show
      interactor, success_key = build_interactor_from_params

      render_interactor interactor, success: success_key, error: :error
    end

    def update
      interactor = Api::OrganisePolicyHooksUpdate.call(
        policy_id: params[:policy_id],
        hook_attributes: update_params['hooks']
      )

      render_interactor interactor, error: :error
    end

    private

    def build_interactor_from_params
      return build_node_action_hook_interactor if params.key?(:node_action_id)
      return build_policy_hooks_interactor if params.key?(:policy_id)
    end

    def build_node_action_hook_interactor
      [
        Api::FetchNodeActionPolicyHook.call(
          node_action_id: params[:node_action_id]
        ),
        :hook
      ]
    end

    def build_policy_hooks_interactor
      [Api::FetchAllPolicyHooks.call(policy_id: params[:policy_id]), :hooks]
    end

    def update_params
      params.permit(hooks: %i[auth_method auth_user ssh_credential _type])
    end
  end
end

# frozen_string_literal: true

module Api
  class DashboardsController < ApplicationController
    def show
      interactor = Api::FetchDashboardData.call

      render_interactor interactor, success: :data, error: :error
    end
  end
end

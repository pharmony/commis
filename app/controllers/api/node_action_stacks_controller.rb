# frozen_string_literal: true

module Api
  class NodeActionStacksController < ApplicationController
    def index
      interactor = Api::FetchAllNodeActionStacks.call

      render_interactor interactor, success: :node_action_stacks, error: :error
    end

    def show
      interactor = Api::FetchAllNodeActionStack.call(
        node_action_stack_id: params[:id]
      )

      render_interactor interactor, success: :node_action_stack, error: :error
    end

    def create
      job = Api::Chef::NodeActionCreationJob.perform_later(
        action_name: params[:name],
        auth_method: params[:auth_method],
        cluster_id: params[:cluster_id],
        node_ip_address: params[:node_ip_address],
        node_name: params[:node_name],
        policy_id: params[:policy_id],
        policy_group_id: params[:policy_group_id],
        repository_id: params[:repository_id],
        ssh_credential: params[:ssh_credential],
        ssh_port: params[:ssh_port],
        ssh_username: params[:ssh_username],
        sudo: params[:sudo]
      )

      render json: job, status: :ok
    end

    def cancel
      interactor = Api::OrganiseNodeActionStackCancelling.call(
        node_action_stack_id: params[:id]
      )

      render_interactor interactor, error: :error
    end
  end
end

# frozen_string_literal: true

module Api
  #
  # Commis settings persistence controller
  #
  class SettingsController < ApplicationController
    def update
      interactor = Api::OrganiseSettingsUpdate.call(
        params: update_params['setting']
      )

      render_interactor interactor, error: :error
    end

    private

    def update_params
      return {} unless params.key?('setting')

      params.permit(setting: { git: %i[git_username git_email],
                               node_action: %i[before_bootstrap
                                               bootstrap_chef_version] })
    end
  end
end

# frozen_string_literal: true

module Api
  #
  # This controller is used in order to update PolicyActionStack objects.
  #
  class PolicyActionStacksController < ApplicationController
    def show
      interactor = Api::OrganisePolicyActionStepsFetching.call(
        policy_action_stack_id: params[:id]
      )

      render_interactor interactor, success: :response, error: :error
    end

    def update
      interactor = Api::OrganisePolicyActionStepsUpdate.call(
        policy_action_stack_id: params[:id],
        steps: params[:policy_action_stack][:steps]
      )

      render_interactor interactor, error: :error
    end
  end
end

# frozen_string_literal: true

module Api
  #
  # Returns the NodeActions for a given NodeActionStack without their logs,
  # also allows to re-run a NodeActionStack.
  #
  class NodeActionsController < ApplicationController
    def index
      interactor = Api::FetchAllNodeActionsForAStack.call(
        node_action_stack_id: params[:node_action_stack_id]
      )

      if interactor.success?
        render json: present_node_actions(interactor.node_actions), status: :ok
      else
        render json: { error: interactor.error }, status: :unprocessable_entity
      end
    end

    def rerun
      node_action = NodeAction.find?(params[:id])

      if node_action
        job = Api::NodeActionReRunJob.perform_later(
          node_action_id: node_action.id
        )

        render json: job, status: :ok
      else
        render json: {}, status: :not_found
      end
    end

    private

    def present_node_actions(node_actions)
      node_actions.collect do |node_action|
        node_action.serializable_hash(except: :logs)
      end
    end
  end
end

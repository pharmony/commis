# frozen_string_literal: true

module Api
  class CookbooksController < ApplicationController
    def index
      interactor = Api::FetchCookbooks.call

      render_interactor interactor, success: :data, error: :error
    end
  end
end

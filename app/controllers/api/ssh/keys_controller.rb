# frozen_string_literal: true

module Api
  module Ssh
    class KeysController < ApplicationController
      def show
        interactor = Api::Ssh::OrganiseFindOrCreateSshKeys.call

        render_interactor interactor, success: :output, error: :error
      end

      def regenerate
        interactor = Api::Ssh::OrganiseSshKeyRegenerating.call

        render_interactor interactor, success: :output, error: :error
      end
    end
  end
end

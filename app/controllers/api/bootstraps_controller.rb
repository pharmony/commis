# frozen_string_literal: true

module Api
  class BootstrapsController < ApplicationController
    def show
      render json: bootstrap_data, status: :ok
    end

    private

    def bootstrap_data
      commis_configuration_hash.merge(setting_hash)
    end

    def commis_configuration_hash
      { configuration: Commis.configuration.as_json }
    end

    def setting
      Settings::FindOrCreateTheSettingInstance.call.setting
    end

    def setting_hash
      { settings: setting.as_json || {} }
    end
  end
end

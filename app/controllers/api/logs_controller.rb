# frozen_string_literal: true

module Api
  #
  # Returns the NodeAction logs
  #
  class LogsController < ApplicationController
    def index
      interactor = Api::FetchNodeActionLogs.call(
        node_action_id: params[:node_action_id]
      )

      render_interactor interactor, success: :logs, error: :error
    end
  end
end

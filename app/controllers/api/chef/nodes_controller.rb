# frozen_string_literal: true

module Api
  module Chef
    class NodesController < ApplicationController
      def index
        interactor = Api::Chef::FetchAllNodes.call

        render_interactor interactor, success: :nodes, error: :error
      end

      def show
        interactor = Api::Chef::FetchNode.call(id: params[:id])

        render_interactor interactor, success: :node, error: :error
      end

      def update
        interactor = Api::Chef::OrganiseNodeUpdate.call(
          id: params[:id],
          attributes: update_params
        )

        render_interactor interactor, success: :node, error: :error
      end

      private

      def update_params
        params.require('node').permit(
          :ssh_auth_method, :ssh_key_path, :ssh_password, :ssh_port,
          :ssh_username, :cluster_id
        )
      end
    end
  end
end

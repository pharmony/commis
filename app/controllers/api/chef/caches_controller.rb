# frozen_string_literal: true

module Api
  module Chef
    class CachesController < ApplicationController
      def destroy
        job = Api::Chef::ClearChefCacheJob.perform_later

        render json: job, status: :ok
      end
    end
  end
end

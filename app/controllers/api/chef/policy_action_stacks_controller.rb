# frozen_string_literal: true

module Api
  module Chef
    #
    # This controller is used in order to get the PolicyActionStack from a given
    # ChefRepository and ChefPolicy,
    # then the `Api::PolicyActionStacksController` is used to update it.
    #
    class PolicyActionStacksController < ApplicationController
      def show
        interactor = Api::Chef::FetchPolicyActionStack.call(
          policy_id: params[:policy_id],
          repository_id: params[:repository_id]
        )

        render_interactor interactor,
                          success: :policy_action_stack,
                          error: :error
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class RepositoriesController < ApplicationController
      def index
        interactor = Api::Chef::FetchAllRepositories.call

        render_interactor interactor, success: :repositories, error: :error
      end

      def show
        interactor = Api::Chef::FetchRepository.call(id: params[:id])

        render_interactor interactor, success: :repository, error: :error
      end

      def create
        if ChefRepository.where(url: params[:git_url]).first
          render json: {
            error: 'Repository already exists'
          }, status: :unprocessable_entity
        else
          job = Api::Chef::RepositoryImportJob.perform_later(params[:git_url])
          render json: job, status: :ok
        end
      end

      def destroy
        interactor = Api::Chef::OrganiseRepositoryDestroy.call(
          repository_id: params[:id]
        )

        render_interactor interactor, error: :error
      end

      def pull
        job = Api::Chef::RepositoryPullJob.perform_later(params[:id])

        render json: job, status: :ok
      end
    end
  end
end

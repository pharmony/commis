# frozen_string_literal: true

module Api
  module Chef
    class PolicyGroupsController < ApplicationController
      def index
        interactor = Api::Chef::FetchAllPolicyGroups.call

        render_interactor interactor, success: :policy_groups, error: :error
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class PoliciesController < ApplicationController
      def index
        interactor = Api::Chef::FetchAllPolicies.call(
          repository_id: params[:repository_id]
        )

        render_interactor interactor, success: :policies, error: :error
      end
    end
  end
end

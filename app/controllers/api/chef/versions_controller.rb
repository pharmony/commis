# frozen_string_literal: true

module Api
  module Chef
    class VersionsController < ApplicationController
      def index
        interactor = Api::Chef::FetchAllVersions.call

        render_interactor interactor, success: :versions, error: :error
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    class NodeActionsController < ApplicationController
      def index
        interactor = Api::Chef::FetchNodeActionStacks.call(
          node_id: params[:node_id]
        )

        render_interactor interactor, success: :node_action_stacks,
                                      error: :error
      end

      def show
        interactor = Api::Chef::OrganiseFetchingNodeActionStack.call(
          node_id: params[:node_id],
          node_action_stack_id: params[:id]
        )

        render_interactor interactor, success: :node_action_stack, error: :error
      end

      def create
        job = Api::Chef::NodeActionCreationJob.perform_later(
          action_name: params[:name],
          node_id: params[:node_id],
          pull_chef_repo: params[:pull_chef_repo],
          repository_id: params[:chef_repository_id]
        )

        render json: job, status: :ok
      end
    end
  end
end

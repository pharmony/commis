# frozen_string_literal: true

module Api
  class ClustersController < ApplicationController
    def index
      interactor = Api::FetchAllClusters.call

      render_interactor interactor, success: :clusters, error: :error
    end

    def create
      interactor = Api::OrganiseClusterCreation.call(
        attributes: create_params
      )

      render_interactor interactor, success: :cluster, error: :error
    end

    def update
      interactor = Api::OrganiseClusterUpdate.call(
        id: params[:id],
        attributes: create_params
      )

      render_interactor interactor, success: :cluster, error: :error
    end

    def destroy
      interactor = Api::OrganiseClusterDeletion.call(
        id: params[:id]
      )

      render_interactor interactor, error: :error
    end

    private

    def create_params
      params.require('cluster').permit(:name, :country_code)
    end
  end
end

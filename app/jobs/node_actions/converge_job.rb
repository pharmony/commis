# frozen_string_literal: true

require_relative 'errors'

# NodeActions::ConvergeJob
module NodeActions
  #
  # ConvergeJob is an ActiveJob which runs the `knife zero converge` action on a
  # given node and a given policy.
  #
  class ConvergeJob < ApplicationJob
    include ChefLogStreamer

    queue_as :default

    # Displayed name of this job in the Commis UI
    PRINTABLE_NAME = 'Node converge'

    def printable_name
      PRINTABLE_NAME
    end

    def perform(node_action_stack_id)
      node_action_stack = NodeActionStack.find(node_action_stack_id)

      return if cancelled?(provider_job_id)

      mark_node_action_stack_as_running(node_action_stack)

      # ChefLogStreamer
      create_log_streamer_job_for!(node_action_stack)

      node_action_stack.node_actions.each do |node_action|
        break if cancelled?(provider_job_id)

        node_action.pull_chef_repo && pull_chef_repo(node_action)

        break if cancelled?(provider_job_id)

        next if run_action(node_action)

        mark_node_action_stack_as_failure(node_action_stack)
        break
      end

      return if node_action_stack.failed?

      mark_node_action_stack_as_success(node_action_stack)
    end

    private

    def error_object?(message_or_error)
      message_or_error.respond_to?(:message)
    end

    def handle_failure_for(node_action, error)
      unless node_action.failed?
        node_action.mark_as_failed
        node_action.save!
      end

      mark_node_action_stack_as_failure(node_action.node_action_stack)

      log_failure_reason(node_action, error)
    end

    def initialise_converge_interactor_for(node_action)
      NodeActions::OrganiseNodeConvergence.call(
        repository: node_action.chef_repository,
        # Required in order to check if the job is cancelled or not
        provider_job_id: provider_job_id,
        node_action: node_action,
        node: node_action.chef_node,
        policy: node_action.chef_policy,
        policy_group: node_action.chef_policy_group ||
                      node_action.chef_policy.try(:chef_policy_group)
      )
    end

    def log_failure_reason(node_action, message_or_error)
      ui = ChefDK::UI.new(node_action)
      ui.msg('')
      message = message_from(message_or_error)
      ui.msg("=> Finished on error: #{message}")

      return unless error_object?(message_or_error)

      return unless Rails.env.development?

      ui.msg("=> Error backtrace: #{message_or_error.backtrace}")
    end

    def mark_node_action_stack_as_failure(node_action_stack)
      return if node_action_stack.failed?

      node_action_stack.mark_as_failed
      node_action_stack.save!
    end

    def mark_node_action_stack_as_running(node_action_stack)
      node_action_stack.mark_as_running
      node_action_stack.save!
    end

    def mark_node_action_stack_as_success(node_action_stack)
      node_action_stack.mark_as_success
      node_action_stack.save!
    end

    def message_from(message_or_error)
      return message_or_error.message if error_object?(message_or_error)

      message_or_error
    end

    def pull_chef_repo(node_action)
      interactor = Api::Chef::OrganiseRepositoryPull.call(
        node_action: node_action,
        repository_id: node_action.chef_repository_id
      )

      return if interactor.success?

      handle_failure_for(node_action, interactor.error)

      false
    end

    def run_action(node_action)
      interactor = initialise_converge_interactor_for(node_action)

      return true if interactor.success?

      handle_failure_for(node_action, interactor.error)

      false
    rescue Exception => error
      Rails.logger.error 'ERROR: An error occured while running the ' \
                         "NodeAction with ID #{node_action.id}: " \
                         "#{error.message}"
      Rails.logger.error error.backtrace

      handle_failure_for(node_action, error)

      nil
    end
  end
end

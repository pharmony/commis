# frozen_string_literal: true

# NodeActions::BootstrapJob
module NodeActions
  #
  # BootstrapJob is an ActiveJob which runs the `knife zero bootstrap` action
  # in order to bring a new node to the chef repository.
  #
  class BootstrapJob < ApplicationJob
    include ChefLogStreamer

    queue_as :default

    # Displayed name of this job in the Commis UI
    PRINTABLE_NAME = 'Node bootstrap'

    def printable_name
      PRINTABLE_NAME
    end

    def perform(node_action_stack_id)
      node_action_stack = NodeActionStack.find(node_action_stack_id)

      return if cancelled?(provider_job_id)

      mark_node_action_stack_as_running(node_action_stack)

      # ChefLogStreamer
      create_log_streamer_job_for!(node_action_stack)

      run_actions_from_stack(node_action_stack)

      return if node_action_stack.failed?

      mark_node_action_stack_as_success(node_action_stack)
    end

    private

    def error_object?(message_or_error)
      message_or_error.respond_to?(:message)
    end

    def handle_exception(node_action, error)
      unless node_action.failed?
        node_action.mark_as_failed
        node_action.save!
      end

      log_failure_reason(node_action, error)
    end

    def handle_failure_for(interactor, node_action)
      node_action.mark_as_failed
      node_action.save!

      log_failure_reason(node_action, interactor.error)
    end

    def initialise_action_interactor_for(node_action)
      if %w[bootstrap converge].include?(node_action.name)
        initialise_and_call_action_interactor_for(node_action)
      elsif node_action.name == 'hook'
        initialise_and_call_hook_interactor_for(node_action)
      end
    end

    def initialise_and_call_action_interactor_for(node_action)
      NodeActions::OrganisePolicyActionStackStepsExecution.call(
        node_action: node_action,
        # Required by the interactors executing the bootstrap or converge action
        policy: node_action.chef_policy,
        policy_group: node_action.chef_policy_group,
        # Required by the OrganisePolicyActionStackStepsExecution organiser
        policy_action_stack_id: node_action.policy_action_stack_id,
        # Required in order to check if the job is cancelled or not
        provider_job_id: provider_job_id,
        # Required by the interactors executing the bootstrap or converge action
        repository: node_action.chef_repository
      )
    end

    def initialise_and_call_hook_interactor_for(node_action)
      NodeActions::OrganiseNodeActionHookExecution.call(
        node_action: node_action
      )
    end

    def log_failure_reason(node_action, message_or_error)
      ui = ChefDK::UI.new(node_action)
      ui.msg('')
      message = message_from(message_or_error)
      ui.msg("=> Finished on error: #{message}")

      return unless error_object?(message_or_error)

      return unless Rails.env.development?

      ui.msg("=> Error backtrace: #{message_or_error.backtrace}")
    end

    def mark_node_action_stack_as_failure(node_action_stack)
      node_action_stack.mark_as_failed
      node_action_stack.save!
    rescue RethinkDB::ReqlRuntimeError
      retry
    end

    def mark_node_action_stack_as_running(node_action_stack)
      node_action_stack.mark_as_running
      node_action_stack.save!
    rescue RethinkDB::ReqlRuntimeError
      retry
    end

    def mark_node_action_stack_as_success(node_action_stack)
      node_action_stack.mark_as_success
      node_action_stack.save!
    rescue RethinkDB::ReqlRuntimeError
      retry
    end

    def message_from(message_or_error)
      return message_or_error.message if error_object?(message_or_error)

      message_or_error
    end

    def run_action(node_action)
      interactor = initialise_action_interactor_for(node_action)

      return true if interactor.success?

      handle_failure_for(interactor, node_action)
    rescue Exception => error
      Rails.logger.error 'ERROR: An error occured while running the ' \
                         "NodeAction with ID #{node_action.id}: " \
                         "#{error.message}"
      Rails.logger.error error.backtrace

      handle_exception(node_action, error)

      nil
    end

    def run_actions_from_stack(node_action_stack)
      node_action_stack.node_actions.each do |node_action|
        break if cancelled?(provider_job_id)

        node_action.pull_chef_repo && pull_chef_repo(node_action)

        break if cancelled?(provider_job_id)

        next if run_action(node_action)

        mark_node_action_stack_as_failure(node_action_stack)
        break
      end
    rescue RethinkDB::ReqlRuntimeError
      retry
    end
  end
end

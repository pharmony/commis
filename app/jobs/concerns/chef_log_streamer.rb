module ChefLogStreamer
  extend ActiveSupport::Concern

  class ChefLogStreamerNotReachableError < StandardError; end

  included do
    include SidekiqWorkerWaiter
  end

  #
  # The log streamer job is a Sidekiq Job that runs in parallel with this job
  # and receives the chef logs and stream them depending on the throughput.
  #
  def create_log_streamer_job_for!(node_action_stack)
    job = ChefLogStreamerJob.perform_later("stream_job_#{node_action_stack.id}",
                                           provider_job_id)

    wait_worker_to_start(job)

    Rails.logger.debug ' 📣️ Publishing a welcome message to ' \
                       "stream_job_#{node_action_stack.id} ..."

    Sidekiq.redis do |conn|
      conn.publish "stream_job_#{node_action_stack.id}",
                   { cmd: 'welcome' }.to_json
    end
  end
end

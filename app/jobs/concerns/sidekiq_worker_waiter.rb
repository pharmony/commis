module SidekiqWorkerWaiter
  extend ActiveSupport::Concern

  def worker_processing?(job)
    job_id = job_id_from(job)
    workers = Sidekiq::Workers.new
    worker = workers.detect do |_, _, work|
      work['payload']['jid'] == job_id
    end

    worker.present?
  end

  def wait_worker_to_start(job)
    Rails.logger.debug " ✋️ Waiting the Sidekiq job #{job.class.name} " \
                       "with JID #{job.provider_job_id} to run ..."

    job_not_started = true
    timeout = 30

    while job_not_started
      processing = worker_processing?(job)

      Rails.logger.debug "#{processing ? 'A' : 'No'} worker processing the " \
                         "job #{job.class.name} with ID " \
                         "#{job.provider_job_id} found."

      job_not_started = processing == false

      if job_not_started
        sleep 1
        timeout -= 1
      end

      next unless timeout <= 0

      Rails.logger.error "Timeout while waiting job #{job.class.name} " \
                         "with ID #{job.provider_job_id} to run!"
      raise ChefLogStreamerNotReachableError
    end

    Rails.logger.debug " 👍️ Job #{job.class.name} with ID " \
                       "#{job.provider_job_id} is running."
  end

  private

  def job_id_from(job)
    case job
    when ActiveJob::Base
      job.provider_job_id
    when Hash
      job_hash = job.stringify_keys
      job_hash['provider_job_id']
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Creates a NodeActionStack with as many NodeAction as the PolicyActionStack
    # has (custom steps and frozen steps) and triggers its execution.
    #
    class NodeActionCreationJob < ApplicationJob
      queue_as :default

      # Displayed name of this job in the Commis UI
      PRINTABLE_NAME = 'Node Action Creation'

      def printable_name
        PRINTABLE_NAME
      end

      def perform(attributes)
        interactor = Api::Chef::OrganiseNodeActionCreation.call(
          attributes.merge!(provider_job_id: provider_job_id)
        )

        raise interactor.error if interactor.failure?
      rescue StandardError => error
        # TODO : Broadcast to the UI the failure of creating the bootstrap
        #        action.
        raise
      end
    end
  end
end

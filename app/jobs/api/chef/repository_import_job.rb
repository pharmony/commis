# frozen_string_literal: true

module Api
  module Chef
    #
    # Import a chef repoistory from a given git URL
    #
    class RepositoryImportJob < ApplicationJob
      queue_as :default

      # Displayed name of this job in the Commis UI
      PRINTABLE_NAME = 'Repository import'

      def printable_name
        PRINTABLE_NAME
      end

      def perform(git_url)
        interactor = Api::Chef::OrganiseRepositoryImport.call(
          git_url: git_url
        )

        raise interactor.error if interactor.failure?
      rescue StandardError => error
        try_to_mark_repo_import_as_failed_for(git_url)
        raise error
      end

      private

      def try_to_mark_repo_import_as_failed_for(git_url)
        repository = ChefRepository.where(url: git_url).first

        return unless repository

        repository.import_failure
        repository.save!

        # Notify the UI about the import error
        CommisChannel::Repositories.broadcast_add(
          repository.attributes
        )
      end
    end
  end
end

# frozen_string_literal: true

module Api
  module Chef
    #
    # Pull a chef repoistory
    #
    class RepositoryPullJob < ApplicationJob
      queue_as :default

      # Displayed name of this job in the Commis UI
      PRINTABLE_NAME = 'Repository pull'

      def printable_name
        PRINTABLE_NAME
      end

      def perform(repository_id)
        interactor = Api::Chef::OrganiseRepositoryPull.call(
          repository_id: repository_id
        )

        raise interactor.error if interactor.failure?
      rescue StandardError => error
        try_to_mark_repo_pull_as_failed_for(repository_id)
        raise error
      end

      private

      def try_to_mark_repo_pull_as_failed_for(repository_id)
        repository = ChefRepository.find?(repository_id)

        return unless repository

        unless repository.aasm(:pull_state).current_state == :pull_failed
          repository.pull_failure
          repository.save!
        end

        # Notify the UI about the import error
        CommisChannel::Repositories.broadcast_add(
          repository.attributes
        )
      end
    end
  end
end

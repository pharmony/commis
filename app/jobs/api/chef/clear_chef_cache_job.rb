# frozen_string_literal: true

module Api
  module Chef
    #
    # Delete the Chef cache folder on the worker container.
    # Running the same from the Rails app would delete the folder from the
    # wrong container.
    #
    class ClearChefCacheJob < ApplicationJob
      queue_as :default

      # Displayed name of this job in the Commis UI
      PRINTABLE_NAME = 'Chef Cache Clear'

      def printable_name
        PRINTABLE_NAME
      end

      def perform
        interactor = Api::Chef::OrganiseCacheClearing.call

        raise interactor.error if interactor.failure?
      end
    end
  end
end

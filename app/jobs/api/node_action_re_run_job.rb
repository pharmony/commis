# frozen_string_literal: true

module Api
  #
  # Run again the given NodeAction.
  #
  class NodeActionReRunJob < ApplicationJob
    queue_as :default

    # Displayed name of this job in the Commis UI
    PRINTABLE_NAME = 'Node Action ReRun'

    def printable_name
      PRINTABLE_NAME
    end

    def perform(attributes)
      interactor = Api::OrganiseNodeActionReRun.call(
        attributes.merge!(provider_job_id: provider_job_id)
      )

      return if interactor.success?

      node_action = NodeAction.find(attributes['node_action_id'])
      handle_exception(node_action, interactor.error)
    end

    private

    def error_object?(message_or_error)
      message_or_error.respond_to?(:message)
    end

    def handle_exception(node_action, error)
      unless node_action.failed?
        node_action.mark_as_failed
        node_action.save!
      end

      log_failure_reason(node_action, error)
    end

    def log_failure_reason(node_action, message_or_error)
      ui = ChefDK::UI.new(node_action)
      ui.msg('')
      message = message_from(message_or_error)
      ui.msg("=> Finished on error: #{message}")

      return unless error_object?(message_or_error)

      return unless Rails.env.development?

      ui.msg("=> Error backtrace: #{message_or_error.backtrace}")
    end

    def message_from(message_or_error)
      return message_or_error.message if error_object?(message_or_error)

      message_or_error
    end
  end
end

# frozen_string_literal: true

#
# Asynchronously persists a given log message for a given NodeAction object
#
class PersistLogJob < ApplicationJob
  queue_as :default

  def perform(node_action_id, message, type, timestamp, log_msg_id)
    NodeActions::LogPersistence::PersistLogMessage.call(
      log_msg_id: log_msg_id,
      message: message,
      node_action_id: node_action_id,
      timestamp: timestamp,
      type: type
    )
  rescue NoBrainer::Error::DocumentNotPersisted
    retry
  end
end

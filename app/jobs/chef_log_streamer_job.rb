# frozen_string_literal: true

#
# This job listen to a Redis pub/sub channel in order to receive the Chef logs
# and stream them.
#
# It calculates the throughput and from then broadcast the log messages
# in realtime or in a delayed manner.
#
class ChefLogStreamerJob < ApplicationJob
  include SidekiqWorkerWaiter

  queue_as :default

  # When receiving more log messages than the defined value in the
  # REALTIME_BROADCASTING_LIMIT constant per seconds
  REALTIME_BROADCASTING_LIMIT = 10

  # Delay before to broadcast the delays log messages.
  DELAYED_BROADCASTING = 8 # seconds

  def perform(channel, parent_provider_job_id)
    create_and_wait_for_the_opertor_job!(channel, parent_provider_job_id)

    Rails.logger.debug " 📡️ Subscribing to the #{channel} queue ..."

    initialise_variables!

    Sidekiq.redis do |conn|
      conn.subscribe(channel) do |on|
        on.message do |_, msg|
          Rails.logger.debug " 📡️ Received message: #{msg.inspect}"

          @payload = JSON.parse(msg)

          if heatbeat?
            reset_throughput_and_stream_delayed_logs!
          elsif worker_must_stop?
            conn.unsubscribe(channel)
          else
            stream_message
          end
        end
      end
    end
  end

  private

  def create_and_wait_for_the_opertor_job!(channel, parent_provider_job_id)
    job = ChefLogStreamerOperatorJob.perform_later(channel,
                                                   provider_job_id,
                                                   parent_provider_job_id)

    wait_worker_to_start(job)
  end

  def initialise_variables!
    @realtime = true
    @throughput = 0

    @log_store = []
    @delay_before_broadcasting_delayed_logs = DELAYED_BROADCASTING
  end

  def heatbeat?
    @payload['cmd'] == 'heartbeat'
  end

  def reset_throughput!
    Rails.logger.debug 'Reseting the throughput!'
    @throughput = 0
  end

  def reset_throughput_and_stream_delayed_logs!
    #
    # The heartbeat is sent each seconds, but in the case the client is lagging
    # a lot, sending the logs each seconds will not help more, therefore we are
    # adding the definied delay in DELAYED_BROADCASTING before broadcasting
    # stored logs.
    #
    if @realtime
      # As soon as coming back to realtime, reseting the variable
      @delay_before_broadcasting_delayed_logs = DELAYED_BROADCASTING
    else
      @delay_before_broadcasting_delayed_logs -= 1
      if @delay_before_broadcasting_delayed_logs.zero?
        stream_delayed_chef_logs_if_any!

        @delay_before_broadcasting_delayed_logs = DELAYED_BROADCASTING
      end
    end

    reset_throughput!
  end

  def stream_delayed_chef_logs_if_any!
    return if @log_store.size.zero?

    Rails.logger.debug " 🔊️ Broadcasting #{@log_store.size} logs ..."

    payloads = @log_store.dup

    @log_store = []

    stream_to_clients!(payloads)
  end

  def stream_message
    update_throughput_and_realtime!

    stream_or_store_log_message
  end

  def stream_or_store_log_message
    # Adds a timestamp in milliseconds to the payload use to sort log lines
    @payload['timestamp'] = (Time.now.to_f * 1000.0).to_i

    if @realtime
      Rails.logger.debug ' 🔉️ Broadcasting log to the application in ' \
                         'realtime ...'
      stream_to_clients!([@payload])
    else
      Rails.logger.debug ' 💾️ Storing log before broadcasting ...'
      @log_store << @payload
    end
  end

  def stream_to_clients!(payloads)
    # Broadcast to the UI through ActionCable
    CommisChannel::NodeActionLogs.broadcast_log(payloads)
  end

  def update_throughput_and_realtime!
    Rails.logger.debug 'Increasing the throughput ...'
    @throughput += 1

    @realtime = @throughput < REALTIME_BROADCASTING_LIMIT

    Rails.logger.debug " 📈️ Current throughput is #{@throughput}. " \
                       "@realtime: #{@realtime.inspect}"
  end

  def worker_must_stop?
    @payload['cmd'] == 'quit'
  end
end

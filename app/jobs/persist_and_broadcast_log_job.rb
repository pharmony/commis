# frozen_string_literal: true

#
# Asynchronously persists and broadcasts a given log message for a given
# NodeAction object
#
class PersistAndBroadcastLogJob < ApplicationJob
  queue_as :default

  def perform(node_action_stack_id, node_action_id, message, type, timestamp, log_msg_id)
    redis_message = {
      node_action_stack_id: node_action_stack_id,
      node_action_id: node_action_id,
      message: message,
      type: type
    }

    Rails.logger.debug ' 📣️ Publishing log to ' \
                       "stream_job_#{node_action_stack_id} ..."

    Sidekiq.redis do |conn|
      conn.publish "stream_job_#{node_action_stack_id}", redis_message.to_json
    end

    # Triggers the job to persist the log in the DB
    PersistLogJob.perform_later(node_action_id, message, type, timestamp,
                                log_msg_id)
  end
end

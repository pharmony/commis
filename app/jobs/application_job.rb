class ApplicationJob < ActiveJob::Base
  def cancelled?(jid)
    Sidekiq.redis { |redis| redis.exists("cancelled-#{jid}") }
  end

  def self.cancel!(jid)
    Sidekiq.redis { |redis| redis.setex("cancelled-#{jid}", 86_400, 1) }
  end
end

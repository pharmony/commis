# frozen_string_literal: true

#
# Fetches available Chef versions and update the database
#
class UpdateAvailableChefVersionsJob < ApplicationJob
  queue_as :default

  def perform
    interactor = Api::Chef::OrganiseFetchingAvailableChefVersions.call

    return if interactor.success?

    Rails.logger.error 'ERROR: Unable to update ChefVersion table: ' \
                       "#{interactor.error}"
  end
end

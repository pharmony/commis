# frozen_string_literal: true

#
# This job manages the ChefLogStreamer one by sending:
#
# - Heartbeat: so that the ChefLogStreamerJob can send delayed logs
# - Quit command: when the parent task (BootstrapJob or ConvergeJob) ends
#
class ChefLogStreamerOperatorJob < ApplicationJob
  include SidekiqWorkerWaiter

  queue_as :default

  # Displayed name of this job in the Commis UI
  PRINTABLE_NAME = 'Chef Log Streamer Operator'

  def printable_name
    PRINTABLE_NAME
  end

  def perform(channel, streamer_job_id, streamer_parent_job_id)
    @channel = channel

    should_run = true
    # Sidekiq Worker API is not realtime (5 seconds delay), therefore we wont
    # consider the job down immediatelly.
    streamer_job_timeout = 10
    streamer_parent_job_timeout = 10

    while should_run
      # 1. Checks if the Bootstrap/Converge job are still running and if the
      #    ChefLogStreamerJob is still running too.
      streamer_running = worker_processing?(provider_job_id: streamer_job_id)
      streamer_parent_running = worker_processing?(
        provider_job_id: streamer_parent_job_id
      )

      # 2. Stops this job when the Bootstrap/Converge job has stopped or
      #    when the chef log stream job has stopped.
      if streamer_running == false
        streamer_job_timeout -= 1

        if streamer_job_timeout.zero?
          Rails.logger.debug ' 😵️ ChefLogStreamerJob is not running'

          💣 && should_run = false
        else
          Rails.logger.debug ' ⚠️  ChefLogStreamerJob is not running. ' \
                             "#{streamer_job_timeout} seconds before the " \
                             'timeout.'
        end
      end

      if streamer_parent_running == false
        streamer_parent_job_timeout -= 1

        if streamer_parent_job_timeout.zero?
          Rails.logger.debug ' 😵️ ChefLogStreamerJob PARENT is not running'

          💣 && should_run = false
        else
          Rails.logger.debug ' ⚠️  ChefLogStreamerJob parent is not running. ' \
                             "#{streamer_parent_job_timeout} seconds before " \
                             'the timeout.'
        end
      end

      # 3. Sends heartbeat
      💓 if streamer_running

      # 4. Waits a second
      sleep 1
    end
  end

  private

  def 💓
    Rails.logger.debug " ❤️  Sending the 'heartbeat' command to the Chef Log " \
                       'streamer job ...'

    send_command 'heartbeat'
  end

  def 💣
    Rails.logger.debug " 💥️ Sending the 'quit' command to the Chef Log " \
                       'streamer job ...'

    send_command 'quit'
  end

  def send_command(command)
    Sidekiq.redis { |conn| conn.publish @channel, { cmd: command }.to_json }
  end
end

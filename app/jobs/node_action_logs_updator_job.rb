# frozen_string_literal: true

#
# Runs periodically and collect the NodeActionTemporaryLogs belonging to the
# same NodeAction and then updates the NodeAction logs with all the logs and
# clear the NodeActionTemporaryLogs objects.
#
# This avoids using locks with `update` which leads to deadlocks when there are
# a lot of logs to be treated, especially on bootstrapping a node.
#
class NodeActionLogsUpdatorJob < ApplicationJob
  class RedoJobError < StandardError; end

  queue_as :default

  # Amount of logs treated for each execution in order to limit the memory usage
  TEMPORARY_LOGS_QUERY_LIMIT = 100

  # Amount of minimum logs to be present to iterate another time
  REDO_MIN_LIMIT = 10

  def perform
    node_action_temporary_logs = NodeActionTemporaryLogs.first

    return unless node_action_temporary_logs

    logs = NodeActionTemporaryLogs.where(
      node_action_id: node_action_temporary_logs.node_action_id
    ).limit(TEMPORARY_LOGS_QUERY_LIMIT).to_a

    return if logs.empty?

    node_action = NodeAction.find!(node_action_temporary_logs.node_action_id)

    node_action.logs |= logs.map(&:log)
    node_action.save!

    NodeActionTemporaryLogs.where(:id.in => logs.map(&:id)).delete_all

    raise RedoJobError if NodeActionTemporaryLogs.count >= REDO_MIN_LIMIT
  rescue RedoJobError
    retry
  end
end

class CommisChannel < ApplicationCable::Channel
  NAME = 'commis_channel'.freeze

  def subscribed
    stream_from NAME
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  # Get triggered by the UI in order to get the jobs stats
  # Stats are pushed via ActionCable
  def job_stats(params)
    interactor = Api::FetchJobsStats.call(include_jobs: params[:include_jobs])
    ActionCable.server.broadcast(NAME, { stats: interactor.stats })
  end

  def self.broadcast(message)
    ActionCable.server.broadcast(NAME, message)
  end
end

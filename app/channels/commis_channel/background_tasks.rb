# frozen_string_literal: true

class CommisChannel
  #
  # An ActionCable message formatter for BackgroundTasks.
  # See the job reducer (app/javascript/src/jobs/reducers.js)
  #
  class BackgroundTasks
    class << self
      def broadcast_add(job_data)
        CommisChannel.broadcast(
          background_tasks: {
            action: :add,
            job: job_data
          }
        )
      end

      def broadcast_update(job_data = { jid: '', status: '', data: {} })
        CommisChannel.broadcast(
          background_tasks: {
            action: :update,
            job: {
              id: job_data[:jid],
              status: job_data[:status]
            }.merge(job_data[:data] || {})
          }
        )
      end

      def broadcast_delete(jid)
        CommisChannel.broadcast(
          background_tasks: {
            action: :delete,
            job: {
              id: jid
            }
          }
        )
      end
    end
  end
end

# frozen_string_literal: true

class CommisChannel
  #
  # An ActionCable message formatter for NodeActions that streams the AASM
  # updates to the UI.
  #
  # CommisChannel::NodeActionState
  #
  class NodeActionState
    class << self
      #
      # Broadcast the NodeAction state updates to the UI
      #
      def broadcast(node_action_stack_id, node_action_id, new_state)
        CommisChannel.broadcast(
          node_action_state: {
            node_action_stack_id: node_action_stack_id,
            node_action_id: node_action_id,
            state: new_state
          }
        )
      end
    end
  end
end

# frozen_string_literal: true

class CommisChannel
  #
  # An ActionCable message formatter for NodeActionStacks that streams the AASM
  # updates to the UI.
  #
  # CommisChannel::NodeActionStackState
  #
  class NodeActionStackState
    class << self
      #
      # Broadcast the NodeAction state updates to the UI
      #
      def broadcast(node_action_stack_id, new_state, provider_job_id = nil)
        state = {
          node_action_stack_id: node_action_stack_id,
          state: new_state
        }

        state.merge!(provider_job_id: provider_job_id) if provider_job_id

        CommisChannel.broadcast(node_action_stack_state: state)
      end
    end
  end
end

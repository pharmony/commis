# frozen_string_literal: true

class CommisChannel
  #
  # An ActionCable message formatter for ChefRepository.
  #
  class Repositories
    class << self
      def broadcast_add(data)
        CommisChannel.broadcast(
          repositories: {
            action: :add,
            repository: data
          }
        )
      end

      def broadcast_update(data)
        CommisChannel.broadcast(
          repositories: {
            action: :update,
            repository: data
          }
        )
      end
    end
  end
end

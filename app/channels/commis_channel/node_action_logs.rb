# frozen_string_literal: true

class CommisChannel
  #
  # An ActionCable message formatter for NodeActions that streams the Chef logs
  # to the UI.
  #
  # CommisChannel::NodeActionLogs
  #
  class NodeActionLogs
    class << self
      #
      # Broadcast a log message to the UI.
      #
      def broadcast_log(payloads)
        CommisChannel.broadcast(node_action_logs: payloads)
      end
    end
  end
end

import React from 'react'
import { connect } from 'react-redux'
import {
  ListGroup,
  ListGroupItem
} from 'reactstrap'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

import AlertMessage from '../components/AlertMessage'
import PageLoader from '../components/PageLoader'

class RepositoryPolicies extends React.Component {
  customiseBootstrapButtonClassFor = policy => (
    `btn-${policy.customised ? 'success' : 'secondary'}`
  )

  hooksButtonClassFor = policy => (
    `btn-${policy.hooked ? 'success' : 'secondary'}`
  )

  render = () => {
    const {
      fetchingPolicies,
      policies,
      policiesFetched,
      policiesFetchFailed,
      repository
    } = this.props

    let repositoryPolicies = []

    if (policiesFetched) {
      const policyIds = repository.chef_policies.map(chefPolicy => chefPolicy.id)
      repositoryPolicies = policies.filter(
        policy => policyIds.includes(policy.id)
      )
    }

    return (
      <React.Fragment>
        <p className="h4 mb-2">Policies</p>
        <div className="d-flex flex-column">
          {fetchingPolicies && (
            <PageLoader message="Loading policies ..." />
          )}

          {policiesFetchFailed && (
            <AlertMessage>
              Something prevented to fetch the policies from
              the backend server. Please retry.
            </AlertMessage>
          )}

          {policiesFetched && repositoryPolicies.length === 0 && (
            <AlertMessage color="info">
              This Chef repository has no policies.
            </AlertMessage>
          )}

          {policiesFetched && repositoryPolicies.length > 0 && (
            <ListGroup>
              {repositoryPolicies.map(policy => (
                <ListGroupItem key={uuidv1()}>
                  <div className={`d-flex flex-column flex-md-row
                                   justify-content-between`}>
                    <div className="d-flex align-items-center">
                      {policy.name}
                    </div>
                    <div className="d-flex">
                      <NavLink
                        className={`btn ${this.customiseBootstrapButtonClassFor(policy)} mr-3`}
                        to={`/repositories/${repository.id}/policies/${policy.name}/actions/bootstrap`}
                      >
                        Customise Bootstrap
                      </NavLink>
                      <NavLink
                        className={`btn ${this.hooksButtonClassFor(policy)}`}
                        to={`/repositories/${repository.id}/policies/${policy.name}/hooks`}
                      >
                        Hooks
                      </NavLink>
                    </div>
                  </div>
                </ListGroupItem>
              ))}
            </ListGroup>
          )}
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  policies: {
    fetchAllFailed: policiesFetchFailed,
    fetching: fetchingPolicies,
    fetched: policiesFetched,
    items: policies
  }
}) => ({
  fetchingPolicies,
  policies,
  policiesFetchFailed,
  policiesFetched
})

RepositoryPolicies.propTypes = {
  fetchingPolicies: PropTypes.bool.isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired
  })),
  policiesFetched: PropTypes.bool.isRequired,
  policiesFetchFailed: PropTypes.bool.isRequired,
  repository: PropTypes.shape({
    chef_policies: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    })).isRequired,
    id: PropTypes.string.isRequired
  }).isRequired
}

RepositoryPolicies.defaultProps = {
  policies: []
}

export default connect(mapStateToProps)(RepositoryPolicies)

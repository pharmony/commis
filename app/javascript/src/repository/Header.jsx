import React from 'react'
import {
  Button,
  Jumbotron,
  Media
} from 'reactstrap'
import confirmModal from 'reactstrap-confirm'
import { connect } from 'react-redux'
import moment from 'moment'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

import GitLogoHelper from '../helpers/GitLogoHelper'
import repositoriesActions from '../repositories/actions'

class RepositoryHeader extends React.Component {
  deleteRepository = async ({ target: { id } } = {}) => {
    const { dispatch } = this.props

    const confirmRepoDelete = await confirmModal({
      title: 'Delete this repository?',
      message: (
        <span>
          Are you sure you want to delete this repository?
          This action is not reversible.
        </span>
      ),
      confirmColor: 'danger',
      confirmText: 'Destroy'
    })

    if (confirmRepoDelete) dispatch(repositoriesActions.destroy(id))
  }

  pullRepository = ({ target: { id } }) => {
    const { dispatch } = this.props

    if (!id) return

    dispatch(repositoriesActions.pull(id))
  }

  isImportingOrPulling = () => {
    const { repository } = this.props

    if (repository.import_state === 'importing') return true

    if (repository.pull_state === 'pulling') return true

    return false
  }

  render = () => {
    const {
      repository: {
        created_at: createdAt,
        id,
        name,
        url
      }
    } = this.props

    return (
      <Jumbotron className="d-flex flex-md-row flex-column py-3 mb-1">
        <div className="d-flex flex-row flex-fill">
          <div className="d-flex">
            <Media
              className="img-lg"
              object
              src={GitLogoHelper.fromUrl(url)}
            />
          </div>
          <div className="d-flex flex-column">
            <div className="d-flex h4">
              {name}
            </div>
            <div className="d-flex">{url}</div>
            <small>
              {`Imported ${moment(createdAt).fromNow()}`}
            </small>
          </div>
        </div>
        <div className="d-flex flex-row align-items-center mb-sm-2">
          <NavLink
            className="btn btn-primary mr-2"
            disabled={this.isImportingOrPulling()}
            to={`/nodes/new?repository_id=${id}`}
          >
            Bootstrap node
          </NavLink>
          <Button
            className="mr-2"
            color="success"
            disabled={this.isImportingOrPulling()}
            id={id}
            onClick={event => this.pullRepository(event)}
          >
            Pull
          </Button>
          <Button
            color="danger"
            disabled={this.isImportingOrPulling()}
            id={id}
            onClick={event => this.deleteRepository(event)}
          >
            Delete
          </Button>
        </div>
      </Jumbotron>
    )
  }
}

RepositoryHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  repository: PropTypes.shape({
    created_at: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    import_state: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    pull_state: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }).isRequired
}

export default connect()(RepositoryHeader)

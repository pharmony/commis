import repositoryConstants from './constants'
import repositoryService from './services'

const fetch = (repositoryId) => {
  const request = () => ({ type: repositoryConstants.FETCH_REQUEST })
  const success = repository => ({
    repository,
    type: repositoryConstants.FETCH_SUCCESS
  })
  const failure = error => ({
    error,
    type: repositoryConstants.FETCH_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    repositoryService.fetch(repositoryId)
      .then(repository => dispatch(success(repository)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  fetch
}

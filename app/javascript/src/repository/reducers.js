import dotProp from 'dot-prop-immutable'

import repositoryConstants from './constants'

const initialState = {
  fetchFailed: false,
  fetching: false,
  item: null
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case repositoryConstants.FETCH_REQUEST:
      newState = dotProp.set(state, 'fetchFailed', false)
      return dotProp.set(newState, 'fetching', true)
    case repositoryConstants.FETCH_SUCCESS:
      newState = dotProp.set(state, 'fetchFailed', false)
      newState = dotProp.set(newState, 'fetching', false)
      return dotProp.set(newState, 'item', action.repository)
    case repositoryConstants.FETCH_FAILURE:
      newState = dotProp.set(state, 'fetchFailed', true)
      newState = dotProp.set(newState, 'fetching', false)
      return dotProp.set(newState, 'item', null)
    default:
      return state
  }
}

import ApiUtils from '../helpers/ApiUtils'
import RepositoriesApi from '../repositories/api'

const fetch = id => (
  new RepositoriesApi()
    .get({ id })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetch
}

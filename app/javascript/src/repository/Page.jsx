import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import BackButton from '../components/BackButton'
import FourOFour from '../app/FourOFour'
import PageLoader from '../components/PageLoader'
import policiesActions from '../policies/actions'
import repositoryActions from './actions'
import RepositoryHeader from './Header'
import RepositoryPolicies from './Policies'

class ChefRepositoryPage extends React.Component {
  componentDidMount = () => {
    const {
      dispatch,
      match: { params: { id } }
    } = this.props

    dispatch(repositoryActions.fetch(id))
    dispatch(policiesActions.fetchAll())
  }

  render = () => {
    const {
      fetchFailed,
      fetching,
      location,
      repository
    } = this.props

    if (fetching) return (<PageLoader message="Loading repository ..." />)

    if (fetchFailed || !repository) return (<FourOFour location={location} />)

    return (
      <div className="d-flex flex-column">
        <BackButton to="/repositories" />
        <RepositoryHeader repository={repository} />
        <hr className="w-100" />
        <RepositoryPolicies repository={repository} />
      </div>
    )
  }
}

const mapStateToProps = ({
  policies: {
    fetchAllFailed: policiesFetchFailed,
    fetching: fetchingPolicies,
    fetched: policiesFetched,
    items: policies
  },
  repository: { fetchFailed, fetching, item: repository }
}) => ({
  fetchFailed,
  fetching,
  fetchingPolicies,
  policies,
  policiesFetchFailed,
  policiesFetched,
  repository
})

ChefRepositoryPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetchFailed: PropTypes.bool.isRequired,
  fetching: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired
  }).isRequired,
  repository: PropTypes.shape({
    id: PropTypes.string.isRequired
  })
}

ChefRepositoryPage.defaultProps = {
  repository: null
}

export default connect(mapStateToProps)(ChefRepositoryPage)

import React from 'react'
import {
  Badge,
  Card,
  CardBody,
  CardHeader
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import SshAuthenticationMethodSelector from '../../../components/SshAuthenticationMethodSelector'

class NodeAuthenticationMethodHook extends React.Component {
  findHookFrom = ({ hooks }) => {
    const authMethodHook = hooks.find(hook => hook._type === this.hookName())
    return authMethodHook
  }

  hookName = () => 'PolicyHooks::NodeAuthorisationHook'

  render() {
    const { handleInputChange, policy } = this.props

    const policyHook = this.findHookFrom(policy)

    return (
      <Card>
        <CardHeader>
          <div>
            Node authentication method
          </div>
          <small>
            Updates the node&apos;s authentication method after having be
            bootstrapped or converged with the&nbsp;
            <Badge className="fs-100" color="primary">{policy.name}</Badge>
            &nbsp;policy.
          </small>
        </CardHeader>
        <CardBody>
          <SshAuthenticationMethodSelector
            handleInputChange={(name, value) => handleInputChange(name, value)}
            selectedAuthMethod={policyHook && policyHook.auth_method}
            sshCredential={policyHook && policyHook.ssh_credential}
            sshUsername={policyHook && policyHook.auth_user}
          />
        </CardBody>
      </Card>
    )
  }
}

NodeAuthenticationMethodHook.propTypes = {
  handleInputChange: PropTypes.func.isRequired,
  policy: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired
}

export default connect()(NodeAuthenticationMethodHook)

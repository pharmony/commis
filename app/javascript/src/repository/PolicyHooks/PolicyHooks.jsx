import React from 'react'
import { Badge, Button, Form } from 'reactstrap'
import { connect } from 'react-redux'
import dotProp from 'dot-prop-immutable'
import PropTypes from 'prop-types'

import AlertMessage from '../../components/AlertMessage'
import PageLoader from '../../components/PageLoader'
import policiesActions from '../../policies/actions'
import NodeAuthenticationMethodHook from './hooks/NodeAuthenticationMethodHook'

class PolicyHooks extends React.Component {
  componentDidMount() {
    const { dispatch, policy } = this.props

    /*
    ** One request to fetch all hooks of the given policy, the policies reducer
    *  saves hooks on the policy object, then each hook components retrieves
    *  their data as needed.
    */
    dispatch(policiesActions.fetchHooksFor(policy.id))
  }

  /*
  ** Determines if the hooks needs to be added or updated.
  */
  addOUpdateNodeAuthenticationMethodHook = (policy, hooks) => {
    const authMethodHookIndex = policy.hooks.findIndex(
      item => item._type === 'PolicyHooks::NodeAuthorisationHook'
    )

    if (authMethodHookIndex > -1) {
      return this.updateNodeAuthenticationMethodHook(
        policy,
        hooks,
        authMethodHookIndex
      )
    }

    return this.addNodeAuthenticationMethodHook(policy, hooks)
  }

  /*
  ** Add a new PolicyHooks::NodeAuthorisationHook hook and return the updated
  *  `hooks`.
  */
  addNodeAuthenticationMethodHook = (policy, hooks) => {
    const {
      selectedAuthMethod,
      sshUsername
    } = this.state

    return dotProp.set(hooks, hooks.length, {
      _type: 'PolicyHooks::NodeAuthorisationHook',
      auth_method: selectedAuthMethod,
      auth_user: sshUsername,
      ssh_credential: this.hookCredential()
    })
  }

  fetchingErrorComponent = ({ name }) => (
    <AlertMessage>
      <React.Fragment>
        Something went wrong while fetching the&nbsp;
        <Badge className="fs-100">
          {name}
        </Badge>
        &nbsp;policy hooks. Please try again.
      </React.Fragment>
    </AlertMessage>
  )

  findPolicyHooksFor = (policy) => {
    const { fetchingHooks, policies } = this.props

    if (fetchingHooks) return null

    const policyWithHooks = policies.find(item => item.id === policy.id)

    if (policyWithHooks && policyWithHooks.hooks === undefined) return null

    return policyWithHooks
  }

  handleInputChange = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  hookCredential = () => {
    const {
      selectedAuthMethod,
      sshKeyPath,
      sshPassword
    } = this.state

    switch (selectedAuthMethod) {
      case 'credential':
        return ''
      case 'password':
        return sshPassword
      case 'ssh_keys':
        return sshKeyPath
      default:
        return ''
    }
  }

  saveHooks = (event) => {
    const { dispatch, policies, policy } = this.props

    event.preventDefault()

    const policyWithHooks = policies.find(item => item.id === policy.id)

    let newHooks = policyWithHooks.hooks

    newHooks = this.addOUpdateNodeAuthenticationMethodHook(
      policyWithHooks,
      newHooks
    )

    dispatch(policiesActions.persistHooks(policy.id, newHooks))
  }

  /*
  ** Updates the PolicyHooks::NodeAuthorisationHook hook values and return
  *  the updated `hooks`.
  */
  updateNodeAuthenticationMethodHook = (policy, hooks, index) => {
    const {
      selectedAuthMethod,
      sshUsername
    } = this.state

    let newHooks = dotProp.set(
      hooks,
      `${index}.auth_method`,
      selectedAuthMethod
    )

    newHooks = dotProp.set(newHooks, `${index}.auth_user`, sshUsername)

    return dotProp.set(
      newHooks,
      `${index}.ssh_credential`,
      this.hookCredential()
    )
  }

  render() {
    const {
      fetchingHooksFailed,
      policy
    } = this.props

    if (fetchingHooksFailed) return this.fetchingErrorComponent(policy)

    const policyWithHooks = this.findPolicyHooksFor(policy)

    if (!policyWithHooks) {
      return <PageLoader message="Fetching policy hooks ..." />
    }

    return (
      <Form>
        <NodeAuthenticationMethodHook
          handleInputChange={
            (name, value) => this.handleInputChange(name, value)
          }
          policy={policyWithHooks}
        />

        <Button
          className="float-right mt-4"
          color="primary"
          onClick={event => this.saveHooks(event)}
        >
          Save
        </Button>
      </Form>
    )
  }
}

const mapStateToProps = ({
  policies: { fetchingHooks, fetchingHooksFailed, items: policies }
}) => ({
  fetchingHooks,
  fetchingHooksFailed,
  policies
})

PolicyHooks.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetchingHooks: PropTypes.bool.isRequired,
  fetchingHooksFailed: PropTypes.bool.isRequired,
  policy: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    hooks: PropTypes.arrayOf(PropTypes.object)
  })).isRequired
}

export default connect(mapStateToProps)(PolicyHooks)

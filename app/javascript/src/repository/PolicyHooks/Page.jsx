import React from 'react'
import { Badge } from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import BackButton from '../../components/BackButton'
import FourOFour from '../../app/FourOFour'
import PageLoader from '../../components/PageLoader'
import policiesActions from '../../policies/actions'
import PolicyHooks from './PolicyHooks'
import repositoryActions from '../actions'
import RepositoryHeader from '../Header'

class ChefRepositoryPolicyHooksPage extends React.Component {
  state = {
    policy: null,
    policyFound: true,
    policyName: null
  }

  componentDidMount = () => {
    const {
      dispatch,
      match: { params: { id, policy_name: policyName } }
    } = this.props

    dispatch(repositoryActions.fetch(id))
    dispatch(policiesActions.fetchAll())

    this.setState({
      policyName
    })
  }

  componentDidUpdate = () => {
    const { fetching, fetchingPolicies, policies } = this.props

    const {
      policy: statePolicy,
      policyName
    } = this.state

    if (fetching || fetchingPolicies) return

    if (statePolicy === null && policies && policies.length > 0) {
      const currentPolicy = policies.find(policy => policy.name === policyName)

      this.setCurrentPolicy(currentPolicy)
    }
  }

  setCurrentPolicy = (policy) => {
    this.setState({ policy, policyFound: policy !== undefined })
  }

  render = () => {
    const { availableAuthMethods, location, repository } = this.props
    const { policy, policyFound } = this.state

    if (!repository) return <PageLoader message="Loading repository ..." />

    if (!policyFound) return (<FourOFour location={location} />)
    if (!policy) return <PageLoader message="Loading policy ..." />

    return (
      <React.Fragment>
        <BackButton to={`/repositories/${repository.id}`} />
        <RepositoryHeader repository={repository} />
        <hr />
        <p className="h3">Hooks</p>
        <div>
          This page allows you to define hooks when a node is bootstrapped or
          converged with the&nbsp;
          <Badge className="fs-100">{policy.name}</Badge>
          &nbsp;policy.
        </div>
        <hr />
        <PolicyHooks
          sshAuthMethods={availableAuthMethods}
          policy={policy}
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  app: { configuration: { ssh: { availableAuthMethods } } },
  policies: {
    fetchAllFailed: policiesFetchFailed,
    fetching: fetchingPolicies,
    fetched: policiesFetched,
    items: policies
  },
  repository: { fetchFailed, fetching, item: repository }
}) => ({
  availableAuthMethods,
  fetchFailed,
  fetching,
  fetchingPolicies,
  policies,
  policiesFetchFailed,
  policiesFetched,
  repository
})

ChefRepositoryPolicyHooksPage.propTypes = {
  availableAuthMethods: PropTypes.arrayOf(PropTypes.string).isRequired,
  dispatch: PropTypes.func.isRequired,
  fetching: PropTypes.bool.isRequired,
  fetchingPolicies: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
      policy_name: PropTypes.string.isRequired
    }).isRequired
  }).isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired
  })),
  repository: PropTypes.shape({
    id: PropTypes.string.isRequired
  })
}

ChefRepositoryPolicyHooksPage.defaultProps = {
  policies: [],
  repository: null
}

export default connect(mapStateToProps)(ChefRepositoryPolicyHooksPage)

import React from 'react'
import { Badge, Button } from 'reactstrap'
import PropTypes from 'prop-types'

const BeginStep = ({ addStepHandler }) => (
  <Badge
    className="d-flex justify-content-center position-relative list-group-top"
    color="primary"
  >
    <div className="h6 mb-0 py-2">Begin</div>
    <Button
      className="badge-float-right"
      color="info"
      onClick={addStepHandler}
    >
      Add step
    </Button>
  </Badge>
)

BeginStep.propTypes = {
  addStepHandler: PropTypes.func.isRequired
}

export default BeginStep

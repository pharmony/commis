import React from 'react'
import arrayMove from 'array-move'
import {
  Button,
  ListGroup
} from 'reactstrap'
import { connect } from 'react-redux'
import dotProp from 'dot-prop-immutable'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import AlertMessage from '../../components/AlertMessage'
import BeginStep from './BeginStep'
import EndStep from './EndStep'
import PageLoader from '../../components/PageLoader'
import policyActionsActions from '../../policy-actions/actions'
import StepModal from './StepModal'
import SortableBootstrapStep from './SortableBootstrapStep'
import SortableConvergeStep from './SortableConvergeStep'
import SortableListGroup from '../../components/SortableListGroup'
import UnsortableBootstrapStep from './UnsortableBootstrapStep'

class PolicyActionCustomiser extends React.Component {
  state = {
    policyActionStackId: null,
    sortableSteps: null,
    stepModalStepId: undefined, // `null` on create, a string on edit
    stepModalVisible: false,
    unsortableSteps: null
  }

  componentDidMount = () => {
    const { dispatch, policy, repository } = this.props

    dispatch(policyActionsActions.fetchStackFor(repository.id, policy.id))
  }

  componentDidUpdate = () => {
    const {
      fetchingStack,
      fetchingStackFailed,
      policyActionStackId,
      stackFetched,
      steps,
      stepsFetched
    } = this.props

    const {
      policyActionStackId: policyActionStackIdFromState,
      sortableSteps,
      unsortableSteps
    } = this.state

    if (fetchingStack || fetchingStackFailed) return

    if (policyActionStackIdFromState && sortableSteps === null
        && unsortableSteps === null && stepsFetched && steps
    ) {
      this.setsSortableAndUnsortableSteps()
    }

    if (policyActionStackIdFromState === null && stackFetched
        && policyActionStackId
    ) {
      this.fetchStepsFrom(policyActionStackId)
    }
  }

  onDeleteStep = ({ target: { id } }) => {
    const { sortableSteps } = this.state

    if (!id) return

    const index = sortableSteps.findIndex(step => step.id === id)

    if (index !== -1) {
      sortableSteps.splice(index, 1)

      this.setState({
        sortableSteps
      })
    }
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState(({ sortableSteps }) => ({
      sortableSteps: arrayMove(sortableSteps, oldIndex, newIndex)
    }))
  }

  buildBootstrapStepFrom = step => ({
    buttons: (
      <div className="d-flex">
        <Button
          className="mr-2"
          color="danger"
          id={step.id}
          onClick={event => this.onDeleteStep(event)}
        >
          Remove
        </Button>
        <Button
          color="success"
          id={step.id}
          onClick={event => this.toggleStepModal(event)}
        >
          Edit
        </Button>
      </div>
    ),
    content: <SortableBootstrapStep policyName={step.policyName} />
  })

  buildConvergeStepFrom = step => ({
    buttons: (
      <div className="d-flex">
        <Button
          className="mr-2"
          color="danger"
          id={step.id}
          onClick={event => this.onDeleteStep(event)}
        >
          Remove
        </Button>
        <Button
          color="success"
          id={step.id}
          onClick={event => this.toggleStepModal(event)}
        >
          Edit
        </Button>
      </div>
    ),
    content: (
      <SortableConvergeStep query={step.query} />
    )
  })

  buildSortableStepFrom = (step) => {
    if (step.type === 'bootstrap') {
      return this.buildBootstrapStepFrom(step)
    }

    return this.buildConvergeStepFrom(step)
  }

  createSortableStep = (attributes) => {
    const { sortableSteps } = this.state

    this.setState({
      sortableSteps: [...sortableSteps, attributes]
    })
  }

  saveStep = (attributes) => {
    const sortableStepsUpdated = this.tryToUpdateSortableSteps(attributes)
    if (sortableStepsUpdated) return

    const unsortableStepsUpdated = this.tryToUpdateUnsortableSteps(attributes)
    if (unsortableStepsUpdated) return

    this.createSortableStep(attributes)
  }

  fetchStepsFrom = (policyActionStackId) => {
    const { dispatch } = this.props

    this.setState({
      policyActionStackId
    }, () => {
      dispatch(policyActionsActions.fetchStepsFrom(policyActionStackId))
    })
  }

  savePolicyAction = () => {
    const { dispatch } = this.props
    const { policyActionStackId, sortableSteps } = this.state

    sortableSteps.map((step, index) => {
      const newStep = step
      newStep.position = index
      return newStep
    })

    dispatch(
      policyActionsActions.persistSteps(policyActionStackId, sortableSteps)
    )
  }

  setsSortableAndUnsortableSteps = () => {
    const { steps } = this.props

    this.setState({
      sortableSteps: steps.sortable,
      unsortableSteps: steps.unsortable.map((unsortableStep) => {
        const step = unsortableStep
        step.frozen = true
        return step
      })
    })
  }

  toggleStepModal = (event) => {
    const { stepModalVisible } = this.state

    // `event` is given for edit action, otherwise it's a create action
    const stepId = event ? event.target.id : null

    this.setState({
      stepModalStepId: stepId,
      stepModalVisible: !stepModalVisible
    })
  }

  tryToUpdateSortableSteps = (attributes) => {
    const { sortableSteps } = this.state

    const stepIndex = sortableSteps.findIndex(
      item => item.id === attributes.id
    )

    if (stepIndex === -1) return false

    const steps = dotProp.set(
      sortableSteps,
      stepIndex,
      attributes
    )

    this.setState({
      sortableSteps: steps
    })

    return true
  }

  tryToUpdateUnsortableSteps = (attributes) => {
    const { unsortableSteps } = this.state

    const stepIndex = unsortableSteps.findIndex(
      item => item.id === attributes.id
    )

    if (stepIndex === -1) return false

    const steps = dotProp.set(
      unsortableSteps,
      stepIndex,
      attributes
    )

    this.setState({
      unsortableSteps: steps
    })

    return true
  }

  render = () => {
    const {
      fetchingStackFailed,
      fetchingStepsFailed,
      policy
    } = this.props

    const {
      sortableSteps,
      stepModalStepId,
      stepModalVisible,
      unsortableSteps
    } = this.state


    if (fetchingStackFailed || fetchingStepsFailed) {
      return (
        <AlertMessage>
          <React.Fragment>
            Something went wrong while fetching the steps.
            {fetchingStackFailed && " (Can't find a steps container)."}
            &nbsp;Please try again.
          </React.Fragment>
        </AlertMessage>
      )
    }

    if (!sortableSteps) return <PageLoader message="Loading steps ..." />

    let step = null

    if (stepModalStepId) {
      step = [...sortableSteps, ...unsortableSteps].find(
        item => item.id === stepModalStepId
      )
    }

    return (
      <React.Fragment>
        <div className="d-flex justify-content-center mt-5">
          <ListGroup className="w-75">
            <BeginStep addStepHandler={this.toggleStepModal} />
            <SortableListGroup
              items={sortableSteps.map(sortableStep => (
                this.buildSortableStepFrom(sortableStep)
              ))}
              key={uuidv1()}
              onSortEnd={sortItems => this.onSortEnd(sortItems)}
            />

            {unsortableSteps.map(unsortableStep => (
              <UnsortableBootstrapStep
                icon="anchor"
                key={uuidv1()}
                onEditStep={event => this.toggleStepModal(event)}
                policyName={unsortableStep.policyName}
                stepId={unsortableStep.id}
              />
            ))}
            <EndStep />
          </ListGroup>
        </div>
        <div className="d-flex justify-content-end mt-5">
          <Button
            color="primary"
            onClick={() => this.savePolicyAction()}
          >
            Save customised bootstrap
          </Button>
        </div>
        {stepModalVisible && (
          <StepModal
            onSaveStep={attributes => this.saveStep(attributes)}
            policy={policy}
            step={step}
            toggle={event => this.toggleStepModal(event)}
          />
        )}
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  policyActions: {
    fetchingStack,
    fetchingStackFailed,
    fetchingSteps,
    fetchingStepsFailed,
    items: steps,
    policyActionStackId,
    stackFetched,
    stepsFetched
  }
}) => ({
  fetchingStack,
  fetchingStackFailed,
  fetchingSteps,
  fetchingStepsFailed,
  policyActionStackId,
  stackFetched,
  steps,
  stepsFetched
})

PolicyActionCustomiser.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetchingStack: PropTypes.bool.isRequired,
  fetchingStackFailed: PropTypes.bool.isRequired,
  fetchingStepsFailed: PropTypes.bool.isRequired,
  policy: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  policyActionStackId: PropTypes.string,
  repository: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired,
  stackFetched: PropTypes.bool.isRequired,
  steps: PropTypes.shape({
    sortable: PropTypes.arrayOf(PropTypes.shape({
      policyName: PropTypes.string,
      query: PropTypes.string
    })),
    unsortable: PropTypes.arrayOf(PropTypes.shape({
      policyName: PropTypes.string
    }))
  }),
  stepsFetched: PropTypes.bool.isRequired
}

PolicyActionCustomiser.defaultProps = {
  policyActionStackId: null,
  steps: null
}

export default connect(mapStateToProps)(PolicyActionCustomiser)

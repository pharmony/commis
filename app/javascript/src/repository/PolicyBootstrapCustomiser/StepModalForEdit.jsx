import React from 'react'
import {
  Input,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import ConvergeQueryDoc from './ConvergeQueryDoc'
import SortableBootstrapStep from './SortableBootstrapStep'

class StepModalForEdit extends React.Component {
  state = {
    convergeQuery: '',
    selectedPolicyName: ''
  }

  componentDidMount = () => {
    const { step } = this.props

    this.setState({
      convergeQuery: step.query,
      selectedPolicyName: step.policyName
    })
  }

  handleQueryChange = (event) => {
    const { handleQueryChange, step } = this.props

    if (step.frozen) return

    const { target: { value } } = event

    this.setState({
      convergeQuery: value
    })

    handleQueryChange(event)
  }

  handleSelectedPolicyChange = (event) => {
    const { handleSelectedPolicyChange, step } = this.props

    if (step.frozen) return

    const { target: { value } } = event

    this.setState({
      selectedPolicyName: value
    })

    handleSelectedPolicyChange(event)
  }

  render = () => {
    const { policies, step } = this.props
    const { convergeQuery, selectedPolicyName } = this.state

    if (step.type === 'bootstrap') {
      if (step.frozen) {
        return <SortableBootstrapStep policyName={selectedPolicyName} />
      }

      return (
        <React.Fragment>
          <Label for="bootstrap_step_policy_name">
            Select the policy to be bootstrapped
          </Label>
          <Input
            id="bootstrap_step_policy_name"
            name="bootstrap_step[policy_name]"
            onChange={event => this.handleSelectedPolicyChange(event)}
            type="select"
            value={selectedPolicyName}
          >
            {policies.map(policy => (
              <option
                key={uuidv1()}
                value={policy.name}
              >
                {policy.name}
              </option>
            ))}
          </Input>
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        <Label for="converge_step_query">
          Enter the converge query
        </Label>
        <Input
          disabled={step.frozen}
          id="converge_step_query"
          name="converge_step[query]"
          onChange={event => this.handleQueryChange(event)}
          type="input"
          value={convergeQuery}
        />
        <ConvergeQueryDoc />
      </React.Fragment>
    )
  }
}

StepModalForEdit.propTypes = {
  handleQueryChange: PropTypes.func.isRequired,
  handleSelectedPolicyChange: PropTypes.func.isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  step: PropTypes.shape({
    frozen: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    policyName: PropTypes.string,
    type: PropTypes.string.isRequired
  }).isRequired
}

export default StepModalForEdit

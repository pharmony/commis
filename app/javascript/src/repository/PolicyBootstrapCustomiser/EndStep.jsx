import React from 'react'
import { Badge } from 'reactstrap'

const EndStep = () => (
  <Badge className="list-group-bottom" color="success">
    <div className="h6 mb-0 py-2">End</div>
  </Badge>
)

export default EndStep

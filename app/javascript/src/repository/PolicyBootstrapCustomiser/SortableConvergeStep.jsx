import React from 'react'
import { Badge } from 'reactstrap'
import PropTypes from 'prop-types'

const SortableConvergeStep = ({ query }) => (
  <React.Fragment>
    <Badge color="info">
      <div className="h6 mb-0">Converge</div>
    </Badge>
    &nbsp;the query&nbsp;
    <code>{query}</code>
    .
  </React.Fragment>
)

SortableConvergeStep.propTypes = {
  query: PropTypes.string.isRequired
}

export default SortableConvergeStep

import React from 'react'
import {
  Button,
  ListGroupItem
} from 'reactstrap'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'

import SortableBootstrapStep from './SortableBootstrapStep'

const UnsortableBootstrapStep = ({ onEditStep, policyName, stepId }) => (
  <ListGroupItem className="d-flex flex-row justify-content-between">
    <div className="d-flex">
      <div className="d-flex align-items-center mr-3">
        <Icofont icon="anchor" size="2" />
      </div>
      <div className="d-flex align-items-center">
        <SortableBootstrapStep policyName={policyName} />
      </div>
    </div>
    <div className="d-flex">
      <Button
        color="success"
        id={stepId}
        onClick={event => onEditStep(event)}
      >
        Edit
      </Button>
    </div>
  </ListGroupItem>
)

UnsortableBootstrapStep.propTypes = {
  onEditStep: PropTypes.func.isRequired,
  policyName: PropTypes.string.isRequired,
  stepId: PropTypes.string.isRequired
}

export default UnsortableBootstrapStep

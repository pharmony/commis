import React from 'react'
import { Badge } from 'reactstrap'
import PropTypes from 'prop-types'

const SortableBootstrapStep = ({ policyName }) => (
  <React.Fragment>
    <Badge color="success">
      <div className="h6 mb-0">Bootstrap</div>
    </Badge>
    &nbsp;node with the&nbsp;
    <Badge>
      <div className="h6 mb-0">
        <strong>{policyName}</strong>
      </div>
    </Badge>
    &nbsp;policy.
  </React.Fragment>
)

SortableBootstrapStep.propTypes = {
  policyName: PropTypes.string.isRequired
}

export default SortableBootstrapStep

import React from 'react'
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import StepModalForCreate from './StepModalForCreate'
import StepModalForEdit from './StepModalForEdit'

class StepModal extends React.Component {
  state = {
    activeTab: 'bootstrap',
    convergeQuery: '',
    selectedPolicyName: ''
  }

  componentDidMount = () => {
    const { policies, step } = this.props

    if (step) {
      this.updateSelectedPolicyName(step.policyName)
    } else {
      this.updateSelectedPolicyName(policies[0].name)
    }
  }

  handleQueryChange = ({ target: { value } }) => {
    this.setState({
      convergeQuery: value
    })
  }

  handleSelectedPolicyChange = ({ target: { value } }) => {
    this.updateSelectedPolicyName(value)
  }

  onSaveStepClicked = () => {
    const { onSaveStep, step, toggle } = this.props
    const { activeTab, convergeQuery, selectedPolicyName } = this.state

    const stepType = step ? step.type : activeTab

    const commonAttributes = {
      id: step ? step.id : uuidv1(),
      type: stepType
    }

    if (stepType === 'bootstrap') {
      commonAttributes.policyName = selectedPolicyName
    } else {
      commonAttributes.query = convergeQuery
    }

    onSaveStep(commonAttributes)

    toggle()

    this.setState({
      activeTab: 'bootstrap',
      convergeQuery: '',
      selectedPolicyName: ''
    })
  }

  toggleTab = (id) => {
    const { activeTab } = this.state

    if (id !== activeTab) {
      this.setState({
        activeTab: id
      })
    }
  }

  updateSelectedPolicyName = (name) => {
    const { selectedPolicyName } = this.state

    if (selectedPolicyName !== name) {
      this.setState({ selectedPolicyName: name })
    }
  }

  render = () => {
    const {
      policies,
      policy,
      step,
      toggle
    } = this.props

    return (
      <Modal isOpen toggle={toggle}>
        <ModalHeader toggle={toggle}>
          {step === null ? 'Add a new step' : 'Edit a step'}
        </ModalHeader>
        <ModalBody>
          {step ? (
            <StepModalForEdit
              handleQueryChange={this.handleQueryChange}
              handleSelectedPolicyChange={this.handleSelectedPolicyChange}
              policies={policies}
              policy={policy}
              step={step}
            />
          ) : (
            <StepModalForCreate
              handleQueryChange={this.handleQueryChange}
              handleSelectedPolicyChange={this.handleSelectedPolicyChange}
              policies={policies}
              policy={policy}
              handleToggleTab={this.toggleTab}
            />
          )}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Cancel</Button>
          {' '}
          <Button
            color="success"
            onClick={this.onSaveStepClicked}
          >
            {step === null ? 'Add' : 'Save'}
          </Button>
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = ({ policies: { items: policies } }) => ({ policies })

StepModal.propTypes = {
  onSaveStep: PropTypes.func.isRequired,
  policy: PropTypes.object.isRequired,
  policies: PropTypes.arrayOf(PropTypes.object).isRequired,
  step: PropTypes.object,
  toggle: PropTypes.func.isRequired
}

StepModal.defaultProps = {
  step: null
}

export default connect(mapStateToProps)(StepModal)

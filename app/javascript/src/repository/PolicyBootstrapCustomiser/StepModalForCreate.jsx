import React from 'react'
import {
  Badge,
  Input,
  Label,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from 'reactstrap'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import ConvergeQueryDoc from './ConvergeQueryDoc'

class StepModalForCreate extends React.Component {
  state = {
    activeTab: 'bootstrap',
    convergeQuery: '',
    selectedPolicyName: ''
  }

  handleQueryChange = (event) => {
    const { handleQueryChange } = this.props

    const { target: { value } } = event

    this.setState({
      convergeQuery: value
    })

    handleQueryChange(event)
  }

  handleSelectedPolicyChange = (event) => {
    const { handleSelectedPolicyChange } = this.props

    const { target: { value } } = event

    this.setState({
      selectedPolicyName: value
    })

    handleSelectedPolicyChange(event)
  }

  toggleTab = (id) => {
    const { handleToggleTab } = this.props
    const { activeTab } = this.state

    if (id !== activeTab) {
      handleToggleTab(id)

      this.setState({
        activeTab: id
      })
    }
  }

  render = () => {
    const { policies, policy } = this.props
    const { activeTab, convergeQuery, selectedPolicyName } = this.state

    return (
      <React.Fragment>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === 'bootstrap' })}
              onClick={() => { this.toggleTab('bootstrap') }}
            >
              Bootstrap
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === 'converge' })}
              onClick={() => { this.toggleTab('converge') }}
            >
              Converge
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="bootstrap">
            <Label for="bootstrap_step_policy_name" className="mt-2">
              Select a policy to be added to the policy&apos;s custom workflow:
            </Label>
            <Input
              id="bootstrap_step_policy_name"
              name="bootstrap_step[policy_name]"
              onChange={event => this.handleSelectedPolicyChange(event)}
              type="select"
              value={selectedPolicyName}
            >
              {policies.map(item => (
                <option
                  key={uuidv1()}
                  value={item.name}
                >
                  {item.name}
                </option>
              ))}
            </Input>
          </TabPane>
          <TabPane tabId="converge">
            <Label for="converge_step_query" className="mt-2">
              Enter the query to be used in order to select the nodes to be
              converged during the&nbsp;
              <Badge>
                <div className="h6 mb-0">
                  <strong>{policy.name}</strong>
                </div>
              </Badge>
              &apos;s custom workflow:
            </Label>
            <Input
              id="converge_step_query"
              name="converge_step[query]"
              onChange={event => this.handleQueryChange(event)}
              type="input"
              value={convergeQuery}
            />
            <ConvergeQueryDoc />
          </TabPane>
        </TabContent>
      </React.Fragment>
    )
  }
}

StepModalForCreate.propTypes = {
  handleQueryChange: PropTypes.func.isRequired,
  handleSelectedPolicyChange: PropTypes.func.isRequired,
  handleToggleTab: PropTypes.func.isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  policy: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired
}

export default StepModalForCreate

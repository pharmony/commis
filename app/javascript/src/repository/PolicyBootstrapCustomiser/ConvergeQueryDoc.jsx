import React from 'react'

const ConvergeQueryDoc = () => (
  <small>
    Learn more about query at&nbsp;
    <a
      href="https://docs.chef.io/chef_search/"
      rel="noopener noreferrer"
      target="_blank"
    >
      Chef Search documentation
    </a>
    .
    <hr />
    <p className="h5">Examples</p>
    <p>
      Converge the nodes having the policy named&nbsp;
      <code>web</code>
      :
      <br />
      <code className="extended">
        policy_name: web
      </code>
    </p>
    <p>
      Converge all the nodes from the&nbsp;
      <code>staging</code>
      &nbsp;policy group:
      <br />
      <code className="extended">
        policy_group: staging
      </code>
    </p>
    <p>
      Converge all the nodes having the policy named&nbsp;
      <code>db</code>
      &nbsp;and from the&nbsp;
      <code>prod</code>
      &nbsp;policy group:
      <br />
      <code className="extended">
        policy_name: db AND policy_group: prod
      </code>
    </p>
    <hr />
    <p className="h5">Commis&apos; specials</p>
    <strong>
      Node&apos;s policy group
    </strong>
    <p>
      You can use the special placeholder&nbsp;
      <code>
        #node-group#
      </code>
      &nbsp;in your query, and it will be replaced by the node&apos;s
      policy group before to execute the query like that:
      <br />
      <code className="extended">
        policy_name: db AND policy_group: #node-group#
      </code>
    </p>
  </small>
)

export default ConvergeQueryDoc

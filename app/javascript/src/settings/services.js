import ApiUtils from '../helpers/ApiUtils'
import { ChefVersionsApi, SettingsApi } from './apis'

const fetch = () => (
  new SettingsApi().show()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchAvailableChefVersions = () => (
  new ChefVersionsApi().all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const save = data => (
  new SettingsApi().update(null, data)
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetch,
  fetchAvailableChefVersions,
  save
}

import React from 'react'
import { Button } from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import PageLoader from '../components/PageLoader'
import TemporaryMessage from '../components/TemporaryMessage'

class SettingPanelSaveBar extends React.Component {
  state = {
    clicked: false
  }

  onButtonClick = (event) => {
    const { onPersist } = this.props

    this.setState({
      clicked: true
    }, onPersist(event))
  }

  unclick = () => {
    this.setState({
      clicked: false
    })
  }

  render() {
    const {
      updating,
      updateFailed,
      updateSucceed
    } = this.props

    const { clicked } = this.state

    return (
      <div className="clearfix">
        {updating && clicked && (
          <PageLoader
            className="float-left"
            message="Saving settings ..."
          />
        )}

        {updateFailed && clicked && (
          <TemporaryMessage
            color="danger"
            onDismiss={this.unclick}
          >
            Something went wrong while saving settings.
          </TemporaryMessage>
        )}

        {updateSucceed && clicked && (
          <TemporaryMessage
            color="success"
            onDismiss={this.unclick}
          >
            The settings has been successfully updated.
          </TemporaryMessage>
        )}

        <Button
          className="float-right"
          color="success"
          disabled={updating}
          onClick={event => this.onButtonClick(event)}
          type="submit"
        >
          Save
        </Button>
      </div>
    )
  }
}

const mapStateToProps = ({
  settings: {
    updateFailed,
    updateSucceed,
    updating
  }
}) => ({
  updateFailed,
  updateSucceed,
  updating
})

SettingPanelSaveBar.propTypes = {
  onPersist: PropTypes.func.isRequired,
  updateFailed: PropTypes.bool.isRequired,
  updateSucceed: PropTypes.bool.isRequired,
  updating: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(SettingPanelSaveBar)

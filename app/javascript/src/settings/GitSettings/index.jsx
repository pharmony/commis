import React from 'react'
import { connect } from 'react-redux'
import {
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'

import AppSshPublicSshKeyWithRegenerate from './AppSshPublicSshKeyWithRegenerate'
import SettingPanelSaveBar from '../SettingPanelSaveBar'
import settingsActions from '../actions'

class GitSettings extends React.Component {
  state = {
    gitEmail: '',
    gitUsername: '',
    userEdit: false // True when user is filling fields
  }

  componentDidMount = () => {
    const { gitEmail, gitUsername } = this.props

    this.setState({
      gitEmail,
      gitUsername
    })
  }

  componentDidUpdate = () => {
    const {
      gitEmail: propsGitEmail,
      gitUsername: propsGitUsername
    } = this.props
    const { gitEmail, gitUsername, userEdit } = this.state

    if (userEdit) return

    if (gitEmail !== propsGitEmail || gitUsername !== propsGitUsername) {
      this.setState({
        gitEmail: propsGitEmail,
        gitUsername: propsGitUsername
      })
    }
  }

  onUsernameChanged = ({ target: { value } }) => {
    this.setState({
      gitUsername: value,
      userEdit: true
    })
  }

  onEmailChanged = ({ target: { value } }) => {
    this.setState({
      gitEmail: value,
      userEdit: true
    })
  }

  persistGitForm = (event) => {
    const { dispatch } = this.props
    const { gitEmail, gitUsername } = this.state

    event.preventDefault()

    dispatch(settingsActions.save({
      git: {
        gitEmail,
        gitUsername
      }
    }))

    this.setState({
      userEdit: false
    })
  }

  render = () => {
    const { updating } = this.props

    const {
      gitEmail,
      gitUsername
    } = this.state

    return (
      <Card>
        <CardHeader>
          Git
        </CardHeader>
        <CardBody>
          <div>
            The Git settings allows Commis to access your Chef repositories.
          </div>
          <small>
            In order to get Commis importing and updating your chef repository
            you have to:

            <ol>
              <li>
                Create a user in your Software Configuration Management (SCM)
                platform
              </li>
              <li>Add the Commis&apos; SSH public key to this user</li>
              <li>
                Fill in the username and email (Email is the most important one
                and must match the user&apos; email address otherwise commits will
                not be recognised)
              </li>
            </ol>
          </small>
          <hr />
          <Form>
            <FormGroup>
              <AppSshPublicSshKeyWithRegenerate />
            </FormGroup>
            <div className="d-flex flex-row">
              <div className="d-flex flex-fill mr-1">
                <FormGroup className="flex-fill">
                  <Label for="ssh_username">
                    Git&nbsp;
                    <code>user.name</code>
                  </Label>
                  <Input
                    disabled={updating}
                    id="ssh_username"
                    name="ssh[username]"
                    onChange={event => this.onUsernameChanged(event)}
                    placeholder="Enter the user name to be used by Git"
                    type="text"
                    value={gitUsername || ''}
                  />
                </FormGroup>
              </div>
              <div className="d-flex flex-fill ml-1">
                <FormGroup className="flex-fill">
                  <Label for="ssh_email">
                    Git&nbsp;
                    <code>user.email</code>
                  </Label>
                  <Input
                    disabled={updating}
                    id="ssh_email"
                    name="ssh[email]"
                    onChange={event => this.onEmailChanged(event)}
                    placeholder="Enter the user email to be used by Git"
                    type="email"
                    value={gitEmail || ''}
                  />
                </FormGroup>
              </div>
            </div>
            <SettingPanelSaveBar
              onPersist={event => this.persistGitForm(event)}
            />
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = ({
  settings: {
    item: { git },
    updating
  }
}) => {
  if (git) {
    const { gitEmail, gitUsername } = git

    return {
      gitEmail,
      gitUsername,
      updating
    }
  }

  return {
    updating
  }
}

GitSettings.propTypes = {
  dispatch: PropTypes.func.isRequired,
  gitEmail: PropTypes.string,
  gitUsername: PropTypes.string,
  updating: PropTypes.bool.isRequired
}

GitSettings.defaultProps = {
  gitEmail: '',
  gitUsername: ''
}

export default connect(mapStateToProps)(GitSettings)

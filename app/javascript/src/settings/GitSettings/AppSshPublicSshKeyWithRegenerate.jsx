import React from 'react'
import { Button } from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AppSshPublicSshKey from '../../components/AppSshPublicSshKey'
import ConfirmSshKeyRegenerate from '../../ssh/ConfirmSshKeyRegenerate'
import PageLoader from '../../components/PageLoader'

const AppSshPublicSshKeyWithRegenerate = (props) => {
  const {
    dispatch,
    regeneratedSshKey,
    regenerating,
    regeneratingFailed
  } = props

  return (
    <div className="clearfix">
      <div>Commis&apos; public SSH key:</div>
      <AppSshPublicSshKey />
      <div>
        {regenerating && (
          <PageLoader
            className="float-left"
            message="Regenerating SSH key ..."
          />
        )}

        {regeneratingFailed && (
          <div className="float-left text-danger">
            Something went wrong while regenerating the SSH Key.
          </div>
        )}

        {regeneratedSshKey && (
          <div className="float-left text-success">
            The SSH Key has been successfully regenerated.
            Remember to update it everywhere.
          </div>
        )}

        <Button
          className="float-right"
          color="danger"
          disabled={regenerating}
          onClick={() => {
            ConfirmSshKeyRegenerate(dispatch)
          }}
        >
          Regenerate
        </Button>
      </div>
    </div>
  )
}

const mapStateToProps = ({
  app: { ssh: { regeneratedSshKey, regenerating, regeneratingFailed } }
}) => ({
  regeneratedSshKey,
  regenerating,
  regeneratingFailed
})

AppSshPublicSshKeyWithRegenerate.propTypes = {
  dispatch: PropTypes.func.isRequired,
  regeneratedSshKey: PropTypes.bool,
  regenerating: PropTypes.bool,
  regeneratingFailed: PropTypes.bool
}

AppSshPublicSshKeyWithRegenerate.defaultProps = {
  regeneratedSshKey: false,
  regenerating: false,
  regeneratingFailed: false
}

export default connect(mapStateToProps)(AppSshPublicSshKeyWithRegenerate)

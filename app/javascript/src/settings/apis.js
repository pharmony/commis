import RestfulClient from 'restful-json-api-client'
import RailsHeaders from '../helpers/RailsHeaders'

export class ChefVersionsApi extends RestfulClient {
  constructor() {
    super('/api/chef', { resource: 'versions' })
  }
}

export class SettingsApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'settings', headers: RailsHeaders })
  }
}

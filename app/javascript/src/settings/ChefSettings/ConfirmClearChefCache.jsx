import React from 'react'
import confirmModal from 'reactstrap-confirm'
import PropTypes from 'prop-types'

import chefCacheActions from '../../chef-cache/actions'

const ConfirmClearChefCache = async (dispatch) => {
  const confirm = await confirmModal({
    title: 'Clear the Chef cache?',
    message: (
      <React.Fragment>
        <p>This action is permanent and not reversible.</p>
        <p>Are you sure you want to regenerate the SSH key?</p>
      </React.Fragment>
    ),
    confirmColor: 'danger',
    confirmText: 'Clear'
  })

  if (confirm) dispatch(chefCacheActions.clear())
}

ConfirmClearChefCache.propTypes = {
  dispatch: PropTypes.func.isRequired
}

export default ConfirmClearChefCache

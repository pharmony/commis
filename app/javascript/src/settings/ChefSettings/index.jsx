import React from 'react'
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import ConfirmClearChefCache from './ConfirmClearChefCache'
import SettingPanelSaveBar from '../SettingPanelSaveBar'
import settingsActions from '../actions'

class ChefSettings extends React.Component {
  state = {
    bootstrapChefVersion: '',
    initialized: false
  }

  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(settingsActions.fetchAvailableChefVersions())
  }

  componentDidUpdate = () => {
    const { bootstraped, nodeActionBootstrapChefVersion } = this.props
    const { bootstrapChefVersion, initialized } = this.state

    if (bootstraped && initialized === false) {
      if (bootstrapChefVersion !== nodeActionBootstrapChefVersion) {
        this.updateBootstrapChefVersion(nodeActionBootstrapChefVersion)
      }
    }
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({
      [name]: value
    })
  }

  persistChefForm = (event) => {
    const { dispatch } = this.props
    const { bootstrapChefVersion } = this.state

    event.preventDefault()

    dispatch(settingsActions.save({
      nodeAction: {
        bootstrapChefVersion
      }
    }))
  }

  updateBootstrapChefVersion = (chefVersion) => {
    this.setState({
      bootstrapChefVersion: chefVersion,
      initialized: true
    })
  }

  render = () => {
    const {
      cacheDir,
      chefDkVersion,
      chefVersion,
      chefZeroVersion,
      dispatch,
      knifeZeroVersion,
      versions
    } = this.props

    const {
      bootstrapChefVersion
    } = this.state

    if (!versions) return <p>Loading versions ...</p>

    return (
      <Card>
        <CardHeader>
          Chef
        </CardHeader>
        <CardBody>
          <p className="lead">Locally</p>
          <dl className="row">
            <dt className="col-sm-2">Chef version:</dt>
            <dd className="col-sm-10">{chefVersion}</dd>

            <dt className="col-sm-2">ChefDk version:</dt>
            <dd className="col-sm-10">{chefDkVersion}</dd>

            <dt className="col-sm-2">ChefZero version:</dt>
            <dd className="col-sm-10">{chefZeroVersion}</dd>

            <dt className="col-sm-2">Knife Zero version:</dt>
            <dd className="col-sm-10">{knifeZeroVersion}</dd>
          </dl>
          <p className="lead">Remotely</p>
          <Form>
            <FormGroup>
              <Label for="knife_bootstrap_chef_version">
                Chef version for bootstrap
              </Label>
              <Input
                id="knife_bootstrap_chef_version"
                name="bootstrapChefVersion"
                onChange={event => this.handleInputChange(event)}
                type="select"
                value={bootstrapChefVersion}
              >
                <option
                  key={uuidv1()}
                  value=""
                >
                  Select a Chef version
                </option>
                {versions.map(availableChefVersion => (
                  <option
                    key={uuidv1()}
                    value={availableChefVersion.version}
                  >
                    {availableChefVersion.version}
                  </option>
                ))}
              </Input>

              <small className="text-muted">
                You can specify the chef version you&apos;d like to use in order
                to bootstrap new nodes here.
              </small>
              <SettingPanelSaveBar
                onPersist={event => this.persistChefForm(event)}
              />
            </FormGroup>
          </Form>
          <hr />
          <div className="d-flex flex-row">
            <div className="d-flex flex-column flex-fill">
              <div className="d-flex h5">Clear chef cache</div>
              <small>
                Clearing the Chef cache will delete all the files from&nbsp;
                <code>{cacheDir}</code>
                &nbsp;which will force Chef to re-download the cookbooks again.
              </small>
            </div>
            <div className="d-flex flex-column justify-content-center">
              <Button
                color="danger"
                onClick={() => ConfirmClearChefCache(dispatch)}
              >
                Clear cache
              </Button>
            </div>
          </div>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = ({
  settings: {
    bootstraped,
    item: {
      chef,
      chefDk,
      chefZero,
      knifeZero,
      nodeAction: {
        bootstrapChefVersion: nodeActionBootstrapChefVersion
      }
    },
    versions
  }
}) => {
  let cacheDir
  let chefDkVersion
  let chefVersion
  let chefZeroVersion
  let knifeZeroVersion

  if (chef) {
    const { cacheDir: chefCacheDir, version } = chef

    cacheDir = chefCacheDir
    chefVersion = version
  }

  if (chefDk) {
    const { version } = chefDk

    chefDkVersion = version
  }

  if (chefZero) {
    const { version } = chefZero

    chefZeroVersion = version
  }

  if (knifeZero) {
    const { version } = knifeZero

    knifeZeroVersion = version
  }

  return {
    bootstraped,
    cacheDir,
    chefDkVersion,
    chefVersion,
    chefZeroVersion,
    knifeZeroVersion,
    nodeActionBootstrapChefVersion,
    versions
  }
}

ChefSettings.propTypes = {
  bootstraped: PropTypes.bool.isRequired,
  cacheDir: PropTypes.string,
  chefDkVersion: PropTypes.string,
  chefVersion: PropTypes.string,
  chefZeroVersion: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  knifeZeroVersion: PropTypes.string,
  nodeActionBootstrapChefVersion: PropTypes.string,
  versions: PropTypes.arrayOf(PropTypes.shape({
    version: PropTypes.string.isRequired
  }))
}

ChefSettings.defaultProps = {
  cacheDir: '?',
  chefDkVersion: '?',
  chefVersion: '?',
  chefZeroVersion: '?',
  knifeZeroVersion: '?',
  nodeActionBootstrapChefVersion: '',
  versions: []
}

export default connect(mapStateToProps)(ChefSettings)

import React from 'react'

import ChefSettings from './ChefSettings'
import GitSettings from './GitSettings'
import NodeActionSettings from './NodeActionSettings'

export default () => (
  <React.Fragment>
    <GitSettings />
    <hr />
    <NodeActionSettings />
    <hr />
    <ChefSettings />
    <hr />
  </React.Fragment>
)

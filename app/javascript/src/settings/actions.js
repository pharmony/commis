import settingsConstants from './constants'
import settingsService from './services'

const fetch = () => {
  const request = () => ({ type: settingsConstants.SAVE_REQUEST })
  const success = setting => ({
    setting,
    type: settingsConstants.SAVE_SUCCESS
  })
  const failure = error => ({
    error,
    type: settingsConstants.SAVE_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    return settingsService.fetch()
      .then(data => dispatch(success(data)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchAvailableChefVersions = () => {
  const request = () => ({ type: settingsConstants.FETCH_AVAILABLE_CHEF_VERSIONS_REQUEST })
  const success = versions => ({
    versions,
    type: settingsConstants.FETCH_AVAILABLE_CHEF_VERSIONS_SUCCESS
  })
  const failure = error => ({
    error,
    type: settingsConstants.FETCH_AVAILABLE_CHEF_VERSIONS_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    return settingsService.fetchAvailableChefVersions()
      .then(versions => dispatch(success(versions)))
      .catch(error => dispatch(failure(error)))
  }
}

const save = (settingsData) => {
  const request = () => ({ type: settingsConstants.SAVE_REQUEST })
  const success = data => ({ data, type: settingsConstants.SAVE_SUCCESS })
  const failure = error => ({
    error,
    type: settingsConstants.SAVE_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    const finalData = {}

    if (settingsData.git) {
      finalData.git = {}

      if (settingsData.git.gitEmail) {
        finalData.git.git_email = settingsData.git.gitEmail
      }

      if (settingsData.git.gitUsername) {
        finalData.git.git_username = settingsData.git.gitUsername
      }
    }

    if (settingsData.nodeAction) {
      finalData.node_action = {}

      finalData.node_action.before_bootstrap = settingsData.nodeAction.beforeBootstrap
      finalData.node_action.bootstrap_chef_version = settingsData.nodeAction.bootstrapChefVersion
    }

    return settingsService.save(finalData)
      .then(() => dispatch(success(settingsData)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  fetch,
  fetchAvailableChefVersions,
  save
}

import React from 'react'
import { connect } from 'react-redux'
import {
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'

import SettingPanelSaveBar from '../SettingPanelSaveBar'
import settingsActions from '../actions'

class NodeActionSettings extends React.Component {
  state = {
    beforeBootstrap: ''
  }

  componentDidMount() {
    const { beforeBootstrap } = this.props

    this.setState({
      beforeBootstrap
    })
  }

  onBeforeBootstrapChanged = ({ target: { value } }) => {
    this.setState({
      beforeBootstrap: value
    })
  }

  persistNodeActionForm = (event) => {
    const { dispatch } = this.props
    const { beforeBootstrap } = this.state

    event.preventDefault()

    dispatch(settingsActions.save({
      nodeAction: {
        beforeBootstrap
      }
    }))
  }

  render() {
    const { beforeBootstrap } = this.state

    return (
      <Card>
        <CardHeader>
          Node Action
        </CardHeader>
        <CardBody>
          <div>
            This panel allows you to customise the way to run the bootstrap
            and converge node actions.
          </div>
          <hr />
          <Form>
            <FormGroup>
              <Label for="knife_before_bootstrap">Command to be executed before a node bootstrap</Label>
              <Input
                id="knife_before_bootstrap"
                name="knife[before_bootstrap]"
                onChange={event => this.onBeforeBootstrapChanged(event)}
                placeholder="Enter the command to be executed on the server before the knife bootstrap one"
                type="text"
                value={beforeBootstrap || ''}
              />
              <small className="text-muted">
                This command will be executed on new nodes,
                before Chef deployment.
              </small>
            </FormGroup>
            <SettingPanelSaveBar
              onPersist={event => this.persistNodeActionForm(event)}
            />
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = ({
  settings: { item: { nodeAction: { beforeBootstrap } } }
}) => ({ beforeBootstrap })

NodeActionSettings.propTypes = {
  beforeBootstrap: PropTypes.string,
  dispatch: PropTypes.func.isRequired
}

NodeActionSettings.defaultProps = {
  beforeBootstrap: ''
}

export default connect(mapStateToProps)(NodeActionSettings)

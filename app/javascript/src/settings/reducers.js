import dotProp from 'dot-prop-immutable'

import appConstants from '../app/constants'
import settingsConstants from './constants'

const initialState = {
  bootstraped: false,
  fetchFailed: false,
  fetchSucceed: false,
  fetching: false,
  item: null, // The setting object from the backend server
  updateFailed: false,
  updateSucceed: false,
  updating: false,
  versions: null // Chef versions from backend
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case appConstants.BOOTSTRAP_SUCCESS:
      if (action.app && action.app.settings) {
        const item = {}

        if (action.app.settings.chef) {
          item.chef = {}

          if (action.app.settings.chef.cache_dir) {
            item.chef.cacheDir = action.app.settings.chef.cache_dir
          }

          if (action.app.settings.chef.version) {
            item.chef.version = action.app.settings.chef.version
          }
        }

        if (action.app.settings.chef_dk) {
          item.chefDk = {}

          if (action.app.settings.chef_dk.version) {
            item.chefDk.version = action.app.settings.chef_dk.version
          }
        }

        if (action.app.settings.chef_zero) {
          item.chefZero = {}

          if (action.app.settings.chef_zero.version) {
            item.chefZero.version = action.app.settings.chef_zero.version
          }
        }

        if (action.app.settings.git) {
          item.git = {}

          if (action.app.settings.git.git_email) {
            item.git.gitEmail = action.app.settings.git.git_email
          }

          if (action.app.settings.git.git_username) {
            item.git.gitUsername = action.app.settings.git.git_username
          }
        }

        if (action.app.settings.knife_zero) {
          item.knifeZero = {}

          if (action.app.settings.knife_zero.version) {
            item.knifeZero.version = action.app.settings.knife_zero.version
          }
        }

        if (action.app.settings.node_action) {
          item.nodeAction = {}

          if (action.app.settings.node_action.before_bootstrap) {
            item.nodeAction.beforeBootstrap = action.app.settings.node_action.before_bootstrap
          }

          if (action.app.settings.node_action.bootstrap_chef_version) {
            item.nodeAction.bootstrapChefVersion = action.app.settings.node_action.bootstrap_chef_version
          }
        }

        newState = dotProp.set(state, 'bootstraped', true)
        return dotProp.set(newState, 'item', item)
      }
      return state
    case settingsConstants.FETCH_AVAILABLE_CHEF_VERSIONS_SUCCESS:
      return dotProp.set(state, 'versions', action.versions)
    case settingsConstants.FETCH_REQUEST:
      newState = dotProp.set(state, 'fetchFailed', false)
      newState = dotProp.set(newState, 'fetchSucceed', false)
      return dotProp.set(newState, 'fetching', true)
    case settingsConstants.FETCH_SUCCESS:
      newState = dotProp.set(state, 'fetchSucceed', true)
      newState = dotProp.set(newState, 'fetching', false)
      return dotProp.set(newState, 'item', action.setting)
    case settingsConstants.FETCH_FAILURE:
      newState = dotProp.set(state, 'fetchFailed', true)
      return dotProp.set(newState, 'fetching', false)
    case settingsConstants.SAVE_REQUEST:
      newState = dotProp.set(state, 'updateFailed', false)
      newState = dotProp.set(newState, 'updateSucceed', false)
      return dotProp.set(newState, 'updating', true)
    case settingsConstants.SAVE_SUCCESS:
      newState = dotProp.set(
        state,
        'item',
        Object.assign(state.item, action.data)
      )
      newState = dotProp.set(newState, 'updating', false)
      return dotProp.set(newState, 'updateSucceed', true)
    case settingsConstants.SAVE_FAILURE:
      newState = dotProp.set(state, 'updating', false)
      return dotProp.set(newState, 'updateFailed', true)
    default:
      return state
  }
}

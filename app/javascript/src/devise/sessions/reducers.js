import deviseConstants from '../constants'

const initialState = {
  loggingIn: false,
  loggedIn: false,
  user: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case deviseConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        loggedIn: false,
        user: null
      }
    case deviseConstants.LOGIN_SUCCESS:
      return {
        loggingIn: false,
        loggedIn: true,
        user: action.user
      }
    case deviseConstants.LOGIN_FAILURE:
      return initialState
    case deviseConstants.LOGOUT:
      return initialState
    default:
      return state
  }
}

import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  Col,
  Row
} from 'reactstrap'

import unlockActions from './actions'

class DeviseUnlocksShow extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      token: ''
    }
  }

  componentDidMount () {
    const urlParams = new URLSearchParams(this.props.location.search)
    const token = urlParams.get('unlock_token')

    this.props.dispatch(unlockActions.unlock(token))
  }

  render() {
    return (
      <React.Fragment>
        <h2>Unlocking your account ...</h2>
      </React.Fragment>
    )
  }
}

export default connect()(DeviseUnlocksShow)

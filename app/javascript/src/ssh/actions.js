import sshConstants from './constants'
import sshService from './services'

const fetchSshKey = () => {
  const request = () => ({ type: sshConstants.SSH_KEY_REQUEST })
  const success = sshKey => ({
    sshKey,
    type: sshConstants.SSH_KEY_SUCCESS
  })
  const failure = error => ({
    error,
    type: sshConstants.SSH_KEY_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    sshService.fetchSshKey()
      .then(response => dispatch(success(response.ssh_key)))
      .catch(error => dispatch(failure(error)))
  }
}

const regenerateSshKey = () => {
  const request = () => ({ type: sshConstants.REGENERATE_SSH_KEY_REQUEST })
  const success = sshKey => ({
    sshKey,
    type: sshConstants.REGENERATE_SSH_KEY_SUCCESS
  })
  const failure = error => ({
    error,
    type: sshConstants.REGENERATE_SSH_KEY_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    sshService.regenerateSshKey()
      .then(response => dispatch(success(response.ssh_key)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  fetchSshKey,
  regenerateSshKey
}

export default {
  REGENERATE_SSH_KEY_REQUEST: 'SSH_KEY_REGENERATE_REQUEST',
  REGENERATE_SSH_KEY_SUCCESS: 'SSH_KEY_REGENERATE_SUCCESS',
  REGENERATE_SSH_KEY_FAILURE: 'SSH_KEY_REGENERATE_FAILURE',

  SSH_KEY_REQUEST: 'SSH_KEY_REQUEST',
  SSH_KEY_SUCCESS: 'SSH_KEY_SUCCESS',
  SSH_KEY_FAILURE: 'SSH_KEY_FAILURE'
}

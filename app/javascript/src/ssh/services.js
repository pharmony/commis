import ApiUtils from '../helpers/ApiUtils'
import SshKeyApi from './api'

const fetchSshKey = () => (
  new SshKeyApi()
    .get()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const regenerateSshKey = () => (
  new SshKeyApi()
    .regenerate()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetchSshKey,
  regenerateSshKey
}

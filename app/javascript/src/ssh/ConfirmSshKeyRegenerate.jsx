import React from 'react'
import confirmModal from 'reactstrap-confirm'
import PropTypes from 'prop-types'

import sshActions from './actions'

const ConfirmSshKeyRegenerate = async (dispatch) => {
  const confirm = await confirmModal({
    title: "Regenerate Commis' SSH key?",
    message: (
      <React.Fragment>
        <p>
          When you regenerate the Commis&apos; SSH key, you have then to update
          it in all your SCM platforms.
        </p>
        <p>This action is permanent and not reversible.</p>
        <p>Are you sure you want to regenerate the SSH key?</p>
      </React.Fragment>
    ),
    confirmColor: 'danger',
    confirmText: 'Confirm'
  })

  if (confirm) dispatch(sshActions.regenerateSshKey())
}

ConfirmSshKeyRegenerate.propTypes = {
  dispatch: PropTypes.func.isRequired
}

export default ConfirmSshKeyRegenerate

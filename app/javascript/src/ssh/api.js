import RestfulClient from 'restful-json-api-client'
import RailsHeaders from '../helpers/RailsHeaders'

export default class SshKeyApi extends RestfulClient {
  constructor() {
    super('/api/ssh', { resource: 'key', headers: RailsHeaders })
  }

  regenerate = () => this.request('POST', { path: 'regenerate' })
}

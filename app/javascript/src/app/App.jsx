import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import alertActions from '../alerts/actions'
import AppLoading from './AppLoading'
import AppRouter from './AppRouter'
import history from '../helpers/History'

class App extends React.Component {
  constructor(props) {
    super(props)

    const { dispatch } = this.props

    // clear alert on location change
    history.listen(() => dispatch(alertActions.clear()))
  }

  render() {
    const {
      loaded,
      loading,
      loadingError
    } = this.props

    if (loaded === false || loading || loadingError) return <AppLoading />

    return <AppRouter />
  }
}

const mapStateToProps = ({ app: { configuration } }) => {
  // configuration is not garanteed to exists
  if (configuration) {
    const { loaded, loading, loading_error: loadingError } = configuration

    return {
      loaded,
      loading,
      loadingError
    }
  }

  return {}
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
  loadingError: PropTypes.bool
}

App.defaultProps = {
  loaded: false,
  loading: false,
  loadingError: null
}

export default connect(mapStateToProps)(App)

import dotProp from 'dot-prop-immutable'

import appConstants from './constants'
import sshConstants from '../ssh/constants'

const initialState = {
  configuration: {
    loaded: false,
    loading: false,
    loading_error: false,
    loading_error_message: null,
    loading_success: false,
    registrationOpened: false,
    ssh: {
      availableAuthMethods: null
    }
  },
  preferences: {
    leftMenuCollapsed: false,
    skipbootstrapWizardPresentation: false
  },
  ssh: {
    fetchtingKey: false,
    keyFetchingFailed: false,
    regeneratedSshKey: false,
    regenerating: false,
    regeneratingFailed: false,
    sshPublicKey: null
  }
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    /*
    ** Bootstrap
    */
    case appConstants.BOOTSTRAP_REQUEST:
      newState = initialState
      // preferences is persisted so keep rehydrated values
      if (state.preferences) {
        Object.keys(state.preferences).map((key) => {
          newState = dotProp.set(
            newState,
            `preferences.${key}`,
            state.preferences[key]
          )
          return newState
        })
      }
      return dotProp.set(newState, 'configuration.loading', true)
    case appConstants.BOOTSTRAP_SUCCESS:
      newState = dotProp.set(
        state,
        'configuration.registrationOpened',
        action.app.configuration.registration_opened
      )
      newState = dotProp.set(
        state,
        'configuration.ssh.availableAuthMethods',
        action.app.configuration.available_auth_methods
      )
      newState = dotProp.set(newState, 'configuration.loaded', true)
      newState = dotProp.set(newState, 'configuration.loading_success', true)
      return dotProp.set(newState, 'configuration.loading', false)
    case appConstants.BOOTSTRAP_FAILURE:
      newState = dotProp.set(state, 'configuration.registrationOpened', false)
      newState = dotProp.set(newState, 'configuration.loading_error', true)
      newState = dotProp.set(newState, 'configuration.loading_success', false)
      newState = dotProp.set(
        newState,
        'configuration.loading_error_message',
        action.error
      )
      return dotProp.set(newState, 'configuration.loading', false)

    /*
    ** Prefrences
    */
    case appConstants.PREFERENCES_UPDATE:
      newState = state

      Object.keys(action.preferences).map((key) => {
        newState = dotProp.set(
          newState,
          `preferences.${key}`,
          action.preferences[key]
        )
        return newState
      })

      return newState

    /*
    ** SSH
    */
    case sshConstants.REGENERATE_SSH_KEY_REQUEST:
      newState = dotProp.set(state, 'ssh.regenerating', true)
      newState = dotProp.set(newState, 'ssh.regeneratedSshKey', false)
      newState = dotProp.set(newState, 'ssh.regeneratingFailed', false)
      return dotProp.set(newState, 'ssh.regeneratedSshKey', false)
    case sshConstants.REGENERATE_SSH_KEY_SUCCESS:
      newState = dotProp.set(state, 'ssh.regenerating', false)
      newState = dotProp.set(newState, 'ssh.regeneratedSshKey', true)
      return dotProp.set(newState, 'ssh.sshPublicKey', action.sshKey)
    case sshConstants.REGENERATE_SSH_KEY_FAILURE:
      newState = dotProp.set(state, 'ssh.regenerating', false)
      return dotProp.set(newState, 'ssh.regeneratingFailed', true)

    case sshConstants.SSH_KEY_REQUEST:
      newState = dotProp.set(state, 'ssh.fetchtingKey', true)
      newState = dotProp.set(newState, 'ssh.keyFetchingFailed', false)
      return dotProp.set(newState, 'ssh.sshPublicKey', null)
    case sshConstants.SSH_KEY_SUCCESS:
      newState = dotProp.set(state, 'ssh.fetchtingKey', false)
      newState = dotProp.set(newState, 'ssh.keyFetchingFailed', false)
      return dotProp.set(newState, 'ssh.sshPublicKey', action.sshKey)
    case sshConstants.SSH_KEY_FAILURE:
      newState = dotProp.set(state, 'ssh.fetchtingKey', false)
      newState = dotProp.set(newState, 'ssh.keyFetchingFailed', true)
      return dotProp.set(newState, 'ssh.sshPublicKey', null)

    default:
      return state
  }
}

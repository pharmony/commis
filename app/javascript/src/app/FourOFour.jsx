import React from 'react'
import { Button } from 'reactstrap'
import PropTypes from 'prop-types'

import history from '../helpers/History'

const FourOFour = ({ location }) => (
  <div className="d-flex flex-column h-100 justify-content-center align-items-center loading-page">
    <div className="app-title">
      <h1>Commis</h1>
    </div>
    <h3>
      404 - No match for&nbsp;
      <code>{location.pathname}</code>
    </h3>
    <Button
      color="primary"
      className="mt-2"
      onClick={() => history.push('/')}
    >
      Back to the homepage
    </Button>
  </div>
)

FourOFour.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
}

export default FourOFour

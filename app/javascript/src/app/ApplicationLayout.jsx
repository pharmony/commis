import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import ActionCableConnector from '../action-cable/connector'
import BreadcrumbComponent from '../components/BreadcrumbComponent'
import FlashMessages from '../alerts/FlashMessages'
import LeftMenu from './LeftMenu'

const ApplicationLayout = (props) => {
  const { children, leftMenuCollapsed } = props

  return (
    <React.Fragment>
      <div className="d-flex content align-items-stretch">
        <LeftMenu />
        <div className={`d-flex flex-column content-inner ${leftMenuCollapsed ? 'expanded' : ''}`}>
          <ActionCableConnector />
          <BreadcrumbComponent {...props} />
          {children}
          <div className="position-absolute w-98 mt-2">
            <FlashMessages />
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = ({
  app: { preferences: { leftMenuCollapsed } }
}) => ({ leftMenuCollapsed })

ApplicationLayout.propTypes = {
  children: PropTypes.element.isRequired,
  leftMenuCollapsed: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(ApplicationLayout)

/*
** Application Router
*
*  You can read more about React router here:
*  https://reacttraining.com/react-router/web/guides/philosophy
*/
import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  Router,
  Route,
  Switch
} from 'react-router-dom'
import uuidv1 from 'uuid/v1'

// App
import ApplicationLayout from './ApplicationLayout'
import ClustersPage from '../clusters/Page'
import ChefRepositoriesPage from '../repositories/Page'
import ChefRepositoryPage from '../repository/Page'
import ChefRepositoryPolicyBootstrapCustomiserPage from '../repository/PolicyBootstrapCustomiser/Page'
import ChefRepositoryPolicyHooksPage from '../repository/PolicyHooks/Page'
import ChefRepositoryImportPage from '../repositories/import/Page'
import history from '../helpers/History'
import DashboardPage from '../dashboard/Page'
import FourOFour from './FourOFour'
import JobsPage from '../jobs/Page'
import LoginLayout from './LoginLayout'
import NodeActionStacksPage from '../node-action-stacks/Page'
import NodeActionList from '../node-action-stacks/List'
import NodesBootstrapPage from '../nodes/Bootstrap/Page'
import NodesPage from '../nodes/Page'
import SettingsPage from '../settings/Page'
import withLoadedNode from '../components/hocs/withLoadedNode'

// Devise
import DeviseConfirmationsNew from '../devise/confirmations/DeviseConfirmationsNew'
import DeviseConfirmationsShow from '../devise/confirmations/DeviseConfirmationsShow'
import DevisePasswordsEdit from '../devise/passwords/DevisePasswordsEdit'
import DevisePasswordsNew from '../devise/passwords/DevisePasswordsNew'
import DeviseRegistrationsNew from '../devise/registrations/DeviseRegistrationsNew'
import DeviseSessionsNew from '../devise/sessions/DeviseSessionsNew'
import DeviseUnlocksNew from '../devise/unlocks/DeviseUnlocksNew'
import DeviseUnlocksShow from '../devise/unlocks/DeviseUnlocksShow'

class AppRouter extends React.Component {
  privateRoutes = () => (
    [
      {
        path: '/',
        component: () => <DashboardPage />
      },
      {
        path: '/clusters',
        component: () => <ClustersPage />
      },
      {
        path: '/dashboard',
        component: () => <DashboardPage />
      },
      {
        path: '/jobs',
        component: () => <JobsPage />
      },
      {
        path: '/nodes',
        component: () => <NodesPage />
      },
      {
        path: '/nodes/new',
        component: props => <NodesBootstrapPage {...props} />
      },
      {
        path: '/nodes/:id',
        component: props => <NodesPage {...props} />
      },
      {
        path: '/nodes/:nodeId/actions/:actionId',
        component: (props) => {
          const { match: { params: { nodeId } } } = props
          const NodeActionPageWithLoadedNode = withLoadedNode(NodeActionStacksPage, nodeId)
          return <NodeActionPageWithLoadedNode {...props} />
        }
      },
      {
        path: '/node-action-stacks/:actionId',
        component: props => <NodeActionStacksPage {...props} />
      },
      {
        path: '/node-action-stacks',
        component: props => <NodeActionList {...props} />
      },
      {
        path: '/repositories',
        component: () => <ChefRepositoriesPage />
      },
      {
        path: '/repositories/import',
        component: () => <ChefRepositoryImportPage />
      },
      {
        path: '/repositories/:id',
        component: props => <ChefRepositoryPage {...props} />
      },
      {
        path: '/repositories/:id/policies/:policy_name/actions/bootstrap',
        component: props => <ChefRepositoryPolicyBootstrapCustomiserPage {...props} />
      },
      {
        path: '/repositories/:id/policies/:policy_name/hooks',
        component: props => <ChefRepositoryPolicyHooksPage {...props} />
      },
      {
        path: '/settings',
        component: () => <SettingsPage />
      }
    ]
  )

  publicRoutes = () => {
    const { registrationOpened } = this.props

    const routes = [
      {
        path: '/users/confirmation/new',
        component: <DeviseConfirmationsNew />
      },
      {
        path: '/users/password/new',
        component: <DevisePasswordsNew />
      },
      {
        path: '/users/sign_in',
        component: <DeviseSessionsNew />
      },
      {
        path: '/users/unlocks/new',
        component: <DeviseUnlocksNew />
      },
      {
        path: '/users/:userId/confirmation',
        component: <DeviseConfirmationsShow />
      },
      {
        path: '/users/:userId/password/edit',
        component: <DevisePasswordsEdit />
      },
      {
        path: '/users/:userId/unlock',
        component: <DeviseUnlocksShow />
      }
    ]

    if (registrationOpened) {
      routes.push({
        path: '/users/sign_up',
        component: <DeviseRegistrationsNew />
      })
    }

    return routes
  }

  render() {
    const { loggedIn } = this.props

    return (
      <Router history={history}>
        <Switch>
          {this.privateRoutes().map(route => (
            <Route
              exact
              key={uuidv1()}
              path={route.path}
              render={props => (loggedIn ? (
                <ApplicationLayout {...props}>
                  {route.component(props)}
                </ApplicationLayout>
              ) : (
                <LoginLayout>
                  <DeviseSessionsNew />
                </LoginLayout>
              ))
              }
            />
          ))}

          {this.publicRoutes().map(route => (
            <Route
              key={uuidv1()}
              path={route.path}
              render={() => (
                <LoginLayout>
                  {route.component}
                </LoginLayout>
              )}
            />
          ))}

          <Route component={FourOFour} />
        </Switch>
      </Router>
    )
  }
}

const mapStateToProps = (state) => {
  const { loggedIn } = state.authentication
  const { configuration: { registrationOpened } } = state.app

  return {
    loggedIn,
    registrationOpened
  }
}

AppRouter.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  registrationOpened: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(AppRouter)

import React from 'react'
import PropTypes from 'prop-types'

import FlashMessages from '../alerts/FlashMessages'

const LoginLayout = (props) => {
  const { children } = props

  return (
    <React.Fragment>
      <div className="position-absolute w-100">
        <FlashMessages className="m-4" />
      </div>
      <div className="d-flex flex-column h-100 justify-content-center align-items-center loading-page">
        <div className="app-title">
          <h1>Commis</h1>
        </div>
        {children}
      </div>
    </React.Fragment>
  )
}

LoginLayout.propTypes = {
  children: PropTypes.element.isRequired
}

export default (LoginLayout)

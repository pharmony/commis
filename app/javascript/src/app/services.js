import ApiFetch from '../helpers/ApiFetch'

const bootstrap = () => ApiFetch.get('/api/bootstrap')

export default {
  bootstrap
}

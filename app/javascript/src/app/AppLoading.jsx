import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AlertMessage from '../components/AlertMessage'
import appActions from './actions'
import PageLoader from '../components/PageLoader'

class AppLoading extends React.Component {
  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(appActions.bootstrap())
  }

  render = () => {
    const {
      loading,
      loadingError
    } = this.props

    return (
      <div className="d-flex flex-column h-100 justify-content-center align-items-center loading-page">
        <div className="app-title">
          <h1>Commis</h1>
        </div>
        {loading && (
          <PageLoader message="Loading application configuration ..." />
        )}

        {loadingError && (
          <AlertMessage>
            Unfortunately an error occured while loading the configuration.
            Please try again.
          </AlertMessage>
        )}
      </div>
    )
  }
}

const mapStateToProps = ({ app: { configuration } }) => {
  // configuration is not garanteed to exists
  if (configuration) {
    const { loaded, loading, loading_error: loadingError } = configuration

    return {
      loaded,
      loading,
      loadingError
    }
  }

  return {}
}

AppLoading.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  loadingError: PropTypes.bool
}

AppLoading.defaultProps = {
  loading: false,
  loadingError: null
}

export default connect(mapStateToProps)(AppLoading)

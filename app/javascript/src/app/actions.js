import appConstants from './constants'
import appServices from './services'

const bootstrap = () => {
  const request = () => ({ type: appConstants.BOOTSTRAP_REQUEST })
  const success = app => ({ app, type: appConstants.BOOTSTRAP_SUCCESS })
  const failure = error => ({ error, type: appConstants.BOOTSTRAP_FAILURE })

  return (dispatch) => {
    dispatch(request())

    appServices.bootstrap()
      .then(data => dispatch(success(data)))
      .catch(error => dispatch(failure(error)))
  }
}

const updatePreferences = preferences => (dispatch) => {
  dispatch({ type: appConstants.PREFERENCES_UPDATE, preferences })
}

export default {
  bootstrap,
  updatePreferences
}

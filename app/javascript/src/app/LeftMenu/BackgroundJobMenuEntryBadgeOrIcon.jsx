import React from 'react'
import { Badge } from 'reactstrap'
import PropTypes from 'prop-types'

import BackgroundTasksWarning from '../../jobs/BackgroundTasksWarning'

const BackgroundJobMenuEntryBadgeOrIcon = ({ fetchStatsFailed, jobCount }) => {
  if (fetchStatsFailed) {
    return <BackgroundTasksWarning />
  }

  if (jobCount > 0) {
    return (
      <Badge
        className="float-right"
        color="primary"
      >
        {jobCount}
      </Badge>
    )
  }

  return null
}

BackgroundJobMenuEntryBadgeOrIcon.propTypes = {
  fetchStatsFailed: PropTypes.bool,
  jobCount: PropTypes.number
}

BackgroundJobMenuEntryBadgeOrIcon.defaultProps = {
  fetchStatsFailed: false,
  jobCount: 0
}

export default BackgroundJobMenuEntryBadgeOrIcon

import React from 'react'
import Icofont from 'react-icofont'
import { NavLink } from 'react-router-dom'
import {
  NavItem,
  Tooltip
} from 'reactstrap'
import PropTypes from 'prop-types'

class NavEntry extends React.Component {
  mounted = false

  state = {
    tooltipOpen: false
  }

  componentDidMount = () => {
    this.mounted = true
  }

  componentWillUnmount = () => {
    this.mounted = false
  }

  toggleTooltip = () => {
    const { tooltipOpen } = this.state
    const { leftMenuCollapsed } = this.props

    if (!this.mounted) { return }

    this.setState({
      tooltipOpen: leftMenuCollapsed && !tooltipOpen
    })
  }

  render = () => {
    const {
      children,
      className,
      disabled,
      id,
      icon,
      leftMenuCollapsed,
      onClick,
      text,
      to
    } = this.props
    const { tooltipOpen } = this.state

    const elementId = id || `toggle-${text.replace(' ', '-').toLowerCase()}`

    const entryContent = (
      <React.Fragment>
        <Icofont
          className={`mr-2 ${disabled && 'text-muted'}`}
          icon={icon}
        />
        <span className={`nav-link-text ${disabled && 'text-muted'}`}>{text}</span>
        {children}
      </React.Fragment>
    )

    return (
      <NavItem>
        {to ? (
          <NavLink
            className={className}
            id={elementId}
            to={to}
          >
            {entryContent}
          </NavLink>
        ) : (
          <a
            className={`${className} cursor-pointer`}
            id={elementId}
            onClick={(event) => onClick(event)}
          >
            {entryContent}
          </a>
        )}

        {leftMenuCollapsed && (
          <Tooltip
            isOpen={tooltipOpen}
            placement="right"
            target={elementId}
            toggle={() => this.toggleTooltip()}
          >
            {text}
          </Tooltip>
        )}
      </NavItem>
    )
  }
}

NavEntry.propTypes = {
  children: PropTypes.element,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  icon: PropTypes.string.isRequired,
  leftMenuCollapsed: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
  to: PropTypes.string
}

NavEntry.defaultProps = {
  children: undefined,
  className: '',
  disabled: false,
  id: null,
  onClick: undefined,
  to: null
}

export default NavEntry

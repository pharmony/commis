import React from 'react'
import { connect } from 'react-redux'
import {
  Nav,
  Navbar,
  NavbarBrand
} from 'reactstrap'
import PropTypes from 'prop-types'

import appActions from '../actions'
import BackgroundJobMenuEntryBadgeOrIcon from './BackgroundJobMenuEntryBadgeOrIcon'
import BackgroundTasksPreview from '../../jobs/BackgroundTasksPreview'
import NavEntry from './NavEntry'

class LeftMenu extends React.Component {
  mounted = false

  state = {
    backgroundTasksOverviewOpened: false,
    navBarAdditionnalClasses: '',
    navLinkClasses: '',
    previousLeftMenuCollapsedValue: false
  }

  componentDidMount = () => {
    this.mounted = true

    const { leftMenuCollapsed } = this.props

    this.updateComponentState(leftMenuCollapsed)
  }

  componentDidUpdate = () => {
    const { leftMenuCollapsed } = this.props
    const { previousLeftMenuCollapsedValue } = this.state

    if (leftMenuCollapsed !== previousLeftMenuCollapsedValue) {
      this.updateComponentState(leftMenuCollapsed)
    }
  }

  componentWillUnmount = () => {
    this.mounted = true
  }

  buildNavBarAdditionnalClasses = () => {
    const { leftMenuCollapsed } = this.props

    return leftMenuCollapsed ? 'shrinked' : ''
  }

  buildNavLinkClasses = () => {
    const { leftMenuCollapsed } = this.props

    const navLinkClasses = ['d-flex', 'w-100', 'align-items-center']
    if (leftMenuCollapsed) {
      navLinkClasses.push('justify-content-center')
    } else {
      navLinkClasses.push('pl-3')
    }

    return navLinkClasses.join(' ')
  }

  toggleSideBar = () => {
    const { dispatch, leftMenuCollapsed } = this.props

    dispatch(appActions.updatePreferences({
      leftMenuCollapsed: !leftMenuCollapsed
    }))
  }

  toggleBackgroundTasksOverview = () => {
    const { backgroundTasksOverviewOpened } = this.state

    if (!this.mounted) { return }

    this.setState({
      backgroundTasksOverviewOpened: !backgroundTasksOverviewOpened
    })
  }

  updateComponentState = (collapsed) => {
    const navLinkClasses = this.buildNavLinkClasses()

    this.setState({
      navBarAdditionnalClasses: this.buildNavBarAdditionnalClasses(),
      navLinkClasses,
      previousLeftMenuCollapsedValue: collapsed
    })
  }

  render = () => {
    const { fetchStatsFailed, jobCount, leftMenuCollapsed } = this.props
    const {
      backgroundTasksOverviewOpened,
      navBarAdditionnalClasses,
      navLinkClasses
    } = this.state

    return (
      <Navbar
        className={`flex-column side-navbar navbar-dark bg-dark align-items-start p-0 h-100 ${navBarAdditionnalClasses}`}
      >
        <NavbarBrand className="d-flex flex-row align-self-center p-0 m-0">
          Commis
        </NavbarBrand>
        <Nav className="flex-fill w-100" navbar>
          <NavEntry
            className={navLinkClasses}
            icon="dashboard-web"
            leftMenuCollapsed={leftMenuCollapsed}
            text="Dashboard"
            to="/dashboard"
          />
          <NavEntry
            className={navLinkClasses}
            icon="chef"
            leftMenuCollapsed={leftMenuCollapsed}
            text="Chef repositories"
            to="/repositories"
          />
          <NavEntry
            className={navLinkClasses}
            icon="cubes"
            leftMenuCollapsed={leftMenuCollapsed}
            text="Clusters"
            to="/clusters"
          />
          <NavEntry
            className={navLinkClasses}
            icon="server"
            leftMenuCollapsed={leftMenuCollapsed}
            text="Nodes"
            to="/nodes"
          />
          <NavEntry
            className={navLinkClasses}
            icon="clock-time"
            leftMenuCollapsed={leftMenuCollapsed}
            text="Node actions"
            to="/node-action-stacks"
          />
        </Nav>
        <Nav className="w-100" navbar>
          <NavEntry
            className={`${navLinkClasses} white-left-menu-entry`}
            icon="gear"
            leftMenuCollapsed={leftMenuCollapsed}
            text="Settings"
            to="/settings"
          />
          <NavEntry
            className={navLinkClasses}
            disabled={fetchStatsFailed}
            id="background-tasks-preview"
            icon="automation"
            leftMenuCollapsed={leftMenuCollapsed}
            onClick={() => !fetchStatsFailed && this.toggleBackgroundTasksOverview()}
            text="Background Tasks"
          >
            <BackgroundJobMenuEntryBadgeOrIcon
              fetchStatsFailed={fetchStatsFailed}
              jobCount={jobCount}
            />
          </NavEntry>
          <BackgroundTasksPreview
            visible={backgroundTasksOverviewOpened}
          />
          <NavEntry
            className={navLinkClasses}
            icon="navigation-menu"
            leftMenuCollapsed={leftMenuCollapsed}
            onClick={() => this.toggleSideBar()}
            text="Collapse"
          />
        </Nav>
      </Navbar>
    )
  }
}

const mapStateToProps = ({
  app: { preferences: { leftMenuCollapsed } },
  jobs: { fetchStatsFailed, jobCount }
}) => ({ fetchStatsFailed, jobCount, leftMenuCollapsed })

LeftMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetchStatsFailed: PropTypes.bool.isRequired,
  jobCount: PropTypes.number.isRequired,
  leftMenuCollapsed: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(LeftMenu)

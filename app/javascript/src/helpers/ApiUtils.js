const buildErrorFrom = (message, response) => {
  const error = new Error(message)
  error.response = response
  return error
}

const checkStatus = (response) => {
  if (response.ok) { return response }

  if (response.status === 404) {
    throw buildErrorFrom(response.statusText, response)
  }

  return response.json().then((json) => {
    throw buildErrorFrom(json.error.message || json.error, response)
  })
}

export default {
  checkStatus
}

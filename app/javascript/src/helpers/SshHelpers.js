const sshAuthMethodNameFrom = (method) => {
  switch (method) {
    case 'none':
      return 'None'
    case 'password':
      return 'Password'
    case 'ssh_keys':
      return 'SSH Keys'
    default:
      return 'Unknown'
  }
}

export default {
  sshAuthMethodNameFrom
}

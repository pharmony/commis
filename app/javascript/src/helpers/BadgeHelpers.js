const colorFrom = (state) => {
  switch (state) {
    case 'enqueued':
    case 'new':
    case 'running':
      return 'info'
    case 'finished':
    case 'healthy':
    case 'success':
      return 'success'
    case 'cancelled':
    case 'failed':
      return 'danger'
    default:
      return 'secondary'
  }
}

export default {
  colorFrom
}

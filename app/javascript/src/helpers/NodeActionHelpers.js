/*
** Helper to build the title of a NodeAction shown from the NodeActionStack's
*  RunningCommand component.
*/
const buildTitleFrom = (nodeAction, options) => {
  switch (nodeAction.name) {
    case 'bootstrap':
      if (options.policy && !options.group) {
        return `Bootstrap the ${options.policy.name} policy`
      }

      if (options.policy && options.group) {
        return `Bootstrap the ${options.policy.name} policy with the
                ${options.group.name} group`
      }

      return 'Bootstrap a node'
    case 'converge':
      if (options.node) {
        return `Converge the node ${options.node.name}`
      }

      return 'Converge node(s)'
    case 'hook':
      if (nodeAction.policy_hook) {
        switch (nodeAction.policy_hook._type) {
          case 'PolicyHooks::NodeAuthorisationHook':
            return "Hook updating the node's authentication method"
          default:
            return 'Hook execution'
        }
      }

      return 'Hook execution'
    default:
      return nodeAction.name
  }
}

export default {
  buildTitleFrom
}

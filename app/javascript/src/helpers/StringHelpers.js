const capitalize = string => string.charAt(0).toUpperCase() + string.slice(1)

const titleize = string => (
  string.replace(/\w\S*/g, matched => (
    matched.charAt(0).toUpperCase() + matched.substr(1).toLowerCase()
  ))
)

export default {
  capitalize,
  titleize
}

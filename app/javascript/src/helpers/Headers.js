const jwtAuthenticationHeaders = () => {
  // return authorization header with jwt token
  const user = JSON.parse(localStorage.getItem('user'))

  if (user && user.token) {
    return { Authorization: `Bearer ${user.token}` }
  }

  return {}
}

const destroy = () => ({
  method: 'DELETE',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

const get = () => ({
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

const post = ({ body }) => ({
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  body
})

export default {
  delete: destroy,
  get,
  post
}

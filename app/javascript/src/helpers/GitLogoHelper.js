/*
** Git Logo Helper
*
*  Gets the Git repository platform logo from the given URL
*/
import gitLogoPng from '../../images/scm-logos/git.png'
import gitlabLogoPng from '../../images/scm-logos/gitlab.png'

const fromUrl = (url) => {
  if (!url) return null

  const captureHostRegex = /git@([A-z.-_]+):.*/
  const host = captureHostRegex.exec(url)[1]

  switch (host) {
    case 'gitlab.com':
      return gitlabLogoPng
    default:
      return gitLogoPng
  }
}

export default {
  fromUrl
}

import ApiUtils from './ApiUtils'
import Headers from './Headers'

const destroy = url => (
  fetch(url, Headers.delete())
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const get = url => (
  fetch(url, Headers.get())
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const post = (url, params = { body: null }) => (
  fetch(url, Headers.post({ body: params.body }))
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  delete: destroy,
  get,
  post
}

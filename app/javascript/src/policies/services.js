import ApiUtils from '../helpers/ApiUtils'
import { PoliciesApi, PolicyHooksApi } from './api'

const fetchAll = () => (
  new PoliciesApi()
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchPolicyHooks = id => (
  new PolicyHooksApi(id)
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const updateHooks = (id, hooks) => (
  new PolicyHooksApi(id)
    .update(null, { hooks })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetchAll,
  fetchPolicyHooks,
  updateHooks
}

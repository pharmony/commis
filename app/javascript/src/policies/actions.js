import alertActions from '../alerts/actions'
import policiesConstants from './constants'
import policiesService from './services'

const fetchAll = () => {
  const request = () => ({ type: policiesConstants.FETCH_ALL_REQUEST })
  const success = policies => ({
    policies,
    type: policiesConstants.FETCH_ALL_SUCCESS
  })
  const failure = error => ({
    error,
    type: policiesConstants.FETCH_ALL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    policiesService.fetchAll()
      .then(policies => dispatch(success(policies)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchHooksFor = (id) => {
  const request = policyId => ({
    policyId,
    type: policiesConstants.FETCH_HOOKS_REQUEST
  })
  const success = (policyId, hooks) => ({
    hooks,
    policyId,
    type: policiesConstants.FETCH_HOOKS_SUCCESS
  })
  const failure = error => ({
    error,
    type: policiesConstants.FETCH_HOOKS_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id))

    policiesService.fetchPolicyHooks(id)
      .then(hooks => dispatch(success(id, hooks)))
      .catch(error => dispatch(failure(error)))
  }
}

const persistHooks = (id, newHooks) => {
  const request = (policyId, hooks) => ({
    hooks,
    policyId,
    type: policiesConstants.UPDATE_HOOKS_REQUEST
  })
  const success = policyId => ({
    policyId,
    type: policiesConstants.UPDATE_HOOKS_SUCCESS
  })
  const failure = error => ({
    error,
    type: policiesConstants.UPDATE_HOOKS_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id, newHooks))

    policiesService.updateHooks(id, newHooks)
      .then(() => {
        dispatch(success(id))
        dispatch(alertActions.success('Hooks saved!'))
      })
      .catch((error) => {
        dispatch(failure(error))
        dispatch(alertActions.error('Something went wrong while saving hooks'))
      })
  }
}

export default {
  fetchAll,
  fetchHooksFor,
  persistHooks
}

import dotProp from 'dot-prop-immutable'

import policiesConstants from './constants'

const initialState = {
  fetchAllFailed: false, // True when backend server replied with an error
  fetched: false, /* True when policies has been fetched at least once from the
                     backend server */
  fetching: false, // True while fetching policies from the backend server
  fetchingHooks: false, // True while fetching policy's hooks
  fetchingHooksFailed: false, /* True when backend server replied with an error
                                 while fetching policy's hooks */
  fetchingHooksSuccess: false, // True when fetching policy's hooks worked fine
  itemCount: 0, // Policies count
  items: [] // Policies from the backend server
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case policiesConstants.FETCH_ALL_REQUEST:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetchingAll', true)
      return dotProp.set(newState, 'fetching', true)
    case policiesConstants.FETCH_ALL_SUCCESS: {
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      newState = dotProp.set(newState, 'fetching', false)
      newState = dotProp.set(newState, 'itemCount', action.policies.length)
      return dotProp.set(
        newState,
        'items',
        action.policies.sort((a, b) => a.name.localeCompare(b.name))
      )
    }
    case policiesConstants.FETCH_ALL_FAILURE:
      newState = dotProp.set(state, 'fetchAllFailed', true)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'fetching', false)

    case policiesConstants.FETCH_HOOKS_REQUEST:
      newState = dotProp.set(state, 'fetchingHooks', true)
      newState = dotProp.set(newState, 'fetchingHooksFailed', false)
      return dotProp.set(newState, 'fetchingHooksSuccess', false)
    case policiesConstants.FETCH_HOOKS_SUCCESS: {
      newState = dotProp.set(state, 'fetchingHooks', false)

      const policyIndex = state.items.findIndex(
        item => item.id === action.policyId
      )

      if (policyIndex > -1) {
        newState = dotProp.set(newState, 'fetchingHooksFailed', false)
        newState = dotProp.set(newState, 'fetchingHooksSuccess', true)
        return dotProp.set(newState, `items.${policyIndex}.hooks`, action.hooks)
      }

      newState = dotProp.set(newState, 'fetchingHooksFailed', true)
      return dotProp.set(newState, 'fetchingHooksSuccess', false)
    }
    case policiesConstants.FETCH_HOOKS_FAILURE:
      newState = dotProp.set(state, 'fetchingHooks', false)
      newState = dotProp.set(newState, 'fetchingHooksFailed', true)
      return dotProp.set(newState, 'fetchingHooksSuccess', false)

    default:
      return state
  }
}

import RestfulClient from 'restful-json-api-client'

import RailsHeaders from '../helpers/RailsHeaders'

export class PoliciesApi extends RestfulClient {
  constructor() {
    super('/api/chef', { resource: 'policies' })
  }
}

export class PolicyHooksApi extends RestfulClient {
  constructor(id) {
    super(`/api/policies/${id}`, {
      resource: 'hooks',
      headers: RailsHeaders
    })
  }
}

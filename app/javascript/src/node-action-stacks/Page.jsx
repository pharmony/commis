import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AlertMessage from '../components/AlertMessage'
import nodesActions from '../nodes/actions'
import nodeActionStackActions from './actions'
import PageLoader from '../components/PageLoader'
import RunningCommand from './RunningCommand'

class NodeActionStacksPage extends React.Component {
  state = {
    fetchingStack: false,
    fetchingStackNodeActions: false,
    nodeActionStackId: null
  }

  componentDidMount = () => {
    console.log('[NodeActionStacksPage::componentDidMount] Mounted ...')

    this.loadNodeActionStackAfterAppLoaded()
  }

  componentDidUpdate = () => this.loadNodeActionStackAndItsNodeActions()

  /*
  ** Waits until the app is loaded, then call
  *  loadNodeActionStackAndItsNodeActions().
  */
  loadNodeActionStackAfterAppLoaded = () => {
    const { loading } = this.props
    const { nodeActionStackId } = this.state

    // The app is still loading, we have to wait ...
    if (loading) return

    // Loading finished, Node not yet loaded, so loading it
    if (!nodeActionStackId) this.loadNodeActionStackAndItsNodeActions()
  }

  dispatchFetchNodeActionStack = (nodeName, actionId) => {
    const { dispatch } = this.props

    dispatch(nodesActions.fetchNodeActionStack(actionId, nodeName))
  }

  loadNodeActionStackAndItsNodeActions = () => {
    const { nodeActionStackId } = this.state

    if (nodeActionStackId) return this.loadNodeActionsFromNodeActionStack()

    return this.loadNodeActionStack()
  }

  loadNodeActionsFromNodeActionStack = () => {
    const { dispatch } = this.props

    const {
      fetchingStackNodeActions,
      nodeActionStackId
    } = this.state

    if (fetchingStackNodeActions) return true

    this.setState({
      fetchingStackNodeActions: true
    }, () => {
      dispatch(nodeActionStackActions.fetchNodeActions(nodeActionStackId))
    })

    return true
  }

  loadNodeActionStack = () => {
    const {
      match: { params: { actionId, nodeId: nodeName } }
    } = this.props
    const { fetchingStack } = this.state

    const currentNodeAction = this.searchNodeActionStackFromLocalStorage(
      actionId
    )

    if (currentNodeAction) {
      this.setState({
        fetchingStack: false,
        nodeActionStackId: currentNodeAction.id
      })
    } else {
      if (fetchingStack) return true

      this.setState({
        fetchingStack: true
      }, () => {
        this.dispatchFetchNodeActionStack(nodeName, actionId)
      })
    }

    return true
  }

  /*
  ** Search in the Redux store in order to try to save API calls.
  *  Note that partial NodeActionStack objects (having a tmpId attribute) will
  *  be ignored so that an API call will be executed in order to fetch the full
  *  version of the object.
  */
  searchNodeActionStackFromLocalStorage = (stackId) => {
    const { nodeActionStacks } = this.props

    const filteredStacks = nodeActionStacks.filter(
      stack => stack.tmpId === undefined
    )

    return filteredStacks.find(stack => stack.id === stackId)
  }

  render = () => {
    const {
      fetchOneFailed,
      fetchingNodeActions,
      fetchingNodeActionsFailed,
      nodeActionStacks
    } = this.props
    const { nodeActionStackId } = this.state


    if (fetchOneFailed) {
      return (
        <AlertMessage>
          The node action could not be loaded, try again.
        </AlertMessage>
      )
    }

    if (!nodeActionStackId) {
      return <PageLoader message="Loading node action stack ..." />
    }

    if (fetchingNodeActions) {
      return <PageLoader message="Loading node actions ..." />
    }

    if (fetchingNodeActionsFailed) {
      return (
        <AlertMessage>
          Could not fetch the node actions for this stack. Please try again.
        </AlertMessage>
      )
    }

    const currentNodeActionStack = nodeActionStacks.find(
      stack => stack.id === nodeActionStackId
    )

    if (!currentNodeActionStack
        || (currentNodeActionStack && !currentNodeActionStack.nodeActions)
    ) {
      return <PageLoader message="Loading stack node actions ..." />
    }

    return (
      <React.Fragment>
        <RunningCommand
          nodeActionStack={currentNodeActionStack}
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  app: { configuration: { loading } },
  nodeActionStacks: {
    fetchingNodeActions,
    fetchingNodeActionsFailed,
    fetchOneFailed,
    items: nodeActionItems
  }
}) => ({
  fetchingNodeActions,
  fetchingNodeActionsFailed,
  fetchOneFailed,
  loading,
  nodeActionStacks: nodeActionItems
})

NodeActionStacksPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetchingNodeActions: PropTypes.bool.isRequired,
  fetchingNodeActionsFailed: PropTypes.bool.isRequired,
  fetchOneFailed: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      actionId: PropTypes.string.isRequired,
      nodeId: PropTypes.string
    }).isRequired
  }).isRequired,
  nodeActionStacks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string
  })).isRequired
}

export default connect(mapStateToProps)(NodeActionStacksPage)

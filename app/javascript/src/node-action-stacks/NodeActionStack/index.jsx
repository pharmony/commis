import React from 'react'
import { connect } from 'react-redux'
import {
  ListGroupItem,
  ListGroupItemText,
  Spinner
} from 'reactstrap'
import Icofont from 'react-icofont'
import moment from 'moment'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

import StatusBadge from '../../components/StatusBadge'

const NodeActionStack = ({
  actionStack,
  compact,
  statuses
}) => {
  const nodeStackStatus = statuses.find(
    item => item.klass === 'NodeActionStack' && item.id === actionStack.id
  )

  const state = nodeStackStatus ? nodeStackStatus.state : actionStack.state

  const mr = `mr-${compact ? '2' : '4'}`

  return (
    <ListGroupItem className={`d-flex flex-row ${compact ? 'p-2' : ''}`}>
      <div
        className={`d-flex flex-column justify-content-center ${mr}`}
      >
        <StatusBadge
          className="w-4"
          state={state}
        />
      </div>
      <div className="d-flex flex-fill align-items-center">
        <ListGroupItemText className="d-flex" tag="div">

          {state === 'creating'
            ? 'Commis is creating the requested action ...'
            : (
              <div>
                A&nbsp;
                {actionStack.type}
                &nbsp;action has been requested&nbsp;
                {moment(actionStack.created_at).fromNow()}
                .
              </div>
            )
          }
        </ListGroupItemText>
      </div>
      <div className="d-flex align-items-center">
        {state === 'creating'
          ? (
            <Spinner type="grow" color="#ffffff" />
          ) : (
            <NavLink
              className="btn btn-success"
              to={`/node-action-stacks/${actionStack.id}`}
            >
              {compact ? (
                <Icofont icon="eye-alt" />
              ) : 'Open'}
            </NavLink>
          )
        }
      </div>
    </ListGroupItem>
  )
}
const mapStateToProps = ({ statuses: { items: statuses } }) => ({ statuses })

NodeActionStack.propTypes = {
  actionStack: PropTypes.shape({
    created_at: PropTypes.string,
    id: PropTypes.string,
    tmpId: PropTypes.string,
    state: PropTypes.string,
    type: PropTypes.string
  }).isRequired,
  compact: PropTypes.bool,
  statuses: PropTypes.arrayOf(PropTypes.shape({
    klass: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired
  })).isRequired
}

NodeActionStack.defaultProps = {
  compact: false
}

export default connect(mapStateToProps)(NodeActionStack)

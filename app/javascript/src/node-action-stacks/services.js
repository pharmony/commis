import ApiUtils from '../helpers/ApiUtils'
import { NodeActionStacksApi, NodeActionStackNodeActionsApi } from './apis'

const cancelNodeActionStack = id => (
  new NodeActionStacksApi()
    .cancel({ id })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchAll = () => (
  new NodeActionStacksApi()
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchNodeActions = nodeActionStackId => (
  new NodeActionStackNodeActionsApi(nodeActionStackId)
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  cancelNodeActionStack,
  fetchAll,
  fetchNodeActions
}

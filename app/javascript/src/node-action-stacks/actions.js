import nodeActionStacksConstants from './constants'
import nodeActionStacksService from './services'

const cancelNodeActionStack = (id) => {
  const request = nodeActionId => ({
    id: nodeActionId,
    type: nodeActionStacksConstants.CANCEL_ACTION_REQUEST
  })
  const success = () => ({
    type: nodeActionStacksConstants.CANCEL_ACTION_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodeActionStacksConstants.CANCEL_ACTION_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id))

    nodeActionStacksService.cancelNodeActionStack(id)
      .then(() => dispatch(success()))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchAll = () => {
  const request = () => ({ type: nodeActionStacksConstants.FETCH_ALL_REQUEST })
  const success = nodeActions => ({
    nodeActions,
    type: nodeActionStacksConstants.FETCH_ALL_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodeActionStacksConstants.FETCH_ALL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    nodeActionStacksService.fetchAll()
      .then(actions => dispatch(success(actions)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchNodeActions = (nodeActionStackId) => {
  const request = id => ({
    nodeActionStackId: id,
    type: nodeActionStacksConstants.FETCH_NODE_ACTIONS_REQUEST
  })
  const success = (id, nodeActions) => ({
    nodeActionStackId: id,
    nodeActions,
    type: nodeActionStacksConstants.FETCH_NODE_ACTIONS_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodeActionStacksConstants.FETCH_NODE_ACTIONS_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeActionStackId))

    nodeActionStacksService.fetchNodeActions(nodeActionStackId)
      .then(actions => dispatch(success(nodeActionStackId, actions)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  cancelNodeActionStack,
  fetchAll,
  fetchNodeActions
}

import dotProp from 'dot-prop-immutable'
import moment from 'moment'

import actionCableConstants from '../action-cable/constants'
import nodeActionsConstants from '../node-actions/constants'
import nodeActionStacksConstants from './constants'
import nodesConstants from '../nodes/constants'

const initialState = {
  fetchAllFailed: false, // True when back server replied with an error
  fetchingAll: false, // True while fetching all node actions
  fetchingNodeActions: false,
  fetchingNodeActionsFailed: false,
  fetchingNodeActionsSucceed: false,
  fetchOneFailed: false, // True when back server replied with an error
  fetched: false, /* True when node actions has been fetched at least once from
                     the backend server */
  fetchingOne: false, /* True while fetching the node action from the backend
                         server */
  items: [] // Node action objects from the backend server
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case actionCableConstants.DATA_RECEIVED:
      if (action.data) {
        if (action.data.node_action_stack_state) {
          const {
            node_action_stack_id: nodeActionStackId,
            provider_job_id: providerJobId
          } = action.data.node_action_stack_state

          newState = state

          /*
          ** When a provider_job_id is given in the message, that means
          *  we received the created NodeActionStack ID, so we can update our
          *  temporary object with the real ID.
          */
          if (providerJobId) {
            const stackIndex = state.items.findIndex(
              item => item.jid === providerJobId
            )

            if (stackIndex > -1) {
              // Sets the NodeActionStack ID
              newState = dotProp.set(
                newState,
                `items.${stackIndex}.id`,
                nodeActionStackId
              )

              /*
              ** Removes the jid but not he tmpId. This will allow to detect
              *  this partial object (we only have its ID right now), so that
              *  accessing a page which would use it will be able to see that
              *  it needs to loads it from the backend server.
              */
              newState = dotProp.delete(newState, `items.${stackIndex}.jid`)
            }
          }

          return newState
        }
      }
      return state

    case nodeActionsConstants.FETCH_HOOK_DATA_SUCCESS: {
      // Search the NodeAction matching the `action.nodeActionId` through all
      // the NodeActionStacks
      let stackIndex
      let nodeActionIndex

      state.items.forEach((stack, index) => {
        const actionIndex = stack.nodeActions.findIndex(
          nodeAction => nodeAction.id === action.nodeActionId
        )

        // NodeAction found
        if (actionIndex > -1) {
          stackIndex = index
          nodeActionIndex = actionIndex
        }
      })

      if (stackIndex > -1 && nodeActionIndex > -1) {
        return dotProp.set(
          state,
          `items.${stackIndex}.nodeActions.${nodeActionIndex}.hook`,
          action.hook
        )
      }

      return state
    }
    case nodeActionStacksConstants.FETCH_ALL_REQUEST:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetchingAll', true)
      return dotProp.set(newState, 'items', [])
    case nodeActionStacksConstants.FETCH_ALL_SUCCESS: {
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'items', action.nodeActions)
    }
    case nodeActionStacksConstants.FETCH_ALL_FAILURE:
      newState = dotProp.set(state, 'fetchAllFailed', true)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'items', [])

    case nodeActionStacksConstants.FETCH_NODE_ACTIONS_REQUEST:
      newState = dotProp.set(state, 'fetchingNodeActions', true)
      newState = dotProp.set(newState, 'fetchingNodeActionsFailed', false)
      return dotProp.set(newState, 'fetchingNodeActionsSucceed', false)

    case nodeActionStacksConstants.FETCH_NODE_ACTIONS_SUCCESS: {
      const itemIndex = state.items.findIndex(
        item => item.id === action.nodeActionStackId
      )

      newState = dotProp.set(state, 'fetchingNodeActions', false)
      newState = dotProp.set(newState, 'fetchingNodeActionsFailed', false)
      newState = dotProp.set(newState, 'fetchingNodeActionsSucceed', true)
      return dotProp.set(
        newState,
        `items.${itemIndex}.nodeActions`,
        action.nodeActions
      )
    }

    case nodeActionStacksConstants.FETCH_NODE_ACTIONS_FAILURE:
      newState = dotProp.set(state, 'fetchingNodeActions', false)
      newState = dotProp.set(newState, 'fetchingNodeActionsFailed', true)
      return dotProp.set(newState, 'fetchingNodeActionsSucceed', false)

    case nodesConstants.BOOTSTRAP_REQUEST_REQUEST:
    case nodesConstants.CONVERGE_REQUEST_REQUEST:
    case nodesConstants.PULL_REPO_AND_CONVERGE_REQUEST_REQUEST:
      return dotProp.set(
        state,
        'items',
        [...state.items, {
          created_at: moment().format(),
          state: 'creating',
          tmpId: action.tmpNodeActionId
        }]
      )
    /* Saving the Job JID on the temporary object so that later, when the
    ** created NodeActionStack will be sent via ActionCable/WebSocket, it will
    *  be possible to link it to this temporary object
    */
    case nodesConstants.BOOTSTRAP_REQUEST_SUCCESS:
    case nodesConstants.CONVERGE_REQUEST_SUCCESS:
    case nodesConstants.PULL_REPO_AND_CONVERGE_REQUEST_SUCCESS: {
      newState = state

      const nodeActionIndex = state.items.findIndex(
        item => item.tmpId === action.tmpNodeActionId
      )

      if (nodeActionIndex > -1) {
        return dotProp.set(
          newState,
          `items.${nodeActionIndex}.jid`,
          action.job.provider_job_id
        )
      }

      return state
    }
    case nodesConstants.BOOTSTRAP_REQUEST_FAILURE:
    case nodesConstants.CONVERGE_REQUEST_FAILURE:
    case nodesConstants.PULL_REPO_AND_CONVERGE_REQUEST_FAILURE: {
      const nodeActionIndex = state.items.findIndex(
        item => item.tmpId === action.tmpNodeActionId
      )

      if (nodeActionIndex > -1) {
        newState = dotProp.set(state, `items.${nodeActionIndex}.finished`, true)
        return dotProp.set(newState, `items.${nodeActionIndex}.failed`, true)
      }
      return state
    }
    case nodesConstants.FETCH_ONE_FOR_A_NODE_REQUEST:
      newState = dotProp.set(state, 'fetchOneFailed', false)
      return dotProp.set(newState, 'fetchingOne', true)
    case nodesConstants.FETCH_ALL_FOR_A_NODE_SUCCESS: {
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'items', action.nodeActions)
    }
    case nodesConstants.FETCH_ONE_FOR_A_NODE_SUCCESS: {
      newState = dotProp.set(state, 'fetchOneFailed', false)
      newState = dotProp.set(newState, 'fetchingOne', false)

      const { nodeActionStack } = action

      if (nodeActionStack === null) return newState

      const stackIndex = state.items.findIndex(
        stack => stack.id === nodeActionStack.id
      )

      /*
      ** Removes existing nodeActionStack.
      *  This happen when a temporary object (with a tmpId) is present, and the
      *  request that triggers this reducer has the full version.
      *  So we remove the temporary object before to insert the full version.
      */
      if (stackIndex > -1) {
        newState = dotProp.delete(newState, `items.${stackIndex}`)
      }

      return dotProp.set(
        newState,
        'items',
        [...newState.items, nodeActionStack]
      )
    }
    case nodesConstants.FETCH_ONE_FOR_A_NODE_FAILURE:
      newState = dotProp.set(state, 'fetchOneFailed', true)
      return dotProp.set(newState, 'fetchingOne', false)

    default:
      return state
  }
}

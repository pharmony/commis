import React from 'react'
import { connect } from 'react-redux'
import { ListGroup } from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import nodeActionStacksActions from './actions'
import NodeActionStack from './NodeActionStack'
import nodesActions from '../nodes/actions'
import NoNodeActionStacks from './NoNodeActionStacks'
import PageLoader from '../components/PageLoader'
import policiesActions from '../policies/actions'

class NodeActionList extends React.Component {
  state = {
    showLoading: true
  }

  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(nodesActions.fetchAll())
    dispatch(nodeActionStacksActions.fetchAll())
    dispatch(policiesActions.fetchAll())
  }

  componentDidUpdate = () => {
    const { actionsFetched, nodesFetched, policiesFetched } = this.props
    const { showLoading } = this.state

    if (actionsFetched && nodesFetched && policiesFetched && showLoading) {
      this.setState({
        showLoading: false
      })
    }
  }

  render = () => {
    const { nodeActionStacks } = this.props
    const { showLoading } = this.state

    if (showLoading) return <PageLoader message="Loading data ..." />

    if (!nodeActionStacks) return <NoNodeActionStacks />

    const sortedNodeActionStacks = nodeActionStacks.sort(
      (a, b) => a.created_at < b.created_at
    )

    return (
      <ListGroup className="flex-fill">
        {sortedNodeActionStacks.map(nodeActionStack => (
          <NodeActionStack actionStack={nodeActionStack} key={uuidv1()} />
        ))}
      </ListGroup>
    )
  }
}

const mapStateToProps = ({
  nodeActionStacks: {
    fetched: actionsFetched,
    items: nodeActionStacks
  },
  nodes: {
    fetched: nodesFetched
  },
  policies: {
    fetched: policiesFetched
  }
}) => ({
  actionsFetched,
  nodeActionStacks,
  nodesFetched,
  policiesFetched
})

NodeActionList.propTypes = {
  actionsFetched: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  nodeActionStacks: PropTypes.arrayOf(PropTypes.shape({
  })),
  nodesFetched: PropTypes.bool.isRequired,
  policiesFetched: PropTypes.bool.isRequired
}

NodeActionList.defaultProps = {
  nodeActionStacks: null
}

export default connect(mapStateToProps)(NodeActionList)

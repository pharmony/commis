import React from 'react'

export default () => (
  <div className="d-flex flex-column align-items-center justify-content-center h-100">
    <span className="lead text-center">
      No history yet.
    </span>
  </div>
)

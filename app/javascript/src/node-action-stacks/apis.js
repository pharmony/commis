import RestfulClient from 'restful-json-api-client'
import RailsHeaders from '../helpers/RailsHeaders'

export class NodeActionStacksApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'node_action_stacks', headers: RailsHeaders })
  }

  cancel = ({ id }) => this.request('POST', { path: `${id}/cancel` })
}

export class NodeActionStackNodeActionsApi extends RestfulClient {
  constructor(id) {
    super(`/api/node_action_stacks/${id}`, {
      resource: 'node_actions',
      headers: RailsHeaders
    })
  }
}

import React from 'react'
import { Card } from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import RunningCommandHeader from './Header'
import RunningCommandLog from './Log'

const RunningCommand = (props) => {
  const { nodeActionStack } = props

  return (
    <Card>
      <RunningCommandHeader
        nodeActionStack={nodeActionStack}
      />
      {nodeActionStack.nodeActions.map(nodeAction => (
        <RunningCommandLog
          key={uuidv1()}
          nodeAction={nodeAction}
          nodeActionStack={nodeActionStack}
        />
      ))}
    </Card>
  )
}

RunningCommand.propTypes = {
  nodeActionStack: PropTypes.shape({
    nodeActions: PropTypes.arrayOf(PropTypes.object).isRequired
  }).isRequired
}

export default RunningCommand

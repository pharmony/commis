import React from 'react'
import {
  Button,
  CardHeader
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import ConfirmCancelAction from './ConfirmCancelAction'
import ConfirmNodeBootstrap from '../../nodes/ConfirmNodeBootstrap'
import ConfirmNodeConverge from '../../nodes/ConfirmNodeConverge'
import ConfirmPullRepoAndNodeConverge from '../../nodes/NodeRepositories/ConfirmPullRepoAndNodeConverge'
import history from '../../helpers/History'
import ShowWhenStateIsIncluded from '../../components/ShowWhenStateIsIncluded'
import StatusBadge from '../../components/StatusBadge'
import StringHelpers from '../../helpers/StringHelpers'
import WithTrackedStatus from '../../components/hocs/withTrackedStatus'

class RunningCommandHeader extends React.Component {
  state = {
    cancelling: false,
    retryTmpId: null
  }

  componentDidUpdate = () => {
    const { nodeActionStacks } = this.props
    const { retryTmpId } = this.state

    if (retryTmpId === null) return

    const nodeActionStack = nodeActionStacks.find(
      item => item.tmpId === retryTmpId
    )

    if (nodeActionStack && nodeActionStack.id !== undefined) {
      history.push(`/node-action-stacks/${nodeActionStack.id}`)
    }
  }

  onConfirm = () => {
    this.setState({
      cancelling: true
    })
  }

  onRetry = (tmpId) => {
    this.setState({
      retryTmpId: tmpId
    })
  }

  handleRetry = () => {
    const {
      dispatch,
      nodeActionStack,
      nodes,
      policies,
      policyGroups,
      repositories
    } = this.props

    // Retrieves the original NodeAction which holds all the information
    const nodeAction = nodeActionStack.nodeActions.find(
      item => item.original === true
    )

    // Retrivies the node details
    const node = nodes.find(item => item.id === nodeAction.chef_node_id)

    // Retrivies the repository details
    const repository = repositories.find(
      item => item.id === nodeAction.chef_repository_id
    )

    switch (nodeActionStack.type) {
      case 'bootstrap': {
        const policy = policies.find(
          item => item.id === nodeAction.chef_policy_id
        )

        const policyGroup = policyGroups.find(
          item => item.id === nodeAction.chef_policy_group_id
        )

        ConfirmNodeBootstrap(
          dispatch,
          nodeAction,
          policy,
          policyGroup,
          this.onRetry
        )

        break
      }
      case 'converge': {
        if (nodeAction.pull_chef_repo) {
          ConfirmPullRepoAndNodeConverge(
            dispatch,
            node,
            repository,
            this.onRetry
          )
        } else {
          ConfirmNodeConverge(dispatch, node, repository, this.onRetry)
        }

        break
      }
      default:
        console.warn('Unknown node action name', nodeActionStack.type)
    }
  }

  render = () => {
    const { dispatch, nodeActionStack } = this.props
    const { cancelling } = this.state

    const ShowWhenTrackedStateIsIncluded = WithTrackedStatus(
      ShowWhenStateIsIncluded,
      'NodeActionStack',
      nodeActionStack.id
    )

    const TrackedStatus = WithTrackedStatus(
      StatusBadge,
      'NodeActionStack',
      nodeActionStack.id
    )

    return (
      <CardHeader className="d-flex justify-content-between align-items-center">
        <div>
          <TrackedStatus className="mr-3" />
          {StringHelpers.titleize(nodeActionStack.type)}
          &nbsp;action
        </div>
        <div>
          <ShowWhenTrackedStateIsIncluded
            states="running"
          >
            <Button
              color="danger"
              disabled={cancelling}
              onClick={() => {
                ConfirmCancelAction(dispatch, nodeActionStack.id, this.onConfirm)
              }}
            >
              {`Cancel${cancelling ? 'ling ...' : ''}`}
            </Button>
          </ShowWhenTrackedStateIsIncluded>
          <ShowWhenTrackedStateIsIncluded
            states={['success', 'failed']}
          >
            <Button
              color="primary"
              onClick={() => this.handleRetry()}
            >
              Retry
            </Button>
          </ShowWhenTrackedStateIsIncluded>
        </div>
      </CardHeader>
    )
  }
}

const mapStateToProps = ({
  nodes: {
    items: nodes
  },
  nodeActionStacks: {
    items: nodeActionStacks
  },
  policies: {
    items: policyItems
  },
  policyGroups: {
    fetched: policyGroupsFetched,
    items: policyGroups
  },
  repositories: {
    items: repositories
  }
}) => ({
  nodeActionStacks,
  nodes,
  policies: policyItems,
  policyGroups,
  policyGroupsFetched,
  repositories
})

RunningCommandHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nodeActionStack: PropTypes.shape({
    chef_node_id: PropTypes.string,
    chef_repository_id: PropTypes.string.isRequired,
    id: PropTypes.string,
    nodeActions: PropTypes.arrayOf(PropTypes.shape({
      chef_repository_id: PropTypes.string.isRequired,
      original: PropTypes.bool
    })),
    state: PropTypes.string,
    type: PropTypes.string
  }).isRequired,
  nodeActionStacks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    tmpId: PropTypes.string
  })).isRequired,
  nodes: PropTypes.arrayOf(PropTypes.object).isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })),
  policyGroups: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string
  })).isRequired,
  repositories: PropTypes.arrayOf(PropTypes.object).isRequired
}

RunningCommandHeader.defaultProps = {
  policies: []
}

export default connect(mapStateToProps)(RunningCommandHeader)

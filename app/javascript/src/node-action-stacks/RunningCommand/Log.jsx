import React from 'react'
import { connect } from 'react-redux'
import {
  Button,
  CardBody,
  Spinner
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

import ConfirmRerunNodeAction from './ConfirmRerunNodeAction'
import NodeActionHelpers from '../../helpers/NodeActionHelpers'
import nodeActionsActions from '../../node-actions/actions'
import nodesActions from '../../nodes/actions'
import PageLoader from '../../components/PageLoader'
import policiesActions from '../../policies/actions'
import policyGroupsActions from '../../policy-groups/actions'
import ShowWhenStateIsIncluded from '../../components/ShowWhenStateIsIncluded'
import StatusBadge from '../../components/StatusBadge'
import WithTrackedStatus from '../../components/hocs/withTrackedStatus'

class RunningCommandLog extends React.Component {
  state = {
    throughput: 0,
    timeout: null
  }

  componentDidMount = () => {
    this.fetchNeededData()

    if (this.nodeActionIsRunning()) {
      // Each seconds reset the throughput
      const timeout = setTimeout(this.resetThroughputAndRecreateTimeout, 3000)
      this.setState({
        timeout
      })
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { throughput } = this.state
    const { throughput: prevThroughput } = prevState

    console.log('[RunningCommandLog](componentDidUpdate) throughput', throughput)

    if (throughput === prevThroughput) {
      this.scrollToBottom()

      if (this.nodeActionIsRunning()) {
        this.increaseThroughput()
      } else {
        this.stopRefreshTimeout()
      }
    }
  }

  componentWillUnmount = () => {
    this.stopRefreshTimeout()
  }

  shouldComponentUpdate = () => {
    const { throughput, timeout } = this.state

    if (timeout === null) {
      return true
    }

    // If the throughput is lower than 10 log lines per seconds, refresh the
    // page, otherwise wait for the throughput reset.
    if (throughput < 5) {
      return true
    }

    return false
  }

  fetchNeededData = () => {
    const { nodeAction } = this.props

    this.fetchNodesIfNeeded()
    this.fetchNodeActionLogs()

    if (nodeAction.name === 'bootstrap') this.fetchBootstrapDependencies()
  }

  fetchBootstrapDependencies = () => {
    const {
      dispatch,
      policiesFetched,
      policyGroupsFetched
    } = this.props

    if (policiesFetched === false) dispatch(policiesActions.fetchAll())
    if (policyGroupsFetched === false) dispatch(policyGroupsActions.fetchAll())
  }

  fetchNodeActionLogs = () => {
    const { dispatch, nodeAction } = this.props

    dispatch(nodeActionsActions.fetchLogs(nodeAction.id))
  }

  fetchNodesIfNeeded = () => {
    const { dispatch, nodesFetched } = this.props

    if (nodesFetched === false) dispatch(nodesActions.fetchAll())
  }

  nodeActionIsRunning = () => {
    const { nodeAction, statuses } = this.props

    const object = statuses.find(
      item => item.klass === 'NodeAction' && item.id === nodeAction.id
    )

    return (['new', 'running'].includes(object.state))
  }

  increaseThroughput = () => {
    const { throughput } = this.state

    this.setState({
      throughput: throughput + 1
    })
  }

  scrollToBottom = () => {
    const { nodeActionStack, statuses } = this.props

    const object = statuses.find(
      item => item.klass === 'NodeActionStack' && item.id === nodeActionStack.id
    )

    if (object && object.state === 'running' && this.bottomLogs) {
      this.bottomLogs.scrollIntoView({ behavior: 'smooth' })
    }
  }

  stopRefreshTimeout = () => {
    const { timeout } = this.state

    if (timeout) clearTimeout(timeout)
  }

  resetThroughputAndRecreateTimeout = () => {
    const timeout = setTimeout(this.resetThroughputAndRecreateTimeout, 3000)

    this.setState({
      throughput: 0,
      timeout
    })
  }

  render = () => {
    console.log('[RunningCommandLog](render) ...')

    const {
      dispatch,
      nodeAction, // nodeAction given to this component as a prop
      nodeActions, // nodeActions from the store which holds the logs
      nodes,
      nodesFetched,
      policies,
      policyGroups,
      statuses
    } = this.props

    if (!nodesFetched) return <PageLoader message="Loading nodes ..." />

    const currentNode = nodes.find(node => node.id === nodeAction.chef_node_id)

    const currentPolicy = policies.find(
      policy => policy.id === nodeAction.chef_policy_id
    )

    const currentGroup = policyGroups.find(
      group => group.id === nodeAction.chef_policy_group_id
    )


    const object = statuses.find(
      item => item.klass === 'NodeAction' && item.id === nodeAction.id
    )

    let nodeActionLogs = null

    if (object && ['new', 'unrunnable'].includes(object.state) === false) {
      nodeActionLogs = nodeActions.find(item => item.id === nodeAction.id)

      if (!nodeActionLogs || (nodeActionLogs && nodeActionLogs.logs === undefined)) {
        return <PageLoader message="Loading node action logs ..." />
      }
    }

    const ShowWhenTrackedStateIsIncluded = WithTrackedStatus(
      ShowWhenStateIsIncluded,
      'NodeAction',
      nodeAction.id
    )

    const TrackedStatus = WithTrackedStatus(
      StatusBadge,
      'NodeAction',
      nodeAction.id
    )

    return (
      <CardBody>
        <div className="d-flex flex-row justify-content-between">
          <div className="d-flex flex-row align-items-center">
            <TrackedStatus className="mr-3" />
            {NodeActionHelpers.buildTitleFrom(nodeAction, {
              node: currentNode,
              policy: currentPolicy,
              group: currentGroup
            })}
          </div>
          <div className="d-flex flex-column">
            <ShowWhenTrackedStateIsIncluded
              states={['failed', 'success']}
            >
              <Button
                onClick={() => {
                  ConfirmRerunNodeAction(
                    dispatch,
                    nodeAction,
                    currentPolicy,
                    currentGroup
                  )
                }}
              >
                Re-run
              </Button>
            </ShowWhenTrackedStateIsIncluded>
          </div>
        </div>
        <pre className="d-flex flex-column">
          {nodeAction.query_result_empty && (
            <ShowWhenTrackedStateIsIncluded
              states="unrunnable"
            >
              The search query didn&apos;t returned any results.
            </ShowWhenTrackedStateIsIncluded>
          )}
          {nodeActionLogs && nodeActionLogs.logs.map(log => (
            <code className="d-flex text-wrap multiline" key={uuidv1()}>
              {log.message === '' ? '\u00a0' : log.message}
            </code>
          ))}
          <ShowWhenTrackedStateIsIncluded
            states="running"
          >
            <Spinner type="grow" color="#ffffff" />
          </ShowWhenTrackedStateIsIncluded>
        </pre>
        <ShowWhenTrackedStateIsIncluded
          states="running"
        >
          <div
            ref={(el) => { this.bottomLogs = el }}
          />
        </ShowWhenTrackedStateIsIncluded>
      </CardBody>
    )
  }
}

const mapStateToProps = ({
  nodeActions: { items: nodeActionLogs },
  nodes: {
    fetched: nodesFetched,
    items: nodes
  },
  policies: {
    fetched: policiesFetched,
    items: policies
  },
  policyGroups: {
    fetched: policyGroupsFetched,
    items: policyGroups
  },
  statuses: { items: statuses }
}) => ({
  nodeActions: nodeActionLogs,
  nodes,
  nodesFetched,
  policiesFetched,
  policies,
  policyGroupsFetched,
  policyGroups,
  statuses
})

RunningCommandLog.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nodeAction: PropTypes.shape({
    chef_node_id: PropTypes.string,
    chef_policy_group_id: PropTypes.string,
    chef_policy_id: PropTypes.string,
    policy_hook: PropTypes.shape({
      _type: PropTypes.string.isRequired
    }),
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    query_result_empty: PropTypes.bool.isRequired,
    state: PropTypes.string.isRequired
  }).isRequired,
  nodeActions: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    logs: PropTypes.arrayOf(PropTypes.shape({
      message: PropTypes.string
    }))
  })).isRequired,
  nodeActionStack: PropTypes.shape({
    id: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired
  }).isRequired,
  nodes: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired,
  nodesFetched: PropTypes.bool.isRequired,
  policiesFetched: PropTypes.bool.isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired,
  policyGroupsFetched: PropTypes.bool.isRequired,
  policyGroups: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired,
  statuses: PropTypes.arrayOf(PropTypes.shape({
    klass: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired
  })).isRequired
}

export default connect(mapStateToProps)(RunningCommandLog)

import confirmModal from 'reactstrap-confirm'
import PropTypes from 'prop-types'

import nodeActionStacksActions from '../actions'

const ConfirmCancelAction = async (dispatch, nodeActionStackId, onConfirm) => {
  const confirm = await confirmModal({
    title: 'Cancel running action ?',
    message: 'Are you sure you want to cancel the running action?',
    confirmColor: 'danger',
    confirmText: 'Confirm'
  })

  if (confirm) {
    onConfirm()
    dispatch(nodeActionStacksActions.cancelNodeActionStack(nodeActionStackId))
  }
}

ConfirmCancelAction.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nodeActionStackId: PropTypes.string.isRequired
}

export default ConfirmCancelAction

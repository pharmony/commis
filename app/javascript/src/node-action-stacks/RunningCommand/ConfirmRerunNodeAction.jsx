import React from 'react'
import { Badge } from 'reactstrap'
import confirmModal from 'reactstrap-confirm'

import nodeActionsActions from '../../node-actions/actions'

const ConfirmRerunNodeAction = async (dispatch, nodeAction, policy, group) => {
  let message = ''

  if (nodeAction.name === 'bootstrap') {
    message = (
      <div>
        Are you sure you want run again this&nbsp;
        <Badge className="fs-100">{nodeAction.name}</Badge>
        &nbsp;action of the&nbsp;
        <Badge className="fs-100">{policy.name}</Badge>
        &nbsp;policy with the&nbsp;
        <Badge className="fs-100">{group.name}</Badge>
        ?
      </div>
    )
  } else {
    message = `Are you sure you want run again this ${nodeAction.name} action?`
  }

  const confirm = await confirmModal({
    title: `Run again this ${nodeAction.name} action?`,
    message,
    confirmColor: 'primary',
    confirmText: 'Run again'
  })

  if (confirm) {
    dispatch(nodeActionsActions.reRun(nodeAction.id))
  }
}

export default ConfirmRerunNodeAction

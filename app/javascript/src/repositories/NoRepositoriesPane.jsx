import React from 'react'
import { NavLink } from 'react-router-dom'

const NoRepositoriesPane = () => (
  <div className="d-flex flex-column align-items-center justify-content-center h-100">
    <p className="lead">
      Commis needs at least one Chef repository but
      you currently have no imported chef repository.
    </p>
    <p>
      Let&lsquo;s get started and
      {' '}
      {
        <NavLink to="/repositories/import">
          import your chef repository
        </NavLink>
      }
      {' '}
      now!
      If you don&lsquo;t have a chef repository, please have a look at
      {' '}
      {
        <a href="https://docs.chef.io/chef_repo.html" rel="noopener noreferrer" target="_blank">
          About the chef-repo
        </a>
      }
      {' '}
      documentation.
    </p>
  </div>
)

export default NoRepositoriesPane

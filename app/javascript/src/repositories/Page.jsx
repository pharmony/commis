import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AlertMessage from '../components/AlertMessage'
import PageLoader from '../components/PageLoader'
import Repositories from './Repositories'
import repositoriesActions from './actions'
import NoRepositoriesPane from './NoRepositoriesPane'

class ChefRepositoriesPage extends React.Component {
  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(repositoriesActions.fetchAll())
  }

  render = () => {
    const { repositories } = this.props
    const {
      fetchingAll,
      fetchAllFailed,
      items
    } = repositories

    const noRepo = fetchingAll === false && fetchAllFailed === false
      && items.length === 0

    return (
      <div className={`d-flex flex-column ${noRepo ? 'h-100' : ''}`}>
        {fetchingAll && (
          <PageLoader message="Loading chef repositories ..." />
        )}

        {fetchAllFailed && (
          <AlertMessage>
            Unfortunately an error occured while fetching the Chef repositories.
            Please try again.
          </AlertMessage>
        )}

        {noRepo && (
          <NoRepositoriesPane />
        )}

        {fetchingAll === false && fetchAllFailed === false && items.length > 0 && (
          <Repositories repositories={items} />
        )}
      </div>
    )
  }
}

const mapStateToProps = ({ repositories }) => ({ repositories })

ChefRepositoriesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  repositories: PropTypes.shape({
    fetchingAll: PropTypes.bool.isRequired,
    fetchAllFailed: PropTypes.bool.isRequired,
    items: PropTypes.arrayOf(PropTypes.object).isRequired
  }).isRequired
}

export default connect(mapStateToProps)(ChefRepositoriesPage)

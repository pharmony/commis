import React from 'react'
import { ListGroup } from 'reactstrap'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import uuidv1 from 'uuid'

import Repository from './Repository'

const Repositories = ({ repositories }) => (
  <React.Fragment>
    <div className="d-flex flex-column align-items-end">
      <NavLink
        className="btn btn-primary"
        color="primary"
        to="/repositories/import"
      >
        Add repository
      </NavLink>
    </div>
    <ListGroup className="mt-3">
      {repositories.map(repository => (
        <Repository
          key={uuidv1()}
          repository={repository}
        />
      ))}
    </ListGroup>
  </React.Fragment>
)

Repositories.propTypes = {
  repositories: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Repositories

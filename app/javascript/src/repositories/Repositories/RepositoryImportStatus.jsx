import React from 'react'
import PropsTypes from 'prop-types'

import PageLoader from '../../components/PageLoader'

const RepositoryImportStatus = ({
  repository: { import_state: importState, pull_state: pullState }
}) => {
  if (pullState === 'pulling') {
    return <span>Pulling ...</span>
  }

  if (pullState === 'pull_failed') {
    return <span>Failed to pull the repository!</span>
  }

  if (importState === 'importing') {
    return <PageLoader message="Importing ..." />
  }

  if (importState === 'import_failed') {
    return <span>Import failed</span>
  }

  return (
    <span>
      {`Unknown import ${importState} and pull ${pullState} statuses`}
    </span>
  )
}

RepositoryImportStatus.propTypes = {
  repository: PropsTypes.shape({
    import_state: PropsTypes.string.isRequired,
    pull_state: PropsTypes.string
  }).isRequired
}

export default RepositoryImportStatus

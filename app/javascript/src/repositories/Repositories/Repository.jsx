import React from 'react'
import {
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Media
} from 'reactstrap'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

import GitLogoHelper from '../../helpers/GitLogoHelper'
import RepositoryImportStatus from './RepositoryImportStatus'
import RepositoryNodeCount from './RepositoryNodeCount'

class Repository extends React.Component {
  isImportingOrPulling = () => {
    const { repository } = this.props

    if (repository.import_state === 'importing') return true

    if (repository.pull_state === 'pulling') return true

    return false
  }

  render = () => {
    const { repository } = this.props

    return (
      <ListGroupItem>
        <Media className="align-items-center">
          <Media left className="mr-3">
            <Media
              className="img-md"
              object
              src={GitLogoHelper.fromUrl(repository.url)}
            />
          </Media>
          <Media body>
            <ListGroupItemHeading>
              <NavLink to={`/repositories/${repository.id}`}>
                {repository.name}
              </NavLink>
            </ListGroupItemHeading>
            <ListGroupItemText className="mb-0">
              {repository.url}
            </ListGroupItemText>
          </Media>
          <Media body>
            {this.isImportingOrPulling() ? (
              <RepositoryImportStatus repository={repository} />
            ) : (
              <ListGroupItemText className="mb-0">
                <RepositoryNodeCount count={repository.node_count} />
              </ListGroupItemText>
            )}
          </Media>
        </Media>
      </ListGroupItem>
    )
  }
}

Repository.propTypes = {
  repository: PropTypes.shape({
    created_at: PropTypes.string,
    id: PropTypes.string,
    import_state: PropTypes.string.isRequired,
    name: PropTypes.string,
    node_count: PropTypes.number,
    pull_state: PropTypes.string,
    url: PropTypes.string.isRequired
  }).isRequired
}

export default Repository

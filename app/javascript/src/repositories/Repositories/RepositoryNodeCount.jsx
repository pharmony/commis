import React from 'react'
import PropsTypes from 'prop-types'

const RepositoryNodeCount = ({ count }) => (
  <span>{`Provisioning ${count} node${count > 1 ? 's' : ''}`}</span>
)

RepositoryNodeCount.propTypes = {
  count: PropsTypes.number.isRequired
}

export default RepositoryNodeCount

import dotProp from 'dot-prop-immutable'

import actionCableConstants from '../action-cable/constants'
import chefRepositoryImportConstants from './import/constants'
import repositoriesConstants from './constants'

const initialState = {
  fetchAllFailed: false, /* True when something wrong happened while fetching
                            repositories */
  fetchingAll: false, /* True while fetching all repositories from the backend
                         server */
  importing: false, /* True while importing a repository */
  itemCount: 0, /* Cached amount of repositories */
  items: [] /* Repository object list */
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case actionCableConstants.DATA_RECEIVED:
      if (action.data) {
        if (action.data.repositories) {
          const { action: repoAction, repository } = action.data.repositories
          switch (repoAction) {
            case 'add': {
              // Removes the pre-built repo
              const preBuiltIndex = state.items.findIndex(
                item => item.url === repository.url
              )

              if (preBuiltIndex > -1) {
                newState = dotProp.delete(state, `items.${preBuiltIndex}`)
              } else {
                newState = state
              }

              // Add the real repo
              newState = dotProp.set(
                newState,
                'itemCount',
                newState.items.length + 1
              )

              return dotProp.set(
                newState,
                'items',
                [...newState.items, repository]
              )
            }
            case 'update': {
              const { id } = repository
              const repoIndex = state.items.findIndex(item => item.id === id)

              if (repoIndex === -1) return state

              return dotProp.set(state, `items.${repoIndex}`, repository)
            }
            default:
              console.warn(
                `[repositories reducer] Unknown action ${repoAction}.`
              )
              return state
          }
        }
      }
      return state

    case chefRepositoryImportConstants.IMPORT_REQUEST:
      return dotProp.set(state, 'importing', true)
    case chefRepositoryImportConstants.IMPORT_FAILURE:
      return dotProp.set(state, 'importing', false)
    case chefRepositoryImportConstants.IMPORT_SUCCESS:
      newState = dotProp.set(state, 'itemCount', state.items.length + 1)
      return dotProp.set(
        newState,
        'items',
        items => [...items, action.repository]
      )

    case repositoriesConstants.FETCH_ALL_REQUEST:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      return dotProp.set(newState, 'fetchingAll', true)
    case repositoriesConstants.FETCH_ALL_SUCCESS:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetchingAll', false)

      /* When importing a repo, we show a pre-built repository, but the
      ** repository is not yet created, so the repositories list from the server
      *  is empty.
      */
      if (state.importing && action.repositories.length === 0) {
        /* In this case we keep our pre-built repository that will be updated a
        ** little bit later (See the actionCableConstants.DATA_RECEIVED part of
        *  this file)
        */
        newState = dotProp.set(newState, 'itemCount', 1)
        const importingRepositories = state.items.filter(
          item => item.import_state === 'importing'
        )
        return dotProp.set(newState, 'items', importingRepositories)
      }

      newState = dotProp.set(newState, 'itemCount', action.repositories.length)
      return dotProp.set(newState, 'items', action.repositories)
    case repositoriesConstants.FETCH_ALL_FAILURE:
      newState = dotProp.set(state, 'fetchAllFailed', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      newState = dotProp.set(newState, 'itemCount', 0)
      return dotProp.set(newState, 'items', [])

    case repositoriesConstants.DESTROY_SUCCESS: {
      const repositoryIndex = state.items.findIndex(item => (
        item.id === action.repositoryId
      ))

      if (repositoryIndex === -1) return state

      return dotProp.delete(state, `items.${repositoryIndex}`)
    }
    default:
      return state
  }
}

import RestfulClient from 'restful-json-api-client'
import RailsHeaders from '../helpers/RailsHeaders'

export default class RepositoriesApi extends RestfulClient {
  constructor() {
    super('/api/chef', { resource: 'repositories', headers: RailsHeaders })
  }

  pull = id => this.request('POST', { path: `${id}/pull` })
}

import ApiUtils from '../helpers/ApiUtils'
import RepositoriesApi from './api'

const destroy = id => (
  new RepositoriesApi()
    .destroy(id)
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchAll = () => (
  new RepositoriesApi()
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const pull = id => (
  new RepositoriesApi()
    .pull(id)
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  destroy,
  fetchAll,
  pull
}

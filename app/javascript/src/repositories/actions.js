import repositoriesConstants from './constants'
import repositoriesService from './services'

import alertActions from '../alerts/actions'

const destroy = (id) => {
  const request = repositoryId => ({
    repositoryId,
    type: repositoriesConstants.DESTROY_REQUEST
  })
  const success = repositoryId => ({
    repositoryId,
    type: repositoriesConstants.DESTROY_SUCCESS
  })
  const failure = error => ({
    error,
    type: repositoriesConstants.DESTROY_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id))

    repositoriesService.destroy(id)
      .then(() => dispatch(success(id)))
      .catch((error) => {
        dispatch(failure(error))
        dispatch(alertActions.error(`Delete failed: ${error.message}`))
      })
  }
}

const fetchAll = () => {
  const request = () => ({ type: repositoriesConstants.FETCH_ALL_REQUEST })
  const success = repositories => ({
    repositories,
    type: repositoriesConstants.FETCH_ALL_SUCCESS
  })
  const failure = error => ({
    error,
    type: repositoriesConstants.FETCH_ALL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    repositoriesService.fetchAll()
      .then(repositories => dispatch(success(repositories)))
      .catch(error => dispatch(failure(error)))
  }
}

const pull = (repositoryId) => {
  const request = () => ({ type: repositoriesConstants.PULL_REQUEST })
  const success = () => ({ type: repositoriesConstants.PULL_SUCCESS })
  const failure = error => ({
    error,
    type: repositoriesConstants.PULL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    repositoriesService.pull(repositoryId)
      .then(() => dispatch(success()))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  destroy,
  fetchAll,
  pull
}

import dotProp from 'dot-prop-immutable'

import chefRepositoryImportConstants from './constants'

const importInitialState = {
  importErrorMessage: null,
  importFailed: false,
  importing: false,
  importUrl: null
}

export default (state = importInitialState, action) => {
  let newState = null

  switch (action.type) {
    case chefRepositoryImportConstants.SSH_KEY_REQUEST:
      return importInitialState
    case chefRepositoryImportConstants.IMPORT_REQUEST:
      newState = dotProp.set(state, 'importErrorMessage', null)
      newState = dotProp.set(newState, 'importFailed', false)
      newState = dotProp.set(newState, 'importing', true)
      return dotProp.set(newState, 'importUrl', action.gitUrl)
    case chefRepositoryImportConstants.IMPORT_SUCCESS:
      newState = dotProp.set(state, 'importErrorMessage', null)
      newState = dotProp.set(newState, 'importFailed', false)
      return dotProp.set(newState, 'importing', false)
    case chefRepositoryImportConstants.IMPORT_FAILURE:
      newState = dotProp.set(state, 'importErrorMessage', action.error.message)
      newState = dotProp.set(newState, 'importFailed', true)
      return dotProp.set(newState, 'importing', false)
    default:
      return state
  }
}

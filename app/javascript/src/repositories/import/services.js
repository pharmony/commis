import ApiUtils from '../../helpers/ApiUtils'
import RepositoriesApi from '../api'

const importRepository = gitUrl => (
  new RepositoriesApi()
    .create({ git_url: gitUrl })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  importRepository
}

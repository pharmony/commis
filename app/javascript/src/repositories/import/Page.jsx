import React from 'react'
import {
  Alert,
  Button,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Jumbotron,
  Label,
  Spinner
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AppSshPublicSshKey from '../../components/AppSshPublicSshKey'
import chefRepositoryImportActions from './actions'

class ChefRepositoryImportPage extends React.Component {
  state = {
    gitUrl: '',
    submitted: false
  }

  handleGitUrlChange = (event) => {
    this.setState({
      gitUrl: event.target.value
    })
  }

  importChefRepo = (event) => {
    const { dispatch } = this.props
    const { gitUrl } = this.state

    event.preventDefault()

    this.setState({
      submitted: true
    })

    if (!gitUrl) { return }

    dispatch(chefRepositoryImportActions.importRepository(gitUrl))
  }

  render() {
    const {
      importErrorMessage,
      importFailed,
      importing
    } = this.props

    const { gitUrl, submitted } = this.state

    return (
      <div className="d-flex flex-column justify-content-center h-100">
        <div className="d-flex flex-column align-items-center">
          <Jumbotron className="w-100">
            <h1>Import your chef repository</h1>
            <p className="lead mb-0">
              Before to proceed with the import you have to setup a user to
              allow commis to access your chef repo.
            </p>
            <p>
              <em>Please ignore this phase if you did it already.</em>
            </p>
            <hr className="my-2" />
            <ol className="w-100">
              <li>Create a user in your SCM platform (Gitlab, Github, ...)</li>
              <li>Allow this user to access your chef repository (read/write)</li>
              <li>Add the following SSH key to that account:</li>
              <AppSshPublicSshKey />
            </ol>

            <Form>
              <FormGroup>
                <Label for="chef-repo-git-url">Git URL (SSH format)</Label>
                <Input
                  id="chef-repo-git-url"
                  invalid={submitted && !gitUrl}
                  name="git-url"
                  onChange={event => this.handleGitUrlChange(event)}
                  placeholder="Enter the Git URL I.e: git@gitlab.com:johndoe/chef.git"
                  value={gitUrl}
                />
                {submitted && !gitUrl && (
                  <FormFeedback>Git URL is required</FormFeedback>
                )}
              </FormGroup>
              <FormGroup>
                {importing && (
                  <div className="d-flex">
                    <Spinner type="grow" color="primary" />
                    <span className="d-flex flex-column justify-content-center ml-3">
                      Importing the Chef Repository ...
                    </span>
                  </div>
                )}

                {importFailed && (
                  <Alert color="danger">
                    <p>
                      Unfortunately an error occured while tying to import your
                      chef repository:
                    </p>
                    <span>{importErrorMessage}</span>
                  </Alert>
                )}
              </FormGroup>
              <Button
                className="float-right"
                color="primary"
                disabled={importing}
                onClick={event => this.importChefRepo(event)}
                type="submit"
              >
                Import
              </Button>
            </Form>
          </Jumbotron>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({
  importChefRepo: {
    importErrorMessage,
    importFailed,
    importing
  }
}) => ({
  importErrorMessage,
  importFailed,
  importing
})

ChefRepositoryImportPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  importErrorMessage: PropTypes.string,
  importFailed: PropTypes.bool.isRequired,
  importing: PropTypes.bool.isRequired
}

ChefRepositoryImportPage.defaultProps = {
  importErrorMessage: null
}

export default connect(mapStateToProps)(ChefRepositoryImportPage)

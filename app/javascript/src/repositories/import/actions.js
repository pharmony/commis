import moment from 'moment'

import alertActions from '../../alerts/actions'
import chefRepositoryImportConstants from './constants'
import chefRepositoryImportService from './services'
import history from '../../helpers/History'

const importRepository = (gitImportUrl) => {
  const request = gitUrl => ({
    gitUrl,
    type: chefRepositoryImportConstants.IMPORT_REQUEST
  })
  const success = repository => ({
    repository,
    type: chefRepositoryImportConstants.IMPORT_SUCCESS
  })
  const failure = error => ({
    error,
    type: chefRepositoryImportConstants.IMPORT_FAILURE
  })

  return (dispatch) => {
    dispatch(request(gitImportUrl))

    chefRepositoryImportService.importRepository(gitImportUrl)
      .then((activeJob) => {
        dispatch(success({
          created_at: moment().format(),
          import_state: 'importing',
          job_id: activeJob.provider_job_id,
          url: activeJob.arguments[0]
        }))
        history.push('/repositories')
        dispatch(alertActions.success(
          'Chef repository import successfully requested!'
        ))
      })
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  importRepository
}

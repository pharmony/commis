export default {
  IMPORT_REQUEST: 'CHEF_REPOSITORY_IMPORT_REQUEST',
  IMPORT_SUCCESS: 'CHEF_REPOSITORY_IMPORT_SUCCESS',
  IMPORT_FAILURE: 'CHEF_REPOSITORY_IMPORT_FAILURE'
}

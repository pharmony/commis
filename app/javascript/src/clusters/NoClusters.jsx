import React from 'react'

export default () => (
  <div className="d-flex flex-column align-items-center align-self-center w-50">
    <p>
      You have no cluster yet.
      You can create one using the &quot;Add cluster&quot; button.
    </p>
  </div>
)

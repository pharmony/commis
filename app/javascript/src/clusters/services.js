import _ from 'lodash'
import ApiUtils from '../helpers/ApiUtils'
import ClustersApi from './api'

const create = attributes => (
  new ClustersApi()
    .create({
      name: attributes.clusterName,
      country_code: attributes.clusterCountryCode
    })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const destroy = clusterId => (
  new ClustersApi()
    .destroy(clusterId)
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchAll = () => (
  new ClustersApi()
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const update = (id, attributes) => (
  new ClustersApi()
    .update(id, _.mapKeys(attributes, (v, k) => _.snakeCase(k)))
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  create,
  destroy,
  fetchAll,
  update
}

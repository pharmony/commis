import React from 'react'
import {
  Button
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import ClusterAttributes from './ClusterAttributes'

import clusterActions from '../actions'

class ClusterSettings extends React.Component {
  state = {
    clusterName: '',
    countryCode: '',
    updated: false
  }

  componentDidMount = () => this.initialiseInputValues()

  componentDidUpdate = () => this.initialiseInputValues()

  initialiseInputValues = () => {
    const { cluster } = this.props

    const {
      country_code: countryCode,
      name
    } = cluster

    const { updated } = this.state

    if (updated) return

    this.setState({
      countryCode,
      clusterName: name,
      updated: true
    })
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({
      [name]: value
    })
  }

  updateCluster = () => {
    const { dispatch, cluster } = this.props
    const {
      countryCode,
      clusterName
    } = this.state

    dispatch(clusterActions.updateCluster(cluster.id, {
      countryCode,
      name: clusterName
    }))
  }

  render = () => {
    const {
      updateFailure,
      updateSuccess,
      updating
    } = this.props

    const {
      countryCode,
      clusterName,
      updated
    } = this.state

    if (!updated) return <p>Waiting ...</p>

    return (
      <React.Fragment>
        <ClusterAttributes
          clusterName={clusterName}
          countryCode={countryCode}
          onInputChange={event => this.handleInputChange(event)}
          updating={updating}
        />

        {updateFailure && (
          <span className="d-flex text-danger">
            Something prevented to update this node.
          </span>
        )}
        {updateSuccess && (
          <span className="d-flex text-success">
            Node successfully updated!
          </span>
        )}


        <div className="d-flex flex-row justify-content-end">
          <Button
            color="primary"
            onClick={event => this.updateCluster(event)}
            type="submit"
          >
            Update
          </Button>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  clusters: { updating, updateFailure, updateSuccess }
}) => ({
  updateFailure,
  updateSuccess,
  updating
})

ClusterSettings.propTypes = {
  dispatch: PropTypes.func.isRequired,
  cluster: PropTypes.shape({
    country_code: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string
  }).isRequired,
  updateFailure: PropTypes.bool.isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updating: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(ClusterSettings)

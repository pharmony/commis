import React from 'react'
import {
  Col,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'
import ReactFlagsSelect from 'react-flags-select'

class ClusterAttributes extends React.Component {
  state = {
    clusterCountryCode: '',
    clusterName: '',
    clusterNameFeedback: null,
    loaded: false
  }

  componentDidMount = () => {
    const {
      countryCode,
      clusterName
    } = this.props

    this.setState({
      clusterCountryCode: countryCode,
      clusterName,
      loaded: true
    })
  }

  handleClusterCountryChange = (countryCode) => {
    const { onInputChange } = this.props

    this.setState({
      clusterCountryCode: countryCode
    }, () => {
      this.ref.toggleOptions()
      onInputChange({ target: { name: 'countryCode', value: countryCode }})
    })
  }

  handleNameChange = (event) => {
    const { target: { value } } = event
    const { onInputChange } = this.props

    this.setState({
      clusterName: value,
      clusterNameFeedback: null
    }, onInputChange(event))
  }

  updateClusterNameFeedback = (feedback) => {
    this.setState({
      clusterNameFeedback: feedback
    })
  }

  render = () => {
    const {
      updating
    } = this.props

    const {
      clusterCountryCode,
      clusterName,
      clusterNameFeedback,
      loaded
    } = this.state

    if (loaded === false) return <p>Loading ...</p>

    return (
      <React.Fragment>
        <p className="h4">Cluster</p>
        <div className="d-flex flex-row justify-content-between">
          <Form className="flex-fill">
            <FormGroup row>
              <Label for="cluster_name" sm={4}>Name</Label>
              <Col sm={8}>
                <Input
                  disabled={updating}
                  id="cluster_name"
                  invalid={clusterNameFeedback}
                  name="clusterName"
                  onChange={event => this.handleNameChange(event)}
                  type="text"
                  value={clusterName}
                />
                {clusterNameFeedback && (
                <FormFeedback>{clusterNameFeedback}</FormFeedback>)}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={4}>Country</Label>
              <Col sm={8}>
                <ReactFlagsSelect
                  className="flag-select-in-form"
                  defaultCountry={clusterCountryCode}
                  onSelect={code => this.handleClusterCountryChange(code)}
                  ref={(ref) => { this.ref = ref }}
                  searchable
                />
              </Col>
            </FormGroup>
          </Form>
        </div>
      </React.Fragment>
    )
  }
}

ClusterAttributes.propTypes = {
  clusterName: PropTypes.string.isRequired,
  countryCode: PropTypes.string.isRequired,
  onInputChange: PropTypes.func.isRequired,
  updating: PropTypes.bool.isRequired
}

export default ClusterAttributes

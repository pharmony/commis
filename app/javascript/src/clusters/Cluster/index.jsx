import React from 'react'
import PropTypes from 'prop-types'
import { Card } from 'reactstrap'

import ClusterHeader from './ClusterHeader'

const Cluster = ({ cluster }) => (
  <Card className="flex-fill mr-4 mb-4">
    <ClusterHeader
      cluster={cluster}
      showPaneOnClickingName
      small
      withinACard
    />
  </Card>
)

Cluster.propTypes = {
  cluster: PropTypes.object.isRequired
}

export default Cluster

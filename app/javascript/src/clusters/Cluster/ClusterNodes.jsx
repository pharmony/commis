import React from 'react'
import { connect } from 'react-redux'
import {
  ListGroup,
  ListGroupItem
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import PageLoader from '../../components/PageLoader'
import nodeActions from '../../nodes/actions'
import NodeHeader from '../../nodes/Node/NodeHeader'

class ClusterNodes extends React.Component {
  componentDidMount = () => {
    const { dispatch, nodeFetched } = this.props

    if (nodeFetched === false) {
      dispatch(nodeActions.fetchAll())
    }
  }

  render = () => {
    const { cluster, nodeFetched, nodes } = this.props

    const clusterNodes = nodes.filter(
      node => node.cluster_id === cluster.id
    ).sort((a, b) => a.name.localeCompare(b.name))

    if (nodeFetched === false) return <PageLoader message="Loading nodes ..." />

    return (
      <React.Fragment>
        <p className="h4">
          Nodes (
          {clusterNodes.length}
          )
        </p>
        <br />
        <ListGroup>
          {clusterNodes.map(node => (
            <ListGroupItem key={uuidv1()}>
              <NodeHeader
                node={node}
                size="small"
              />
            </ListGroupItem>
          ))}
        </ListGroup>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  nodes: { fetched, items: nodes }
}) => ({
  nodeFetched: fetched, nodes
})

ClusterNodes.propTypes = {
  cluster: PropTypes.shape({
    country_code: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  nodeFetched: PropTypes.bool.isRequired,
  nodes: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired
}

export default connect(mapStateToProps)(ClusterNodes)

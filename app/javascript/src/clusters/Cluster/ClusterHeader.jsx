import React from 'react'
import { connect } from 'react-redux'
import {
  Button,
  CardBody,
  Media
} from 'reactstrap'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'
import ReactFlagsSelect from 'react-flags-select'

import NodeHeaderName from './ClusterHeaderName'
import clustersActions from '../actions'

class ClusterHeader extends React.Component {
  state = {
    clusterCountryCode: '',
    clusterName: '',
    loaded: false,
    updating: false
  }

  componentDidMount = () => {
    const { cluster } = this.props

    this.updateStateFrom(cluster)
  }

  componentDidUpdate = () => {
    const { cluster, clusters } = this.props

    const { updating } = this.state

    if (updating) return

    const updatedCluster = clusters.find(item => item.id === cluster.id)

    if (this.clusterHasUpdates(updatedCluster)) {
      this.updateStateFrom(updatedCluster)
    }
  }

  clusterHasUpdates = (cluster) => {
    const {
      clusterCountryCode,
      clusterName
    } = this.state

    return cluster.country_code !== clusterCountryCode || cluster.name !== clusterName
  }

  updateStateFrom = (cluster) => {
    this.setState({
      clusterCountryCode: cluster.country_code,
      clusterName: cluster.name,
      loaded: true,
      updating: true
    }, () => this.setState({
      updating: false
    }))
  }

  showClusterEditPane = () => {
    const { dispatch, cluster: { id } } = this.props

    dispatch(clustersActions.showClusterEditPane(id))
  }

  render = () => {
    const {
      cluster,
      showPaneOnClickingName,
      small,
      withinACard
    } = this.props

    const {
      clusterCountryCode,
      clusterName,
      loaded,
      updating
    } = this.state

    if (loaded === false || updating) return <p>Loading ...</p>

    const content = (
      <Media>
        <Media className="d-flex flex-column align-items-center mr-3" left>
          <Icofont icon="cubes" size={small ? '3' : '5'} />
        </Media>
        <Media body>
          <div className="d-flex flex-column">
            {showPaneOnClickingName ? (
              <Button
                className="text-left p-0"
                color="link"
                onClick={() => this.showClusterEditPane()}
              >
                <NodeHeaderName name={clusterName} small={small} />
              </Button>
            ) : (
              <NodeHeaderName name={clusterName} small={small} />
            )}
            <div className="d-flex flex-row align-items-center">
              <ReactFlagsSelect
                className="color-inherit"
                defaultCountry={clusterCountryCode}
                disabled
              />
              {showPaneOnClickingName && (
                <React.Fragment>
                  &nbsp;-&nbsp;
                  {cluster.node_count || 0}
                  &nbsp;Nodes
                </React.Fragment>
              )}
            </div>
          </div>
        </Media>
      </Media>
    )

    return (
      withinACard ? (
        <CardBody>
          {content}
        </CardBody>
      ) : (
        content
      )
    )
  }
}

const mapStateToProps = ({
  clusters: {
    updating,
    updateFailure,
    updateSuccess,
    items: clusters
  }
}) => ({
  clusters,
  updateFailure,
  updateSuccess,
  updating
})

ClusterHeader.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.object).isRequired,
  cluster: PropTypes.shape({
    country_code: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    node_count: PropTypes.number.isRequired
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  showPaneOnClickingName: PropTypes.bool,
  small: PropTypes.bool,
  withinACard: PropTypes.bool
}

ClusterHeader.defaultProps = {
  showPaneOnClickingName: false,
  small: false,
  withinACard: false
}

export default connect(mapStateToProps)(ClusterHeader)

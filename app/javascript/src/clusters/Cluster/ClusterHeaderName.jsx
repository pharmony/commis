import React from 'react'
import PropTypes from 'prop-types'

const ClusterHeaderName = ({ name, small }) => (
  small ? (
    <p className="lead mb-0">{name}</p>
  ) : (
    <h2 className="mb-0">{name}</h2>
  )
)

ClusterHeaderName.propTypes = {
  name: PropTypes.string.isRequired,
  small: PropTypes.bool.isRequired
}

export default ClusterHeaderName

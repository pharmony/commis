import React from 'react'
import {
  Button,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ReactFlagsSelect from 'react-flags-select'

class ClusterModal extends React.Component {
  state = {
    clusterName: '',
    clusterNameFeedback: null,
    clusterCountryCode: null
  }

  clearClusterNameFeedback = () => {
    this.setState({
      clusterNameFeedback: null
    })
  }

  handleNameChange = ({ target: { value } }) => {
    this.setState({
      clusterName: value,
      clusterNameFeedback: null
    })
  }

  handleClusterCountryChange = (countryCode) => {
    this.setState({
      clusterCountryCode: countryCode
    }, this.ref.toggleOptions())
  }

  onSaveClicked = () => {
    const { onSave, onToggle } = this.props
    const { clusterName, clusterCountryCode } = this.state

    if (clusterName === '') {
      this.updateClusterNameFeedback('The cluster name is mandatory')
      return false
    }

    onSave({
      clusterName,
      clusterCountryCode
    })

    onToggle()

    this.setState({
      clusterName: '',
      clusterCountryCode: null
    })

    return true
  }

  updateClusterNameFeedback = (feedback) => {
    this.setState({
      clusterNameFeedback: feedback
    })
  }

  render = () => {
    const {
      onToggle
    } = this.props

    const {
      clusterName,
      clusterNameFeedback
    } = this.state

    return (
      <Modal isOpen>
        <ModalHeader>
          New cluster
        </ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup row>
              <Label for="cluster_name" sm={2}>Name</Label>
              <Col sm={10}>
                <Input
                  id="cluster_name"
                  invalid={clusterNameFeedback ? true : false}
                  name="cluster[name]"
                  onChange={event => this.handleNameChange(event)}
                  type="text"
                  value={clusterName}
                />
                {clusterNameFeedback && (
                <FormFeedback>{clusterNameFeedback}</FormFeedback>)}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={2}>Country</Label>
              <Col sm={10}>
                <ReactFlagsSelect
                  className="flag-select-in-form"
                  onSelect={countryCode => this.handleClusterCountryChange(countryCode)}
                  ref={(ref) => { this.ref = ref }}
                  searchable
                />
              </Col>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={onToggle}>Cancel</Button>
          {' '}
          <Button
            color="success"
            onClick={this.onSaveClicked}
          >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    )
  }
}

ClusterModal.propTypes = {
  onSave: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired
}

export default connect()(ClusterModal)

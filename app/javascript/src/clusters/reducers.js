import _ from 'lodash'
import dotProp from 'dot-prop-immutable'

import clustersConstants from './constants'

const initialState = {
  fetchAllFailed: false, // True when back server replied with an error
  fetchingAll: false, // True while fetching all clusters
  fetched: false, /* True when clusters has been fetched at least once from the
                     backend server */
  itemCount: 0, // Cluster count
  items: [], // Clusters from the backend server
  openEditPane: false, // True when the right panel should be visible
  showPaneClusterId: '', // The cluster ID to be displayed in the right side panel
  updating: false, // True while requesting Node update
  updateFailure: false, // True when something prevented to save the node
  updateSuccess: false // True when the node has been successfully updated
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case clustersConstants.FETCH_ALL_REQUEST:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      return dotProp.set(newState, 'fetchingAll', true)
    case clustersConstants.FETCH_ALL_SUCCESS: {
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      newState = dotProp.set(newState, 'itemCount', action.clusters.length)
      return dotProp.set(
        newState,
        'items',
        action.clusters.sort((a, b) => a.name.localeCompare(b.name))
      )
    }
    case clustersConstants.FETCH_ALL_FAILURE:
      newState = dotProp.set(state, 'fetchAllFailed', true)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'items', [])

    case clustersConstants.CREATE_SUCCESS: {
      newState = dotProp.set(state, 'itemCount', state.itemCount + 1)
      const allClusters = [...state.items, action.cluster]
      return dotProp.set(
        newState,
        'items',
        allClusters.sort((a, b) => a.name.localeCompare(b.name))
      )
    }
    case clustersConstants.DESTROY_SUCCESS: {
      const index = state.items.findIndex(cluster => cluster.id === action.id)

      if (index > -1) {
        newState = dotProp.delete(state, `items.${index}`)
        return dotProp.set(newState, 'itemCount', state.itemCount - 1)
      }

      return state
    }

    case clustersConstants.HIDE_CLUSTER_EDIT_PANE:
      newState = dotProp.set(state, 'openEditPane', false)
      return dotProp.set(newState, 'showPaneClusterId', '')

    case clustersConstants.SHOW_CLUSTER_EDIT_PANE:
      newState = dotProp.set(state, 'openEditPane', true)
      return dotProp.set(newState, 'showPaneClusterId', action.id)

    /*
    ** Cluster Update
    */
    case clustersConstants.UPDATE_REQUEST:
      newState = dotProp.set(state, 'updating', true)
      newState = dotProp.set(newState, 'updateFailure', false)
      return dotProp.set(newState, 'updateSuccess', false)
    case clustersConstants.UPDATE_FAILURE:
      newState = dotProp.set(state, 'updating', false)
      newState = dotProp.set(newState, 'updateFailure', true)
      return dotProp.set(newState, 'updateSuccess', false)
    case clustersConstants.UPDATE_SUCCESS: {
      const clusterIndex = state.items.findIndex(cluster => cluster.id === action.id)
      const clusterObject = state.items.find(cluster => cluster.id === action.id)

      newState = dotProp.set(
        state,
        `items.${clusterIndex}`,
        {
          ...clusterObject,
          ..._.mapKeys(action.attributes, (v, k) => _.snakeCase(k))
        }
      )
      newState = dotProp.set(newState, 'updating', false)
      newState = dotProp.set(newState, 'updateFailure', false)
      return dotProp.set(newState, 'updateSuccess', true)
    }

    default:
      return state
  }
}

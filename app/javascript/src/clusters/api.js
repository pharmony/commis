import RestfulClient from 'restful-json-api-client'

import RailsHeaders from '../helpers/RailsHeaders'

export default class ClustersApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'clusters', headers: RailsHeaders })
  }
}

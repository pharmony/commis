import React from 'react'
import { connect } from 'react-redux'
import { Button } from 'reactstrap'
import PropTypes from 'prop-types'

import AlertMessage from '../components/AlertMessage'
import ClusterModal from './ClusterModal'
import Clusters from './Clusters'
import clustersActions from './actions'
// import Nodes from './Nodes'
import NoClusters from './NoClusters'
import PageLoader from '../components/PageLoader'

class ClustersPage extends React.Component {
  state = {
    showClusterModal: false
  }

  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(clustersActions.fetchAll())
  }

  createCluster = (cluster) => {
    const { dispatch } = this.props

    dispatch(clustersActions.create(cluster))
  }

  toggleClusterModal = () => {
    const { showClusterModal } = this.state

    this.setState({
      showClusterModal: !showClusterModal
    })
  }

  render = () => {
    const {
      clusterCount,
      clusters,
      fetched,
      fetchingAll,
      fetchAllFailed
    } = this.props

    const {
      showClusterModal
    } = this.state

    return (
      <React.Fragment>
        {fetched && (
          <div className="d-flex flex-column align-items-end">
            <Button
              color="primary"
              onClick={() => this.toggleClusterModal()}
            >
              Add cluster
            </Button>
          </div>
        )}

        {showClusterModal && (
          <ClusterModal
            onSave={cluster => this.createCluster(cluster)}
            onToggle={() => this.toggleClusterModal()}
          />
        )}

        <div className={`d-flex flex-column ${clusterCount > 0 ? '' : 'justify-content-center'} h-100`}>
          {fetchingAll && (
            <PageLoader message="Loading clusters ..." />
          )}

          {fetchAllFailed && (
            <AlertMessage>
              Unfortunately an error occured while fetching the clusters.
              Please try again.
            </AlertMessage>
          )}

          {fetched && fetchAllFailed === false && clusterCount === 0 && (
            <NoClusters />
          )}

          {fetched && clusterCount > 0 && (
            <Clusters clusters={clusters} />
          )}
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  clusters: {
    fetched,
    fetchingAll,
    fetchAllFailed,
    itemCount: clusterCount,
    items: clusters
  }
}) => ({
  clusterCount,
  clusters,
  fetched,
  fetchingAll,
  fetchAllFailed
})

ClustersPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  clusterCount: PropTypes.number.isRequired,
  clusters: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetched: PropTypes.bool.isRequired,
  fetchingAll: PropTypes.bool.isRequired,
  fetchAllFailed: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(ClustersPage)

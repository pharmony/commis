import React from 'react'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import Cluster from './Cluster'
import ClusterEditPane from './ClusterEditPane'

const Clusters = ({ clusters }) => (
  <React.Fragment>
    <ClusterEditPane />

    <div className="d-flex flex-row flex-wrap mt-4">
      {clusters.map(cluster => (
        <Cluster
          key={uuidv1()}
          cluster={cluster}
        />
      ))}
    </div>
  </React.Fragment>
)

Clusters.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Clusters

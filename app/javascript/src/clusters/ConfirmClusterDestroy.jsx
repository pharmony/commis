import React from 'react'
import confirmModal from 'reactstrap-confirm'
import PropTypes from 'prop-types'

import clusterActions from './actions'

const ConfirmClusterDestroy = async (dispatch, cluster) => {
  const confirm = await confirmModal({
    title: 'Confirm cluster destroy',
    message: (
      <React.Fragment>
        <p>
          Are you sure you want to destroy the cluster&nbsp;
          {cluster.name}
          ?
        </p>
      </React.Fragment>
    ),
    confirmColor: 'danger',
    confirmText: 'Confirm'
  })

  if (confirm) dispatch(clusterActions.destroy(cluster.id))
}

ConfirmClusterDestroy.propTypes = {
  cluster: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string
  }).isRequired,
  dispatch: PropTypes.func.isRequired
}

export default ConfirmClusterDestroy

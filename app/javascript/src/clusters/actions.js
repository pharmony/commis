import alertActions from '../alerts/actions'
import clustersConstants from './constants'
import clustersService from './services'

const hideClusterEditPane = () => dispatch => dispatch({
  type: clustersConstants.HIDE_CLUSTER_EDIT_PANE
})


const create = (attributes) => {
  const request = cluster => ({
    cluster,
    type: clustersConstants.CREATE_REQUEST
  })
  const success = newCluster => ({
    cluster: newCluster,
    type: clustersConstants.CREATE_SUCCESS
  })
  const failure = error => ({
    error,
    type: clustersConstants.CREATE_FAILURE
  })

  return (dispatch) => {
    dispatch(request(attributes))

    clustersService.create(attributes)
      .then((cluster) => {
        dispatch(alertActions.success(
          'Cluster successfully created.'
        ))

        dispatch(success(cluster))
      })
      .catch((error) => {
        dispatch(alertActions.error(
          'Something prevented the cluster creation.'
        ))

        dispatch(failure(error))
      })
  }
}

const destroy = (clusterId) => {
  const request = id => ({ id, type: clustersConstants.DESTROY_REQUEST })
  const success = destroyedId => ({
    id: destroyedId,
    type: clustersConstants.DESTROY_SUCCESS
  })
  const failure = error => ({
    error,
    type: clustersConstants.DESTROY_FAILURE
  })

  return (dispatch) => {
    dispatch(request(clusterId))

    clustersService.destroy(clusterId)
      .then(() => {
        dispatch(alertActions.success(
          'Cluster successfully destroyed.'
        ))

        dispatch(hideClusterEditPane())

        dispatch(success(clusterId))
      })
      .catch((error) => {
        dispatch(alertActions.error(
          'Something prevented to destroy this cluster.'
        ))

        dispatch(failure(error))
      })
  }
}

const fetchAll = () => {
  const request = () => ({ type: clustersConstants.FETCH_ALL_REQUEST })
  const success = clusters => ({
    clusters,
    type: clustersConstants.FETCH_ALL_SUCCESS
  })
  const failure = error => ({
    error,
    type: clustersConstants.FETCH_ALL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    clustersService.fetchAll()
      .then(clusters => dispatch(success(clusters)))
      .catch(error => dispatch(failure(error)))
  }
}

const showClusterEditPane = id => dispatch => dispatch({
  id,
  type: clustersConstants.SHOW_CLUSTER_EDIT_PANE
})

const updateCluster = (id, attributes) => {
  const request = (clusterId, clusterAttributes) => ({
    attributes: clusterAttributes,
    id: clusterId,
    type: clustersConstants.UPDATE_REQUEST
  })
  const success = (clusterId, clusterAttributes) => ({
    attributes: clusterAttributes,
    id: clusterId,
    type: clustersConstants.UPDATE_SUCCESS
  })
  const failure = error => ({
    error,
    type: clustersConstants.UPDATE_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id, attributes))

    clustersService.update(id, attributes)
      .then(() => dispatch(success(id, attributes)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  create,
  destroy,
  fetchAll,
  hideClusterEditPane,
  showClusterEditPane,
  updateCluster
}

import React from 'react'
import { Button } from 'reactstrap'
import { connect } from 'react-redux'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'
import SlidingPane from 'react-sliding-pane'

import clustersActions from './actions'
import ClusterHeader from './Cluster/ClusterHeader'
import ClusterNodes from './Cluster/ClusterNodes'
import ClusterSettings from './ClusterSettings'
import ConfirmClusterDestroy from './ConfirmClusterDestroy'
import PageLoader from '../components/PageLoader'

class ClusterEditPane extends React.Component {
  state = {
    loadingCluster: false,
    clusterId: '',
    clusterObject: null
  }

  componentDidMount = () => this.loadNodeIfNeeded()

  componentDidUpdate = () => this.loadNodeIfNeeded()

  loadNode = () => {
    const { clusters } = this.props
    const { clusterId } = this.state

    const currentNode = clusters.find(cluster => cluster.id === clusterId)

    // Use the version from the cache
    this.setState({
      loadingCluster: false,
      clusterObject: currentNode
    })
  }

  closePane = () => {
    const { dispatch } = this.props

    this.setState({
      clusterId: null
    }, () => dispatch(clustersActions.hideClusterEditPane()))
  }

  loadNodeIfNeeded = () => {
    const { showPaneClusterId } = this.props
    const { loadingCluster, clusterId } = this.state

    if (loadingCluster) return

    if (clusterId !== showPaneClusterId) {
      this.setState({
        loadingCluster: true,
        clusterId: showPaneClusterId
      }, () => this.loadNode())
    }
  }

  showConfirmDestroyCluster = () => {
    const { dispatch } = this.props
    const { clusterObject } = this.state

    ConfirmClusterDestroy(dispatch, clusterObject)
  }

  render = () => {
    const { openEditPane } = this.props
    const { clusterObject } = this.state

    return (
      <SlidingPane
        closeIcon={<Icofont icon="close-line" size="2" />}
        isOpen={openEditPane}
        onRequestClose={() => this.closePane()}
        title="Cluster details"
        width="50%"
      >
        {clusterObject === null && (
          <PageLoader message="Loading cluster ..." />
        )}

        {clusterObject && (
          <React.Fragment>
            <ClusterHeader
              contextualMenu={false}
              cluster={clusterObject}
            />
            <hr />
            <div className="d-flex flex-row justify-content-end">
              <Button
                color="danger"
                onClick={() => this.showConfirmDestroyCluster()}
              >
                Delete
              </Button>
            </div>
            <hr />
            <ClusterSettings cluster={clusterObject} />
            <hr />
            <ClusterNodes cluster={clusterObject} />
          </React.Fragment>
        )}
      </SlidingPane>
    )
  }
}

const mapStateToProps = ({
  clusters: { openEditPane, showPaneClusterId, items }
}) => ({
  clusters: items,
  openEditPane,
  showPaneClusterId
})

ClusterEditPane.propTypes = {
  dispatch: PropTypes.func.isRequired,
  clusters: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired,
  openEditPane: PropTypes.bool.isRequired,
  showPaneClusterId: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(ClusterEditPane)

import dotProp from 'dot-prop-immutable'

import policyActionsConstants from './constants'
import { presentStepsForStorage } from './presenters'

const initialState = {
  fetchingStack: false,
  fetchingStackFailed: false,
  fetchingSteps: false,
  fetchingStepsFailed: false,
  items: null,
  persistingSteps: false,
  persistingStepsFailed: false,
  persistingStepsSucceed: false,
  policyActionStackId: null,
  stackFetched: false,
  stepsFetched: false
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case policyActionsConstants.FETCH_STACK_REQUEST:
      newState = dotProp.set(state, 'fetchingStack', true)
      newState = dotProp.set(newState, 'fetchingStackFailed', false)
      newState = dotProp.set(newState, 'items', null)
      newState = dotProp.set(newState, 'policyActionStackId', null)
      return dotProp.set(newState, 'stackFetched', false)
    case policyActionsConstants.FETCH_STACK_SUCCESS:
      newState = dotProp.set(state, 'fetchingStack', false)
      newState = dotProp.set(newState, 'fetchingStackFailed', false)
      newState = dotProp.set(newState, 'stackFetched', true)
      return dotProp.set(newState, 'policyActionStackId', action.stack.id)
    case policyActionsConstants.FETCH_STACK_FAILURE:
      newState = dotProp.set(state, 'fetchingStack', false)
      newState = dotProp.set(newState, 'fetchingStackFailed', true)
      return dotProp.set(newState, 'stackFetched', true)

    case policyActionsConstants.FETCH_STEPS_REQUEST:
      newState = dotProp.set(state, 'fetchingSteps', true)
      newState = dotProp.set(newState, 'fetchingStepsFailed', false)
      return dotProp.set(newState, 'stepsFetched', false)
    case policyActionsConstants.FETCH_STEPS_SUCCESS: {
      newState = dotProp.set(state, 'fetchingSteps', false)
      newState = dotProp.set(newState, 'fetchingStepsFailed', false)
      newState = dotProp.set(newState, 'stepsFetched', true)

      const finalItems = {}

      finalItems.sortable = presentStepsForStorage(action.steps.sortable)
      finalItems.unsortable = presentStepsForStorage(action.steps.unsortable)

      return dotProp.set(newState, 'items', finalItems)
    }
    case policyActionsConstants.FETCH_STEPS_FAILURE:
      newState = dotProp.set(state, 'fetchingSteps', false)
      newState = dotProp.set(newState, 'fetchingStepsFailed', true)
      return dotProp.set(newState, 'stepsFetched', true)

    case policyActionsConstants.PERSIST_STEPS_REQUEST:
      newState = dotProp.set(state, 'persistingSteps', true)
      return dotProp.set(newState, 'persistingStepsFailed', false)
    case policyActionsConstants.PERSIST_STEPS_SUCCESS: {
      newState = dotProp.set(
        state,
        'items.sortable',
        presentStepsForStorage(action.steps)
      )
      newState = dotProp.set(newState, 'persistingSteps', false)
      newState = dotProp.set(newState, 'persistingStepsFailed', false)
      return dotProp.set(newState, 'persistingStepsSucceed', true)
    }
    case policyActionsConstants.PERSIST_STEPS_FAILURE:
      newState = dotProp.set(state, 'persistingSteps', false)
      newState = dotProp.set(newState, 'persistingStepsFailed', true)
      return dotProp.set(newState, 'persistingStepsSucceed', false)
    default:
      return state
  }
}

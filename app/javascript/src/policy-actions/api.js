import RestfulClient from 'restful-json-api-client'

import RailsHeaders from '../helpers/RailsHeaders'

export class ChefPolicyActionStacksApi extends RestfulClient {
  constructor(repositoryId, policyId) {
    super(`/api/chef/repositories/${repositoryId}/policies/${policyId}`, {
      resource: 'policy_action_stack'
    })
  }
}

export class PolicyActionStacksApi extends RestfulClient {
  constructor() {
    super('/api', {
      headers: RailsHeaders,
      resource: 'policy_action_stacks'
    })
  }
}

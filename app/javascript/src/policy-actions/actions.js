import alertsActions from '../alerts/actions'
import policyActionsConstants from './constants'
import policyActionsService from './services'
import { presentStepsForRequest } from './presenters'

const fetchStackFor = (repositoryId, policyId) => {
  const request = (chefRepositoryId, chefPolicyId) => ({
    policyId: chefPolicyId,
    repositoryId: chefRepositoryId,
    type: policyActionsConstants.FETCH_STACK_REQUEST
  })
  const success = stack => ({
    stack,
    type: policyActionsConstants.FETCH_STACK_SUCCESS
  })
  const failure = error => ({
    error,
    type: policyActionsConstants.FETCH_STACK_FAILURE
  })

  return (dispatch) => {
    dispatch(request(repositoryId, policyId))

    policyActionsService.fetchStackFor(repositoryId, policyId)
      .then(stack => dispatch(success(stack)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchStepsFrom = (policyActionStackId) => {
  const request = id => ({
    policyActionStackId: id,
    type: policyActionsConstants.FETCH_STEPS_REQUEST
  })
  const success = steps => ({
    steps,
    type: policyActionsConstants.FETCH_STEPS_SUCCESS
  })
  const failure = error => ({
    error,
    type: policyActionsConstants.FETCH_STEPS_FAILURE
  })

  return (dispatch) => {
    dispatch(request(policyActionStackId))

    policyActionsService.fetchStepsFor(policyActionStackId)
      .then(steps => dispatch(success(steps)))
      .catch(error => dispatch(failure(error)))
  }
}

const persistSteps = (policyActionStackId, steps) => {
  const request = (stackId, stackSteps) => ({
    policyActionStackId: stackId,
    steps: stackSteps,
    type: policyActionsConstants.PERSIST_STEPS_REQUEST
  })
  const success = persistedSteps => ({
    steps: persistedSteps,
    type: policyActionsConstants.PERSIST_STEPS_SUCCESS
  })
  const failure = error => ({
    error,
    type: policyActionsConstants.PERSIST_STEPS_FAILURE
  })

  return (dispatch) => {
    dispatch(request(steps))

    const presentedSteps = presentStepsForRequest(steps)

    policyActionsService.persistSteps(policyActionStackId, {
      steps: presentedSteps
    })
      .then(() => {
        dispatch(alertsActions.success('Workflow successfully updated'))
        dispatch(success(steps))
      })
      .catch((error) => {
        dispatch(alertsActions.error(`Something went wrong while updating this
                                      workflow. Please try agian.`))
        dispatch(failure(error))
      })
  }
}

export default {
  fetchStackFor,
  fetchStepsFrom,
  persistSteps
}

const presentBootstrapStepForRequest = ({ policyName, position, type }) => ({
  policy_name: policyName,
  position,
  type
})

const presentBootstrapStepForStorage = ({
  id,
  policy_name: policyName,
  position,
  type
}) => ({
  id,
  policyName,
  position,
  type
})

const presentConvergeStepForRequest = ({
  query,
  position,
  type
}) => ({
  query,
  position,
  type
})

const presentConvergeStepForStorage = ({
  id,
  query,
  position,
  type
}) => ({
  id,
  query,
  position,
  type
})

const presentStep = (step, options) => {
  if (step.type === 'bootstrap') {
    if (options.for === 'request') {
      return presentBootstrapStepForRequest(step)
    }
    return presentBootstrapStepForStorage(step)
  }

  if (options.for === 'request') {
    return presentConvergeStepForRequest(step)
  }
  return presentConvergeStepForStorage(step)
}

export const presentStepsForRequest = steps => (
  steps.map(step => (presentStep(step, { for: 'request' })))
)

export const presentStepsForStorage = steps => (
  steps.map(step => (presentStep(step, { for: 'storage' })))
)

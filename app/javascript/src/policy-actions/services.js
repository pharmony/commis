import ApiUtils from '../helpers/ApiUtils'
import { PolicyActionStacksApi, ChefPolicyActionStacksApi } from './api'

const fetchStackFor = (repositoryId, policyId) => (
  new ChefPolicyActionStacksApi(repositoryId, policyId)
    .get()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchStepsFor = policyActionStackId => (
  new PolicyActionStacksApi()
    .get({ id: policyActionStackId })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const persistSteps = (policyActionStackId, steps) => (
  new PolicyActionStacksApi()
    .update(policyActionStackId, steps)
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetchStackFor,
  fetchStepsFor,
  persistSteps
}

import RestfulClient from 'restful-json-api-client'

export default class ChefCacheApi extends RestfulClient {
  constructor() {
    super('/api/chef', { resource: 'cache' })
  }
}

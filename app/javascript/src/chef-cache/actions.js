import alertActions from '../alerts/actions'
import chefCacheConstants from './constants'
import chefCacheService from './services'

const clear = () => {
  const request = () => ({ type: chefCacheConstants.CLEAR_CHEF_CACHE_REQUEST })
  const success = () => ({ type: chefCacheConstants.CLEAR_CHEF_CACHE_SUCCESS })
  const failure = error => ({
    error,
    type: chefCacheConstants.CLEAR_CHEF_CACHE_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    return chefCacheService.clear()
      .then(() => {
        dispatch(success())
        dispatch(alertActions.success('Chef cache successfully cleared!'))
      })
      .catch((error) => {
        dispatch(failure(error))
        dispatch(alertActions.error('Something went wrong!'))
      })
  }
}

export default {
  clear
}

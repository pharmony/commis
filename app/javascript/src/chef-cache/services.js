import ApiUtils from '../helpers/ApiUtils'
import ChefCacheApi from './api'

const clear = () => (
  new ChefCacheApi().destroy()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  clear
}

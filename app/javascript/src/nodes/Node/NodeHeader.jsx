import React from 'react'
import { connect } from 'react-redux'
import {
  Badge,
  Button,
  CardBody,
  Media
} from 'reactstrap'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'

import BadgeHelpers from '../../helpers/BadgeHelpers'
import NodeHeaderName from './NodeHeaderName'
import nodesActions from '../actions'
import StringHelpers from '../../helpers/StringHelpers'

class NodeHeader extends React.Component {
  iconSizeFrom = (size) => {
    switch (size) {
      case 'small':
        return '2'
      case 'medium':
        return '3'
      case 'default':
        return '5'
      default:
        return '5'
    }
  }

  showNodeShowPane = () => {
    const { dispatch, node: { id } } = this.props

    dispatch(nodesActions.showNodeEditPane(id))
  }

  render = () => {
    const {
      node,
      showPaneOnClickingName,
      size,
      withinACard
    } = this.props

    const {
      hostname,
      ipaddress,
      name,
      state
    } = node

    const content = (
      <Media>
        <Media className="d-flex flex-column align-items-center mr-3" left>
          <Icofont icon="server" size={this.iconSizeFrom(size)} />
          <div className="d-flex">
            <Badge color={BadgeHelpers.colorFrom(state)}>
              {StringHelpers.titleize(state)}
            </Badge>
          </div>
        </Media>
        <Media body>
          <div className="d-flex flex-column">
            {showPaneOnClickingName ? (
              <Button
                className="text-left p-0"
                color="link"
                onClick={() => this.showNodeShowPane()}
              >
                <NodeHeaderName name={name} size={size} />
              </Button>
            ) : (
              <NodeHeaderName name={name} size={size} />
            )}
            <div className="d-flex align-items-center">
              <Icofont className="mr-1" icon="world" size={size === 'default' ? '2' : '1'} />
              <p className={`mb-0 ${size === 'default' ? 'lead' : ''}`}>
                {`${ipaddress} (${hostname})`}
              </p>
            </div>
          </div>
        </Media>
      </Media>
    )

    return (
      withinACard ? (
        <CardBody className="pb-0">
          {content}
        </CardBody>
      ) : (
        content
      )
    )
  }
}

NodeHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  node: PropTypes.shape({
    chef_policies: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    })).isRequired,
    hostname: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    ipaddress: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired
  }).isRequired,
  showPaneOnClickingName: PropTypes.bool,
  size: PropTypes.string,
  withinACard: PropTypes.bool
}

NodeHeader.defaultProps = {
  showPaneOnClickingName: false,
  size: 'default',
  withinACard: false
}

export default connect()(NodeHeader)

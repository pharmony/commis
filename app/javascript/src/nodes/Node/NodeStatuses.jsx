import React from 'react'
import {
  Badge,
  CardBody
} from 'reactstrap'

export default () => (
  <CardBody className="py-0">
    <div className="d-flex flex-row flex-wrap justify-content-between">
      <dl className="d-flex mb-0">
        <dt className="mr-2">Provisioning:</dt>
        <dd className="mb-0">
          <Badge color="success">Success</Badge>
        </dd>
      </dl>
    </div>
  </CardBody>
)

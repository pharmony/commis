import React from 'react'
import { CardBody } from 'reactstrap'
import PropTypes from 'prop-types'

import formatBytes from '../../helpers/FormatBytes'

const NodeHardware = ({
  node: {
    cpu_count, cpu_cores, memory_total, filesystem_total, filesystem_type
  }
}) => (
  <CardBody className="py-0">
    <div className="d-flex flex-row flex-wrap justify-content-between">
      <dl className="d-flex mb-0">
        <dt className="mr-1">CPU:</dt>
        <dd className="mb-0">
          {cpu_count ? `${cpu_count} (${cpu_cores} cores)` : 'Unknown'}
        </dd>
      </dl>
      <dl className="d-flex mb-0">
        <dt className="mr-1">Memory:</dt>
        <dd className="mb-0">
          {memory_total ? formatBytes(memory_total) : 'Unknown'}
        </dd>
      </dl>
      <dl className="d-flex mb-0">
        <dt className="mr-1">Filesystem:</dt>
        <dd className="mb-0">
          {filesystem_total ? `${formatBytes(filesystem_total)} (${filesystem_type})` : 'Unknown'}
        </dd>
      </dl>
    </div>
  </CardBody>
)

NodeHardware.propTypes = {
  node: PropTypes.shape({
    cpu_count: PropTypes.number
  }).isRequired
}

export default NodeHardware

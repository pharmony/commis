import React from 'react'
import PropTypes from 'prop-types'
import { Card } from 'reactstrap'

import NodeHeader from './NodeHeader'
import NodeHardware from './NodeHardware'
import NodePolicies from './NodePolicies'
import NodeSoftware from './NodeSoftware'
import NodeStatuses from './NodeStatuses'

const Node = ({ node }) => (
  <Card className={`node node-${node.state} mr-4 mb-4`}>
    <NodeHeader
      node={node}
      showPaneOnClickingName
      size="medium"
      withinACard
    />
    <hr />
    <NodeStatuses node={node} />
    <hr />
    <NodePolicies node={node} />
    <hr />
    <NodeHardware node={node} />
    <hr />
    <NodeSoftware node={node} />
  </Card>
)

Node.propTypes = {
  node: PropTypes.object.isRequired
}

export default Node

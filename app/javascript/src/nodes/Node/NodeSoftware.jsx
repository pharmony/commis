import React from 'react'
import { CardBody } from 'reactstrap'
import PropTypes from 'prop-types'

const NodeSoftware = ({
  node: {
    platform, platform_version, chef_version, kernel
  }
}) => (
  <CardBody className="py-0">
    <div className="d-flex flex-row flex-wrap justify-content-between">
      <dl className="d-flex">
        <dt className="mr-1">OS:</dt>
        <dd className="mb-0">{`${platform} ${platform_version}`}</dd>
      </dl>
      <dl className="d-flex">
        <dt className="mr-1">Chef:</dt>
        <dd className="mb-0">{chef_version}</dd>
      </dl>
      <dl className="d-flex">
        <dt className="mr-1">Kernel:</dt>
        <dd className="mb-0">{kernel ? kernel : 'Unknown'}</dd>
      </dl>
    </div>
  </CardBody>
)

NodeSoftware.propTypes = {
  node: PropTypes.shape({
    platform: PropTypes.string,
    platform_version: PropTypes.string,
    chef_version: PropTypes.string
  }).isRequired
}

export default NodeSoftware

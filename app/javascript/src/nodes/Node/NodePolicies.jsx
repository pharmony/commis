import React from 'react'
import { connect } from 'react-redux'
import {
  Badge,
  CardBody
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

import PageLoader from '../../components/PageLoader'

const NodePolicies = (props) => {
  const { node, policies, policyGroups } = props

  const nodeChefPolicies = node.chef_policies.map(chefPolicy => chefPolicy.id)

  let nodePolicy = policies.find(
    policy => nodeChefPolicies.includes(policy.id)
  )

  const nodeChefPolicyGroups = node.chef_policy_groups.map(
    chefPolicyGroup => chefPolicyGroup.id
  )

  let nodePolicyGroup = policyGroups.find(
    group => nodeChefPolicyGroups.includes(group.id)
  )

  // When no policies
  if (!nodePolicy) {
    nodePolicy = { name: 'None', color: 'secondary' }
  }

  if (!nodePolicyGroup) {
    nodePolicyGroup = { name: 'None', color: 'secondary' }
  }

  return (
    <CardBody className="py-0">
      <div className="d-flex flex-row flex-wrap justify-content-between">
        <dl className="d-flex mb-0">
          <dt className="mr-2">Policies:</dt>
          <dd className="mb-0">
            {nodePolicy ? (
              <Badge
                color={nodePolicy.color || 'info'}
                key={uuidv1()}
              >
                {nodePolicy.name}
              </Badge>
            ) : (
              <PageLoader message="Loading policies ..." />
            )}
          </dd>
        </dl>
        <dl className="d-flex mb-0">
          <dt className="mr-2">Groups:</dt>
          <dd className="mb-0">
            {nodePolicyGroup ? (
              <Badge
                color={nodePolicyGroup.color || 'info'}
                key={uuidv1()}
              >
                {nodePolicyGroup.name}
              </Badge>
            ) : (
              <PageLoader message="Loading policies ..." />
            )}
          </dd>
        </dl>
      </div>
    </CardBody>
  )
}

const mapStateToProps = ({
  policies: { items: policies },
  policyGroups: { items: policyGroups }
}) => ({
  policies,
  policyGroups
})

NodePolicies.propTypes = {
  node: PropTypes.shape({
    chef_policies: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    })).isRequired,
    chef_policy_groups: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired
    })).isRequired
  }).isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  policyGroups: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired
}

export default connect(mapStateToProps)(NodePolicies)

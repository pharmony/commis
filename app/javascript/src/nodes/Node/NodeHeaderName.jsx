import React from 'react'
import Proptypes from 'prop-types'

const NodeHeaderName = ({ name, size }) => (
  size === 'default' ? (
    <h2>{name}</h2>
  ) : (
    <p className="lead mb-0">{name}</p>
  )
)

NodeHeaderName.propTypes = {
  name: Proptypes.string.isRequired,
  size: Proptypes.string
}

NodeHeaderName.defaultProps = {
  size: 'default'
}

export default NodeHeaderName

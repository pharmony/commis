import alertActions from '../alerts/actions'
import nodesConstants from './constants'
import nodesService from './services'

/*
** The bootstrap action sends the a request to the backend server in order to
*  create a new NodeActionStack, but this task is executed asynchronously as
*  a Sidekiq job, so we are receiving the Sidekiq job details.
*  The job ID is saved with the nodeActionTmpId as a temporary object which will
*  be replaced later, when the server will send the created NodeActionStack via
*  ActionCable/WebSocket.
*  When a valid NodeActionStack will be sent, the user will be redirected to the
*  execution page of this NodeActionStack.
*/
const bootstrap = (nodeActionTmpId, options) => {
  const request = tmpNodeActionId => ({
    tmpNodeActionId,
    type: nodesConstants.BOOTSTRAP_REQUEST_REQUEST
  })
  const success = (tmpNodeActionId, job) => ({
    job,
    tmpNodeActionId,
    type: nodesConstants.BOOTSTRAP_REQUEST_SUCCESS
  })
  const failure = (tmpNodeActionId, error) => ({
    error,
    tmpNodeActionId,
    type: nodesConstants.BOOTSTRAP_REQUEST_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeActionTmpId))

    nodesService.bootstrap(options)
      .then((job) => {
        dispatch(success(nodeActionTmpId, job))
        dispatch(alertActions.success('Bootstrap request successfully created.'))
      })
      .catch((error) => {
        dispatch(alertActions.error(`Something went wrong while creating the
                                     bootstrap request, please try again.`))
        dispatch(failure(nodeActionTmpId, error))
      })
  }
}

/*
** The converge action sends the a request to the backend server in order to
*  create a new NodeActionStack, but this task is executed asynchronously as
*  a Sidekiq job, so we are receiving the Sidekiq job details.
*  The job ID is saved with the nodeActionTmpId as a temporary object which will
*  be replaced later, when the server will send the created NodeActionStack via
*  ActionCable/WebSocket.
*  When a valid NodeActionStack will be sent, the user will be redirected to the
*  execution page of this NodeActionStack.
*/
const converge = (nodeActionTmpId, nodeName, chefRepositoryId) => {
  const request = (tmpNodeActionId, givenChefRepositoryId, givenNodeName) => ({
    chefRepositoryId: givenChefRepositoryId,
    nodeName: givenNodeName,
    tmpNodeActionId,
    type: nodesConstants.CONVERGE_REQUEST_REQUEST
  })
  const success = (tmpNodeActionId, job) => ({
    job,
    tmpNodeActionId,
    type: nodesConstants.CONVERGE_REQUEST_SUCCESS
  })
  const failure = (tmpNodeActionId, error) => ({
    error,
    tmpNodeActionId,
    type: nodesConstants.CONVERGE_REQUEST_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeActionTmpId, nodeName, chefRepositoryId))

    nodesService.converge(nodeName, chefRepositoryId)
      // history.push(`/nodes/${nodeName}/actions/${action.id}`)
      .then(job => dispatch(success(nodeActionTmpId, job)))
      .catch((error) => {
        dispatch(alertActions.error('Something prevented to converge this node.'))
        dispatch(failure(nodeActionTmpId, error))
      })
  }
}

const fetchAll = () => {
  const request = () => ({ type: nodesConstants.FETCH_ALL_REQUEST })
  const success = nodes => ({
    nodes,
    type: nodesConstants.FETCH_ALL_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodesConstants.FETCH_ALL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    nodesService.fetchAll()
      .then(nodes => dispatch(success(nodes)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchNodeActions = (nodeId) => {
  const request = currentNodeId => ({
    nodeId: currentNodeId,
    type: nodesConstants.FETCH_ALL_FOR_A_NODE_REQUEST
  })
  const success = nodeActions => ({
    nodeActions,
    type: nodesConstants.FETCH_ALL_FOR_A_NODE_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodesConstants.FETCH_ALL_FOR_A_NODE_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeId))

    nodesService.fetchNodeActions(nodeId)
      .then(actions => dispatch(success(actions)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchNodeActionStack = (id, nodeId) => {
  const request = (currentNodeId, actionId) => ({
    actionId,
    nodeId: currentNodeId,
    type: nodesConstants.FETCH_ONE_FOR_A_NODE_REQUEST
  })
  const success = nodeActionStack => ({
    nodeActionStack,
    type: nodesConstants.FETCH_ONE_FOR_A_NODE_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodesConstants.FETCH_ONE_FOR_A_NODE_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeId, id))

    nodesService.fetchNodeActionStack(id, nodeId)
      .then(stack => dispatch(success(stack)))
      .catch(error => dispatch(failure(error)))
  }
}

const fetchOne = (id) => {
  const request = nodeId => ({
    id: nodeId,
    type: nodesConstants.FETCH_ONE_REQUEST
  })
  const success = node => ({
    node,
    type: nodesConstants.FETCH_ONE_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodesConstants.FETCH_ONE_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id))

    if (!id) {
      console.error('Requested to fetch one node, but the given ID is invalid ',
        `(Given id: ${id})`)
    } else {
      nodesService.fetchOne(id)
        .then(node => dispatch(success(node)))
        .catch(error => dispatch(failure(error)))
    }
  }
}

const hideNodeEditPane = () => dispatch => dispatch({
  type: nodesConstants.HIDE_NODE_EDIT_PANE
})

/*
** The pullRepoAndConverge action sends the a request to the backend server
*  in order to create a new NodeActionStack, but this task is executed
*  asynchronously as a Sidekiq job, so we are receiving the Sidekiq job details.
*  The job ID is saved with the nodeActionTmpId as a temporary object which will
*  be replaced later, when the server will send the created NodeActionStack via
*  ActionCable/WebSocket.
*  When a valid NodeActionStack will be sent, the user will be redirected to the
*  execution page of this NodeActionStack.
*/
const pullRepoAndConverge = (nodeActionTmpId, nodeName, repositoryId) => {
  const request = (tmpNodeActionId, name, chefRepositoryId) => ({
    nodeName: name,
    repositoryId: chefRepositoryId,
    tmpNodeActionId,
    type: nodesConstants.PULL_REPO_AND_CONVERGE_REQUEST_REQUEST
  })
  const success = (tmpNodeActionId, job) => ({
    job,
    tmpNodeActionId,
    type: nodesConstants.PULL_REPO_AND_CONVERGE_REQUEST_SUCCESS
  })
  const failure = (tmpNodeActionId, error) => ({
    error,
    tmpNodeActionId,
    type: nodesConstants.PULL_REPO_AND_CONVERGE_REQUEST_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeActionTmpId, nodeName, repositoryId))

    nodesService.pullRepoAndConverge(nodeName, repositoryId)
      .then(job => dispatch(success(nodeActionTmpId, job)))
      .catch((error) => {
        dispatch(alertActions.error('Something went wrong.'))
        dispatch(failure(error))
      })
  }
}

const showNodeEditPane = id => dispatch => dispatch({
  id,
  type: nodesConstants.SHOW_NODE_EDIT_PANE
})

const updateNode = (id, attributes) => {
  const request = (nodeId, nodeAttributes) => ({
    attributes: nodeAttributes,
    id: nodeId,
    type: nodesConstants.UPDATE_REQUEST
  })
  const success = (nodeId, nodeAttributes) => ({
    attributes: nodeAttributes,
    id: nodeId,
    type: nodesConstants.UPDATE_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodesConstants.UPDATE_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id, attributes))

    nodesService.update(id, attributes)
      .then(() => dispatch(success(id, attributes)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  bootstrap,
  converge,
  fetchAll,
  fetchNodeActions,
  fetchNodeActionStack,
  fetchOne,
  hideNodeEditPane,
  pullRepoAndConverge,
  showNodeEditPane,
  updateNode
}

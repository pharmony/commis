import React from 'react'
import { Badge, Media } from 'reactstrap'
import confirmModal from 'reactstrap-confirm'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import GitLogoHelper from '../helpers/GitLogoHelper'
import nodesActions from './actions'

const ConfirmNodeConverge = async (dispatch, node, repository, onConfirm) => {
  const confirm = await confirmModal({
    title: `Converge ${node.name}?`,
    message: (
      <span>
        Are you sure you want to converge the node&nbsp;
        <Badge className="fs-100" color="primary">{node.name}</Badge>
        &nbsp;using the Chef repository&nbsp;
        <Badge className="fs-100">
          <Media className="d-flex flex-row align-items-center">
            <Media left>
              <Media
                className="img-xs"
                object
                src={GitLogoHelper.fromUrl(repository.url)}
              />
            </Media>
            <Media body>
              {repository.name}
            </Media>
          </Media>
        </Badge>
        &nbsp;?
      </span>
    ),
    confirmColor: 'primary',
    confirmText: 'Converge'
  })

  if (confirm) {
    const nodeActionTmpId = uuidv1()

    if (onConfirm) onConfirm(nodeActionTmpId)

    dispatch(nodesActions.converge(nodeActionTmpId, node.name, repository.id))
  }
}

ConfirmNodeConverge.propTypes = {
  dispatch: PropTypes.func.isRequired,
  node: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired,
  repository: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }).isRequired,
  onConfirm: PropTypes.func
}

ConfirmNodeConverge.defaultProps = {
  onConfirm: null
}

export default ConfirmNodeConverge

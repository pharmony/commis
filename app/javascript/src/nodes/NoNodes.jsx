import React from 'react'
import { NavLink } from 'react-router-dom'

export default (props) => {
  const { repositoryCount } = props

  return (
    <div className="d-flex flex-column align-items-center align-self-center w-50">
      <p className="lead text-center">
        {repositoryCount === 0 && (
          `Commis needs at least one Chef repository to display nodes but you
          currently have no imported chef repository.`
        )}

        {repositoryCount > 0 && (
          `There are no managed nodes found in your ${repositoryCount}
          repositories.

          Please bootstrap at least one node.`
        )}
      </p>

      {repositoryCount === 0 && (
        <p>
          Let&lsquo;s get started and
          {' '}
          {
            <NavLink to="/repositories/import">
              import your chef repository
            </NavLink>
          }
          {' '}
          now!
        </p>
      )}
    </div>
  )
}

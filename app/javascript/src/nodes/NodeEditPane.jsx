import React from 'react'
import { connect } from 'react-redux'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'
import SlidingPane from 'react-sliding-pane'

import nodesActions from './actions'
import NodeHeader from './Node/NodeHeader'
import NodeHistory from './NodeHistory'
import NodeRepositories from './NodeRepositories'
import NodeSettings from './NodeSettings'
import PageLoader from '../components/PageLoader'

class NodeEditPane extends React.Component {
  state = {
    loadingNode: false,
    nodeId: '',
    nodeObject: null
  }

  componentDidMount = () => this.loadNodeIfNeeded()

  componentDidUpdate = () => this.loadNodeIfNeeded()

  loadNode = () => {
    const { nodes } = this.props
    const { nodeId } = this.state

    const currentNode = nodes.find(node => node.id === nodeId)

    // Use the version from the cache
    this.setState({
      loadingNode: false,
      nodeObject: currentNode
    })
  }

  closePane = () => {
    const { dispatch } = this.props

    this.setState({
      nodeId: null
    }, () => dispatch(nodesActions.hideNodeEditPane()))
  }

  loadNodeIfNeeded = () => {
    const { showPaneNodeId } = this.props
    const { loadingNode, nodeId } = this.state

    if (loadingNode) return

    if (nodeId !== showPaneNodeId) {
      this.setState({
        loadingNode: true,
        nodeId: showPaneNodeId
      }, () => this.loadNode())
    }
  }

  render = () => {
    const { openEditPane } = this.props
    const { nodeObject } = this.state

    return (
      <SlidingPane
        closeIcon={<Icofont icon="close-line" size="2" />}
        isOpen={openEditPane}
        onRequestClose={() => this.closePane()}
        title="Node details"
        width="50%"
      >
        {nodeObject === null && (
          <PageLoader message="Loading node ..." />
        )}

        {nodeObject && (
          <React.Fragment>
            <NodeHeader
              contextualMenu={false}
              node={nodeObject}
            />
            <hr />
            <NodeSettings node={nodeObject} />
            <hr />
            <NodeRepositories node={nodeObject} />
            <hr />
            <NodeHistory node={nodeObject} />
          </React.Fragment>
        )}
      </SlidingPane>
    )
  }
}

const mapStateToProps = ({
  nodes: { openEditPane, showPaneNodeId, items }
}) => ({
  nodes: items,
  openEditPane,
  showPaneNodeId
})

NodeEditPane.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nodes: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired,
  openEditPane: PropTypes.bool.isRequired,
  showPaneNodeId: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(NodeEditPane)

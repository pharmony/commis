import React, { useState } from 'react'
import {
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

const NodesFilters = ({ clusters, onFilterUpdate }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false)

  const toggle = () => setDropdownOpen(prevState => !prevState)

  const onClusterFilter = (clusterId) => {
    onFilterUpdate({ clusterId })
    toggle()
  }

  return (
    <div className="d-flex flex-row">
      <div className="d-flex flow-column align-items-center mr-4">
        <strong>Filters:</strong>
      </div>

      <Dropdown isOpen={dropdownOpen} toggle={toggle}>
        <DropdownToggle caret>
          Cluster&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </DropdownToggle>
        <DropdownMenu>
          <Button
            className="dropdown-item"
            key={uuidv1()}
            onClick={() => { onClusterFilter({ clusterId: null }) }}
          >
            All clusters
          </Button>

          {clusters.map(cluster => (
            <Button
              className="dropdown-item"
              key={uuidv1()}
              onClick={() => { onClusterFilter({ clusterId: cluster.id }) }}
            >
              {cluster.name}
            </Button>
          ))}
        </DropdownMenu>
      </Dropdown>
    </div>
  )
}

NodesFilters.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  onFilterUpdate: PropTypes.func.isRequired
}

export default NodesFilters

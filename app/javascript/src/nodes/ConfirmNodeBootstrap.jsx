import React from 'react'
import { Badge } from 'reactstrap'
import confirmModal from 'reactstrap-confirm'
import uuidv1 from 'uuid/v1'

import nodesActions from './actions'

const ConfirmNodeBootstrap = async (
  dispatch,
  nodeAction,
  policy,
  group,
  onConfirm
) => {
  const {
    auth_method: selectedAuthMethod,
    cluster_id: selectedClusterId,
    node_ip_address: nodeIpAddress,
    node_name: nodeName,
    chef_policy_id: selectedPolicyId,
    chef_policy_group_id: selectedPolicyGroupId,
    chef_repository_id: selectedRepositoryId,
    ssh_credential: sshCredential,
    ssh_port: sshPort,
    ssh_username: sshUsername,
    sudo
  } = nodeAction

  const confirm = await confirmModal({
    title: `Bootstrap the node ${nodeName}?`,
    message: (
      <span>
        Are you sure you want to bootstrap the&nbsp;
        <Badge className="fs-100">{policy.name}</Badge>
        &nbsp;policy using the policy group&nbsp;
        <Badge className="fs-100">{group.name}</Badge>
        &nbsp;to the node&nbsp;
        <Badge className="fs-100" color="primary">{nodeName}</Badge>
        &nbsp;?
      </span>
    ),
    confirmColor: 'primary',
    confirmText: 'Bootstrap'
  })

  if (confirm) {
    const nodeActionTmpId = uuidv1()

    if (onConfirm) onConfirm(nodeActionTmpId)

    dispatch(nodesActions.bootstrap(nodeActionTmpId, {
      authMethod: selectedAuthMethod,
      clusterId: selectedClusterId,
      nodeIpAddress,
      nodeName,
      policyId: selectedPolicyId,
      policyGroupId: selectedPolicyGroupId,
      repositoryId: selectedRepositoryId,
      sshCredential,
      sshPort,
      sshUsername,
      sudo
    }))
  }
}

export default ConfirmNodeBootstrap

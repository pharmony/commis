import React from 'react'
import { connect } from 'react-redux'
import { ListGroup } from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import nodeActions from '../actions'
import NodeActionStack from '../../node-action-stacks/NodeActionStack'
import NoNodeHistory from './NoNodeHistory'

class NodeHistory extends React.Component {
  componentDidMount = () => {
    const { dispatch, node: { id } } = this.props

    dispatch(nodeActions.fetchNodeActions(id))
  }

  render = () => {
    const { stacks } = this.props

    if (stacks.length === 0) return <NoNodeHistory />

    const sortedNodeActionStacks = stacks.sort(
      (a, b) => a.created_at < b.created_at
    )

    return (
      <ListGroup className="flex-fill">
        {sortedNodeActionStacks.map(nodeActionStack => (
          <NodeActionStack
            actionStack={nodeActionStack}
            compact
            key={uuidv1()}
          />
        ))}
      </ListGroup>
    )
  }
}

const mapStateToProps = ({
  nodeActionStacks: { items: stacks }
}) => ({
  stacks
})

NodeHistory.propTypes = {
  stacks: PropTypes.arrayOf(PropTypes.shape({
    created_at: PropTypes.string,
    id: PropTypes.string,
    tmpId: PropTypes.string
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
  node: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
}

export default connect(mapStateToProps)(NodeHistory)

import RestfulClient from 'restful-json-api-client'

import RailsHeaders from '../helpers/RailsHeaders'

export class NodesApi extends RestfulClient {
  constructor() {
    super('/api/chef', { resource: 'nodes', headers: RailsHeaders })
  }
}
export class NodeNodeActionsApi extends RestfulClient {
  constructor(nodeId) {
    super(`/api/chef/nodes/${nodeId}`, {
      resource: 'actions',
      headers: RailsHeaders
    })
  }
}

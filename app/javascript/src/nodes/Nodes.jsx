import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

import Node from './Node'
import NodeEditPane from './NodeEditPane'
import nodesActions from './actions'
import NodesFilters from './NodesFilters'

class Nodes extends React.Component {
  state = {
    filter: {},
    showNodeFromUrl: false
  }

  componentDidMount = () => this.showNodeShowPageIfRequested()

  componentDidUpdate = () => this.showNodeShowPageIfRequested()

  // Dispatch the show node show pane action when the URL params has a node ID
  showNodeShowPageIfRequested = () => {
    const { dispatch, match, nodes } = this.props
    const { showNodeFromUrl } = this.state

    if (!match) return

    const { params: { id } } = match

    if (nodes.length === 0 || id === undefined || showNodeFromUrl) return

    const selectedNode = nodes.find(node => node.name === id)

    if (selectedNode) {
      dispatch(nodesActions.showNodeShowPane(selectedNode.id))

      this.setState({
        showNodeFromUrl: true
      })
    }
  }

  updateFilterOptions = ({ clusterId }) => {
    this.setState({
      filter: clusterId
    })
  }

  render = () => {
    const { clusters, nodes } = this.props
    const { filter: { clusterId } } = this.state

    let sortedAndFilteredNodes = nodes.sort(
      (a, b) => a.name.localeCompare(b.name)
    )

    if (clusterId) {
      sortedAndFilteredNodes = sortedAndFilteredNodes.filter(
        node => node.cluster_id === clusterId
      )
    }

    return (
      <React.Fragment>
        <div className="d-flex justify-content-between">
          <NodesFilters
            clusters={clusters}
            onFilterUpdate={options => this.updateFilterOptions(options)}
          />
          <NavLink
            className="btn btn-primary"
            to="/nodes/new"
          >
            Bootstrap node
          </NavLink>
        </div>
        <div className="d-flex flex-row flex-wrap mt-4">
          {sortedAndFilteredNodes.map(node => (
            <Node
              node={node}
              key={uuidv1()}
            />
          ))}
        </div>
        <NodeEditPane />
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  clusters: { items: clusters },
  nodes: { items: nodes }
}) => ({
  clusters,
  nodes
})

Nodes.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.object).isRequired,
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }),
  nodes: PropTypes.arrayOf(PropTypes.object).isRequired
}

Nodes.defaultProps = {
  match: {
    params: {}
  }
}

export default connect(mapStateToProps)(Nodes)

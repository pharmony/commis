import dotProp from 'dot-prop-immutable'

import nodesConstants from './constants'

const initialState = {
  fetchAllFailed: false, // True when back server replied with an error
  fetchingAll: false, // True while fetching all nodes
  fetchOneFailed: false, // True when back server replied with an error
  fetched: false, /* True when nodes has been fetched at least once from the
                     backend server */
  fetchingOne: false, // True while fetching the node from the backend server
  itemCount: 0, // Node count
  items: [], // Nodes from the backend server
  openEditPane: false, // True when the right panel should be visible
  showPaneNodeId: '', // The node ID to be displayed in the right side panel
  stats: {}, // Dashboard node stats object by statuses {'healthy': 3, 'dead': 1}
  updating: false, // True while requesting Node update
  updateFailure: false, // True when something prevented to save the node
  updateSuccess: false // True when the node has been successfully updated
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case nodesConstants.FETCH_ALL_REQUEST:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      return dotProp.set(newState, 'fetchingAll', true)
    case nodesConstants.FETCH_ALL_SUCCESS: {
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      newState = dotProp.set(newState, 'itemCount', action.nodes.length)

      const stats = action.nodes.reduce((data, node) => {
        const stat = data
        stat[node.state] += 1
        return stat
      }, { healthy: 0, degraded: 0, danger: 0 })

      newState = dotProp.set(newState, 'stats', stats)
      return dotProp.set(newState, 'items', action.nodes)
    }
    case nodesConstants.FETCH_ALL_FAILURE:
      newState = dotProp.set(state, 'fetchAllFailed', true)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'items', [])

    case nodesConstants.FETCH_ONE_REQUEST:
      newState = dotProp.set(state, 'fetchOneFailed', false)
      return dotProp.set(newState, 'fetchingOne', true)
    case nodesConstants.FETCH_ONE_SUCCESS: {
      newState = dotProp.set(state, 'fetchOneFailed', false)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingOne', false)
      newState = dotProp.set(newState, 'itemCount', state.items.length + 1)
      newState = dotProp.set(
        newState,
        'stats',
        state.stats[action.node.state] + 1
      )

      return dotProp.set(newState, 'items', [...state.items, action.node])
    }
    case nodesConstants.FETCH_ONE_FAILURE:
      newState = dotProp.set(state, 'fetchOneFailed', true)
      newState = dotProp.set(newState, 'fetched', true)
      return dotProp.set(newState, 'fetchingOne', false)

    case nodesConstants.HIDE_NODE_EDIT_PANE:
      newState = dotProp.set(state, 'openEditPane', false)
      return dotProp.set(newState, 'showPaneNodeId', '')

    case nodesConstants.SHOW_NODE_EDIT_PANE:
      newState = dotProp.set(state, 'openEditPane', true)
      return dotProp.set(newState, 'showPaneNodeId', action.id)

    /*
    ** Node Update
    */
    case nodesConstants.UPDATE_REQUEST:
      newState = dotProp.set(state, 'updating', true)
      newState = dotProp.set(newState, 'updateFailure', false)
      return dotProp.set(newState, 'updateSuccess', false)
    case nodesConstants.UPDATE_FAILURE:
      newState = dotProp.set(state, 'updating', false)
      newState = dotProp.set(newState, 'updateFailure', true)
      return dotProp.set(newState, 'updateSuccess', false)
    case nodesConstants.UPDATE_SUCCESS: {
      const nodeIndex = state.items.findIndex(node => node.id === action.id)
      const nodeObject = state.items.find(node => node.id === action.id)

      newState = dotProp.set(
        state,
        `items.${nodeIndex}`,
        { ...nodeObject, ...action.attributes }
      )
      newState = dotProp.set(newState, 'updating', false)
      newState = dotProp.set(newState, 'updateFailure', false)
      return dotProp.set(newState, 'updateSuccess', true)
    }
    default:
      return state
  }
}

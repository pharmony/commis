import React from 'react'
import { connect } from 'react-redux'
import {
  Form,
  Input,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

const NodeCluster = ({
  clusters,
  onClusterChange,
  selectedClusterId,
  updating
}) => (
  <React.Fragment>
    <p>Cluster</p>
    <div className="d-flex flex-row flex-wrap justify-content-between">
      <Form className="d-flex flex-column flex-fill">
        <div className="d-flex justify-content-between">
          <Label for="chef_node_cluster_id" className="d-none">Method:</Label>
          <Input
            disabled={updating}
            id="chef_node_cluster_id"
            name="chef_node[cluster_id]"
            onChange={event => onClusterChange(event)}
            type="select"
            value={selectedClusterId || ''}
          >
            <option key={uuidv1()} />
            {clusters.map(cluster => (
              <option
                key={uuidv1()}
                value={cluster.id}
              >
                {cluster.name}
              </option>
            ))}
          </Input>
        </div>
      </Form>
    </div>
  </React.Fragment>
)

const mapStateToProps = ({
  clusters: { items: clusters }
}) => ({
  clusters
})

NodeCluster.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.object).isRequired,
  onClusterChange: PropTypes.func.isRequired,
  node: PropTypes.shape({
    id: PropTypes.string.isRequired,
    cluster_id: PropTypes.string
  }).isRequired,
  selectedClusterId: PropTypes.string.isRequired,
  updating: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(NodeCluster)

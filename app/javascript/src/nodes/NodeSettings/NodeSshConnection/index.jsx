import React from 'react'
import { connect } from 'react-redux'
import {
  Form,
  FormGroup,
  Input,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

import SshHelpers from '../../../helpers/SshHelpers'

class NodeSshConnection extends React.Component {
  state = {
    showUsername: false,
    showPassword: false,
    showKeyPath: false
  }

  componentDidMount = () => {
    const { selectedAuthMethod } = this.props

    this.showHideFieldsFrom(selectedAuthMethod)
  }

  showHideFieldsFrom = (value) => {
    switch (value) {
      case 'none':
        this.hideSshFields()
        break
      case 'password':
        this.showUsernameAndPasswordFields()
        break
      case 'ssh_keys':
        this.showUsernameAndKeypathFields()
        break
      default:
        console.warn(`${value} is an unknown option.`)
    }
  }

  handleSshAuthMethodChange = (event) => {
    const { target: { value } } = event
    const { onInputChange } = this.props

    this.showHideFieldsFrom(value)

    onInputChange(event)
  }

  hideSshFields = () => {
    this.setState({
      showUsername: false,
      showPassword: false,
      showKeyPath: false
    })
  }

  showUsernameAndPasswordFields = () => {
    this.setState({
      showUsername: true,
      showPassword: true,
      showKeyPath: false
    })
  }

  showUsernameAndKeypathFields = () => {
    this.setState({
      showUsername: true,
      showPassword: false,
      showKeyPath: true
    })
  }

  render = () => {
    const {
      onInputChange,
      selectedAuthMethod,
      sshAvailableAuthMethods,
      sshKeyPath,
      sshPassword,
      sshPort,
      sshUsername,
      updating
    } = this.props

    const {
      showUsername,
      showPassword,
      showKeyPath
    } = this.state

    return (
      <React.Fragment>
        <p>SSH</p>
        <div className="d-flex flex-row flex-wrap justify-content-between">
          <Form className="d-flex flex-column flex-fill">
            <div className="d-flex justify-content-between">
              <FormGroup>
                <Label for="chef_node_ssh_port">Port:</Label>
                <Input
                  disabled={updating}
                  id="chef_node_ssh_port"
                  name="sshPort"
                  onChange={event => onInputChange(event)}
                  type="text"
                  value={sshPort || '22'}
                />
              </FormGroup>
            </div>
            <div className="d-flex justify-content-between">
              <FormGroup>
                <Label for="chef_node_ssh_auth_method">Method:</Label>
                <Input
                  disabled={updating}
                  id="chef_node_ssh_auth_method"
                  name="selectedAuthMethod"
                  onChange={event => this.handleSshAuthMethodChange(event)}
                  type="select"
                  value={selectedAuthMethod}
                >
                  {sshAvailableAuthMethods.map(authMethod => (
                    <option
                      key={uuidv1()}
                      value={authMethod}
                    >
                      {SshHelpers.sshAuthMethodNameFrom(authMethod)}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              {showUsername && (
                <FormGroup>
                  <Label for="chef_node_ssh_username">Username:</Label>
                  <Input
                    disabled={updating}
                    id="chef_node_ssh_username"
                    name="sshUsername"
                    onChange={event => onInputChange(event)}
                    type="text"
                    value={sshUsername || ''}
                  />
                </FormGroup>
              )}
              {showPassword && (
                <FormGroup>
                  <Label for="chef_node_ssh_password">Password:</Label>
                  <Input
                    disabled={updating}
                    id="chef_node_ssh_password"
                    name="sshPassword"
                    onChange={event => onInputChange(event)}
                    type="password"
                    value={sshPassword || ''}
                  />
                </FormGroup>
              )}
              {showKeyPath && (
                <FormGroup>
                  <Label for="chef_node_ssh_key_path">SSH key path:</Label>
                  <Input
                    disabled={updating}
                    id="chef_node_ssh_key_path"
                    name="sshKeyPath"
                    onChange={event => onInputChange(event)}
                    type="text"
                    value={sshKeyPath || ''}
                  />
                </FormGroup>
              )}
            </div>
          </Form>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  app: { configuration: { ssh: { availableAuthMethods } } }
}) => ({
  sshAvailableAuthMethods: availableAuthMethods
})

NodeSshConnection.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  selectedAuthMethod: PropTypes.string.isRequired,
  sshAvailableAuthMethods: PropTypes.arrayOf(PropTypes.string).isRequired,
  sshKeyPath: PropTypes.string.isRequired,
  sshPassword: PropTypes.string.isRequired,
  sshPort: PropTypes.string.isRequired,
  sshUsername: PropTypes.string.isRequired,
  updating: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(NodeSshConnection)

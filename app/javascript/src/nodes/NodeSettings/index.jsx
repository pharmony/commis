import React from 'react'
import {
  Button
} from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import NodeCluster from './NodeCluster'
import NodeSshConnection from './NodeSshConnection'

import nodeActions from '../actions'

class NodeSettings extends React.Component {
  state = {
    selectedAuthMethod: '',
    selectedClusterId: '',
    sshKeyPath: '',
    sshPassword: '',
    sshPort: '',
    sshUsername: '',
    updated: false
  }

  componentDidMount = () => this.initialiseInputValues()

  componentDidUpdate = () => this.initialiseInputValues()

  initialiseInputValues = () => {
    const { node, sshAvailableAuthMethods } = this.props

    const {
      cluster_id: selectedClusterId,
      ssh_auth_method: sshAuthMethod,
      ssh_key_path: sshKeyPath,
      ssh_password: sshPassword,
      ssh_port: sshPort,
      ssh_username: sshUsername
    } = node

    const { updated } = this.state

    if (updated) return

    const selectedAuthMethod = sshAvailableAuthMethods.find(
      (authMethod, index) => index === sshAuthMethod
    )

    this.setState({
      selectedAuthMethod,
      selectedClusterId,
      sshKeyPath: sshKeyPath || '',
      sshPassword: sshPassword || '',
      sshPort: sshPort.toString(),
      sshUsername,
      updated: true
    })
  }

  handleClusterChange = ({ target: { value } }) => {
    this.setState({
      selectedClusterId: value
    })
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({
      [name]: value
    })
  }

  updateNode = () => {
    const { sshAvailableAuthMethods, dispatch, node } = this.props
    const {
      selectedAuthMethod,
      selectedClusterId,
      sshKeyPath,
      sshPassword,
      sshPort,
      sshUsername
    } = this.state

    const sshAuthMethod = sshAvailableAuthMethods.findIndex(
      authMethod => authMethod === selectedAuthMethod
    )

    dispatch(nodeActions.updateNode(node.id, {
      cluster_id: selectedClusterId,
      ssh_auth_method: sshAuthMethod,
      ssh_key_path: sshKeyPath,
      ssh_password: sshPassword,
      ssh_port: sshPort,
      ssh_username: sshUsername
    }))
  }

  render = () => {
    const {
      node,
      updateFailure,
      updateSuccess,
      updating
    } = this.props

    const {
      selectedAuthMethod,
      selectedClusterId,
      sshKeyPath,
      sshPassword,
      sshPort,
      sshUsername,
      updated
    } = this.state

    if (!updated) return <p>Waiting ...</p>

    return (
      <React.Fragment>
        <NodeCluster
          node={node}
          onClusterChange={event => this.handleClusterChange(event)}
          selectedClusterId={selectedClusterId}
          updating={updating}
        />
        <hr />
        <NodeSshConnection
          node={node}
          onInputChange={event => this.handleInputChange(event)}
          selectedAuthMethod={selectedAuthMethod}
          sshKeyPath={sshKeyPath}
          sshPassword={sshPassword}
          sshPort={sshPort}
          sshUsername={sshUsername}
          updating={updating}
        />

        {updateFailure && (
          <span className="d-flex text-danger">
            Something prevented to update this node.
          </span>
        )}
        {updateSuccess && (
          <span className="d-flex text-success">
            Node successfully updated!
          </span>
        )}


        <div className="d-flex flex-row justify-content-end">
          <Button
            color="primary"
            onClick={event => this.updateNode(event)}
            type="submit"
          >
            Update
          </Button>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  app: { configuration: { ssh: { availableAuthMethods } } },
  nodes: { updating, updateFailure, updateSuccess }
}) => ({
  sshAvailableAuthMethods: availableAuthMethods,
  updateFailure,
  updateSuccess,
  updating
})

NodeSettings.propTypes = {
  dispatch: PropTypes.func.isRequired,
  node: PropTypes.shape({
    cluster_id: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    ssh_auth_method: PropTypes.number,
    ssh_key_path: PropTypes.string,
    ssh_password: PropTypes.string,
    ssh_port: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    ssh_username: PropTypes.string
  }).isRequired,
  sshAvailableAuthMethods: PropTypes.arrayOf(PropTypes.string).isRequired,
  updateFailure: PropTypes.bool.isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updating: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(NodeSettings)

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AlertMessage from '../components/AlertMessage'
import clustersActions from '../clusters/actions'
import nodesActions from './actions'
import Nodes from './Nodes'
import NoNodes from './NoNodes'
import PageLoader from '../components/PageLoader'
import policiesActions from '../policies/actions'
import policyGroupsActions from '../policy-groups/actions'

class NodesPage extends React.Component {
  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(nodesActions.fetchAll())
    dispatch(policiesActions.fetchAll())
    dispatch(policyGroupsActions.fetchAll())
    dispatch(clustersActions.fetchAll())
  }

  render = () => {
    const {
      nodes: { fetchingAll, fetchAllFailed },
      repositoryCount
    } = this.props

    if (fetchingAll === false && fetchAllFailed === false && repositoryCount > 0) {
      return <Nodes {...this.props} />
    }

    return (
      <div className="d-flex flex-column justify-content-center h-100">
        {fetchingAll && (
          <PageLoader message="Loading nodes ..." />
        )}

        {fetchAllFailed && (
          <AlertMessage>
            Unfortunately an error occured while fetching the nodes.
            Please try again.
          </AlertMessage>
        )}

        {fetchingAll === false && fetchAllFailed === false && repositoryCount === 0 && (
          <NoNodes repositoryCount={repositoryCount} />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { nodes } = state
  const { itemCount } = state.repositories

  return {
    nodes,
    repositoryCount: itemCount
  }
}

NodesPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nodes: PropTypes.shape({
    fetchingAll: PropTypes.bool.isRequired,
    fetchAllFailed: PropTypes.bool.isRequired
  }).isRequired,
  repositoryCount: PropTypes.number.isRequired
}

export default connect(mapStateToProps)(NodesPage)

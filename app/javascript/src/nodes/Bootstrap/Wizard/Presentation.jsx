import React from 'react'
import { Button, CustomInput } from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import appActions from '../../../app/actions'

class BootstrapWizardPresentation extends React.Component {
  state = {
    skipStep: false
  }

  moveToNextStep = () => {
    const { dispatch, nextStep } = this.props
    const { skipStep } = this.state

    if (skipStep) {
      dispatch(appActions.updatePreferences({
        skipbootstrapWizardPresentation: skipStep
      }))
    }

    nextStep()
  }

  toggleSkipStep = () => {
    const { skipStep } = this.state

    this.setState({
      skipStep: !skipStep
    })
  }

  render = () => {
    const { skipStep } = this.state
    const { show } = this.props

    if (show === false) return null

    return (
      <React.Fragment>
        <br />
        <p className="lead">
          This page allows you to add new nodes to your chef repository as you
          would do with the&nbsp;
          <code>knife zero bootstrap</code>
          &nbsp;command.
        </p>

        <p>
          In order to bootstrap a node you will have to select the chef
          repository where you&apos;d like to add the new node, the policy name
          and group to use, and finally the node SSH connection details like the
          IP address and port to open the connection, and the authentication
          method and credentials.
        </p>

        <p>
          After having submitted the form, you will be redirected to a page
          where you will follow the node bootstrapping by looking at the Chef
          logs.
        </p>

        <p>
          At the end of the bootstrapping process, if all went well, the new
          node JSON file will be added and commited to you chef repository.
        </p>

        <div className="d-flex flex-row justify-content-between align-items-center mt-5">
          <CustomInput
            id="skip-form-presentation-step"
            label="Don't show this step next time"
            name="skip-step"
            onChange={() => this.toggleSkipStep()}
            type="switch"
            value={skipStep}
          />

          <Button
            color="success"
            onClick={() => this.moveToNextStep()}
          >
            Let&apos;s get started
          </Button>
        </div>
      </React.Fragment>
    )
  }
}

BootstrapWizardPresentation.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nextStep: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired
}

export default connect()(BootstrapWizardPresentation)

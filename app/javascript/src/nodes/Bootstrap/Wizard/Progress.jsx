import React from 'react'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid/v1'

const BootstrapWizardProgress = ({ show, step, stepCount }) => {
  if (show === false) return null

  return (
    <div className="d-flex justify-content-around wizard-progress my-3">
      {Array(stepCount).fill().map((_, index) => (
        <React.Fragment key={uuidv1()}>
          <div className="d-flex flex-column justify-content-center">
            <div className={`step ${step === (index + 1) ? 'done' : ''}`}>
              {index + 1}
            </div>
          </div>

          {(index + 1) < stepCount && (
            <Icofont
              icon="long-arrow-right"
              size="5"
            />
          )}
        </React.Fragment>
      ))}
    </div>
  )
}

BootstrapWizardProgress.propTypes = {
  show: PropTypes.bool.isRequired,
  step: PropTypes.number.isRequired,
  stepCount: PropTypes.number.isRequired
}

export default BootstrapWizardProgress

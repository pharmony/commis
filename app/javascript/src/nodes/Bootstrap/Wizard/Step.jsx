import React from 'react'
import PropTypes from 'prop-types'

const BootstrapWizardStep = ({ children, show, title }) => {
  if (show === false) return null

  return (
    <div className="d-flex flex-column">
      <h2 className="h4 text-center my-4">{title}</h2>
      {children}
    </div>
  )
}

BootstrapWizardStep.propTypes = {
  children: PropTypes.element.isRequired,
  show: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
}

export default BootstrapWizardStep

import React from 'react'
import { connect } from 'react-redux'
import {
  Button,
  Container,
  CustomInput,
  Form,
  FormGroup,
  Input,
  Jumbotron,
  Label
} from 'reactstrap'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import uuidv1 from 'uuid/v1'

import AlertMessage from '../../components/AlertMessage'
import BootstrapWizardPresentation from './Wizard/Presentation'
import BootstrapWizardProgress from './Wizard/Progress'
import BootstrapWizardStep from './Wizard/Step'
import clustersActions from '../../clusters/actions'
import history from '../../helpers/History'
import nodesActions from '../actions'
import policiesActions from '../../policies/actions'
import policyGroupsActions from '../../policy-groups/actions'
import SshAuthenticationMethodSelector from '../../components/SshAuthenticationMethodSelector'

class NodesBootstrapPage extends React.Component {
  state = {
    bootstrapRequestFailed: false,
    nodeActionTmpId: '',
    nodeIpAddress: '',
    nodeName: '',
    selectedClusterId: '',
    selectedRepositoryId: '',
    selectedAuthMethod: '',
    selectedPolicyGroupId: '',
    selectedPolicyId: '',
    sshKeyPath: '',
    sshPassword: '',
    sshPort: '22',
    sshUsername: 'root',
    stepCount: 2,
    submitted: false,
    sudo: true,
    visibleStep: 0
  }

  componentDidMount = () => {
    const {
      dispatch,
      location: { search },
      repositories,
      skipbootstrapWizardPresentation
    } = this.props

    const parsed = queryString.parse(search)

    this.setState({
      selectedRepositoryId: (repositories.length === 1 ? repositories[0].id : parsed.repository_id) || '',
      visibleStep: skipbootstrapWizardPresentation ? 1 : 0
    })

    dispatch(clustersActions.fetchAll())

    dispatch(policiesActions.fetchAll())

    dispatch(policyGroupsActions.fetchAll())
  }

  componentDidUpdate = () => {
    const { nodeActionStacks } = this.props
    const { nodeActionTmpId, submitted } = this.state

    const temporaryNodeAction = nodeActionStacks.find(
      stack => stack.tmpId === nodeActionTmpId
    )

    if (submitted && temporaryNodeAction) {
      if (temporaryNodeAction.id) {
        if (temporaryNodeAction.failed) {
          this.setState({
            bootstrapRequestFailed: true,
            submitted: false
          })
        } else {
          history.push(`/node-action-stacks/${temporaryNodeAction.id}`)
        }
      }
    }
  }

  boostrapNode = (event) => {
    const { dispatch } = this.props

    const {
      nodeIpAddress,
      nodeName,
      selectedAuthMethod,
      selectedClusterId,
      selectedRepositoryId,
      selectedPolicyGroupId,
      selectedPolicyId,
      sshKeyPath,
      sshPassword,
      sshPort,
      sshUsername,
      sudo
    } = this.state

    event.preventDefault()

    if (this.formIsNotValid()) return

    const nodeActionTmpId = uuidv1()

    this.setState({
      nodeActionTmpId,
      submitted: true
    }, () => {
      dispatch(nodesActions.bootstrap(nodeActionTmpId, {
        authMethod: selectedAuthMethod,
        clusterId: selectedClusterId,
        nodeIpAddress,
        nodeName,
        policyId: selectedPolicyId,
        policyGroupId: selectedPolicyGroupId,
        repositoryId: selectedRepositoryId,
        sshCredential: sshPassword || sshKeyPath,
        sshPort,
        sshUsername,
        sudo
      }))
    })
  }

  formIsNotValid = () => {
    const { stepCount } = this.state

    return Array(stepCount).fill().find(
      (_, index) => this.stepIsNotValid(index + 1)
    )
  }

  handleSshAuthenticationMethodSelectorInputChange = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({
      [name]: value
    })
  }

  onNextStep = (event) => {
    const { visibleStep } = this.state

    if (event) event.preventDefault()

    this.setState({
      visibleStep: visibleStep + 1
    })
  }

  previousStep = (event) => {
    const { visibleStep } = this.state

    if (event) event.preventDefault()

    this.setState({
      visibleStep: visibleStep - 1
    })
  }

  stepIsNotValid = (step) => {
    const {
      nodeIpAddress,
      nodeName,
      selectedAuthMethod,
      selectedPolicyGroupId,
      selectedPolicyId,
      selectedRepositoryId,
      sshKeyPath,
      sshPassword,
      sshPort,
      sshUsername,
      visibleStep
    } = this.state

    switch (step || visibleStep) {
      // New node repository and policy
      case 1:
        if (selectedPolicyGroupId === '') return true
        if (selectedPolicyId === '') return true
        if (selectedRepositoryId === '') return true

        return false
      // New node SSH connection details
      case 2:
        if (nodeIpAddress === '' || ['', 'none'].includes(selectedAuthMethod)) {
          return true
        }
        if (nodeName === '') return true
        if (sshPort === '' || sshPort === '0') return true

        if (sshUsername === '') return true
        if (selectedAuthMethod === 'password' && sshPassword === '') return true
        if (selectedAuthMethod === 'ssh_keys' && sshKeyPath === '') return true
        if (selectedRepositoryId === '') return true

        return false
      default:
        return true
    }
  }

  toggleNodeSudo = () => {
    const { sudo } = this.state

    this.setState({
      sudo: !sudo
    })
  }

  render = () => {
    const {
      clusters,
      policies,
      policyGroups,
      repositories
    } = this.props

    const {
      bootstrapRequestFailed,
      nodeIpAddress,
      nodeName,
      selectedAuthMethod,
      selectedClusterId,
      selectedPolicyGroupId,
      selectedPolicyId,
      selectedRepositoryId,
      sshKeyPath,
      sshPassword,
      sshPort,
      sshUsername,
      stepCount,
      submitted,
      sudo,
      visibleStep
    } = this.state

    return (
      <Jumbotron className="wizard">
        <h1>Node bootstrap</h1>

        <BootstrapWizardPresentation
          nextStep={() => this.onNextStep()}
          show={visibleStep === 0}
        />

        <BootstrapWizardProgress
          show={visibleStep > 0}
          step={visibleStep}
          stepCount={stepCount}
        />

        <BootstrapWizardStep
          show={visibleStep === 1}
          title="New node repository and policy"
        >
          <Container className="w-50">
            <Form>
              <h3 className="h5">Node repository</h3>
              <FormGroup>
                <Label for="selected-repository-id">Repository:</Label>
                {repositories.length === 1 ? (
                  <Input
                    disabled
                    type="select"
                    value={selectedRepositoryId}
                  >
                    <option
                      value={repositories[0].id}
                    >
                      {repositories[0].name}
                    </option>
                  </Input>
                ) : (
                  <Input
                    id="selected-repository-id"
                    name="selectedRepositoryId"
                    onChange={event => this.handleInputChange(event)}
                    type="select"
                    value={selectedRepositoryId}
                  >
                    <option
                      key={uuidv1()}
                      value=""
                    >
                      Select a repository
                    </option>
                    {repositories.map(repository => (
                      <option
                        key={uuidv1()}
                        value={repository.id}
                      >
                        {repository.name}
                      </option>
                    ))}
                  </Input>
                )}
              </FormGroup>

              <br />
              <h3 className="h5">Node cluster</h3>
              <FormGroup>
                <Label for="selected-cluster-id">Cluster:</Label>
                <Input
                  id="selected-cluster-id"
                  name="selectedClusterId"
                  onChange={event => this.handleInputChange(event)}
                  type="select"
                  value={selectedClusterId}
                >
                  <option
                    key={uuidv1()}
                    value=""
                  >
                    Select a cluster
                  </option>
                  {clusters.map(cluster => (
                    <option
                      key={uuidv1()}
                      value={cluster.id}
                    >
                      {cluster.name}
                    </option>
                  ))}
                </Input>
              </FormGroup>

              <br />
              <h3 className="h5">Node policy</h3>
              <FormGroup>
                <Label for="selected-policy-id">Policy:</Label>
                <Input
                  id="selected-policy-id"
                  name="selectedPolicyId"
                  onChange={event => this.handleInputChange(event)}
                  type="select"
                  value={selectedPolicyId}
                >
                  <option
                    key={uuidv1()}
                    value=""
                  >
                    Select a policy
                  </option>
                  {policies.map(policy => (
                    <option
                      key={uuidv1()}
                      value={policy.id}
                    >
                      {policy.name}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="selected-policy-group-id">Policy group:</Label>
                <Input
                  id="selected-policy-group-id"
                  name="selectedPolicyGroupId"
                  onChange={event => this.handleInputChange(event)}
                  type="select"
                  value={selectedPolicyGroupId}
                >
                  <option
                    key={uuidv1()}
                    value=""
                  >
                    Select a policy group
                  </option>
                  {policyGroups.map(policyGroup => (
                    <option
                      key={uuidv1()}
                      value={policyGroup.id}
                    >
                      {policyGroup.name}
                    </option>
                  ))}
                </Input>
              </FormGroup>
              <Button
                className="float-right"
                disabled={this.stepIsNotValid()}
                color="success"
                onClick={event => this.onNextStep(event)}
                type="submit"
              >
                Next
              </Button>
            </Form>
          </Container>
        </BootstrapWizardStep>
        <BootstrapWizardStep
          show={visibleStep === 2}
          title="New node SSH connection details"
        >
          <Container className="w-50">
            <Form>
              <h3 className="h5">Node details</h3>
              <FormGroup>
                <Label for="node-name">Name:</Label>
                <Input
                  id="node-name"
                  name="nodeName"
                  onChange={event => this.handleInputChange(event)}
                  placeholder="Enter the node name."
                  value={nodeName}
                />
              </FormGroup>
              <FormGroup className="d-flex flex-row">
                <div className="d-flex flex-column flex-fill">
                  <Label for="node-ip-address">IP Address:</Label>
                  <Input
                    id="node-ip-address"
                    name="nodeIpAddress"
                    onChange={event => this.handleInputChange(event)}
                    placeholder="Enter the IP address of the node to be bootstraped."
                    value={nodeIpAddress}
                  />
                </div>
                <div className="d-flex flex-column">
                  <Label for="node-ssh-port">SSH Port:</Label>
                  <Input
                    id="node-ssh-port"
                    name="sshPort"
                    onChange={event => this.handleInputChange(event)}
                    value={sshPort}
                  />
                </div>
              </FormGroup>
              <FormGroup>
                <Label for="node-sudo">Sudo</Label>
                <CustomInput
                  id="node-sudo"
                  label="Use sudo when running a command."
                  name="sudo"
                  onChange={() => this.toggleNodeSudo()}
                  type="switch"
                  defaultChecked={sudo}
                />
              </FormGroup>
              <h3 className="h5">SSH</h3>
              <SshAuthenticationMethodSelector
                handleInputChange={(name, value) => {
                  this.handleSshAuthenticationMethodSelectorInputChange(name, value)
                }}
                selectedAuthMethod={selectedAuthMethod}
                sshCredential={
                  selectedAuthMethod === 'password' ? sshPassword : sshKeyPath
                }
                sshUsername={sshUsername}
              />
              {bootstrapRequestFailed && (
                <AlertMessage>
                  Something prevented the bootstrap request to be created.
                  Please try again.
                </AlertMessage>
              )}

              <Button
                color="primary"
                onClick={event => this.previousStep(event)}
                type="submit"
              >
                Previous
              </Button>

              <Button
                className="float-right"
                disabled={this.stepIsNotValid() || submitted}
                color="success"
                onClick={event => this.boostrapNode(event)}
                type="submit"
              >
                {`Bootstrap${submitted ? 'ping ...' : ''}`}
              </Button>
            </Form>
          </Container>
        </BootstrapWizardStep>
      </Jumbotron>
    )
  }
}

const mapStateToProps = ({
  app: {
    configuration: { ssh: { availableAuthMethods } },
    preferences: { skipbootstrapWizardPresentation }
  },
  clusters: { items: clusters },
  nodeActionStacks: { items: nodeActionStacks },
  policies: { items: policies },
  policyGroups: { items: policyGroups },
  repositories: { items: repositories }
}) => ({
  clusters,
  nodeActionStacks,
  policies,
  policyGroups,
  repositories,
  skipbootstrapWizardPresentation,
  sshAvailableAuthMethods: availableAuthMethods
})

NodesBootstrapPage.propTypes = {
  clusters: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string
  }),
  nodeActionStacks: PropTypes.arrayOf(PropTypes.shape({
    tmpId: PropTypes.string
  })).isRequired,
  policies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  policyGroups: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  repositories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  skipbootstrapWizardPresentation: PropTypes.bool.isRequired
}

NodesBootstrapPage.defaultProps = {
  location: {}
}

export default connect(mapStateToProps)(NodesBootstrapPage)

import _ from 'lodash'
import ApiUtils from '../helpers/ApiUtils'
import { NodeActionStacksApi } from '../node-action-stacks/apis'
import { NodesApi, NodeNodeActionsApi } from './api'

const bootstrap = (options) => {
  const railsOptions = _.mapKeys(options, (v, k) => _.snakeCase(k))

  return new NodeActionStacksApi()
    .create(Object.assign({
      name: 'bootstrap'
    }, railsOptions))
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
}

const converge = (nodeId, chefRepositoryId) => (
  new NodeNodeActionsApi(nodeId)
    .create({
      chef_repository_id: chefRepositoryId,
      name: 'converge'
    })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchAll = () => (
  new NodesApi()
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchNodeActions = id => (
  new NodeNodeActionsApi(id)
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const fetchNodeActionStack = (id, nodeId) => {
  let apiResponse = null

  if (nodeId) {
    apiResponse = new NodeNodeActionsApi(nodeId).get({ id })
  } else {
    apiResponse = new NodeActionStacksApi().get({ id })
  }

  return apiResponse
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
}

const fetchOne = id => (
  new NodesApi()
    .get({ id })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const pullRepoAndConverge = (nodeId, chefRepositoryId) => (
  new NodeNodeActionsApi(nodeId)
    .create({
      name: 'converge',
      chef_repository_id: chefRepositoryId,
      pull_chef_repo: true
    })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const update = (id, attributes) => (
  new NodesApi()
    .update(id, attributes)
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  bootstrap,
  converge,
  fetchAll,
  fetchNodeActions,
  fetchNodeActionStack,
  fetchOne,
  pullRepoAndConverge,
  update
}

import React from 'react'
import { connect } from 'react-redux'
import {
  Button,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Media
} from 'reactstrap'
import PropTypes from 'prop-types'

import ConfirmNodeConverge from '../ConfirmNodeConverge'
import ConfirmPullRepoAndNodeConverge from './ConfirmPullRepoAndNodeConverge'
import GitLogoHelper from '../../helpers/GitLogoHelper'
import PageLoader from '../../components/PageLoader'

class NodeRepositoryAction extends React.Component {
  state = {
    convergeButtonOpen: false,
    policy: null,
    policyGroup: null,
    nodeRepository: null
  }

  componentDidMount = () => this.buildComponentData()

  buildComponentData = () => {
    const {
      node,
      policies,
      policyGroups,
      repositories,
      repository
    } = this.props

    const nodeRepository = repositories.find(repo => repo.id === repository.id)

    const nodePolicy = node.chef_policies.find(
      policy => policy.chef_repository_id === repository.id
    )

    const nodePolicyGroup = node.chef_policy_groups.find(
      policyGroup => policyGroup.chef_repository_id === repository.id
    )

    let currentPolicy

    if (nodePolicy) {
      currentPolicy = policies.find(
        policy => policy.id === nodePolicy.id
      )
    }

    let currentPolicyGroup

    if (nodePolicyGroup) {
      currentPolicyGroup = policyGroups.find(
        policyGroup => policyGroup.id === nodePolicyGroup.id
      )
    }

    this.setState({
      policy: currentPolicy,
      policyGroup: currentPolicyGroup,
      nodeRepository
    })
  }

  toggleConvergeButtonDropdown = () => {
    const { convergeButtonOpen } = this.state

    this.setState({
      convergeButtonOpen: !convergeButtonOpen
    })
  }

  render = () => {
    const {
      dispatch,
      node
    } = this.props
    const {
      convergeButtonOpen,
      policy,
      policyGroup,
      nodeRepository
    } = this.state

    if (!policy || !policyGroup) {
      return (
        <tr>
          <td colSpan="4">
            <PageLoader message="Loading data ..." />
          </td>
        </tr>
      )
    }

    return (
      <tr>
        <td>
          <Media className="d-flex flex-row align-items-center">
            <Media left>
              <Media
                className="img-sm"
                object
                src={GitLogoHelper.fromUrl(nodeRepository.url)}
              />
            </Media>
            <Media body>
              {nodeRepository.name}
            </Media>
          </Media>
        </td>
        <td>{policy.name}</td>
        <td>{policyGroup.name}</td>
        <td>
          <div className="d-flex flex-row justify-content-end">
            <ButtonDropdown
              isOpen={convergeButtonOpen}
              toggle={() => this.toggleConvergeButtonDropdown()}
            >
              <Button
                className="btn btn-success float-right"
                onClick={() => ConfirmNodeConverge(dispatch, node, nodeRepository)}
              >
                Converge
              </Button>
              <DropdownToggle caret color="success" />
              <DropdownMenu>
                <DropdownItem
                  onClick={() => {
                    ConfirmPullRepoAndNodeConverge(dispatch, node, nodeRepository)
                  }}
                >
                  Pull Repo & Converge
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </div>
        </td>
      </tr>
    )
  }
}

const mapStateToProps = ({
  policies: { items: policies },
  policyGroups: { items: policyGroups },
  repositories: { items: repositories }
}) => ({
  policies,
  policyGroups,
  repositories
})

NodeRepositoryAction.propTypes = {
  dispatch: PropTypes.func.isRequired,
  node: PropTypes.shape({
    chef_policies: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      chef_repository_id: PropTypes.string.isRequired
    })),
    chef_policy_groups: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      chef_repository_id: PropTypes.string.isRequired
    })).isRequired
  }).isRequired,
  policies: PropTypes.arrayOf(PropTypes.object).isRequired,
  policyGroups: PropTypes.arrayOf(PropTypes.object).isRequired,
  repositories: PropTypes.arrayOf(PropTypes.object).isRequired,
  repository: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
}

export default connect(mapStateToProps)(NodeRepositoryAction)

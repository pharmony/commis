import React from 'react'
import confirmModal from 'reactstrap-confirm'
import { Badge, Media } from 'reactstrap'
import uuidv1 from 'uuid'

import GitLogoHelper from '../../helpers/GitLogoHelper'
import nodesActions from '../actions'

const ConfirmPullRepoAndNodeConverge = async (
  dispatch,
  node,
  repository,
  onConfirm
) => {
  const confirm = await confirmModal({
    title: `Pull repository and converge ${node.name}?`,
    message: (
      <span>
        Are you sure you want to pull the chef repository and then converge
        the node&nbsp;
        <Badge className="fs-100" color="primary">{node.name}</Badge>
        &nbsp;using the Chef repository&nbsp;
        <Badge className="fs-100">
          <Media className="d-flex flex-row align-items-center">
            <Media left>
              <Media
                className="img-xs"
                object
                src={GitLogoHelper.fromUrl(repository.url)}
              />
            </Media>
            <Media body>
              {repository.name}
            </Media>
          </Media>
        </Badge>
        &nbsp;?
      </span>
    ),
    confirmColor: 'primary',
    confirmText: 'Converge'
  })

  if (confirm) {
    const nodeActionTmpId = uuidv1()

    if (onConfirm) onConfirm(nodeActionTmpId)

    dispatch(nodesActions.pullRepoAndConverge(
      nodeActionTmpId, node.name, repository.id
    ))
  }
}

export default ConfirmPullRepoAndNodeConverge

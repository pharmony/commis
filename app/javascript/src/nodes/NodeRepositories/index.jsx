import React from 'react'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import NodeRepositoryAction from './NodeRepositoryAction'

const NodeRepositories = ({ node }) => (
  <React.Fragment>
    <p>Repositories</p>
    <table className="w-100">
      <thead>
        <tr>
          <td>Name</td>
          <td>Policy Name</td>
          <td>Policy Group</td>
          <td />
        </tr>
      </thead>
      <tbody>
        {node.chef_repositories.map(chefRepository => (
          <NodeRepositoryAction
            key={uuidv1()}
            node={node}
            repository={chefRepository}
          />
        ))}
      </tbody>
    </table>
  </React.Fragment>
)

NodeRepositories.propTypes = {
  node: PropTypes.shape({
    chef_repositories: PropTypes.arrayOf(PropTypes.object)
  }).isRequired
}

export default NodeRepositories

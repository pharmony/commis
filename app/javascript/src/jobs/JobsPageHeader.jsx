import React from 'react'
import Icofont from 'react-icofont'

export default () => (
  <div className="d-flex flex-column align-items-end">
    <a
      className="btn btn-success"
      href="/sidekiq"
      rel="noopener noreferrer"
      target="_blank"
    >
      <span className="mr-1">Sidekiq</span>
      <Icofont icon="external-link" />
    </a>
  </div>
)

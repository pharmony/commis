import React from 'react'
import {
  Badge,
  ListGroup,
  ListGroupItem,
  ListGroupItemText,
  Popover,
  PopoverBody,
  PopoverHeader
} from 'reactstrap'
import { connect } from 'react-redux'
import moment from 'moment'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import uuidv1 from 'uuid/v1'

import BadgeHelpers from '../helpers/BadgeHelpers'
import StringHelpers from '../helpers/StringHelpers'

class BackgroundTasksPreview extends React.Component {
  state = {}

  badgeColorFrom = (status) => {
    switch (status) {
      case 'enqueued':
        return 'info'
      case 'running':
        return 'success'
      case 'failed':
        return 'danger'
      default:
        return 'info'
    }
  }

  render = () => {
    const {
      jobs,
      visible
    } = this.props

    const nonFinishedJobs = jobs.filter(job => job.status !== 'finished')

    return (
      <Popover
        isOpen={visible}
        placement="right"
        target="background-tasks-preview"
      >
        <PopoverHeader>Background Tasks</PopoverHeader>
        <PopoverBody>
          {nonFinishedJobs.length === 0 && (
            <div className="lead text-center py-3">No background tasks right now.</div>
          )}

          {nonFinishedJobs.length > 0 && (
            <ListGroup className="flex-fill">
              {jobs.map(job => (
                <ListGroupItem className="d-flex justify-content-between" key={uuidv1()}>
                  <ListGroupItemText className="mb-0 d-flex flex-column">
                    <strong>{job.name}</strong>
                    <span>{moment(job.requested).fromNow()}</span>
                  </ListGroupItemText>
                  <ListGroupItemText className="d-flex align-items-center" tag="div">
                    <Badge color={BadgeHelpers.colorFrom(job.status)}>
                      {StringHelpers.titleize(job.status)}
                    </Badge>
                  </ListGroupItemText>
                </ListGroupItem>
              ))}
            </ListGroup>
          )}

          <NavLink
            className="d-flex justify-content-center btn btn-success mt-2"
            to="/jobs"
          >
            See all jobs
          </NavLink>
        </PopoverBody>
      </Popover>
    )
  }
}

const mapStateToProps = ({ jobs: { items } }) => ({ jobs: items })

BackgroundTasksPreview.propTypes = {
  jobs: PropTypes.arrayOf(PropTypes.shape({
    status: PropTypes.string.isRequired
  })).isRequired,
  visible: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(BackgroundTasksPreview)

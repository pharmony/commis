import dotProp from 'dot-prop-immutable'

import actionCableConstants from '../action-cable/constants'
import jobsConstants from './constants'

const initialState = {
  fetchStatsFailed: false, /* True when the backend server returned an error
                              on fetching stats */
  initialised: false, /* False when Commis has just been loaded, true after the
                         first load from the backend server */
  jobCount: 0, // Number of jobs no matter their states
  items: [], // Job oject list used to be displayed in the preview and jobs page
  openShowPane: false, // True when the right panel should be open
  showPaneJobId: '', // ID of the Job to be shown in the right panel
  stats: {
    processSetSize: 0 /* Number of processes running the jobs. When 0 Commis
                         shows a warning icon in the left menu */
  }
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case actionCableConstants.DATA_RECEIVED:
      if (action.data) {
        // Backend server connectivity
        if (action.data.stats) {
          newState = dotProp.set(state, 'fetchStatsFailed', false)
          newState = dotProp.set(newState, 'initialised', true)

          if (action.data.stats && action.data.stats.jobs) {
            action.data.stats.jobs.map((job) => {
              newState = dotProp.set(
                newState,
                'items',
                [...state.items, job]
              )
              return newState
            })

            newState = dotProp.set(newState, 'jobCount', newState.items.length)
          }

          return dotProp.set(
            newState,
            'stats.processSetSize',
            action.data.stats.process_set_size
          )
        }

        // Background Tasks updates
        if (action.data.background_tasks) {
          const { action: taskAction, job } = action.data.background_tasks
          switch (taskAction) {
            case 'add': {
              const jobIndex = state.items.findIndex(item => item.id === job.id)

              // The job is already present, it's just a retry job.
              if (jobIndex > -1) return state

              newState = dotProp.set(state, 'jobCount', state.jobCount + 1)
              return dotProp.set(
                newState,
                'items',
                [...state.items, job]
              )
            }
            case 'update': {
              const { id, status } = job
              const jobIndex = state.items.findIndex(item => item.id === id)

              if (jobIndex === -1) return state

              newState = dotProp.set(
                state,
                `items.${jobIndex}.status`,
                status
              )

              if (job.error) {
                newState = dotProp.set(
                  newState,
                  `items.${jobIndex}.error`,
                  job.error
                )
              }

              return newState
            }
            case 'delete': {
              const { id } = job
              const jobIndex = state.items.findIndex(item => item.id === id)
              if (jobIndex === -1) return state
              newState = dotProp.set(state, 'jobCount', state.jobCount - 1)
              return dotProp.delete(newState, `items.${jobIndex}`)
            }
            default:
              console.warn(`[BackgroundTasks reducer] Unknown action ${taskAction}.`)
              return state
          }
        } else {
          return state
        }
      } else {
        return state
      }
    case actionCableConstants.CONNECTED:
      return dotProp.set(state, 'fetchStatsFailed', false)
    case actionCableConstants.DISCONNECTED:
      return dotProp.set(state, 'fetchStatsFailed', true)

    case jobsConstants.HIDE_JOB_SHOW_PANE:
      newState = dotProp.set(state, 'openShowPane', false)
      return dotProp.set(newState, 'showPaneJobId', '')

    case jobsConstants.SHOW_JOB_SHOW_PANE:
      newState = dotProp.set(state, 'openShowPane', true)
      return dotProp.set(newState, 'showPaneJobId', action.id)

    default:
      return state
  }
}

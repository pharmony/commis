import React from 'react'

import JobsPageHeader from './JobsPageHeader'
import JobsPage from './JobsPage'

export default () => (
  <React.Fragment>
    <JobsPageHeader />
    <JobsPage />
  </React.Fragment>
)

import jobsConstants from './constants'

const hideJobShowPane = () => dispatch => dispatch({
  type: jobsConstants.HIDE_JOB_SHOW_PANE
})

const showJobShowPane = id => dispatch => dispatch({
  id,
  type: jobsConstants.SHOW_JOB_SHOW_PANE
})

export default {
  hideJobShowPane,
  showJobShowPane
}

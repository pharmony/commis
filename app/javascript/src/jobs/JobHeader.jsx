import React from 'react'
import {
  Badge,
  Media
} from 'reactstrap'
import Icofont from 'react-icofont'
import PropTypes from 'prop-types'

import BadgeHelpers from '../helpers/BadgeHelpers'
import StringHelpers from '../helpers/StringHelpers'

const JobHeader = ({ job: { id, name, status } }) => (
  <Media>
    <Media className="d-flex flex-column align-items-center mr-3" left>
      <Icofont className="mb-1" icon="gear" size="5" />
      <div className="d-flex">
        <Badge color={BadgeHelpers.colorFrom(status)}>
          {StringHelpers.titleize(status)}
        </Badge>
      </div>
    </Media>
    <Media body>
      <div className="d-flex flex-column">
        <h2>{name}</h2>
        <div className="d-flex align-items-center">
          <Icofont className="mr-2" icon="finger-print" size="2" />
          <p className="mb-0 lead">{id}</p>
        </div>
      </div>
    </Media>
  </Media>
)

JobHeader.propTypes = {
  job: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired
  }).isRequired
}

export default JobHeader

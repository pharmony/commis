import React from 'react'
import { connect } from 'react-redux'
import {
  Badge,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
  Media
} from 'reactstrap'
import Icofont from 'react-icofont'
import moment from 'moment'
import PropTypes from 'prop-types'

import BadgeHelpers from '../helpers/BadgeHelpers'
import jobsActions from './actions'
import StringHelpers from '../helpers/StringHelpers'

class Job extends React.Component {
  showJobShowPane = () => {
    const { dispatch, job: { id } } = this.props

    dispatch(jobsActions.showJobShowPane(id))
  }

  render = () => {
    const { job } = this.props
    const {
      id,
      name,
      status,
      requested
    } = job

    return (
      <ListGroupItem>
        <Media className="align-items-center">
          <Media left className="d-flex flex-column align-items-center mr-3">
            <Icofont className="mb-1" icon="gear" size="3" />
            <div className="d-flex">
              <Badge color={BadgeHelpers.colorFrom(status)}>
                {StringHelpers.titleize(status)}
              </Badge>
            </div>
          </Media>
          <Media body>
            <ListGroupItemHeading
              className="btn-link cursor-pointer"
              onClick={() => this.showJobShowPane()}
            >
              {name}
              <small>
                <i>{` -- ${id}`}</i>
              </small>
            </ListGroupItemHeading>
            <ListGroupItemText className="mb-0">
              {moment(requested).fromNow()}
            </ListGroupItemText>
          </Media>
        </Media>
      </ListGroupItem>
    )
  }
}

Job.propTypes = {
  dispatch: PropTypes.func.isRequired,
  job: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired
}

export default connect()(Job)

import React from 'react'
import Icofont from 'react-icofont'
import { Tooltip } from 'reactstrap'

export default class BackgroundTasksWarning extends React.Component {
  mounted = false

  state = {
    tooltipOpen: false
  }

  componentDidMount = () => {
    this.mounted = true
  }

  componentWillUnmount = () => {
    this.mounted = false
  }

  toggleTooltip = () => {
    const { tooltipOpen } = this.state

    if (!this.mounted) return

    this.setState({
      tooltipOpen: !tooltipOpen
    })
  }

  render = () => {
    const { tooltipOpen } = this.state

    return (
      <React.Fragment>
        <Icofont
          className="warning-icon"
          id="background-tasks-warning"
          icon="warning"
        />
        <Tooltip
          isOpen={tooltipOpen}
          placement="right"
          target="background-tasks-warning"
          toggle={() => this.toggleTooltip()}
        >
          Unable to get the background task stats.
        </Tooltip>
      </React.Fragment>
    )
  }
}

import React from 'react'
import { connect } from 'react-redux'
import { ListGroup } from 'reactstrap'
import uuidv1 from 'uuid/v1'

import Job from './Job'
import JobShowPane from './JobShowPane'
import NoJobs from './NoJobs'

const JobsPage = ({ jobs }) => (
  <div className="d-flex flex-row justify-content-center h-100">
    {jobs.length === 0 && (
      <NoJobs />
    )}

    {jobs.length > 0 && (
      <React.Fragment>
        <ListGroup className="flex-fill mt-4">
          {jobs.map(job => (
            <Job job={job} key={uuidv1()} />
          ))}
        </ListGroup>
        <JobShowPane />
      </React.Fragment>
    )}
  </div>
)

const mapStateToProps = ({ jobs: { items } }) => ({ jobs: items })

export default connect(mapStateToProps)(JobsPage)

import React from 'react'

export default () => (
  <div className="d-flex flex-column align-items-center align-self-center w-50">
    <p className="lead text-center">
      No background jobs yet.
    </p>
  </div>
)

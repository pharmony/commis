import React from 'react'
import {
  Button
} from 'reactstrap'
import { connect } from 'react-redux'
import Icofont from 'react-icofont'
import { NavLink } from 'react-router-dom'
import SlidingPane from 'react-sliding-pane'

import jobsActions from './actions'
import JobHeader from './JobHeader'
import PageLoader from '../components/PageLoader'

class JobShowPane extends React.Component {
  state = {
    isOpen: false,
    jobId: '',
    jobObject: null
  }

  componentDidUpdate = () => {
    const { openShowPane, showPaneJobId } = this.props
    const { isOpen, jobId } = this.state

    if (isOpen === false && openShowPane === true && jobId !== showPaneJobId) {
      this.setState({
        isOpen: true,
        jobId: showPaneJobId
      }, () => this.loadJob())
    } else if (isOpen === true && openShowPane === false && showPaneJobId === '' && jobId !== '') {
      this.setState({
        isOpen: false,
        jobId: ''
      })
    }
  }

  loadJob = () => {
    const { jobs } = this.props
    const { jobId } = this.state

    const currentJob = jobs.find(job => job.id === jobId)

    // Use the version from the cache
    this.setState({
      jobObject: currentJob
    })
  }

  closePane = () => {
    const { dispatch } = this.props

    dispatch(jobsActions.hideJobShowPane())
  }

  render = () => {
    const { isOpen, jobObject } = this.state

    return (
      <SlidingPane
        closeIcon={<Icofont icon="close-line" size="2" />}
        isOpen={isOpen}
        onRequestClose={() => this.closePane()}
        title="Job details"
        width="50%"
      >
        {jobObject === null && (
          <PageLoader message="Loading job ..." />
        )}

        {jobObject && jobObject.error && (
          <React.Fragment>
            <JobHeader job={jobObject} />
            <hr />
            <div className="d-flex flex-column">
              <h3>Message</h3>
              <pre>
                <code>{jobObject.error.message}</code>
              </pre>
              <h3>Backtrace</h3>
              <div className="d-flex justify-content-center">
                {jobObject.error.backtrace ? (
                  <pre>
                    <code className="no-word-wrap">{jobObject.error.backtrace.join("\n")}</code>
                  </pre>
                ) : (
                  <span className="d-flex">No job stacktrace.</span>
                )}
              </div>
            </div>
          </React.Fragment>
        )}
      </SlidingPane>
    )
  }
}

const mapStateToProps = ({ jobs: { items, openShowPane, showPaneJobId } }) => ({
  jobs: items,
  openShowPane,
  showPaneJobId
})

export default connect(mapStateToProps)(JobShowPane)

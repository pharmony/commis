import RestfulClient from 'restful-json-api-client'

export default class DashboardApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'dashboard' })
  }
}

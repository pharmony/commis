import dashboardConstants from './constants'
import dashboardService from './services'

const fetchData = () => {
  const request = () => ({ type: dashboardConstants.FETCH_DATA_REQUEST })
  const success = data => ({
    data,
    type: dashboardConstants.FETCH_DATA_SUCCESS
  })
  const failure = error => ({
    error,
    type: dashboardConstants.FETCH_DATA_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    dashboardService.fetchData()
      .then(data => dispatch(success(data)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  fetchData
}

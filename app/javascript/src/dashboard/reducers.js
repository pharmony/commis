import dotProp from 'dot-prop-immutable'

import dashboardConstants from './constants'
import chefRepositoryImportConstants from '../repositories/import/constants'

const initialState = {
  policyCount: 0, // Cached amount of policies used across repos
  hasRepositories: false, // Cached repositories presence in commis
  loadingInitialData: false, // True while requesting data to the server
  loadingInitialDataFailed: false, // True on error received while loading data
  repositoryCount: 0 // Cached amount of repositories managed in Commis
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case chefRepositoryImportConstants.IMPORT_SUCCESS:
      return dotProp.set(state, 'hasRepositories', true)

    case dashboardConstants.FETCH_DATA_REQUEST:
      return dotProp.set(initialState, 'loadingInitialData', true)
    case dashboardConstants.FETCH_DATA_FAILURE:
      newState = dotProp.set(state, 'loadingInitialData', false)
      newState = dotProp.set(newState, 'loadingInitialDataFailed', true)
      return dotProp.set(newState, 'hasRepositories', false)
    case dashboardConstants.FETCH_DATA_SUCCESS: {
      const {
        cluster_count: clusterCount,
        repository_count: repositoryCount
      } = action.data

      newState = dotProp.set(state, 'clusterCount', clusterCount)
      newState = dotProp.set(newState, 'hasRepositories', repositoryCount > 0)
      newState = dotProp.set(newState, 'loadingInitialData', false)
      newState = dotProp.set(newState, 'loadingInitialDataFailed', false)
      return dotProp.set(newState, 'repositoryCount', repositoryCount)
    }
    default:
      return state
  }
}

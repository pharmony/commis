import React from 'react'
import { connect } from 'react-redux'
import { Doughnut } from 'react-chartjs-2'
import PropTypes from 'prop-types'

import nodesActions from '../../nodes/actions'

class NodeCount extends React.Component {
  componentDidMount = () => {
    const { fetched, dispatch } = this.props

    if (fetched === false) {
      dispatch(nodesActions.fetchAll())
    }
  }

  render = () => {
    const { count, stats } = this.props

    return (
      <div className="d-flex justify-content-center align-items-center" id="node-count-card">
        <div className="d-flex flex-column flex-fill justify-content-center align-items-center align-self-stretch position-relative">
          <span className="h0">{count}</span>
          <small className="graph-title">Nodes</small>
          <Doughnut
            data={{
              labels: ['Healthy', 'Degraded', 'Danger'],
              datasets: [{
                data: [stats.healthy, stats.degraded, stats.danger],
                backgroundColor: ['#00bc8c', '#F39C12', '#E74C3C'],
                borderWidth: [0, 0, 0]
              }]
            }}
            legend={{ display: false }}
            options={{
              cutoutPercentage: 85,
              maintainAspectRatio: false
            }}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ nodes: { fetched, itemCount, stats } }) => ({
  fetched, count: itemCount, stats
})

NodeCount.propTypes = {
  count: PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
  fetched: PropTypes.bool.isRequired,
  stats: PropTypes.shape({
    danger: PropTypes.number,
    degraded: PropTypes.number,
    healthy: PropTypes.number
  }).isRequired
}

export default connect(mapStateToProps)(NodeCount)

import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

const ClusterCount = ({ count }) => (
  <div className="card align-self-center">
    <div className="card-body text-center">
      <h5 className="card-title">Clusters</h5>
      <NavLink className="h2" to="/clusters">
        {count}
      </NavLink>
    </div>
  </div>
)

ClusterCount.propTypes = {
  count: PropTypes.number
}

ClusterCount.defaultProps = {
  count: 0
}

export default ClusterCount

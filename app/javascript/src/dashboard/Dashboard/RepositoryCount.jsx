import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

const RepositoryCount = ({ count }) => (
  <div className="card align-self-center">
    <div className="card-body text-center">
      <h5 className="card-title">Repositories</h5>
      <NavLink className="h2" to="/repositories">
        {count}
      </NavLink>
    </div>
  </div>
)

RepositoryCount.propTypes = {
  count: PropTypes.number
}

RepositoryCount.defaultProps = {
  count: 0
}

export default RepositoryCount

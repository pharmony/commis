import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import StatusBadge from '../../components/StatusBadge'
import StringHelpers from '../../helpers/StringHelpers'

const CommisActivityTableRow = ({ nodeActionStack }) => (
  <tr>
    <td><strong>{StringHelpers.capitalize(nodeActionStack.type)}</strong></td>
    <td>{moment(nodeActionStack.created_at).fromNow()}</td>
    <td>
      <StatusBadge state={nodeActionStack.state} />
    </td>
    <td>Admin</td>
    <td>
      <NavLink to={`/node-action-stacks/${nodeActionStack.id}`}>
        Details
      </NavLink>
    </td>
  </tr>
)

CommisActivityTableRow.propTypes = {
  nodeActionStack: PropTypes.shape({
    created_at: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  }).isRequired
}

export default CommisActivityTableRow

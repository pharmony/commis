import React from 'react'
import { Table } from 'reactstrap'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import CommisActivityTableRow from './CommisActivityTableRow'
import nodeActionsStacksActions from '../../node-action-stacks/actions'
import PageLoader from '../../components/PageLoader'

class CommisActivityTable extends React.Component {
  state = {
    lastNActivities: 10
  }

  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(nodeActionsStacksActions.fetchAll())
  }

  render = () => {
    const { stackItems, fetchingNodeActions } = this.props
    const { lastNActivities } = this.state

    if (fetchingNodeActions) {
      return <PageLoader message="Loading action list ..." />
    }

    const sortedStackItems = stackItems.sort(
      (a, b) => a.created_at < b.created_at
    ).slice(0, lastNActivities)

    return (
      <React.Fragment>
        <p>{`Last ${lastNActivities} activities`}</p>
        <Table>
          <thead>
            <tr>
              <th>Action</th>
              <th>When</th>
              <th>Status</th>
              <th>Requester</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {sortedStackItems.map(stackItem => (
              <CommisActivityTableRow
                nodeActionStack={stackItem}
                key={uuidv1()}
              />
            ))}
          </tbody>
        </Table>
        <div className="d-flex justify-content-end">
          <NavLink
            className="btn btn-primary"
            to="/node-actions"
          >
            See all acitvities
          </NavLink>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  nodeActionStacks: { fetchingAll: fetchingNodeActions, items }
}) => ({
  stackItems: items,
  fetchingNodeActions
})

CommisActivityTable.propTypes = {
  stackItems: PropTypes.arrayOf(PropTypes.shape({
    created_at: PropTypes.string.isRequired
  })).isRequired,
  fetchingNodeActions: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(CommisActivityTable)

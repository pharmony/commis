import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import CommisActivityTable from './CommisActivityTable'
import ClusterCount from './ClusterCount'
import NodeCount from './NodeCount'
import RepositoryCount from './RepositoryCount'

const Dashboard = (props) => {
  const {
    clusterCount,
    repositoryCount
  } = props

  return (
    <React.Fragment>
      <div className="d-flex flex-row justify-content-center justify-content-around dashboard mt-3 mb-5">
        <RepositoryCount count={repositoryCount} />
        <NodeCount />
        <ClusterCount count={clusterCount} />
      </div>

      <CommisActivityTable />
    </React.Fragment>
  )
}

const mapStateToProps = ({
  dashboard: {
    clusterCount,
    repositoryCount
  }
}) => ({
  clusterCount,
  repositoryCount
})

Dashboard.propTypes = {
  clusterCount: PropTypes.number.isRequired,
  repositoryCount: PropTypes.number.isRequired
}

export default connect(mapStateToProps)(Dashboard)

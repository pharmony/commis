import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import AlertMessage from '../components/AlertMessage'
import Dashboard from './Dashboard'
import dashboardActions from './actions'
import EmptyDashboard from './EmptyDashboard'
import PageLoader from '../components/PageLoader'

class DashboardPage extends React.Component {
  componentDidMount = () => {
    const { dispatch } = this.props

    dispatch(dashboardActions.fetchData())
  }

  render() {
    const {
      loadingInitialData,
      loadingInitialDataFailed,
      hasRepositories
    } = this.props

    if (hasRepositories) return <Dashboard />

    return (
      <div className="d-flex flex-column justify-content-center h-100">
        {loadingInitialData && (
          <PageLoader message="Loading chef repositories ..." />
        )}

        {loadingInitialDataFailed && (
          <AlertMessage>
            Unfortunately an error occured while loading your dashboard.
            Please try again.
          </AlertMessage>
        )}

        {hasRepositories === false && (
          <EmptyDashboard />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const {
    loadingInitialData,
    loadingInitialDataFailed,
    nodeCount,
    hasRepositories
  } = state.dashboard

  return {
    loadingInitialData,
    loadingInitialDataFailed,
    nodeCount,
    hasRepositories
  }
}

DashboardPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  loadingInitialData: PropTypes.bool.isRequired,
  loadingInitialDataFailed: PropTypes.bool.isRequired,
  hasRepositories: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(DashboardPage)

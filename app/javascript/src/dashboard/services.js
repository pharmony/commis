import ApiUtils from '../helpers/ApiUtils'
import DashboardApi from './api'

const fetchData = () => (
  new DashboardApi()
    .get()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetchData
}

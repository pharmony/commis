import React from 'react'
import { NavLink } from 'react-router-dom'

export default () => (
  <div className="d-flex flex-column align-items-center dashboard">
    <div className="d-flex flex-row">
      {'Your dashboard is empty. Please import a'}
      <NavLink className="mx-1" to="/repositories">
        Chef Repo
      </NavLink>
      {'first.'}
    </div>
  </div>
)

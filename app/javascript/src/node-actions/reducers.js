import dotProp from 'dot-prop-immutable'

import actionCableConstants from '../action-cable/constants'
import nodeActionsConstants from './constants'

const initialState = {
  items: [] // Node action logs from the backend server
}

export default (state = initialState, action) => {
  switch (action.type) {
    /*
    ** When receiving logs from the WebSocket while provisionning a node
    */
    case actionCableConstants.DATA_RECEIVED:
      if (action.data && action.data.node_action_logs) {
        const firstNodeAction = action.data.node_action_logs[0]
        const { node_action_id: nodeActionId } = firstNodeAction

        const logs = action.data.node_action_logs.map(nodeActionLog => ({
          message: nodeActionLog.message,
          timestamp: nodeActionLog.timestamp,
          type: nodeActionLog.type
        }))

        // Searching for an existing NodeAction
        const nodeActionIndex = state.items.findIndex(item => (
          item.id === nodeActionId
        ))

        // NodeAction already exists with some logs
        if (nodeActionIndex > -1) {
          const nodeAction = state.items[nodeActionIndex]

          // Merge existing logs with new logs
          const appendedLogs = [...nodeAction.logs, ...logs]

          // Sort them by timestamp
          const sortedLogs = appendedLogs.sort((a, b) => (
            a.timestamp - b.timestamp
          ))

          // Appends the log line to the NodeAction logs
          return dotProp.set(
            state,
            `items.${nodeActionIndex}.logs`,
            sortedLogs
          )
        }

        // Sort logs by timestamp
        const sortedLogs = logs.sort((a, b) => (
          a.timestamp - b.timestamp
        ))

        // NodeAction is not yet present
        const nodeActionLog = { id: nodeActionId, logs: sortedLogs }

        return dotProp.set(
          state,
          'items',
          [...state.items, nodeActionLog]
        )
      }

      return state

    case nodeActionsConstants.FETCH_LOGS_SUCCESS: {
      const { id, logs } = action

      if (logs.length === 0) return state

      const nodeActionIndex = state.items.findIndex(
        item => item.id === id
      )

      if (nodeActionIndex > -1) {
        return dotProp.set(
          state,
          `items.${nodeActionIndex}.logs`,
          logs
        )
      }

      const nodeActionLog = { id, logs }

      return dotProp.set(
        state,
        'items',
        [...state.items, nodeActionLog]
      )
    }
    default:
      return state
  }
}

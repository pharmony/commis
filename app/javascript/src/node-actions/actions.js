import nodeActionsConstants from './constants'
import nodeActionsService from './services'

const fetchLogs = (nodeActionid) => {
  const request = nodeActionId => ({
    id: nodeActionId,
    type: nodeActionsConstants.FETCH_LOGS_REQUEST
  })
  const success = (id, logs) => ({
    id,
    logs,
    type: nodeActionsConstants.FETCH_LOGS_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodeActionsConstants.FETCH_LOGS_FAILURE
  })

  return (dispatch) => {
    dispatch(request(nodeActionid))

    nodeActionsService.fetchLogs(nodeActionid)
      .then(logs => dispatch(success(nodeActionid, logs)))
      .catch(error => dispatch(failure(error)))
  }
}

const reRun = (id) => {
  const request = nodeActionId => ({
    id: nodeActionId,
    type: nodeActionsConstants.RE_RUN_REQUEST
  })
  const success = () => ({
    type: nodeActionsConstants.RE_RUN_SUCCESS
  })
  const failure = error => ({
    error,
    type: nodeActionsConstants.RE_RUN_FAILURE
  })

  return (dispatch) => {
    dispatch(request(id))

    nodeActionsService.reRun(id)
      .then(() => dispatch(success()))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  fetchLogs,
  reRun
}

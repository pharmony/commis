import RestfulClient from 'restful-json-api-client'
import RailsHeaders from '../helpers/RailsHeaders'

export class NodeActionLogsApi extends RestfulClient {
  constructor(nodeActionId) {
    super(`/api/node_actions/${nodeActionId}`, {
      resource: 'logs',
      headers: RailsHeaders
    })
  }
}

export class NodeActionsApi extends RestfulClient {
  constructor() {
    super('/api', { resource: 'node_actions', headers: RailsHeaders })
  }

  reRun = ({ id }) => this.request('POST', { path: `${id}/rerun` })
}

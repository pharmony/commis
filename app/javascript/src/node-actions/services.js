import ApiUtils from '../helpers/ApiUtils'
import { NodeActionLogsApi, NodeActionsApi } from './apis'

const fetchLogs = id => (
  new NodeActionLogsApi(id)
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

const reRun = id => (
  new NodeActionsApi()
    .reRun({ id })
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetchLogs,
  reRun
}

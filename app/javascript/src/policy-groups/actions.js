import policyGroupsConstants from './constants'
import policyGroupsService from './services'

const fetchAll = () => {
  const request = () => ({ type: policyGroupsConstants.FETCH_ALL_REQUEST })
  const success = policyGroups => ({
    policyGroups,
    type: policyGroupsConstants.FETCH_ALL_SUCCESS
  })
  const failure = error => ({
    error,
    type: policyGroupsConstants.FETCH_ALL_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    policyGroupsService.fetchAll()
      .then(policyGroups => dispatch(success(policyGroups)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  fetchAll
}

import dotProp from 'dot-prop-immutable'

import policyGroupsConstants from './constants'

const initialState = {
  fetchAllFailed: false, // True when back server replied with an error
  fetched: false, /* True when policy groups has been fetched at least once from
                     the backend server */
  fetching: false, // True while fetching policy groups from the backend server
  itemCount: 0, // policy groups count
  items: [] // policy groups from the backend server
}

export default (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case policyGroupsConstants.FETCH_ALL_REQUEST:
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetchingAll', true)
      return dotProp.set(newState, 'fetching', true)
    case policyGroupsConstants.FETCH_ALL_SUCCESS: {
      newState = dotProp.set(state, 'fetchAllFailed', false)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      newState = dotProp.set(newState, 'fetching', false)
      newState = dotProp.set(newState, 'itemCount', action.policyGroups.length)
      return dotProp.set(
        newState,
        'items',
        action.policyGroups.sort((a, b) => a.name.localeCompare(b.name))
      )
    }
    case policyGroupsConstants.FETCH_ALL_FAILURE:
      newState = dotProp.set(state, 'fetchAllFailed', true)
      newState = dotProp.set(newState, 'fetched', true)
      newState = dotProp.set(newState, 'fetchingAll', false)
      return dotProp.set(newState, 'fetching', false)

    default:
      return state
  }
}

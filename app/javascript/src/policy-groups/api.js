import RestfulClient from 'restful-json-api-client'

export default class PolicyGroupsApi extends RestfulClient {
  constructor() {
    super('/api/chef', { resource: 'policy_groups' })
  }
}

import ApiUtils from '../helpers/ApiUtils'
import PolicyGroupsApi from './api'

const fetchAll = () => (
  new PolicyGroupsApi()
    .all()
    .then(ApiUtils.checkStatus)
    .then(response => response.json())
)

export default {
  fetchAll
}

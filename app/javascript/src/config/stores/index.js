import { applyMiddleware, createStore } from 'redux'
import { createFilter } from 'redux-persist-transform-filter'
import { createLogger } from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import thunkMiddleware from 'redux-thunk'

import rootReducer from './reducers'

const loggerMiddleware = createLogger()

const savePreferences = createFilter(
  'app',
  ['preferences']
)

const persistConfig = {
  key: 'root',
  storage,
  transforms: [savePreferences],
  whitelist: [
    'app',
    'authentication',
    'repositories'
  ]
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default () => {
  const store = createStore(
    persistedReducer,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
  const persistor = persistStore(store)
  return { store, persistor }
}

import { combineReducers } from 'redux'

import actionCable from '../../action-cable/reducers'
import app from '../../app/reducers'
import alert from '../../alerts/reducer'
import authentication from '../../devise/sessions/reducers'
import clusters from '../../clusters/reducers'
import {
  confirmation,
  resendConfirmation
} from '../../devise/confirmations/reducers'
import dashboard from '../../dashboard/reducers'
import importChefRepo from '../../repositories/import/reducers'
import jobs from '../../jobs/reducers'
import nodeActions from '../../node-actions/reducers'
import nodeActionStacks from '../../node-action-stacks/reducers'
import nodes from '../../nodes/reducers'
import password from '../../devise/passwords/reducers'
import policies from '../../policies/reducers'
import policyActions from '../../policy-actions/reducers'
import policyGroups from '../../policy-groups/reducers'
import registration from '../../devise/registrations/reducers'
import repositories from '../../repositories/reducers'
import repository from '../../repository/reducers'
import settings from '../../settings/reducers'
import statuses from '../../statuses/reducers'
import unlock from '../../devise/unlocks/reducers'

const rootReducer = combineReducers({
  actionCable,
  app,
  alert,
  authentication,
  clusters,
  confirmation,
  dashboard,
  importChefRepo,
  jobs,
  nodeActions,
  nodeActionStacks,
  nodes,
  password,
  policies,
  policyActions,
  policyGroups,
  registration,
  repositories,
  repository,
  resendConfirmation,
  settings,
  statuses,
  unlock
})

export default rootReducer

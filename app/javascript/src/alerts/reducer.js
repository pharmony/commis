import dotProp from 'dot-prop-immutable'

import alertConstants from './constants'
import Utils from '../helpers/Utils'

const initialState = {
  messages: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case alertConstants.REMOVE: {
      const messageIndex = state.messages.findIndex(
        message => message.id === action.messageId
      )
      if (messageIndex > -1) {
        return dotProp.delete(state, `messages.${messageIndex}`)
      }
      return state
    }
    case alertConstants.SUCCESS:
      return {
        ...state,
        messages: [...state.messages, {
          type: 'success',
          text: action.message,
          id: Utils.genId()
        }]
      }
    case alertConstants.ERROR:
      return {
        ...state,
        messages: [...state.messages, {
          type: 'danger',
          text: action.message,
          id: Utils.genId()
        }]
      }
    case alertConstants.CLEAR:
      return {
        ...state,
        messages: []
      }
    default:
      return state
  }
}

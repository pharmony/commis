import { Alert } from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import alertActions from './actions'

class FlashMessage extends React.Component {
  state = {
    visible: true
  }

  componentDidMount = () => {
    const { timeout } = this.props

    this.timer = setTimeout(
      this.onDismiss,
      timeout
    )
  }

  componentWillUnmount = () => {
    clearTimeout(this.timer)
  }

  onDismiss = () => {
    const { dispatch, message } = this.props

    this.setState({
      visible: false
    }, () => (
      dispatch(alertActions.remove(message.id))
    ))
  }

  render = () => {
    const { className, message } = this.props
    const { visible } = this.state

    return (
      <div className={`alerts-container d-flex flex-fill ${className}`}>
        <Alert
          color={message.type}
          className="d-flex flex-fill"
          isOpen={visible}
          toggle={() => this.onDismiss()}
        >
          {message.text}
        </Alert>
      </div>
    )
  }
}

FlashMessage.propTypes = {
  className: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  message: PropTypes.shape({
    text: PropTypes.string.isRequired
  }).isRequired,
  timeout: PropTypes.number
}

FlashMessage.defaultProps = {
  className: '',
  timeout: 3000
}

export default connect()(FlashMessage)

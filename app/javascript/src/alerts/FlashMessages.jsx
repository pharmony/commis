import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import FlashMessage from './FlashMessage'

const FlashMessages = (props) => {
  const { className, messages } = props

  return (
    messages.map(message => (
      <FlashMessage
        className={className}
        key={message.id}
        message={message}
      />
    ))
  )
}

const mapStateToProps = ({ alert: { messages } }) => ({ messages })

FlashMessages.propTypes = {
  className: PropTypes.string,
  messages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired
  })).isRequired
}

FlashMessages.defaultProps = {
  className: ''
}

export default connect(mapStateToProps)(FlashMessages)

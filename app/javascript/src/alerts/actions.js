import alertConstants from './constants'

const clear = () => ({ type: alertConstants.CLEAR })

const error = message => ({ message, type: alertConstants.ERROR })

const remove = messageId => ({ messageId, type: alertConstants.REMOVE })

const success = message => ({ message, type: alertConstants.SUCCESS })

export default {
  clear,
  error,
  remove,
  success
}

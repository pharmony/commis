import dotProp from 'dot-prop-immutable'

import actionCableConstants from '../action-cable/constants'
import nodeActionStacksConstants from '../node-action-stacks/constants'
import nodesConstants from '../nodes/constants'

/*
** This reducer saves various object statuses like NodeActionStack or NodeAction
*  statuses so that receiving an update of their status will not refresh the
*  entire page but only the badge.
*/
const initialState = {
  items: [] // The setting object from the backend server
}

const updateOrCreateItem = (state, object) => {
  const {
    klass: ObjectKlass,
    id: ObjectId,
    state: ObjectState
  } = object

  const itemIndex = state.items.findIndex(item => (
    item.id === ObjectId && item.klass === ObjectKlass
  ))

  // Update existing object
  if (itemIndex > -1) {
    return dotProp.set(
      state,
      `items.${itemIndex}.state`,
      ObjectState
    )
  }

  // Add the new object
  return dotProp.set(
    state,
    'items',
    [...state.items, object]
  )
}

export default (state = initialState, action) => {
  let newState = state

  switch (action.type) {
    case actionCableConstants.DATA_RECEIVED: {
      const object = {
        klass: null,
        id: null,
        state: null
      }

      if (action.data) {
        /*
        ** ~~~~ NodeActionStack ~~~~
        */
        if (action.data.node_action_stack_state) {
          const {
            node_action_stack_id: nodeActionStackId,
            state: actionState
          } = action.data.node_action_stack_state

          object.klass = 'NodeActionStack'
          object.id = nodeActionStackId
          object.state = actionState
        }

        /*
        ** ~~~~ NodeAction ~~~~
        */
        if (action.data.node_action_state) {
          const {
            node_action_id: nodeActionId,
            state: actionState
          } = action.data.node_action_state

          object.klass = 'NodeAction'
          object.id = nodeActionId
          object.state = actionState
        }

        if (object.id) newState = updateOrCreateItem(state, object)
      }

      return newState
    }
    case nodeActionStacksConstants.FETCH_ALL_SUCCESS:
      action.nodeActions.map((nodeActionStack) => {
        const {
          id: nodeActionStackId,
          state: nodeActionStackState
        } = nodeActionStack

        const object = {
          klass: 'NodeActionStack',
          id: nodeActionStackId,
          state: nodeActionStackState
        }

        newState = updateOrCreateItem(newState, object)

        return true
      })

      return newState

    case nodeActionStacksConstants.FETCH_NODE_ACTIONS_SUCCESS:
      action.nodeActions.map((nodeAction) => {
        const {
          id: nodeActionId,
          state: nodeActionState
        } = nodeAction

        const object = {
          klass: 'NodeAction',
          id: nodeActionId,
          state: nodeActionState
        }

        newState = updateOrCreateItem(newState, object)

        return true
      })

      return newState

    case nodesConstants.FETCH_ONE_FOR_A_NODE_SUCCESS: {
      const {
        id: nodeActionStackId,
        state: nodeActionState
      } = action.nodeActionStack

      const object = {
        klass: 'NodeActionStack',
        id: nodeActionStackId,
        state: nodeActionState
      }

      return updateOrCreateItem(newState, object)
    }
    default:
      return state
  }
}

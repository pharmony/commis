import ActionCable from 'actioncable'

const channelName = () => 'CommisChannel'

// ActionCable.startDebugging()

const createAndSubscribe = (
  dispatch, connected, disconnected, onError, onReceive
) => new Promise((resolve, reject) => {
  try {
    const cable = ActionCable.createConsumer()

    cable.connection.events.error = error => dispatch(onError(error))

    const channel = cable.subscriptions.create(channelName(), {
      connected: () => dispatch(connected(channel)),
      disconnected: () => dispatch(disconnected()),
      received: data => dispatch(onReceive(data))
    })

    resolve(cable)
  } catch (error) {
    reject(error)
  }
})

const destroy = (cable, channel) => new Promise((resolve, reject) => {
  try {
    cable.subscriptions.remove(channel)
    resolve()
  } catch (error) {
    reject(error)
  }
})

const send = (channel, command, data) => new Promise((resolve, reject) => {
  try {
    if (channel.perform(command, data)) {
      resolve()
    } else {
      reject({ command, data })
    }
  } catch (error) {
    reject(error)
  }
})

export default {
  createAndSubscribe,
  destroy,
  send
}

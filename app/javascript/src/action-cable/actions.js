import actionCableConstants from './constants'
import actionCableService from './services'

const send = ({ command, data }) => {
  const request = (sentCommand, sentData) => ({
    command: sentCommand,
    data: sentData,
    type: actionCableConstants.SEND_REQUEST
  })
  const success = () => ({
    type: actionCableConstants.SEND_SUCCESS
  })
  const failure = error => ({
    error,
    type: actionCableConstants.SEND_FAILURE
  })

  return (dispatch, getState) => {
    dispatch(request(command, data))

    const { channel } = getState().actionCable

    actionCableService.send(channel, command, data)
      .then(() => dispatch(success()))
      .catch(error => dispatch(failure(error)))
  }
}

const onConnected = channel => (dispatch) => {
  dispatch({
    channel,
    type: actionCableConstants.CONNECTED
  })

  dispatch(send({
    command: 'job_stats',
    data: { include_jobs: true }
  }))
}

const onDisconnected = () => (dispatch) => {
  dispatch({
    type: actionCableConstants.DISCONNECTED
  })
}

const onError = error => (dispatch) => {
  dispatch({
    error,
    type: actionCableConstants.ERROR
  })
}

const onReceive = data => (dispatch) => {
  dispatch({
    data,
    type: actionCableConstants.DATA_RECEIVED
  })
}

const create = () => {
  const request = () => ({ type: actionCableConstants.CREATE_REQUEST })
  const success = cable => ({
    cable,
    type: actionCableConstants.CREATE_SUCCESS
  })
  const failure = error => ({
    error,
    type: actionCableConstants.CREATE_FAILURE
  })

  return (dispatch) => {
    dispatch(request())

    actionCableService.createAndSubscribe(dispatch, onConnected, onDisconnected, onError, onReceive)
      .then(cable => dispatch(success(cable)))
      .catch(error => dispatch(failure(error)))
  }
}

export default {
  create,
  send
}

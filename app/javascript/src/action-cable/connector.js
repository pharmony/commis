import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import actionCableActions from './actions'

class ActionCableConnector extends React.Component {
  componentDidMount = () => {
    const { cable, dispatch } = this.props

    if (cable) return

    dispatch(actionCableActions.create())
  }

  render = () => null
}

const mapStateToProps = ({ actionCable: { cable } }) => ({ cable })

ActionCableConnector.propTypes = {
  cable: PropTypes.object,
  dispatch: PropTypes.func.isRequired
}

ActionCableConnector.defaultProps = {
  cable: null
}

export default connect(mapStateToProps)(ActionCableConnector)

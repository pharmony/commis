import React from 'react'
import { Alert } from 'reactstrap'
import PropTypes from 'prop-types'

class TemporaryMessage extends React.Component {
  state = {
    display: false
  }

  componentDidMount() {
    const { onDismiss } = this.props

    this.setState({
      display: true
    }, () => setTimeout(() => {
      this.setState({
        display: false
      }, () => {
        onDismiss()
      })
    }, 3000))
  }

  render() {
    const { color, children } = this.props
    const { display } = this.state

    return (
      <Alert
        className="float-left mb-0"
        color={color}
        isOpen={display}
      >
        <span>
          {children}
        </span>
      </Alert>
    )
  }
}

TemporaryMessage.propTypes = {
  color: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]).isRequired,
  onDismiss: PropTypes.func
}

TemporaryMessage.defaultProps = {
  color: 'primary',
  onDismiss: () => {}
}

export default TemporaryMessage

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import nodesActions from '../../nodes/actions'
import PageLoader from '../PageLoader'

export default (WrappedComponent, nodeId) => {
  class WithLoadedNode extends React.Component {
    state = {
      fetchingNode: false, // True while loading the node
      loadedNode: false, // True when node has been successfully loaded
      node: null // The loaded node object
    }

    componentDidMount = () => this.loadNodeAfterAppLoaded()

    componentDidUpdate = () => this.loadNodeAfterAppLoaded()

    /*
    ** Waits until the app is loaded, then call loadNode()
    */
    loadNodeAfterAppLoaded = () => {
      const { loading } = this.props
      const { node } = this.state

      // The app is still loading, we have to wait ...
      if (loading) return

      // Loading finished, Node not yet loaded, so loading it
      if (!node) this.loadNode()
    }

    /*
    ** Loads the given node
    *
    *  Use the node object from the redux store if possible, otherwise load it
    *  from the backend server.
    */
    loadNode = () => {
      const {
        dispatch,
        nodes
      } = this.props
      const { fetchingNode } = this.state

      if (nodes.length) {
        const currentNode = nodes.find(node => [node.name, node.id].includes(nodeId))
        this.setState({
          fetchingNode: false,
          loadedNode: true,
          node: currentNode
        })
      } else {
        if (fetchingNode) return

        dispatch(nodesActions.fetchOne(nodeId))
        this.setState({
          fetchingNode: true,
          loadedNode: false
        })
      }
    }

    render = () => {
      const { loadedNode, node } = this.state

      if (node === null) return <PageLoader message="Loading node ..." />

      return <WrappedComponent loadedNode={loadedNode} node={node} {...this.props} />
    }
  }

  const mapStateToProps = ({
    app: { configuration: { loading } },
    nodes: { items: nodeItems }
  }) => ({
    loading,
    nodes: nodeItems
  })

  WithLoadedNode.propTypes = {
    dispatch: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    nodes: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired
    })).isRequired
  }

  return connect(mapStateToProps)(WithLoadedNode)
}

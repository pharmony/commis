import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

export default (WrappedComponent, klass, id) => {
  class WithTrackedStatus extends React.Component {
    state = {
      klass: null, // The object class for which the state is tracked
      id: null, // The object ID for which the state is tracked
      state: null // The object state for which the state is tracked
    }

    componentDidMount = () => {
      this.setState({
        klass,
        id
      })
    }

    componentDidUpdate = () => {
      const {
        klass: stateKlass,
        id: stateId,
        state: stateState
      } = this.state

      const { statuses } = this.props

      const object = statuses.find(
        item => item.klass === stateKlass && item.id === stateId
      )

      if (object) {
        if (object.state !== stateState) {
          this.updateStateAs(object.state)
        }
      }
    }

    fetchStateFromStore = () => {
      const { statuses } = this.props

      const object = statuses.find(
        item => item.klass === klass && item.id === id
      )

      return object ? object.state : 'unknown'
    }

    updateStateAs = state => this.setState({ state })

    render = () => {
      const componentState = this.fetchStateFromStore()

      return <WrappedComponent state={componentState} {...this.props} />
    }
  }

  const mapStateToProps = ({ statuses: { items: statuses } }) => ({ statuses })

  WithTrackedStatus.propTypes = {
    dispatch: PropTypes.func.isRequired,
    statuses: PropTypes.arrayOf(PropTypes.shape({
      klass: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      state: PropTypes.string.isRequired
    })).isRequired
  }

  return connect(mapStateToProps)(WithTrackedStatus)
}

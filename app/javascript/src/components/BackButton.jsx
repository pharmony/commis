import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

const BackButton = ({ to }) => (
  <div className="d-flex mb-3">
    <NavLink
      activeClassName="btn-active"
      className="d-flex btn btn-secondary"
      to={to}
    >
      Back
    </NavLink>
  </div>
)

BackButton.propTypes = {
  to: PropTypes.string.isRequired
}

export default BackButton

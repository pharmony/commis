/*
** ShowWhenStateIsIncluded is a component that shows its children from props
*  when the given `state` is included within the `states` list.
*
*  This component is useful when used with the WithTrackedStatus HOC like so:
*
*  const ShowWhenTrackedStateIsIncluded = WithTrackedStatus(
*    ShowWhenStateIsIncluded,
*    'NodeAction',
*    nodeAction.id
*  )
*
*  <ShowWhenTrackedStateIsIncluded
*    state="running"
*  >
*    This text is only visible when the NodeAction state is "running".
*  </ShowWhenTrackedStateIsIncluded>
*/
import PropTypes from 'prop-types'

const ShowWhenStateIsIncluded = ({
  children,
  state,
  states
}) => {
  if ([].concat(states).includes(state)) return children

  return null
}

ShowWhenStateIsIncluded.propTypes = {
  state: PropTypes.string.isRequired,
  states: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]).isRequired
}

export default ShowWhenStateIsIncluded

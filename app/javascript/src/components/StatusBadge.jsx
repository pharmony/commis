import React from 'react'
import { Badge } from 'reactstrap'
import PropTypes from 'prop-types'

import BadgeHelpers from '../helpers/BadgeHelpers'
import StringHelpers from '../helpers/StringHelpers'

const StatusBadge = ({ className, state }) => (
  <Badge
    className={className}
    color={BadgeHelpers.colorFrom(state)}
  >
    {StringHelpers.titleize(state)}
  </Badge>
)

StatusBadge.propTypes = {
  className: PropTypes.string,
  state: PropTypes.string.isRequired
}

StatusBadge.defaultProps = {
  className: ''
}

export default StatusBadge

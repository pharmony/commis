import React from 'react'
import { ListGroup } from 'reactstrap'
import { SortableContainer } from 'react-sortable-hoc'
import PropTypes from 'prop-types'
import uuidv1 from 'uuid'

import SortableListGroupItem from './SortableListGroupItem'

const SortableListGroup = SortableContainer(({ items }) => (
  <ListGroup>
    {items.map((item, index) => (
      <SortableListGroupItem
        buttons={item.buttons}
        className={
          `rounded-0 ${index === (items.length - 1) ? 'no-bottom-border' : ''}`
        }
        content={item.content}
        index={index}
        key={uuidv1()}
      />
    ))}
  </ListGroup>
))

SortableListGroup.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    buttons: PropTypes.element,
    content: PropTypes.element
  })).isRequired
}

export default SortableListGroup

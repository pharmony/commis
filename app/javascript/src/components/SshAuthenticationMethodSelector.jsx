import React from 'react'
import { connect } from 'react-redux'
import {
  FormGroup,
  Input,
  Label
} from 'reactstrap'
import uuidv1 from 'uuid'
import PropTypes from 'prop-types'

import SshHelpers from '../helpers/SshHelpers'

class SshAuthenticationMethodSelector extends React.Component {
  state = {
    selectedAuthMethod: 'none',
    showKeyPath: false,
    showPassword: false,
    showUsername: false,
    sshKeyPath: '',
    sshPassword: '',
    sshUsername: ''
  }

  componentDidMount() {
    const {
      selectedAuthMethod,
      sshCredential,
      sshUsername
    } = this.props

    const newState = {
      selectedAuthMethod,
      sshUsername
    }

    if (selectedAuthMethod === 'password') {
      newState.sshPassword = sshCredential
    }

    if (selectedAuthMethod === 'ssh_keys') {
      newState.sshKeyPath = sshCredential
    }

    this.setState(newState, () => {
      this.showHideFieldsFrom(selectedAuthMethod)
      this.initialiseHookKeys()
    })
  }

  handleInputChange = ({ target: { name, value } }) => {
    const { handleInputChange } = this.props

    if (name === 'selectedAuthMethod') this.showHideFieldsFrom(value)

    this.setState({
      [name]: value
    }, () => handleInputChange(name, value))
  }

  hideSshFields = () => {
    this.setState({
      showUsername: false,
      showPassword: false,
      showKeyPath: false
    })
  }

  /*
  ** This function triggers the `handleInputChange` function for each keys
  *  so that the `PolicyHooks` component has the initial values immediately.
  */
  initialiseHookKeys = () => {
    const { handleInputChange } = this.props

    const keys = [
      'selectedAuthMethod',
      'sshKeyPath',
      'sshPassword',
      'sshUsername'
    ]

    keys.map(key => handleInputChange(key, this.state[key]))
  }

  showHideFieldsFrom = (value) => {
    switch (value) {
      case '':
      case 'none':
        this.hideSshFields()
        break
      case 'password':
        this.showUsernameAndPasswordFields()
        break
      case 'ssh_keys':
        this.showUsernameAndKeypathFields()
        break
      default:
        console.warn(`${value} is an unknown option.`)
    }
  }

  showUsernameAndPasswordFields = () => {
    this.setState({
      showUsername: true,
      showPassword: true,
      showKeyPath: false
    })
  }

  showUsernameAndKeypathFields = () => {
    this.setState({
      showUsername: true,
      showPassword: false,
      showKeyPath: true
    })
  }

  render() {
    const { availableAuthMethods } = this.props
    const {
      selectedAuthMethod,
      showKeyPath,
      showPassword,
      showUsername,
      sshKeyPath,
      sshPassword,
      sshUsername
    } = this.state

    return (
      <React.Fragment>
        <FormGroup>
          <Label for="selected-auth-method">SSH authentication method:</Label>
          <Input
            id="selected-auth-method"
            name="selectedAuthMethod"
            onChange={event => this.handleInputChange(event)}
            type="select"
            value={selectedAuthMethod}
          >
            {availableAuthMethods.map(authMethod => (
              <option
                key={uuidv1()}
                value={authMethod}
              >
                {SshHelpers.sshAuthMethodNameFrom(authMethod)}
              </option>
            ))}
          </Input>
        </FormGroup>
        {showUsername && (
          <FormGroup>
            <Label for="ssh-username">SSH Usename:</Label>
            <Input
              id="ssh-username"
              name="sshUsername"
              onChange={event => this.handleInputChange(event)}
              placeholder="Enter the SSH username to be use to connect to the node."
              value={sshUsername}
            />
          </FormGroup>
        )}
        {showPassword && (
          <FormGroup>
            <Label for="ssh-password">SSH Password:</Label>
            <Input
              id="ssh-password"
              name="sshPassword"
              onChange={event => this.handleInputChange(event)}
              placeholder="If required, enter the SSH password to be use to connect to the node."
              value={sshPassword}
            />
          </FormGroup>
        )}
        {showKeyPath && (
          <FormGroup>
            <Label for="ssh-key-path">SSH Key path:</Label>
            <Input
              id="ssh-key-path"
              name="sshKeyPath"
              onChange={event => this.handleInputChange(event)}
              placeholder="Enter the SSH key path to be use to connect to the node."
              value={sshKeyPath}
            />
          </FormGroup>
        )}
      </React.Fragment>
    )
  }
}

const mapStateToProps = ({
  app: { configuration: { ssh: { availableAuthMethods } } }
}) => ({
  availableAuthMethods
})

SshAuthenticationMethodSelector.propTypes = {
  availableAuthMethods: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleInputChange: PropTypes.func.isRequired,
  selectedAuthMethod: PropTypes.string,
  sshCredential: PropTypes.string,
  sshUsername: PropTypes.string
}

SshAuthenticationMethodSelector.defaultProps = {
  selectedAuthMethod: '',
  sshCredential: '',
  sshUsername: ''
}

export default connect(mapStateToProps)(SshAuthenticationMethodSelector)

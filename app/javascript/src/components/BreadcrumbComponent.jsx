import React from 'react'
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import uuidv1 from 'uuid/v1'

import StringHelpers from '../helpers/StringHelpers'

const BreadcrumbComponent = (props) => {
  const { location: { pathname } } = props

  const pathNames = pathname.split('/')
  let builtPath = ''

  if (['/', '/dashboard'].includes(pathname)) return null

  return (
    <Breadcrumb className="mt-2">
      {pathNames.map((breadcrumbItem, index) => {
        if (index === (pathNames.length - 1)) {
          return (
            <BreadcrumbItem key={uuidv1()} active>
              {StringHelpers.titleize(breadcrumbItem)}
            </BreadcrumbItem>
          )
        }

        builtPath += builtPath === '/' ? breadcrumbItem : `/${breadcrumbItem}`
        return (
          <BreadcrumbItem key={uuidv1()}>
            <NavLink to={builtPath}>
              {StringHelpers.titleize(breadcrumbItem)}
            </NavLink>
          </BreadcrumbItem>
        )
      })}
    </Breadcrumb>
  )
}

BreadcrumbComponent.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
}

export default BreadcrumbComponent

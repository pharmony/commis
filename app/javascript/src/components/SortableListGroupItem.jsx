import React from 'react'
import { ListGroupItem } from 'reactstrap'
import Icofont from 'react-icofont'
import { SortableElement } from 'react-sortable-hoc'
import PropTypes from 'prop-types'

const SortableListGroupItem = SortableElement(({
  buttons, className, content
}) => (
  <ListGroupItem
    className={`d-flex flex-row justify-content-between ${className}`}
  >
    <div className="d-flex">
      <div className="d-flex align-items-center mr-3">
        <Icofont icon="navigation-menu" size="2" />
      </div>
      <div className="d-flex align-items-center">
        {content}
      </div>
    </div>
    {buttons}
  </ListGroupItem>
))

SortableListGroupItem.propTypes = {
  buttons: PropTypes.element,
  className: PropTypes.string,
  content: PropTypes.element.isRequired
}

SortableListGroupItem.defaultProps = {
  button: null,
  className: null
}

export default SortableListGroupItem

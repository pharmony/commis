import React from 'react'
import { Alert } from 'reactstrap'
import PropTypes from 'prop-types'

const AlertMessage = ({ children, color }) => (
  <div className="d-flex justify-content-center">
    <Alert color={color}>
      <span>
        {children}
      </span>
    </Alert>
  </div>
)

AlertMessage.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]).isRequired,
  color: PropTypes.string
}

AlertMessage.defaultProps = {
  color: 'danger'
}

export default AlertMessage

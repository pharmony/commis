import PropTypes from 'prop-types'
import React from 'react'
import { Spinner } from 'reactstrap'

const PageLoader = (props) => {
  const { className, message } = props

  return (
    <div className={`d-flex justify-content-center ${className}`}>
      <Spinner type="grow" color="primary" />
      <span className="d-flex flex-column justify-content-center ml-3">
        {message}
      </span>
    </div>
  )
}

PageLoader.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string.isRequired
}

PageLoader.defaultProps = {
  className: ''
}

export default PageLoader

import React from 'react'
import { Alert } from 'reactstrap'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import PageLoader from './PageLoader'
import sshActions from '../ssh/actions'

class AppSshPublicSshKey extends React.Component {
  componentDidMount = () => {
    const { dispatch } = this.props
    dispatch(sshActions.fetchSshKey())
  }

  render = () => {
    const {
      fetchtingKey,
      keyFetchingFailed,
      sshPublicKey
    } = this.props

    if (fetchtingKey) return <PageLoader message="Generating the SSH key ..." />

    if (keyFetchingFailed) {
      return (
        <Alert color="danger">
          <span>
            Unfortunately an error occured while fetching the SSH key.
            Please try again.
          </span>
        </Alert>
      )
    }

    return (
      <pre>
        <code className="multiline">
          {sshPublicKey}
        </code>
      </pre>
    )
  }
}

const mapStateToProps = ({
  app: { ssh: { fetchtingKey, keyFetchingFailed, sshPublicKey } }
}) => ({
  fetchtingKey,
  keyFetchingFailed,
  sshPublicKey
})

AppSshPublicSshKey.propTypes = {
  dispatch: PropTypes.func.isRequired,
  fetchtingKey: PropTypes.bool.isRequired,
  keyFetchingFailed: PropTypes.bool.isRequired,
  sshPublicKey: PropTypes.string
}

AppSshPublicSshKey.defaultProps = {
  sshPublicKey: null
}

export default connect(mapStateToProps)(AppSshPublicSshKey)

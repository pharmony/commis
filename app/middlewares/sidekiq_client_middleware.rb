#
# Sidekiq Middleware to broadcast enqueued jobs to the UI via ActionCable.
# @see https://github.com/mperham/sidekiq/blob/master/lib/sidekiq/middleware/chain.rb
#
class SidekiqClientMiddleware
  def call(worker_class, msg, queue, redis_pool)
    result = yield

    job_args = result['args'][0]

    job_klass = job_args['job_class'].constantize

    return unless defined?(job_klass::PRINTABLE_NAME)

    CommisChannel::BackgroundTasks.broadcast_add(
      args: job_args['arguments'].dup.map do |arg|
        arg.delete('_aj_symbol_keys')
        arg
      end,
      id: result['jid'],
      name: job_args['job_class'].constantize::PRINTABLE_NAME,
      requested: Time.at(result['created_at']),
      status: :enqueued
    )

    result
  end
end

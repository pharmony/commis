# frozen_string_literal: true

# require 'chef'
# Chef::Log.level = :trace

#
# Sidekiq Middleware to broadcast job execution to the UI via ActionCable.
# @see https://github.com/mperham/sidekiq/blob/master/lib/sidekiq/middleware/chain.rb
#
class SidekiqServerMiddleware
  def call(_worker_instance, message, _queue)
    if job_has_a_printable_name?(message)
      CommisChannel::BackgroundTasks.broadcast_update(jid: message['jid'],
                                                      status: :running)
    end

    yield

    if job_has_a_printable_name?(message)
      CommisChannel::BackgroundTasks.broadcast_update(jid: message['jid'],
                                                      status: :finished)
    end
  rescue Exception => error
    broadcast_caught_error(message, error) if job_has_a_printable_name?(message)

    raise
  end

  private

  def broadcast_caught_error(message, error)
    params = { jid: message['jid'], status: :failed, data: {
      error: {
        backtrace: error.backtrace,
        message: "#{error.message} (#{error.class.name})"
      }
    } }
    CommisChannel::BackgroundTasks.broadcast_update(params)
  end

  def job_has_a_printable_name?(message)
    job_klass = message['wrapped'].constantize

    job_klass.const_defined?(:PRINTABLE_NAME)
  end
end

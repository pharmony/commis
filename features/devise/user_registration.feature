Feature: User registration
  In order to access the authenticated section
  As a visitor
  I want to register an account

  Scenario: Register an account
    When I try to register a new account
    Then I should have a 404 error

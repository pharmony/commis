Feature: User login
  In order to access Commis
  As an administrator
  I want to login

  Scenario: Login with the default admin account
    When I try to login with the default admin account
    Then I should be on Commis' dashboard page

# frozen_string_literal: true

Then(/^I should be on Commis' dashboard page$/) do
  find(:css, '#app .content-inner .dashboard')
end

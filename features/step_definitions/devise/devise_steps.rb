# frozen_string_literal: true

When(/^I try to register a new account$/) do
  visit '/users/sign_up'
end

When(/^I try to login with the default admin account$/) do
  visit '/'

  admin_user = User.first
  expect(admin_user).to be_present

  fill_in 'Email', with: admin_user.email
  fill_in 'Password', with: 'commis'

  click_button 'Login'
end

Then(/^I should have a 404 error$/) do
  expect(page).to have_content('404')
end

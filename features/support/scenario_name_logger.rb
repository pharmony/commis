# frozen_string_literal: true

#
# Write the name of the running scenario in the Rails logs to ease the debugging
#

Around do |scenario, block|
  Rails.logger.debug "-------> Before Scenario: #{scenario.name}"
  block.call
  Rails.logger.debug "<------- After Scenario: #{scenario.name}"
end

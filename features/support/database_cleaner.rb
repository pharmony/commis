# frozen_string_literal: true

#
# Clear the database before running the test suite, and after each scenario.
# See http://nobrainer.io/docs/db_management/
#

# Configures the tables and synchronizes the indexes
NoBrainer.sync_schema

# Truncates all the tables in the database
NoBrainer.purge!

# Run the db/seeds.rb
Rails.application.load_seed

After do
  NoBrainer.purge!
end

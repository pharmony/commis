# frozen_string_literal: true

#
# Displays the browser logs after a scenario failed using the
# capybara-chromedriver-logger gem.
#

After do |scenario, _|
  if scenario.failed?
    Capybara::Chromedriver::Logger::TestHooks.after_example!
  end
end

# frozen_string_literal: true

module Commis
  module AttributesBuilders
    #
    # Build a Hash of attributes for a ChefNode document
    #
    # Commis::AttributesBuilders::Node
    class Node
      def self.build_from(json)
        {
          name: json['name'],
          chef_version: extract_chef_version_from(json),
          kernel: extract_kernel_from(json),
          platform: extract_platform_from(json),
          platform_version: extract_platform_version_from(json),
          ipaddress: extract_ipaddress_from(json),
          memory_total: extract_memory_total_from(json),
          filesystem_total: extract_filesystem_total_from(json),
          filesystem_type: extract_filesystem_type_from(json),
          hostname: extract_hostname_from(json),
          cpu_count: extract_cpu_count_from(json),
          cpu_cores: extract_cpu_cores_from(json),
          policy_name: json['policy_name'],
          policy_group: json['policy_group']
        }
      end

      def self.extract_chef_version_from(json)
        return nil unless json['automatic']
        return nil unless json['automatic']['chef_packages']
        return nil unless json['automatic']['chef_packages']['chef']

        json['automatic']['chef_packages']['chef']['version']
      end

      def self.extract_kernel_from(json)
        return nil unless json['automatic']
        return nil unless json['automatic']['kernel']

        [
          json['automatic']['kernel']['name'],
          json['automatic']['kernel']['release']
        ].join(' ')
      end

      def self.extract_platform_from(json)
        return nil unless json['automatic']

        json['automatic']['platform']
      end

      def self.extract_platform_version_from(json)
        return nil unless json['automatic']

        json['automatic']['platform_version']
      end

      def self.extract_ipaddress_from(json)
        return nil unless json['automatic']

        json['automatic']['ipaddress']
      end

      def self.extract_memory_total_from(json)
        return nil unless json['automatic']
        return nil unless json['automatic']['memory']

        memory_total = json['automatic']['memory']['total']
        if memory_total.ends_with?('kB')
          memory_total = memory_total.gsub(/kB$/, '').to_i
          memory_total *= 1024
        end

        memory_total
      end

      def self.filesystem_by_mountpoint_keys?(json)
        return false unless json['automatic']
        return false unless json['automatic']['filesystem']
        return false unless json['automatic']['filesystem']['by_mountpoint']

        true
      end

      def self.extract_filesystem_total_from(json)
        return nil unless filesystem_by_mountpoint_keys?(json)
        return nil unless json['automatic']['filesystem']['by_mountpoint']['/']

        total = json['automatic']['filesystem']['by_mountpoint']['/']['kb_size']
        total.to_i * 1024
      end

      def self.extract_filesystem_type_from(json)
        return nil unless filesystem_by_mountpoint_keys?(json)
        return nil unless json['automatic']['filesystem']['by_mountpoint']['/']

        json['automatic']['filesystem']['by_mountpoint']['/']['fs_type']
      end

      def self.extract_hostname_from(json)
        return nil unless json['automatic']

        json['automatic']['hostname']
      end

      def self.extract_cpu_cores_from(json)
        return nil unless json['automatic']
        return nil unless json['automatic']['cpu']

        json['automatic']['cpu']['total']
      end

      def self.extract_cpu_count_from(json)
        return nil unless json['automatic']
        return nil unless json['automatic']['cpu']

        json['automatic']['cpu']['real']
      end

      def self.extract_cpu_name_from(json)
        return nil unless json['automatic']
        return nil unless json['automatic']['cpu']

        json['automatic']['cpu']['total']
      end
    end
  end
end

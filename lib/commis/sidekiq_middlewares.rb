# frozen_string_literal: true

# require 'app/middlewares/sidekiq_client_middleware'
# require 'app/middlewares/sidekiq_server_middleware'

module Commis
  #
  # Calls the Sidekiq Middleware `add` method in order to add the Commis
  # middleware which are catching added and removed tasks from Sidekiq
  #
  class SidekiqMiddlewares
    def self.install!
      Sidekiq.configure_server do |config|
        config.server_middleware { |chain| chain.add SidekiqServerMiddleware }
      end

      Sidekiq.configure_client do |config|
        config.client_middleware { |chain| chain.add SidekiqClientMiddleware }
      end
    end
  end
end

module Commis
  class StdErr
    include Commis::PersistAndBroadcastLog

    attr_accessor :node_action

    def initialize(node_action)
      self.node_action = node_action
    end

    def puts(message)
      persist_and_broadcast(message, :puts)
    end
  end
end

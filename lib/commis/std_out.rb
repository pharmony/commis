module Commis
  class StdOut
    include Commis::PersistAndBroadcastLog

    attr_accessor :node_action

    def initialize(node_action)
      self.node_action = node_action
    end

    def print(message)
      persist_and_broadcast(message, :print)
    end

    def puts(message); end
  end
end

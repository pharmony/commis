# frozen_string_literal: true

require_relative 'persist_and_broadcast_log'

module Commis
  # Commis::LogDevice
  class LogDevice
    include Commis::PersistAndBroadcastLog

    attr_accessor :sync
    attr_accessor :node_action

    def initialize(node_action)
      self.node_action = node_action
    end

    def write(message)
      persist_and_broadcast(message, :msg)
    end

    def close; end
  end
end

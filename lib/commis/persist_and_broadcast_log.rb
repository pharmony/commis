# frozen_string_literal: true

module Commis
  #
  # This class take care of persisting and broadcasting a log message.
  #
  module PersistAndBroadcastLog
    def persist_and_broadcast(message, type)
      return unless node_action

      node_action.increment_log_msg_id!

      PersistAndBroadcastLogJob.perform_later(
        node_action.node_action_stack_id,
        node_action.id,
        build_final_message(message),
        type.to_s,
        Time.zone.now.to_i,
        node_action.log_msg_id
      )
    end

    private

    def build_final_message(message)
      # Unfreeze message if required
      (message.frozen? ? message.dup : message).force_encoding('utf-8')
    end
  end
end

# frozen_string_literal: true

# Source: https://thoughtbot.com/blog/mygem-configure-block
module Commis
  class Configuration
    # Defines if the user registrations is open.
    #
    # When true the "Register" button, on the login page, will appear and anyone
    # accessing the app can create an account and login in the app.
    # Default: false
    attr_accessor :registration_opened

    # Lists the available SSH authentication methods (Read only)
    attr_reader :available_auth_methods

    def initialize
      @registration_opened = false
      @available_auth_methods = AppConstants::Ssh::AUTH_METHODS
    end
  end
end

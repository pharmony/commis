# frozen_string_literal: true

require 'commis/configuration'
require 'commis/sidekiq_middlewares'

module Commis
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end
end

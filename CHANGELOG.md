# Commis Changelog

## Unreleased



## 1.3.0 - (2020-05-20)

 - Upgrades all the JavaScript libraries
 - Upgrades Rails, Bootsnap, Devise, SASS-Rails, Interactor, Sidekiq and Nobrainer
 - Fixes left menu background color
 - Fixes bootstraping an existing node which wasn't taking the given SSH port from the form in order to bootstrap the node [#61](https://gitlab.com/pharmony/commis/issues/61) zedtux
 - Adds a switch to use `sudo` or not from the bootstrap wizard [#31](https://gitlab.com/pharmony/commis/issues/31) zedtux
 - Adds a mechanism to delay Chef logs solving the performance issues while bootstraping a node [#63](https://gitlab.com/pharmony/commis/issues/63) zedtux
 - Updates the chef-dk and knife-zero gems
 - Allows specifying the Chef version to be installed on bootstrapping a node
 - Fixes creating/deleting policy groups when pulling a chef repository
 - Fixes bootstrap wizard not showing the latest data
 - Prevents sending useless background task status updates to the UI
 - Stores NodeActionStack and NodeAction statuses separately [#70](https://gitlab.com/pharmony/commis/issues/70) zedtux
 - Automatically select the repository when having only one from the bootstrap wizard
 - Creates missing DB indexes [#62](https://gitlab.com/pharmony/commis/issues/62) zedtux
 - Shows the node's policy group from the nodes page
 - Fixes Node - Repository/Policy/Group link [#16](https://gitlab.com/pharmony/commis/issues/16) zedtux
 - Fixes custum bootstrap having converge steps [#71](https://gitlab.com/pharmony/commis/issues/71) zedtux
 - Adds the dynamic query attribute `#node-group#` into custom bootstrap converge step [#69](https://gitlab.com/pharmony/commis/issues/69) zedtux

## 1.2.2 - (2019-10-25)

 - Fixes updating converge action as failed when an error occurred [#58](https://gitlab.com/pharmony/commis/issues/58) zedtux
 - Allows using a non-default SSH port [#59](https://gitlab.com/pharmony/commis/issues/59) zedtux

## 1.2.1 - (2019-10-17)

 - Catches errors while converging a node so that the node action can be marked as failed, and the error can be logged in.
 - Chef repository pull now updates the link of a policy with a policy group [#57](https://gitlab.com/pharmony/commis/issues/57) zedtux

## 1.2.0 - (2019-10-07)

 - Moves node action log saving to the DB in Sidekiq jobs [#43](https://gitlab.com/pharmony/commis/issues/43) zedtux
 - Implements Customisable Policy Steps edit [#42](https://gitlab.com/pharmony/commis/issues/42) zedtux
 - Implements Policy hooks [#51](https://gitlab.com/pharmony/commis/issues/51) zedtux

## 1.1.0 - (2019-09-27)

 - Fixes displaying the "Clear chef cache" description on small screens
 - Updates nodes and policies when pulling an existing repository [#19](https://gitlab.com/pharmony/commis/issues/19) zedtux
 - Improves nodes page when node data are missing (Missing data for CPU, memory and so on)
 - Implements the before bootstrap hook [#38](https://gitlab.com/pharmony/commis/issues/38) zedtux
 - Implements the node action customiser [#40](https://gitlab.com/pharmony/commis/issues/40) zedtux

## 1.0.2 - (2019-09-09)

 - Fixes knife config file path between node actions [#37](https://gitlab.com/pharmony/commis/issues/37) zedtux

## 1.0.1 - (2019-07-26)

 - Fixes bootstraping an empty node, where UTF-8 characters was present in the node action log, and breaking ActiveSupport [#34](https://gitlab.com/pharmony/commis/issues/34) zedtux
 - Fixes issue when Chef loads the config file. Now Commis passes the config file path via the `--config` flag [#30](https://gitlab.com/pharmony/commis/issues/30) zedtux
 - Fixes the "Pull" button from the Chef Repositories page which wasn't really reseting the git repository [#27](https://gitlab.com/pharmony/commis/issues/27) zedtux
 - Fixes the "Retry" button from the converge node action page [#26](https://gitlab.com/pharmony/commis/issues/26) zedtux

## 1.0.0 - (2019-07-23)

 - Initial version
